

import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;

public class FieldsAliasing {
	
	private KeyValueHolder holder1;
	private KeyValueHolder holder2;
	
	public FieldsAliasing(){
		holder1 = new KeyValueHolder();
		holder2 = new KeyValueHolder();
	}
	
	public KeyValueHolder getHolder(int k){
		if (k == 1){
			return holder1;
		} else {
			return holder2;
		}
	}
	
	public void doSomething(){
		holder1 = getHolder(10);
	}
	
	public void test(Session session) {
		final KeyValueHolder holder1 = this.holder1;
		final KeyValueHolder holder2 = this.holder2;
		final String firstValue = new StringBuilder().append("bob").toString();
		holder1.value = firstValue;
		holder2.value = "alice";
		session.execute(new SimpleStatement("SELECT * FROM table1 WHERE name = ?", holder1.value));
		session.execute(new SimpleStatement("SELECT * FROM table1 WHERE name = ?", holder2.value));
	}
	
	private static class KeyValueHolder {
		
		public String value;
	}
}