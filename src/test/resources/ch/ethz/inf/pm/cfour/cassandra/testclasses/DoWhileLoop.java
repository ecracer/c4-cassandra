

import com.datastax.driver.core.Session;

public class DoWhileLoop {
	
	public void test(Session session) {
		int i=0;
		do {
			session.execute("SELECT * FROM test_table WHERE i = " + i);
			i++;
		} while (i<20);
		
		int j=0;
		do {
			session.execute("SELECT * FROM test_table WHERE i = " + i);
			j++;
		} while (j<20);
		
		session.execute("SELECT * FROM test_table WHERE i = " + i);
	}
}