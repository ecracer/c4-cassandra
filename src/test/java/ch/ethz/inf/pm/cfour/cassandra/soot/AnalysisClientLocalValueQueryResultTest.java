package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.analysis.TransactionGraphTransformer;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisClientLocalValueQueryResultTest extends AbstractSootTest {

	public AnalysisClientLocalValueQueryResultTest() throws IOException {
		super("ClientLocalValueQueryResult", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(
				new Options()).transform(executeGraphs);

		assertEquals(3, executeGraphs.size());

		final SchemaInformation schema = SchemaInformation.builder()
				.addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "tracks")
				.addPrimaryKeyColumn("users2", "username").addNonPrimaryKeyColumn("users2", "tracks")
				.addPrimaryKeyColumn("users3", "username").addNonPrimaryKeyColumn("users3", "tracks")
				.build();

		final Options options = new Options();
		options.setClientLocalAreGlobalUnique(true);

		int violations = new CFourSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema).getVerifiedViolationsSize2();
		assertEquals(1, violations);
	}
}
