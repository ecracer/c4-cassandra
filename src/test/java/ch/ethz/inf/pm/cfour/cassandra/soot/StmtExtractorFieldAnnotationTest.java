package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldAnnotationTest extends AbstractSootTest {

	public StmtExtractorFieldAnnotationTest() throws IOException {
		super("FieldAnnotation", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			assertEquals(1, cqls.size());
			assertTrue(cqls.contains("SELECT * FROM test_table"));
		}
	}
}
