package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import soot.ArrayType;
import soot.PrimType;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.Type;

/**
 * Helper for classifying known types
 */
public class SootTypeUtils {

	public static boolean isImmutable(final soot.Type type) {
		return type instanceof PrimType || isString(type) || isConsistencyLevel(type) || isClass(type) || isNull(type) || isInteger(type)
				|| isUUID(type);
	}

	public static boolean isPrimitive(final soot.Type type) {
		return type instanceof PrimType;
	}

	public static boolean isClass(final soot.Type type) {
		return isOfRefType(type, "java.lang.Class");
	}

	public static boolean isObject(final soot.Type type) {
		return isOfRefType(type, "java.lang.Object");
	}

	public static SootClass getObjectClass() {
		return Scene.v().getSootClass("java.lang.Object");
	}

	public static boolean isObjectArray(final soot.Type type) {
		return isOfArrayType(type, "java.lang.Object");
	}

	public static boolean isList(final soot.Type type) {
		return isOfRefType(type, "java.util.List");
	}

	public static boolean isString(final soot.Type type) {
		return isOfRefType(type, "java.lang.String");
	}

	public static boolean isStringArray(final soot.Type type) {
		return isOfArrayType(type, "java.lang.String");
	}

	public static boolean isStringBuilder(final soot.Type type) {
		return isOfRefType(type, "java.lang.StringBuilder");
	}

	public static boolean isCharSequence(Type type) {
		return isOfRefType(type, "java.lang.CharSequence");
	}

	public static boolean isInteger(final soot.Type type) {
		return isOfRefType(type, "java.lang.Integer");
	}

	public static boolean isDate(final soot.Type type) {
		return isOfRefType(type, "java.util.Date");
	}

	public static boolean isLocalDate(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.LocalDate");
	}

	public static boolean isInt(final soot.Type type) {
		Objects.requireNonNull(type);
		return type instanceof soot.IntType;
	}

	public static boolean isBool(final soot.Type type) {
		Objects.requireNonNull(type);
		return type instanceof soot.BooleanType;
	}

	public static boolean isNull(final soot.Type type) {
		Objects.requireNonNull(type);
		return type instanceof soot.NullType;
	}

	public static boolean isUUID(final soot.Type type) {
		return isOfRefType(type, "java.util.UUID");
	}

	public static boolean isUUIDs(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.utils.UUIDs");
	}

	public static boolean isClientLocalValuesAnnotation(final soot.Type type) {
		return isOfRefType(type, ClientLocalValues.class.getName());
	}

	public static boolean isSetsUtility(Type type) {
		return isOfRefType(type, "com.google.common.collect.Sets");
	}

	public static boolean isConsistencyLevel(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.ConsistencyLevel");
	}

	public static boolean isSession(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.Session") || isOfRefType(type, "com.datastax.driver.core.AbstractSession");
	}

	public static boolean isResultSet(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.ResultSet");
	}

	public static boolean isRow(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.Row");
	}

	public static boolean implementsSession(final soot.Type type) {
		if (isSession(type)) {
			return true;
		}
		if (type instanceof RefType) {
			return isSubclass(((RefType) type).getSootClass(), "com.datastax.driver.core.Session");
		}
		return false;
	}

	private static boolean isSubclass(final SootClass clazz, final String className) {
		if (clazz.getName().equals(className)) {
			return true;
		} else if (clazz.hasSuperclass() && isSubclass(clazz.getSuperclass(), className)) {
			return true;
		} else if (clazz.getInterfaceCount() > 0) {
			for (final SootClass intf : clazz.getInterfaces()) {
				if (isSubclass(intf, className)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isBindMarker(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.BindMarker");
	}
	public static boolean isOrderingArray(final soot.Type type) {
		return isOfArrayType(type, "com.datastax.driver.core.querybuilder.Ordering");
	}
	public static boolean isClause(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Clause");
	}
	public static boolean isAssignment(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Assignment");
	}

	public static boolean isStatement(final soot.Type type) {
		Objects.requireNonNull(type);
		if (type instanceof RefType) {
			if (isBoundStatement(type) || isPreparedStatement(type) || isBatchStatement(type) || isSimpleStatement(type)
					|| isOfRefType(type, "com.datastax.driver.core.RegularStatement")
					|| isOfRefType(type, "com.datastax.driver.core.Statement")
					|| isOfRefType(type, "com.datastax.driver.core.StatementWrapper")
					|| isOfRefType(type, "com.datastax.driver.core.querybuilder.BuiltStatement")
					|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Truncate")
					|| isBatch(type) || isDelete(type) || isInsert(type) || isUpdate(type) || isSelect(type)) {

				return true;
			}
		}
		return false;
	}

	public static boolean isPreparedStatement(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.PreparedStatement")
				|| isOfRefType(type, "com.datastax.driver.core.DefaultPreparedStatement");
	}

	public static boolean isBoundStatement(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.BoundStatement");
	}

	public static boolean isBatchStatement(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.BatchStatement");
	}

	public static boolean isSimpleStatement(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.SimpleStatement");
	}

	public static boolean isQueryBuilder(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.QueryBuilder");
	}

	public static boolean isBatch(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Batch")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Batch$Options");
	}

	public static boolean isInsert(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Insert")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Insert$Options");
	}

	public static boolean isSelect(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Select")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Select$Selection")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Select$SelectionOrAlias")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Select$Builder")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Select$Where");
	}

	public static boolean isDelete(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Builder")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Conditions")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Options")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Selection")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Where");
	}

	public static boolean isUpdate(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Update")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Assignments")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Conditions")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Options")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$IfExists")
				|| isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Where");
	}

	public static boolean isDeleteConditions(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Conditions");
	}

	public static boolean isDeleteWhere(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Delete$Where");
	}

	public static boolean isUpdateAssignment(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Assignments");
	}

	public static boolean isUpdateConditions(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Conditions");
	}

	public static boolean isUpdateWhere(final soot.Type type) {
		return isOfRefType(type, "com.datastax.driver.core.querybuilder.Update$Where");
	}

	private static boolean isOfRefType(final soot.Type type, final String typeName) {
		Objects.requireNonNull(type);
		Objects.requireNonNull(typeName);
		return type instanceof RefType && typeName.equals(((RefType) type).getClassName());
	}

	private static boolean isOfArrayType(final soot.Type type, final String baseTypeName) {
		return type instanceof ArrayType && ((ArrayType) type).numDimensions == 1 && isOfRefType(((ArrayType) type).baseType, baseTypeName);
	}

	public static VarType resolveVarType(final soot.Type type) {
		if (isImmutable(type)) {
			return VarType.IMMUTABLE;
		} else if (isStringBuilder(type) || isStatement(type) || isObject(type) || isCharSequence(type) || isDate(type) || isLocalDate(type)
				|| isSession(type) || isResultSet(type) || isRow(type) || isClause(type) || isAssignment(type) || isBindMarker(type)
				|| isOrderingArray(type) || (type instanceof RefType && (((RefType) type).getClassName().startsWith("java.lang")
						|| ((RefType) type).getClassName().startsWith("java.util")))) {
			return VarType.OBJECT;
		} else if (type instanceof ArrayType) {
			final ArrayType arrayType = (ArrayType) type;
			final VarType baseVarType = resolveVarType(arrayType.baseType);
			if (baseVarType.equals(VarType.EXCLUDED)) {
				return VarType.EXCLUDED;
			} else {
				return VarType.ARRAY;
			}
		} else if (type instanceof RefType) {
			if (Scene.v().getApplicationClasses().contains(((RefType) type).getSootClass())) {
				return VarType.INCLUDED;
			}
		}
		return VarType.EXCLUDED;
	}
}
