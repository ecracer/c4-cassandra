package ch.ethz.inf.pm.cfour.cassandra.soot.execution;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.GraphToDotUtil;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.AnalyzedMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.GeneralMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.SessionMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ApplyConditionProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CaughtExceptionProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.DefaultProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.IdentityStmtProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.IfStmtProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.NopProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ReturnStmtProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ThrowExceptionProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignArrayRefPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignCastExprPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignFieldRefPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignImmediatePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignNewArrayExprPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignNewExprPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignPhiExprPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.AnnotationMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.ResultSetMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.StatementMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.StringBuilderMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBAssignmentInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBBindMarkerInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBClauseInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBDeleteInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBInsertInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBSelectInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb.QBUpdateInvokePoint;
import soot.Body;
import soot.Immediate;
import soot.Local;
import soot.Scene;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.CastExpr;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.ConditionExpr;
import soot.jimple.DynamicInvokeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.ThrowStmt;
import soot.jimple.toolkits.callgraph.Edge;
import soot.shimple.PhiExpr;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.scalar.ForwardFlowAnalysis;

/**
 * Builds an ExecuteGraph for a single method.
 */
class ExecuteGraphAnalysis extends ForwardFlowAnalysis<Unit, ExecuteGraphFlow> {

	private final static Logger LOG = LogManager.getLogger(ExecuteGraphAnalysis.class);

	private final Set<SootMethod> includedMethods;
	private final SootMethod method;
	private final SootOptions options;
	private final MutableGraph<AbstractProgramPoint> executeGraph = GraphBuilder.directed().allowsSelfLoops(true).build();
	private final CallStack callStack;
	private final AbstractProgramPoint entryNode;
	private final AbstractProgramPoint exitNode;

	public ExecuteGraphAnalysis(final Set<SootMethod> includedMethods, final SootMethod method, final SootOptions options,
			final Body body) {
		super(new ExceptionalUnitGraph(body));
		this.includedMethods = Objects.requireNonNull(includedMethods);
		this.method = Objects.requireNonNull(method);
		this.options = Objects.requireNonNull(options);
		this.callStack = CallStack.create(SootMethodUtils.methodFrom(this.method));
		this.entryNode = NopProgramPoint.create(this.callStack);
		this.exitNode = NopProgramPoint.create(this.callStack);
		executeGraph.addNode(this.entryNode);
		executeGraph.addNode(this.exitNode);
		doAnalysis();
		for (final Unit u : graph.getTails()) {
			for (final AbstractProgramPoint lastProgramPoint : getFlowAfter(u).getLastProgramPoints()) {
				executeGraph.putEdge(lastProgramPoint, this.exitNode);
			}
		}
		if (LOG.isTraceEnabled()) {
			try {
				final File tempDir = Files.createTempDirectory("sootAnalysisGraph").toFile();
				GraphToDotUtil.logGraphToTemp(tempDir, method, executeGraph);
				Files.write(Paths.get(tempDir.getAbsolutePath(), "method.shimple"), body.toString().getBytes(), StandardOpenOption.CREATE);
				LOG.trace("Graph is written to " + tempDir.getAbsolutePath());
			} catch (IOException e) {
				LOG.error("Error logging graph", e);
			}
		} ;
	}

	public ExecuteGraph getExecuteGraph() {
		final Set<AbstractProgramPoint> sources = new HashSet<>(executeGraph.successors(this.entryNode));
		final Set<AbstractProgramPoint> sinks = new HashSet<>(executeGraph.predecessors(this.exitNode));
		final boolean bypassPossible = sources.contains(this.exitNode);
		sources.remove(this.exitNode);
		sinks.remove(this.entryNode);
		executeGraph.removeNode(this.entryNode);
		executeGraph.removeNode(this.exitNode);
		return ExecuteGraph.create(executeGraph, sources, sinks, bypassPossible);
	}

	@Override
	protected ExecuteGraphFlow newInitialFlow() {
		return new ExecuteGraphFlow();
	}

	@Override
	protected ExecuteGraphFlow entryInitialFlow() {
		final ExecuteGraphFlow flow = new ExecuteGraphFlow();
		flow.addLastProgramPoint(entryNode);
		return flow;
	}

	@Override
	protected void merge(ExecuteGraphFlow in1, ExecuteGraphFlow in2, ExecuteGraphFlow out) {
		in1.mergeWith(in2, out);
	}

	@Override
	protected void copy(ExecuteGraphFlow source, ExecuteGraphFlow dest) {
		source.copyTo(dest);
	}

	@Override
	protected void flowThrough(ExecuteGraphFlow in, Unit d, ExecuteGraphFlow out) {
		in = handlePossibleIfPred(in, d);
		out.clear();
		final Stmt stmt = (Stmt) d;
		if (stmt instanceof InvokeStmt && addInvokeAbstraction(null, stmt, in, out)) {
			return;
		} else if (stmt instanceof AssignStmt && addAssignAbstraction((AssignStmt) stmt, in, out)) {
			return;
		} else if (stmt instanceof IdentityStmt) {
			if (((IdentityStmt) stmt).getRightOp() instanceof CaughtExceptionRef) {
				setAbstractProgramPoint(CaughtExceptionProgramPoint.create(callStack, (IdentityStmt) stmt), in, out);
			} else {
				setAbstractProgramPoint(IdentityStmtProgramPoint.create(callStack, (IdentityStmt) stmt), in, out);
			}
			return;
		} else if (stmt instanceof ThrowStmt) {
			setAbstractProgramPoint(ThrowExceptionProgramPoint.create(callStack, (ThrowStmt) stmt), in, out);
			return;
		} else if (stmt instanceof ReturnStmt) {
			setAbstractProgramPoint(ReturnStmtProgramPoint.create(callStack, (ReturnStmt) stmt), in, out);
			return;
		} else if (stmt instanceof IfStmt) {
			setAbstractProgramPoint(IfStmtProgramPoint.create(callStack, (IfStmt) stmt), in, out);
			return;
		} else if (stmt instanceof GotoStmt || stmt instanceof ReturnVoidStmt) {
			// side effect free
			in.copyTo(out);
			return;
		}

		final AbstractProgramPoint defaultAbstraction = DefaultProgramPoint.create(callStack, stmt);
		if (defaultAbstraction != null) {
			setAbstractProgramPoint(defaultAbstraction, in, out);
		} else {
			if (LOG.isDebugEnabled()) {
				LOG.debug("No abstraction needed for " + stmt);
			}
			in.copyTo(out);
		}
	}

	/**
	 * Inserts condition boxes into the execute graph if necessary (effect of the if statement)
	 * 
	 * @param in
	 * @param d
	 * @return
	 */
	private ExecuteGraphFlow handlePossibleIfPred(ExecuteGraphFlow in, Unit d) {
		final ExecuteGraphFlow newIn = newInitialFlow();
		for (final AbstractProgramPoint lastPoint : in.getLastProgramPoints()) {
			if (lastPoint instanceof IfStmtProgramPoint) {
				final IfStmtProgramPoint ifStmtPoint = (IfStmtProgramPoint) lastPoint;
				final ApplyConditionProgramPoint applyCondition;
				if (d.equals(ifStmtPoint.ifStmt.getTarget())) {
					applyCondition = ApplyConditionProgramPoint.create(callStack, (ConditionExpr) ifStmtPoint.ifStmt.getCondition(), false);
				} else {
					applyCondition = ApplyConditionProgramPoint.create(callStack, (ConditionExpr) ifStmtPoint.ifStmt.getCondition(), true);
				}
				executeGraph.addNode(applyCondition);
				executeGraph.putEdge(lastPoint, applyCondition);
				newIn.addLastProgramPoint(applyCondition);
			} else {
				newIn.addLastProgramPoint(lastPoint);
			}
		}
		return newIn;
	}

	private void setAbstractProgramPoint(final AbstractProgramPoint abstraction, final ExecuteGraphFlow in, final ExecuteGraphFlow out) {
		executeGraph.addNode(abstraction);
		in.getLastProgramPoints().forEach(pp -> executeGraph.putEdge(pp, abstraction));
		out.addLastProgramPoint(abstraction);
	}

	private boolean addAssignAbstraction(final AssignStmt assign, final ExecuteGraphFlow in, final ExecuteGraphFlow out) {
		if (assign.containsFieldRef()) {
			setAbstractProgramPoint(AssignFieldRefPoint.create(callStack, assign), in, out);
			return true;
		} else if (assign.containsArrayRef()) {
			setAbstractProgramPoint(AssignArrayRefPoint.create(callStack, assign), in, out);
			return true;
		} else if (assign.containsInvokeExpr()) {
			return addInvokeAbstraction((Local) assign.getLeftOp(), assign, in, out);
		}

		final Value right = assign.getRightOp();
		if (right instanceof NewExpr) {
			setAbstractProgramPoint(AssignNewExprPoint.create(callStack, assign), in, out);
			return true;
		} else if (right instanceof NewArrayExpr) {
			setAbstractProgramPoint(AssignNewArrayExprPoint.create(callStack, assign), in, out);
			return true;
		} else if (right instanceof CastExpr) {
			setAbstractProgramPoint(AssignCastExprPoint.create(callStack, assign), in, out);
			return true;
		} else if (right instanceof PhiExpr) {
			setAbstractProgramPoint(AssignPhiExprPoint.create(callStack, assign), in, out);
			return true;
		} else if (right instanceof Immediate) {
			setAbstractProgramPoint(AssignImmediatePoint.create(callStack, assign), in, out);
			return true;
		}
		return false;
	}

	private boolean addInvokeAbstraction(final Local left, final Stmt invokeStmt, final ExecuteGraphFlow in, final ExecuteGraphFlow out) {
		if (!invokeStmt.containsInvokeExpr()) {
			throw new IllegalArgumentException();
		}

		final InvokeExpr invoke = invokeStmt.getInvokeExpr();
		if (invoke instanceof DynamicInvokeExpr) {
			// we cannot handle dynamic invokes --> use default
			return false;
		}

		final Iterator<Edge> callingMethodsIt = Scene.v().getCallGraph().edgesOutOf(invokeStmt);
		final Set<SootMethod> callingMethods = new HashSet<>();
		while (callingMethodsIt.hasNext()) {
			final SootMethod next = callingMethodsIt.next().getTgt().method();
			if (next.isStaticInitializer()) {
				// we captured static initializers in the field analysis
				// also, a static initializer is only executed once
				continue;
			}
			callingMethods.add(next);
		}
		if (callingMethods.isEmpty()) {
			// source is missing, add declared soot method
			callingMethods.add(invoke.getMethod());
		}
		final HashMap<AbstractProgramPoint, Boolean> possibleAbstractions = new HashMap<>();
		boolean allIncluded = true;
		for (final SootMethod next : callingMethods) {

			final int methodId = SootMethodUtils.getMethodId(next);

			final AbstractProgramPoint nextKnownAbstraction;
			// handle calls to unknown methods --> should point to analyzed methods
			if (SootMethodUtils.UNKNOWN.isInInterval(methodId)) {
				if (includedMethods.contains(next)) {
					final AnalyzedMethodInvokePoint nextInvoke = AnalyzedMethodInvokePoint.create(callStack, left, invokeStmt,
							methodId, next);

					possibleAbstractions.put(nextInvoke, next.isStaticInitializer());
				} else if (!next.isStaticInitializer()) {
					allIncluded = false;
				}
				nextKnownAbstraction = null;
			}

			// General Methods
			else if (SootMethodUtils.GENERAL.isInInterval(methodId)) {
				nextKnownAbstraction = GeneralMethodInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.STRING_BUILDER.isInInterval(methodId)) {
				nextKnownAbstraction = StringBuilderMethodInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.SESSION.isInInterval(methodId)) {
				nextKnownAbstraction = SessionMethodInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.STATEMENT.isInInterval(methodId)) {
				nextKnownAbstraction = StatementMethodInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.ANNOTATIONS.isInInterval(methodId)) {
				nextKnownAbstraction = AnnotationMethodInvokePoint.create(callStack, left, invokeStmt, methodId, options);
			} else if (SootMethodUtils.RESULT_SET.isInInterval(methodId)) {
				nextKnownAbstraction = ResultSetMethodInvokePoint.create(callStack, left, invokeStmt, methodId);
			}

			// QueryBuilders
			else if (SootMethodUtils.QB_INSERT.isInInterval(methodId)) {
				nextKnownAbstraction = QBInsertInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_SELECT.isInInterval(methodId)) {
				nextKnownAbstraction = QBSelectInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_DELETE.isInInterval(methodId)) {
				nextKnownAbstraction = QBDeleteInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_UPDATE.isInInterval(methodId)) {
				nextKnownAbstraction = QBUpdateInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_BINDMARKER.isInInterval(methodId)) {
				nextKnownAbstraction = QBBindMarkerInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_CLAUSE.isInInterval(methodId)) {
				nextKnownAbstraction = QBClauseInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else if (SootMethodUtils.QB_ASSIGNMENT.isInInterval(methodId)) {
				nextKnownAbstraction = QBAssignmentInvokePoint.create(callStack, left, invokeStmt, methodId);
			} else {
				throw new RuntimeException("Method " + methodId + " is known, but no abstraction exists");
			}

			if (nextKnownAbstraction != null) {
				possibleAbstractions.put(nextKnownAbstraction, next.isStaticInitializer());
			}
		}
		if (!allIncluded || possibleAbstractions.size() == 0) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Missing abstractions for " + invokeStmt + ". Add default if necessary.");
			}
			final AbstractProgramPoint defaultPP = DefaultProgramPoint.create(callStack, invokeStmt);
			if (defaultPP != null) {
				possibleAbstractions.put(defaultPP, false);
			}
		}

		if (invoke instanceof InstanceInvokeExpr
				&& SootTypeUtils.resolveVarType(((InstanceInvokeExpr) invoke).getBase().getType()) == VarType.EXCLUDED) {
			// we cannot handle methods on excluded base vars 
			return false;
		}

		if (possibleAbstractions.size() > 0) {
			for (final Map.Entry<AbstractProgramPoint, Boolean> possibleAbstraction : possibleAbstractions.entrySet()) {
				final AbstractProgramPoint next;
				final AbstractProgramPoint last;

				if (possibleAbstraction.getKey() instanceof AnalyzedMethodInvokePoint) {
					final AnalyzedMethodInvokePoint invokePoint = (AnalyzedMethodInvokePoint) possibleAbstraction.getKey();
					executeGraph.addNode(invokePoint);
					executeGraph.addNode(invokePoint.methodReturnPoint);
					next = invokePoint;
					last = invokePoint.methodReturnPoint;
				} else {
					executeGraph.addNode(possibleAbstraction.getKey());
					next = possibleAbstraction.getKey();
					last = possibleAbstraction.getKey();
				}

				if (possibleAbstraction.getValue()) {
					// static initializer
					in.getLastProgramPoints().forEach(pp -> {
						executeGraph.putEdge(pp, next);
						executeGraph.putEdge(last, pp);
					});
				} else {
					in.getLastProgramPoints().forEach(pp -> {
						executeGraph.putEdge(pp, next);
					});
					out.addLastProgramPoint(last);
				}
			}
		} else {
			in.copyTo(out);
		}
		return true;
	}
}