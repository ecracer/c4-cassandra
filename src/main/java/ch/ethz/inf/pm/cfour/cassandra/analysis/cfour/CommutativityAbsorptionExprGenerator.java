package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.and;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.or;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;

import ch.ethz.inf.pm.cfour.Expr;
import ch.ethz.inf.pm.cfour.GlobalVar;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnCounterUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnPartUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnUnknownValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;

/**
 * Helper for generating the system specification of the CFour input
 */
public class CommutativityAbsorptionExprGenerator {

	private final SchemaInformation schemaInformation;
	private final VariableProvider varProvider;
	private final Options options;
	private final Map<String, GlobalVar> primaryKeyEncodings;

	private CommutativityAbsorptionExprGenerator(final SchemaInformation schemaInformation, final Options options,
			final VariableProvider varProvider) {
		this.schemaInformation = Objects.requireNonNull(schemaInformation);
		this.options = Objects.requireNonNull(options);
		this.varProvider = Objects.requireNonNull(varProvider);
		this.primaryKeyEncodings = new HashMap<>();
	}

	public static CommutativityAbsorptionExprGenerator create(final SchemaInformation schemaInformation, final Options options) {
		return new CommutativityAbsorptionExprGenerator(schemaInformation, options, VariableProvider.create());
	}

	public static CommutativityAbsorptionExprGenerator create(final SchemaInformation schemaInformation, final Options options,
			final VariableProvider varProvider) {
		return new CommutativityAbsorptionExprGenerator(schemaInformation, options, varProvider);
	}

	/**
	 * @param table
	 * @param column
	 * @return expr that encodes if column is a primary key of table
	 */
	private Expr getIsPrimaryKeyExpr(final String table, final String column) {
		final Boolean isPrimaryKey = schemaInformation.isPartOfPrimaryKey(table, column);
		if (Boolean.TRUE.equals(isPrimaryKey)) {
			return CFourFactory.TRUE;
		} else if (Boolean.FALSE.equals(isPrimaryKey)) {
			return CFourFactory.FALSE;
		} else {
			final String key = table + "_" + column;
			if (!primaryKeyEncodings.containsKey(key)) {
				primaryKeyEncodings.put(key, varProvider.newBoolGlobalVar(key));
			}
			return primaryKeyEncodings.get(key);
		}
	}

	/**
	 * @param part
	 * @return true if the part is a NOP (e.g. SELECT * FROM tweets WHERE tweet_id IN ())
	 */
	private boolean doesNeitherWriteNorRead(final StatementPart part) {
		for (final Integer idx : part.getDefiniteConstraintIdxs().values()) {
			final Constraint constraint = part.getConstraint(idx);
			if (constraint instanceof Constraint.INValsConstraint && ((Constraint.INValsConstraint) constraint).values.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param part
	 * @return Expr that encodes if all primary key columns are constrained using equality. If this is the case, at most
	 *         one row is returned or updated from the statement
	 */
	private Expr getHasEQConstraintOnAllPKColumnsExpr(final StatementPart part) {
		// alle nicht eingeschränkten columns müssen nicht-pk-sein
		final List<Expr> parts = new ArrayList<>();
		final Set<String> nonConstrainedColumns = new HashSet<>(schemaInformation.getWrittenColumns(part.table));
		for (final int constraintIdx : part.getDefiniteConstraintIdxs().values()) {
			final Constraint constraint = part.getConstraint(constraintIdx);
			if (constraint instanceof Constraint.EQConstraint) {
				nonConstrainedColumns.remove(constraint.column);
			}
		}
		for (final String column : nonConstrainedColumns) {
			parts.add(CFourFactory.not(getIsPrimaryKeyExpr(part.table, column)));
		}
		if (parts.isEmpty()) {
			return CFourFactory.TRUE;
		} else {
			return CFourFactory.and(parts);
		}
	}

	/**
	 * @param leftIdx
	 * @param leftValue
	 * @param rightIdx
	 * @param rightValue
	 * @param equality
	 *            whether to use equal or unequal
	 * @return Expr that encodes that leftValue is equal (unequal) to rightValue
	 */
	private Expr calcEquality(final int leftIdx, final ConcreteValue leftValue, final int rightIdx, final ConcreteValue rightValue,
                              final boolean equality) {
		if (options.isValueAnalysisEnabled() && leftValue != null && rightValue != null) {
			if (leftValue instanceof NullValue && rightValue instanceof NullValue) {
				return equality ? CFourFactory.TRUE : CFourFactory.FALSE;
			} else if ((leftValue instanceof StringValue && rightValue instanceof StringValue) ||
					(leftValue instanceof IntValue && rightValue instanceof IntValue)) {
				if (leftValue.equals(rightValue)) {
					return equality ? CFourFactory.TRUE : CFourFactory.FALSE;
				} else {
					return equality ? CFourFactory.FALSE : CFourFactory.TRUE;
				}
			} else if (leftValue instanceof UUIDValue && rightValue instanceof UUIDValue) {
				final UUIDValue leftUUID = (UUIDValue) leftValue;
				final UUIDValue rightUUID = (UUIDValue) rightValue;
				if (leftUUID.isRandom && rightUUID.isRandom && !leftUUID.createdIn.equals(rightUUID.createdIn)) {
					return equality ? CFourFactory.FALSE : CFourFactory.TRUE;
				} else if (leftUUID.isRandom && rightUUID.isRandom && leftUUID.createdIn.equals(rightUUID.createdIn)) {
					if (equality) {
						return CFourFactory.and(CFourFactory.equal(CFourFactory.intClientIdArgLeft(), CFourFactory.intClientIdArgRight()),
								CFourFactory.equal(CFourFactory.intArgLeft(leftIdx), CFourFactory.intArgRight(rightIdx)));
					} else {
						return CFourFactory.or(CFourFactory.unequal(CFourFactory.intClientIdArgLeft(), CFourFactory.intClientIdArgRight()),
								CFourFactory.unequal(CFourFactory.intArgLeft(leftIdx), CFourFactory.intArgRight(rightIdx)));
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param leftIdx
	 * @param leftValue
	 * @param rightIdx
	 * @param rightValue
	 * @return Expr that encodes leftValue != rightValue
	 */
	private Expr unequalExpr(final int leftIdx, final ConcreteValue leftValue, final int rightIdx, final ConcreteValue rightValue) {
		final Expr valueExpr = calcEquality(leftIdx, leftValue, rightIdx, rightValue, false);
		return valueExpr != null ? valueExpr : CFourFactory.unequal(CFourFactory.intArgLeft(leftIdx), CFourFactory.intArgRight(rightIdx));
	}

	/**
	 * @param leftIdx
	 * @param leftValue
	 * @param rightIdx
	 * @param rightValue
	 * @return Expr that encodes leftValue == rightValue
	 */
	private Expr equalExpr(final int leftIdx, final ConcreteValue leftValue, final int rightIdx, final ConcreteValue rightValue) {
		final Expr valueExpr = calcEquality(leftIdx, leftValue, rightIdx, rightValue, true);
		return valueExpr != null ? valueExpr : CFourFactory.equal(CFourFactory.intArgLeft(leftIdx), CFourFactory.intArgRight(rightIdx));
	}

	/**
	 * @param wpart1
	 * @param wpart2
	 * @return Expr that encodes whether two updates write the same values to common columns (used for encoding
	 *         commutativity)
	 */
	private Expr getCommonColumnExpr(final StatementPart.UpdatePart wpart1, final StatementPart.UpdatePart wpart2) {
		if (wpart1 instanceof StatementPart.DeletePart || wpart2 instanceof StatementPart.DeletePart) {
			// two deletes update the columns to the same value. A delete and an insert are not commutative. DeleteAll & StrictUpdate commute
			if (wpart1 instanceof StatementPart.DeletePart && wpart2 instanceof StatementPart.DeletePart) {
				return CFourFactory.TRUE;
			} else if ((wpart1 instanceof StatementPart.DeletePart && wpart2 instanceof StatementPart.UpsertPart && !((StatementPart.UpsertPart) wpart2).canInsert)
					|| (wpart2 instanceof StatementPart.DeletePart && wpart1 instanceof StatementPart.UpsertPart && !((StatementPart.UpsertPart) wpart1).canInsert)) {
				return CFourFactory.TRUE;
			} else {
				return CFourFactory.FALSE;
			}
		}
		final StatementPart.UpsertPart upsPart1 = (StatementPart.UpsertPart) wpart1;
		final StatementPart.UpsertPart upsPart2 = (StatementPart.UpsertPart) wpart2;
		final ListMultimap<String, Integer> colIdxs1 = upsPart1.components.getPossiblyUpdatedColumnIdxs();
		final ListMultimap<String, Integer> colIdxs2 = upsPart2.components.getPossiblyUpdatedColumnIdxs();

		final List<Expr> commonColumnExpr = new ArrayList<>();
		commonColumnExpr.add(CFourFactory.TRUE);
		for (final Entry<String, Integer> colIdx1 : colIdxs1.entries()) {
			if (!colIdxs2.containsKey(colIdx1.getKey())) {
				continue;
			} else if (schemaInformation.isColumnNeverRead(upsPart1.table, colIdx1.getKey())) {
				continue;
			}
			final ColumnUpdate update1 = upsPart1.components.getColumnUpdate(colIdx1.getValue());
			for (final Integer colIdx2 : colIdxs2.get(colIdx1.getKey())) {
				final ColumnUpdate update2 = upsPart2.components.getColumnUpdate(colIdx2);
				if (update1 instanceof ColumnPartUpdate || update2 instanceof ColumnPartUpdate) {
					return CFourFactory.FALSE;
				}
				if (update1 instanceof ColumnCounterUpdate && update2 instanceof ColumnCounterUpdate) {
					continue;
				} else if (update1 instanceof ColumnSetUpdate && update2 instanceof ColumnSetUpdate) {
					if (((ColumnSetUpdate) update1).operation == ((ColumnSetUpdate) update2).operation) {
						continue;
					} else {
						commonColumnExpr.add(CFourFactory.or(getIsPrimaryKeyExpr(wpart1.table, colIdx1.getKey()),
								unequalExpr(colIdx1.getValue(), update1.getValue(), colIdx2, update2.getValue())));
					}
				} else {
					commonColumnExpr.add(CFourFactory.or(getIsPrimaryKeyExpr(wpart1.table, colIdx1.getKey()),
							equalExpr(colIdx1.getValue(), update1.getValue(), colIdx2, update2.getValue())));
				}
			}
		}
		return CFourFactory.and(commonColumnExpr);
	}

	/**
	 * @param wpart
	 * @param rpart
	 * @return Expr that encodes that wpart only writes to columns that rpart does not select or that are display
	 *         columns
	 */
	private Expr writesAtMostCommonDisplayColumns(final StatementPart.UpdatePart wpart, final StatementPart.QueryPart rpart) {
		if (rpart.areAllDisplayColumns()) {
			return CFourFactory.TRUE;
		} else if (wpart instanceof StatementPart.DeletePart) {
			// a row may be deleted that SELECT may include in the result
			return CFourFactory.FALSE;
		}

		final StatementPart.UpsertPart upsert = (StatementPart.UpsertPart) wpart;
		final Set<String> colsThatMayBeUpdated = upsert.components.getPossiblyUpdatedColumnIdxs().keySet();

		if (colsThatMayBeUpdated.stream().allMatch(col -> rpart.isDisplayColumn(col))) {
			// a new row may be inserted that SELECT may include in the result
			return upsert.canInsert ? CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()) : CFourFactory.TRUE;
		} else if (rpart instanceof StatementPart.SelectAllPart) {
			return CFourFactory.FALSE;
		}

		final List<Expr> parts = new ArrayList<>();
		if (upsert.canInsert) {
			parts.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()));
		}
		final StatementPart.SelectColsPart selectCols = (StatementPart.SelectColsPart) rpart;
		for (final String colThatMayBeUpdated : colsThatMayBeUpdated) {
			if (selectCols.isSelectingColumn(colThatMayBeUpdated) && !selectCols.isDisplayColumn(colThatMayBeUpdated)) {
				parts.add(getIsPrimaryKeyExpr(wpart.table, colThatMayBeUpdated));
			}
		}
		return parts.isEmpty() ? CFourFactory.TRUE : CFourFactory.and(parts);
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr which implies that different rows are used by part1 than by part2
	 */
	private Expr getUnequalConstraintExpr(final StatementPart part1, final StatementPart part2) {
		if (!part1.table.equals(part2.table)) {
			return CFourFactory.TRUE;
		}
		final ListMultimap<String, Integer> part1ConstraintIdxs = part1.getPossibleConstraintIdxs();
		final ListMultimap<String, Integer> part2ConstraintIdxs = part2.getPossibleConstraintIdxs();
		final List<Expr> constraintExpr = new ArrayList<>();
		constraintExpr.add(CFourFactory.FALSE);
		for (final String column : part1ConstraintIdxs.keySet()) {
			for (final Integer part1ConstraintIdx : part1ConstraintIdxs.get(column)) {
				final Constraint part1Constraint = part1.getConstraint(part1ConstraintIdx);
				for (final Integer part2ConstraintIdx : part2ConstraintIdxs.get(column)) {
					final Constraint part2Constraint = part2.getConstraint(part2ConstraintIdx);
					if (part1Constraint instanceof Constraint.EQConstraint && part2Constraint instanceof Constraint.EQConstraint) {
						constraintExpr.add(CFourFactory.and(getIsPrimaryKeyExpr(part1.table, column),
								unequalExpr(part1ConstraintIdx, part1Constraint.getValue(), part2ConstraintIdx,
										part2Constraint.getValue())));
					}
				}
			}
		}
		return CFourFactory.or(constraintExpr);
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr which implies Equality
	 */
	private Expr getEqualConstraintExpr(final StatementPart part1, final StatementPart part2) {
		return getPart2ConstraintIncludesPart1ConstraintExpr(part1, part2, true);
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr which implies that part2 includes all part1 rows
	 */
	private Expr getPart2ConstraintIncludesPart1ConstraintExpr(final StatementPart part1, final StatementPart part2,
			final boolean extendToEquality) {

		if (!part1.table.equals(part2.table)) {
			return CFourFactory.FALSE;
		}

		final ListMultimap<String, Integer> part1ConstraintIdxs = part1.getPossibleConstraintIdxs();
		final ListMultimap<String, Integer> part2ConstraintIdxs = part2.getPossibleConstraintIdxs();
		final List<Expr> constraintExpr = new ArrayList<>();
		for (final String column : part2ConstraintIdxs.keySet()) {
			if (!part1ConstraintIdxs.containsKey(column)) {
				constraintExpr.add(CFourFactory.not(getIsPrimaryKeyExpr(part1.table, column)));
			} else {
				for (final Integer part2ConstraintIdx : part2ConstraintIdxs.get(column)) {
					final Constraint part2Constraint = part2.getConstraint(part2ConstraintIdx);
					for (final Integer part1ConstraintIdx : part1ConstraintIdxs.get(column)) {
						final Constraint part1Constraint = part1.getConstraint(part1ConstraintIdx);
						if (part1Constraint instanceof Constraint.EQConstraint && part2Constraint instanceof Constraint.EQConstraint) {
							constraintExpr.add(CFourFactory.or(CFourFactory.not(getIsPrimaryKeyExpr(part1.table, column)),
									equalExpr(part1ConstraintIdx, part1Constraint.getValue(), part2ConstraintIdx,
											part2Constraint.getValue())));
						} else {
							constraintExpr.add(CFourFactory.not(getIsPrimaryKeyExpr(part1.table, column)));
						}
					}
				}
			}
		}
		if (extendToEquality) {
			for (final String column : part1ConstraintIdxs.keySet()) {
				if (!part2ConstraintIdxs.containsKey(column)) {
					constraintExpr.add(CFourFactory.not(getIsPrimaryKeyExpr(part1.table, column)));
				}
			}
		}
		return constraintExpr.isEmpty() ? CFourFactory.FALSE : CFourFactory.and(constraintExpr);
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr that encodes iff part2 writes to a superset of part1 (in cols and rows)
	 */
	private Expr getPart2IncludesPart1Expr(final StatementPart.UpdatePart part1, final StatementPart.UpdatePart part2) {
		if (!part1.table.equals(part2.table)) {
			return CFourFactory.FALSE;
		}
		if (doesNeitherWriteNorRead(part1)) {
			return CFourFactory.TRUE;
		} else if (doesNeitherWriteNorRead(part2)) {
			return CFourFactory.FALSE;
		}
		return CFourFactory.and(getPart2ConstraintIncludesPart1ConstraintExpr(part1, part2, false));
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Where part1 and part2 commute for all event arguments
	 */
	public boolean isAlwaysCommuting(final StatementPart part1, final StatementPart part2) {
		return CFourFactory.TRUE.equals(getCommutativityExpr(part2 instanceof StatementPart.UpdatePart ? part2 : part1, part2 instanceof StatementPart.UpdatePart ? part1 : part2));
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr that implies that part1;part2 == part2;part1 and part2;part1 == part1;part2
	 */
	public Expr getCommutativityExpr(final StatementPart part1, final StatementPart part2) {
		if (!part1.table.equals(part2.table)) {
			return CFourFactory.TRUE;
		}
		// acts like a skip, e.g. SELECT * FROM table1 WHERE key IN ()
		// happens for example when in-list is concatenated using empty list
		if (doesNeitherWriteNorRead(part1) || doesNeitherWriteNorRead(part2)) {
			return CFourFactory.TRUE;
		}

		if (part1 instanceof StatementPart.QueryPart && part2 instanceof StatementPart.QueryPart) {
			// two reads always commute
			return CFourFactory.TRUE;
		} else if (part1 instanceof StatementPart.UpdatePart && part2 instanceof StatementPart.UpdatePart) {
			final StatementPart.UpdatePart update1 = (StatementPart.UpdatePart) part1;
			final StatementPart.UpdatePart update2 = (StatementPart.UpdatePart) part2;
			// two writes commute iff
			// - they write the same values into common columns
			// OR
			// - they write to different rows
			// OR
			// - both are upserts that do nothing
			final List<Expr> orExprs = new ArrayList<>();
			orExprs.add(getCommonColumnExpr(update1, update2));
			orExprs.add(getUnequalConstraintExpr(part1, part2));
			if (part1 instanceof StatementPart.UpsertPart && part2 instanceof StatementPart.UpsertPart) {
				orExprs.add(CFourFactory.and(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()), CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight()), CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft()),
						CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgRight())));
			}
			return CFourFactory.or(orExprs);
		} else {
			// a read and a write commute iff
			// - other columns are read than written (or the columns read are display columns)
			// OR 
			// - another row is read than is written
			// OR
			// - update is a nop

			// First part is updatePart (has to be specified in this way)
			final StatementPart.UpdatePart upPart = (StatementPart.UpdatePart) part1;
			final StatementPart.QueryPart queryPart = (StatementPart.QueryPart) part2;

			final List<Expr> orExprs = new ArrayList<>();
			orExprs.add(writesAtMostCommonDisplayColumns(upPart, queryPart));
			orExprs.add(getUnequalConstraintExpr(part1, part2));
			if (upPart instanceof StatementPart.UpsertPart) {
				orExprs.add(CFourFactory.and(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()), CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft())));
			}
			return CFourFactory.or(orExprs);
		}
	}

	/**
	 * Returns an expression that implies that part1 is absorbed by part2
	 * 
	 * @param part1
	 *            leftPart
	 * @param part2
	 *            rightPart
	 * @return
	 */
	public Expr getAbsorptionExpr(final StatementPart part1, final StatementPart part2) {
		if (!(part1 instanceof StatementPart.UpdatePart) || !(part2 instanceof StatementPart.UpdatePart) || !part1.table.equals(part2.table)) {
			return CFourFactory.FALSE;
		}
		final StatementPart.UpdatePart wpart1 = (StatementPart.UpdatePart) part1;
		final StatementPart.UpdatePart wpart2 = (StatementPart.UpdatePart) part2;

		final Expr part1IsNop;
		if (wpart1 instanceof StatementPart.UpsertPart) {
			part1IsNop = CFourFactory.and(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()), CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft()));
		} else {
			part1IsNop = CFourFactory.FALSE;
		}

		if (wpart2 instanceof StatementPart.DeletePart ||
				(wpart2 instanceof StatementPart.UpsertPart && schemaInformation.updatesAllColumnsEverUpdated(wpart2.table,
						((StatementPart.UpsertPart) wpart2).components.getPossiblyUpdatedColumnIdxs().keySet()))) {
			// all columns are updated / deleted --> absorbs other updates on the same row
			return CFourFactory.or(part1IsNop, CFourFactory.and(getPart2IncludesPart1Expr(wpart1, wpart2)));
		}

		if (wpart1 instanceof StatementPart.DeletePart) {
			return CFourFactory.FALSE;
		}

		final StatementPart.UpsertPart upsPart1 = (StatementPart.UpsertPart) wpart1;
		final StatementPart.UpsertPart upsPart2 = (StatementPart.UpsertPart) wpart2;

		final ImmutableListMultimap<String, Integer> colIdxs1 = upsPart1.components.getPossiblyUpdatedColumnIdxs();
		final ImmutableListMultimap<String, Integer> colIdxs2 = upsPart2.components.getPossiblyUpdatedColumnIdxs();

		// for each update must exists an update in the second part that absorbs the first (if the columns is not part of the pk)
		List<Expr> absorptionExpr = new ArrayList<>();
		absorptionExpr.add(CFourFactory.TRUE);
		for (final Entry<String, Integer> colIdx1 : colIdxs1.entries()) {
			Expr columnAbsorbedExpr = CFourFactory.FALSE;
			for (final Integer colIdx2 : colIdxs2.get(colIdx1.getKey())) {
				final ColumnUpdate update2 = upsPart2.components.getColumnUpdate(colIdx2);
				if (update2 instanceof ColumnValueUpdate || update2 instanceof ColumnUnknownValueUpdate) {
					columnAbsorbedExpr = CFourFactory.TRUE;
				} else if (update2 instanceof ColumnSetUpdate) {
					final ColumnUpdate update1 = upsPart1.components.getColumnUpdate(colIdx1.getValue());
					if (update1 instanceof ColumnSetUpdate) {
						// adding / removing the same element absorbs previous operations
						columnAbsorbedExpr = CFourFactory.or(equalExpr(colIdx1.getValue(), update1.getValue(), colIdx2, update2.getValue()),
								columnAbsorbedExpr);
					}
				}
			}
			absorptionExpr.add(CFourFactory.or(columnAbsorbedExpr, getIsPrimaryKeyExpr(wpart1.table, colIdx1.getKey())));
		}
		absorptionExpr.add(getPart2IncludesPart1Expr(wpart1, wpart2));
		return CFourFactory.or(part1IsNop, CFourFactory.and(absorptionExpr));
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr that implies part1;part2 == part2;part1
	 */
	public Expr getAsymetricCommutativityExpr(final StatementPart part1, final StatementPart part2) {
		if (isAlwaysCommuting(part1, part2)) {
			return null;
		}

		final List<Expr> orConstraintExpr = new ArrayList<>();
		if (part1 instanceof StatementPart.UpsertPart && part2 instanceof StatementPart.UpsertPart) {
			// INSERT IF NOT EXISTS, UPSERT --> falls INSERT nichts einfügt, wird es auch nach einem UPSERT nichts einfügen
			if (((StatementPart.UpsertPart) part1).ifNotExists) {
				orConstraintExpr.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()));
			}
			// UPDATE, INSERT_IF_NOT_EXISTS (vor Update muss schon Datensatz existieren, da dieses ansonsten einfügen würde, das aber nicht passiert)
			if (((StatementPart.UpsertPart) part2).ifNotExists) {
				if (((StatementPart.UpsertPart) part1).ifExists) {
					// UPDATE muss ein update machen, INSERT darf nichts einfügen
					orConstraintExpr.add(
							CFourFactory.and(CFourFactory.boolUpdatesExistingRowsArgLeft(), CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()), CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight())));
				} else if (!((StatementPart.UpsertPart) part1).canInsert) {
					return CFourFactory.TRUE;
				} else {
					// INSERT fügt entweder nichts ein oder UPDATE fügt nichts ein (d.h. Update aktualisiert einfach) 
					orConstraintExpr.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()));
					orConstraintExpr.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight()));
				}
			}

			// INSERT, UPDATE IF EXISTS, welches nichts Updated
			if (((StatementPart.UpsertPart) part2).ifExists) {
				orConstraintExpr.add(CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgRight()));
			}
		}
		// DELETE, UPDATE IF EXISTS (vor dem Delete kann schon noch geupdated werden, falls nichts neues hinzukommt...)
		if (part1 instanceof StatementPart.DeletePart && part2 instanceof StatementPart.UpsertPart) {
			if (((StatementPart.UpsertPart) part2).ifExists) {
				return CFourFactory.TRUE;
			} else {
				orConstraintExpr.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight()));
			}
		}
		if (!options.isSideChannelsEnabled()) {
			// STMT1, STMT2 if constraint of STMT2 includes random UUID and stmts are on different clients
			final ListMultimap<String, Integer> part1ConstraintIdxs = part1.getDefiniteConstraintIdxs();
			final ListMultimap<String, Integer> part2ConstraintIdxs = part2.getPossibleConstraintIdxs();
			for (final String column : part2ConstraintIdxs.keySet()) {
				for (final Integer part2ConstraintIdx : part2ConstraintIdxs.get(column)) {
					final Constraint part2Constraint = part2.getConstraint(part2ConstraintIdx);

					if (part2Constraint instanceof Constraint.EQValConstraint
							&& ((Constraint.EQValConstraint) part2Constraint).value instanceof UUIDValue
							&& ((UUIDValue) ((Constraint.EQValConstraint) part2Constraint).value).isRandom) {

						boolean stmt1CannotIncludeUUIDInResult = part1ConstraintIdxs.containsKey(column);
						for (final Integer part1ConstraintIdx : part1ConstraintIdxs.get(column)) {
							final Constraint part1Constraint = part1.getConstraint(part1ConstraintIdx);
							if (!(part1Constraint instanceof Constraint.EQConstraint) && !(part1Constraint instanceof Constraint.INConstraint)) {
								// Could be range query
								stmt1CannotIncludeUUIDInResult = false;
							}
						}
						if (stmt1CannotIncludeUUIDInResult) {
							orConstraintExpr.add(CFourFactory.unequal(CFourFactory.intClientIdArgLeft(), CFourFactory.intClientIdArgRight()));
						}
					}
				}
			}
		}
		// INSERT, SELECT, falls SELECT kein Resultat selektiert
		if (part1 instanceof StatementPart.UpsertPart && part2 instanceof StatementPart.QueryPart) {
			orConstraintExpr.add(CFourFactory.and(CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft()), CFourFactory.boolReturnsEmptyResultArgRight()));
		}
		// SELECT, UPDATE falls SELECT eine Row mit nur PK-Parts selektiert (oder UPDATE IF NOT EXISTS), d.h. Resultat darf nicht leer sein
		if (part1 instanceof StatementPart.QueryPart && part2 instanceof StatementPart.UpsertPart) {
			final StatementPart.QueryPart qpart1 = (StatementPart.QueryPart) part1;
			final StatementPart.UpsertPart upspart2 = (StatementPart.UpsertPart) part2;
			final List<Expr> parts = new ArrayList<>();
			parts.add(CFourFactory.not(CFourFactory.boolReturnsEmptyResultArgLeft()));
			if (!((StatementPart.UpsertPart) part2).ifNotExists) {
				// alle selektierten Rows müssen PK-Part sein oder vom Upsert nicht aktualisiert werden
				final Set<String> columnsSelected;
				if (part1 instanceof StatementPart.SelectColsPart) {
					columnsSelected = new HashSet<>();
					for (final ColumnSelection columnSel : ((StatementPart.SelectColsPart) part1).getSelectedColumns()) {
						columnsSelected.add(columnSel.column);
					}
				} else {
					columnsSelected = schemaInformation.getWrittenColumns(((StatementPart.SelectAllPart) part1).table);
				}
				final Set<String> updatedColumns = upspart2.components.getPossiblyUpdatedColumnIdxs().keySet();
				for (final String column : columnsSelected) {
					if (!qpart1.isDisplayColumn(column) && updatedColumns.contains(column)) {
						parts.add(getIsPrimaryKeyExpr(part1.table, column));
					}
				}
			}
			parts.add(getHasEQConstraintOnAllPKColumnsExpr(part1));
			orConstraintExpr.add(CFourFactory.and(parts));
		}

		return orConstraintExpr.isEmpty() ? null : CFourFactory.or(orConstraintExpr);
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr that encodes that part1 synchronizes with part2 (LWTs)
	 */
	public Expr getSynchronizingOperationExpr(final StatementPart part1, final StatementPart part2) {
		if (!(part1 instanceof StatementPart.UpdatePart) || !(part2 instanceof StatementPart.UpdatePart) || !part1.table.equals(part2.table)) {
			return null;
		}
		final StatementPart.UpdatePart wpart1 = (StatementPart.UpdatePart) part1;
		final StatementPart.UpdatePart wpart2 = (StatementPart.UpdatePart) part2;

		if (!wpart1.isLWT || !wpart2.isLWT) {
			return null;
		}

		// we have synchronization if the same row is updated
		// all pk-parts have to be specified in an update, so we can simply encode equality
		final Expr eqConstr = getEqualConstraintExpr(part1, part2);
		return CFourFactory.FALSE.equals(eqConstr) ? null : eqConstr;
	}

	/**
	 * @param part1
	 * @param part2
	 * @return Expr that must hold iff part1 --ca--> part2
	 */
	public Expr getLegalityExpr(final StatementPart part1, final StatementPart part2) {
		if (!part1.table.equals(part2.table)) {
			return null;
		}

		if (part1 instanceof StatementPart.QueryPart && part2 instanceof StatementPart.QueryPart && !schemaInformation.doDeletesHappenOnTable(part1.table)) {
			// we have monotonic reads
			// either part1 returns empty result or part2 returns non-empty-result or part2 selects other rows then part1
			return CFourFactory.or(CFourFactory.boolReturnsEmptyResultArgLeft(), CFourFactory.not(getEqualConstraintExpr(part1, part2)), CFourFactory.not(CFourFactory.boolReturnsEmptyResultArgRight()));
		} else if (part1 instanceof StatementPart.QueryPart && part2 instanceof StatementPart.UpsertPart && !schemaInformation.doDeletesHappenOnTable(part1.table)) {
			// if a query selects a result and constraints the pk, an update cannot insert on the same pk
			return CFourFactory.or(CFourFactory.boolReturnsEmptyResultArgLeft(), CFourFactory.not(getHasEQConstraintOnAllPKColumnsExpr(part1)),
					CFourFactory.not(getEqualConstraintExpr(part1, part2)), CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight()));
		} else if (part1 instanceof StatementPart.UpsertPart && part2 instanceof StatementPart.UpsertPart && !schemaInformation.doDeletesHappenOnTable(part1.table)) {
			// either we have distinct updates or the second update cannot insert new rows if no deletes happen on table
			// updates have to specify single rows, so we do not need to check for EQConstraints
			return CFourFactory.or(CFourFactory.not(getEqualConstraintExpr(part1, part2)), CFourFactory.not(CFourFactory.boolInsertsNewRowsArgRight()));
		} else if (part1 instanceof StatementPart.UpsertPart && part2 instanceof StatementPart.QueryPart && !schemaInformation.doDeletesHappenOnTable(part1.table)) {
			// if the query includes the update and no deletes happen, it cannot return an empty row
			return CFourFactory.or(CFourFactory.and(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()), CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft())),
					CFourFactory.not(getEqualConstraintExpr(part1, part2)), CFourFactory.not(CFourFactory.boolReturnsEmptyResultArgRight()));
		}

		return null;
	}
}
