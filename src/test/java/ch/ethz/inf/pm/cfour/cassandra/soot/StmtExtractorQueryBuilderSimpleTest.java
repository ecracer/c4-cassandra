package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorQueryBuilderSimpleTest extends AbstractSootTest {

	public StmtExtractorQueryBuilderSimpleTest() throws IOException {
		super("QueryBuilderSimple", "insert1", "select1", "select2", "select3", "select4", "delete1", "delete2", "delete3", "delete4",
				"update1", "update2");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(11, result.size());

		for (final SessionExecuteInvoke call : result) {
			final Set<String> cqls = getCQLs(call);
			assertEquals(1, cqls.size());

			final String query;
			switch (call.executedFrom.callStack.method.methodName) {
				case "insert1" :
					query = "INSERT INTO insert1 (username, password) VALUES (?, ?) IF NOT EXISTS";
					break;
				case "select1" :
					query = "SELECT * FROM select1 WHERE username = ?";
					break;
				case "select2" :
					query = "SELECT DISTINCT col1, col2 FROM select2 WHERE key IN (?)";
					break;
				case "select3" :
					query = "SELECT count(*) FROM select3";
					break;
				case "select4" :
					query = "SELECT col1 AS password, col2, col3 AS temp1, col4 AS temp2 FROM select4 WHERE col1 = ? AND col3 < ?";
					break;
				case "delete1" :
					query = "DELETE FROM delete1 WHERE col1 = ? IF EXISTS";
					break;
				case "delete2" :
					query = "DELETE col1, col2 FROM keyspace.delete2 WHERE key IN (?) AND col3 >= ? IF col1 = ? AND col2 > ?";
					break;
				case "delete3" :
					query = "DELETE col1[?] FROM delete3";
					break;
				case "delete4" :
					query = "DELETE FROM delete4 WHERE col1 = ? AND col3 IN (?)";
					break;
				case "update1" :
					query = "UPDATE u.update1 SET collist[?] = ?, colval = ?, colcounter = colcounter - 1 WHERE col1 = ? IF EXISTS";
					break;
				case "update2" :
					query = "UPDATE update2 SET colmap[?] = ? IF col1 = ? AND col2 CONTAINS ?";
					break;
				default :
					query = "";
			}
			assertEquals(query, cqls.iterator().next());
		}
	}
}