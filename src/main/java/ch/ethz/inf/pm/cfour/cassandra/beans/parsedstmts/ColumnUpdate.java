package ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;

/**
 * Base class for updates of column (e.g. =, +{?})
 */
public abstract class ColumnUpdate {

	// If new classes are added --> Check GraphBuilder & CommutativityAbsorptionExprGenerator

	public final String column;

	protected ColumnUpdate(final String column) {
		this.column = Objects.requireNonNull(column);
	}

	public abstract String getLabel();

	public abstract ConcreteValue getValue();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column == null) ? 0 : column.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColumnUpdate other = (ColumnUpdate) obj;
		if (column == null) {
			if (other.column != null)
				return false;
		} else if (!column.equals(other.column))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	public static class ColumnValueUpdate extends ColumnUpdate {

		public final ConcreteValue value;

		private ColumnValueUpdate(final String column, final ConcreteValue value) {
			super(column);
			this.value = Objects.requireNonNull(value);
		}

		public static ColumnValueUpdate create(final String column, final ConcreteValue value) {
			return new ColumnValueUpdate(column, value);
		}

		@Override
		public String getLabel() {
			return column + "_to_" + Math.abs(value.hashCode());
		}

		@Override
		public ConcreteValue getValue() {
			return value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ColumnValueUpdate other = (ColumnValueUpdate) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}

	public static class ColumnCounterUpdate extends ColumnUpdate {

		private ColumnCounterUpdate(final String column) {
			super(column);
		}

		public static ColumnCounterUpdate create(final String column) {
			return new ColumnCounterUpdate(column);
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}

		@Override
		public String getLabel() {
			return column + "_counter";
		}
	}

	public static class ColumnSetUpdate extends ColumnUpdate {

		public static enum SetOp {
			ADD, REMOVE
		};

		public final SetOp operation;

		private ColumnSetUpdate(final String column, final SetOp operation) {
			super(column);
			this.operation = Objects.requireNonNull(operation);
		}

		public static ColumnSetUpdate create(final String column, final SetOp operation) {
			return new ColumnSetUpdate(column, operation);
		}

		@Override
		public String getLabel() {
			return column + (operation == SetOp.ADD ? "_setadd" : "_setrem") + "_unk";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((operation == null) ? 0 : operation.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ColumnSetUpdate other = (ColumnSetUpdate) obj;
			if (operation != other.operation)
				return false;
			return true;
		}
	}

	public static class ColumnSetValueUpdate extends ColumnSetUpdate {

		public final ConcreteValue value;

		private ColumnSetValueUpdate(final String column, final SetOp operation, final ConcreteValue value) {
			super(column, operation);
			this.value = Objects.requireNonNull(value);
		}

		public static ColumnSetValueUpdate create(final String column, final SetOp operation, final ConcreteValue value) {
			return new ColumnSetValueUpdate(column, operation, value);
		}

		@Override
		public String getLabel() {
			return column + (operation == SetOp.ADD ? "_setadd_" : "_setrem_") + Math.abs(value.hashCode());
		}

		@Override
		public ConcreteValue getValue() {
			return value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			ColumnSetValueUpdate other = (ColumnSetValueUpdate) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}

	public static class ColumnUnknownValueUpdate extends ColumnUpdate {

		private ColumnUnknownValueUpdate(final String column) {
			super(column);
		}

		public static ColumnUnknownValueUpdate create(final String column) {
			return new ColumnUnknownValueUpdate(column);
		}

		@Override
		public String getLabel() {
			return column + "_to_unknown";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}
	}

	public static class ColumnPartUpdate extends ColumnUpdate {

		private ColumnPartUpdate(final String column) {
			super(column);
		}

		public static ColumnPartUpdate create(final String column) {
			return new ColumnPartUpdate(column);
		}

		@Override
		public String getLabel() {
			return column + "_part_to_unknown";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}
	}
}
