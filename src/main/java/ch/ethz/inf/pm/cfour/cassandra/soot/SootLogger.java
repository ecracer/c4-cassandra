package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Replaces the standard output of soot with a logger
 */
public class SootLogger extends PrintStream {

	private static final Logger LOG = LogManager.getLogger(SootLogger.class);
	private StringBuilder stringBuilder;

	public SootLogger() {
		super(System.out);
	}

	@Override
	public void print(String x) {
		if (stringBuilder != null) {
			stringBuilder.append(x);
		} else {
			stringBuilder = new StringBuilder(x);
		}
	}

	@Override
	public void println(String x) {
		if (stringBuilder == null) {
			log(x);
		} else {
			stringBuilder.append(x);
			log(stringBuilder.toString());
			stringBuilder = null;
		}
	}

	private void log(String log) {
		LOG.debug(log);
	}
}
