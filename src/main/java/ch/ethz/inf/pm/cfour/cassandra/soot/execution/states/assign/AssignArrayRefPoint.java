package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.IntConstant;

/**
 * Transformer for reading xor writing an array element
 */
public class AssignArrayRefPoint extends AbstractAssignProgramPoint {

	private AssignArrayRefPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!assign.containsArrayRef()) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignArrayRefPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignArrayRefPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final ArrayRef arrayRef = assign.getArrayRef();
		// should never be excluded (as the array would be excluded otherwise)
		final VarLocal leftLocal;
		final VarLocalOrImmutableValue rightVal;
		final boolean isRightExcluded;
		if (assign.getLeftOp() instanceof ArrayRef) {
			leftLocal = null;
			if (assign.getRightOp() instanceof Local) {
				final VarLocal rightLocal = SootValUtils.transformLocal((Local) assign.getRightOp(), id.callStack);
				isRightExcluded = rightLocal.varType.equals(VarType.EXCLUDED);
				if (!isRightExcluded && !state.isLocalInitialized(rightLocal)) {
					state.setToBottom();
					return;
				}
				rightVal = rightLocal;
			} else {
				rightVal = SootValUtils.transformImmutableValue((Constant) assign.getRightOp(), id);
				isRightExcluded = false;
			}
		} else {
			leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
			rightVal = null;
			isRightExcluded = false;
		}

		final VarLocal baseLocal = SootValUtils.transformLocal((Local) arrayRef.getBase(), id.callStack);
		if (baseLocal.varType.equals(VarType.EXCLUDED)) {
			if (leftLocal != null && !leftLocal.varType.equals(VarType.EXCLUDED)) {
				state.setUnknown(leftLocal, id);
			} else if (rightVal != null && !isRightExcluded) {
				throw new RuntimeException("non-excluded value is put into excluded array");
			}
			return;
		}
		final Set<ArrayReference> baseRefs = state.ensureNonNullPointsTo(baseLocal, ArrayReference.class);
		if (baseRefs.isEmpty()) {
			state.setToBottom();
			return;
		}
		if (!(arrayRef.getIndex() instanceof IntConstant)) {
			// we do not know which element is accessed --> set array to unknown if an element can escape
			if (leftLocal != null) {
				state.setUnknown(leftLocal, id);
			} else if (!isRightExcluded && rightVal instanceof VarLocal) {
				state.setToUnknown(rightVal, id);
			}
			setArrayElementsToUnknown(baseRefs, state);
			return;
		}
		final int index = ((IntConstant) arrayRef.getIndex()).value;
		if (index < 0) {
			state.setToBottom();
			return;
		}
		final Map<ArrayReference, RefArrayValue> matchingArrayValues = new HashMap<>();

		for (final ArrayReference baseRef : baseRefs) {
			final RefArrayValue arrayVal = state.getArrayValue(baseRef);
			if (arrayVal.isUnknownLength() || index < arrayVal.getLength()) {
				matchingArrayValues.put(baseRef, arrayVal);
			}
		}
		if (matchingArrayValues.size() == 0) {
			state.setToBottom();
			return;
		} else {
			state.updateArrayRefs(baseLocal, matchingArrayValues.keySet());
			if (leftLocal != null) {
				setLocalFromArray(leftLocal, matchingArrayValues, index, state);
			} else {
				setArrayFromRight(rightVal, isRightExcluded, matchingArrayValues, index, state);
			}
		}

	}

	private void setLocalFromArray(final VarLocal leftLocal, final Map<ArrayReference, RefArrayValue> matchingArrayValues, final int index,
			final ExecutionState state) {

		final Set<VarLocalOrImmutableValue> values = new HashSet<>();
		boolean hasUnknown = false;
		for (final Entry<ArrayReference, RefArrayValue> entry : matchingArrayValues.entrySet()) {
			if (entry.getValue().isUnknown()) {
				hasUnknown = true;
				break;
			} else {
				values.addAll(entry.getValue().getElement(index));
			}
		}
		if (hasUnknown) {
			state.setUnknown(leftLocal, id);
			setArrayElementsToUnknown(matchingArrayValues.keySet(), state);
		} else {
			state.setLocalFromPhi(leftLocal, values, id);
		}
	}

	private void setArrayFromRight(final VarLocalOrImmutableValue rightVal, final boolean isRightExcluded,
			final Map<ArrayReference, RefArrayValue> matchingArrayValues, final int index, final ExecutionState state) {
		final boolean mustAlias = matchingArrayValues.size() == 1;
		if (isRightExcluded) {
			setArrayElementsToUnknown(matchingArrayValues.keySet(), state);
			for (final Entry<ArrayReference, RefArrayValue> entry : matchingArrayValues.entrySet()) {
				state.updateArrayValue(entry.getKey(), entry.getValue().toUnknown(), id);
			}
		} else {
			for (final Entry<ArrayReference, RefArrayValue> entry : matchingArrayValues.entrySet()) {
				final RefArrayValue newVal;
				if (entry.getValue().isUnknown()) {
					newVal = entry.getValue();
					// we plug references into array --> if not immutable --> we do not know what happens
					state.setToUnknown(rightVal, id);
				} else {
					final RefArrayValue refArray = (RefArrayValue) entry.getValue();
					final Set<VarLocalOrImmutableValue> values = new HashSet<>();
					if (!mustAlias) {
						values.addAll(refArray.getElement(index));
					}
					values.add(rightVal);
					newVal = refArray.setElement(index, values);
				}
				state.updateArrayValue(entry.getKey(), newVal, id);
			}
		}
	}

	private void setArrayElementsToUnknown(final Set<ArrayReference> baseRefs, final ExecutionState state) {
		for (final ArrayReference baseRef : baseRefs) {
			final RefArrayValue array = state.getArrayValue(baseRef);
			if (!array.isUnknown()) {
				state.setToUnknown(array.getAllElements(), id);
			}
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignArrayRefPoint(id, assign);
	}
}
