package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke;

import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_ASYNC_STMT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_ASYNC_STRING_NO_BINDS;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_ASYNC_STRING_WITH_BINDS;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_STMT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_STRING_NO_BINDS;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_EXEC_STRING_WITH_BINDS;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_PREPARE_STMT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.SESSION_PREPARE_STRING;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ResultSetValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.StringToStatementTransformer;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.Stmt;

public class SessionMethodInvokePoint extends AbstractInvokeProgramPoint {

	private SessionMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static SessionMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
                                                  final int methodId) {
		return new SessionMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == SESSION_EXEC_STRING_NO_BINDS || methodId == SESSION_EXEC_STRING_WITH_BINDS
				|| methodId == SESSION_EXEC_ASYNC_STRING_NO_BINDS || methodId == SESSION_EXEC_ASYNC_STRING_WITH_BINDS) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			final VarLocalOrImmutableValue arg1;
			if (methodId == SESSION_EXEC_STRING_WITH_BINDS || methodId == SESSION_EXEC_ASYNC_STRING_WITH_BINDS) {
				arg1 = args.get(1);
			} else {
				arg1 = null;
			}

			final Set<StatementValue> stmts = this.<StatementValue> resolveConcreteAndTransform(arg0, state,
					stmt -> {
						if (stmt instanceof NullValue) {
							return Collections.emptySet();
						} else if (arg1 == null) {
							return new StringToStatementTransformer(stmt, new ImmutableValue[0]).transform();
						} else {
							return this.<StatementValue> transformToImmutableArray(arg1, state,
									binds -> new StringToStatementTransformer(stmt, binds).transform());
						}
					});
			final boolean async = methodId == SESSION_EXEC_ASYNC_STRING_NO_BINDS || methodId == SESSION_EXEC_ASYNC_STRING_WITH_BINDS;
			if (stmts.isEmpty()) {
				state.setToBottom();
			} else {
				final SessionExecuteInvoke executeInvoke = SessionExecuteInvoke.create(id, async, stmts);
				if (leftLocal != null && !leftLocal.varType.equals(VarType.EXCLUDED)) {
					state.setBaseValue(leftLocal, BaseReference.create(id), s(ResultSetValue.create(executeInvoke)), id, null);
				}
				state.addSessionExecuteInvoke(executeInvoke);
			}
			return true;

		} else if (methodId == SESSION_EXEC_STMT || methodId == SESSION_EXEC_ASYNC_STMT) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			final Set<StatementValue> stmts = this.<StatementValue> resolveConcreteAndTransform(arg0, state, stmt -> {
				if (stmt instanceof StatementValue) {
					return s((StatementValue) stmt);
				} else if (stmt instanceof NullValue) {
					return Collections.emptySet();
				} else {
					return s(StatementValue.createUnknown());
				}
			});
			if (stmts.isEmpty()) {
				state.setToBottom();
			} else {
				final boolean async = methodId == SESSION_EXEC_ASYNC_STMT;
				final SessionExecuteInvoke executeInvoke = SessionExecuteInvoke.create(id, async, stmts);
				if (leftLocal != null && !leftLocal.varType.equals(VarType.EXCLUDED)) {
					state.setBaseValue(leftLocal, BaseReference.create(id), s(ResultSetValue.create(executeInvoke)), id, null);
				}
				state.addSessionExecuteInvoke(executeInvoke);
			}
			return true;

		} else if (methodId == SESSION_PREPARE_STRING) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				state.setBaseValue(leftLocal, BaseReference.create(id),
						resolveConcreteAndTransform(arg0, state, query -> new StringToStatementTransformer(query, null).transform()),
						id, null);
			}
			return true;

		} else if (methodId == SESSION_PREPARE_STMT) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				state.setBaseValue(leftLocal, BaseReference.create(id), resolveConcreteAndTransform(arg0, state, stmt -> s(stmt)), id,
						null);
			}
			return true;
		}

		return false;
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new SessionMethodInvokePoint(id, left, invokeStmt, methodId);
	}
}
