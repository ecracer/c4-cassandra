package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourLegalityTest2 extends AbstractCFourTest {

	public CFourLegalityTest2() throws IOException {
		super("CFourLegalitySpec2");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "login").addNonPrimaryKeyColumn("users", "password")
				.addNonPrimaryKeyColumn("users", "room_names").addTableWithoutDelete("users")
				.addColumnsWritten("users", ImmutableSet.of("login", "password", "room_names")).build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isLegalitySpecEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(7, result.getTotalVerifiedViolationsSize4());
		}
	}
}
