package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.AbstractConcreteValueVisitor;
import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ConcreteValueVisitor;

public class StringBuilderValue implements ConcreteValue {

	private final static StringBuilderValue EMPTY_STRING_BUILDER = new StringBuilderValue(ImmutableList.<ImmutableValue> of());

	public final ImmutableList<ImmutableValue> values;

	private StringBuilderValue(final ImmutableList<ImmutableValue> values) {
		this.values = values;
	}

	public static ConcreteValue create() {
		return EMPTY_STRING_BUILDER;
	}

	public static StringBuilderValue create(final List<ImmutableValue> values) {
		return create(values, 0);
	}

	private static StringBuilderValue create(final List<ImmutableValue> values, final int checkFrom) {
		final ImmutableList.Builder<ImmutableValue> collectedValues = ImmutableList.builder();
		if (checkFrom > 0) {
			collectedValues.addAll(values.subList(0, checkFrom));
		}
		final Iterator<ImmutableValue> valueIt = values.listIterator(checkFrom);
		final List<ImmutableValue> normalizedList = new ArrayList<>();
		while (valueIt.hasNext()) {
			final ImmutableValue next = valueIt.next();
			if (next instanceof StringBuilderValue) {
				normalizedList.addAll(((StringBuilderValue) next).values);
			} else if (next instanceof StringValueContainerValue) {
				final StringValueContainerValue svc = (StringValueContainerValue) next;
				if (svc.value instanceof StringBuilderValue) {
					normalizedList.addAll(((StringBuilderValue) svc.value).values);
				} else {
					normalizedList.add(svc);
				}
			} else {
				normalizedList.add(next);
			}
		}

		StringBuilder builder = new StringBuilder();
		for (ImmutableValue next : normalizedList) {
			if (next instanceof StringValue) {
				builder.append(((StringValue) next).string);
			} else {
				if (builder.length() > 0) {
					collectedValues.add(StringValue.create(builder.toString()));
					builder = new StringBuilder();
				}
				collectedValues.add(next);
			}
		}
		if (builder.length() > 0) {
			collectedValues.add(StringValue.create(builder.toString()));
		}
		return new StringBuilderValue(collectedValues.build());
	}

	public StringBuilderValue addValue(final ImmutableValue value) {
		Objects.requireNonNull(value);
		return create(ImmutableList.<ImmutableValue> builder().addAll(values).add(value).build());
	}

	@Override
	public <R> R apply(ConcreteValueVisitor<R> visitor) {
		return visitor.visitStringBuilderValue(this);
	}

	@Override
	public ConcreteValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (equals(newValue)) {
			return this;
		} else if (newValue instanceof StringBuilderValue) {
			final StringBuilderValue newValueS = (StringBuilderValue) newValue;
			final int countNew = VALUE_COUNTER.visit(newValueS);
			if (countNew < 20 || countNew == VALUE_COUNTER.visit(this)) {
				return newValueS;
			}
		}
		return UnknownMutableValue.create();
	}

	private final static AbstractConcreteValueVisitor<Integer> VALUE_COUNTER = new AbstractConcreteValueVisitor<Integer>() {

		@Override
		public Integer visitDefault(ConcreteValue value) {
			return 1;
		}

		@Override
		public Integer visitStringBuilderValue(StringBuilderValue stringBuilderValue) {
			int res = 0;
			for (final ImmutableValue val : stringBuilderValue.values) {
				res += visit(val);
			}
			return res;
		}

		@Override
		public Integer visitImmutableValue(ImmutableValue immutableValue) {
			if (immutableValue instanceof StringValueContainerValue) {
				return visit(((StringValueContainerValue) immutableValue).value);
			} else if (immutableValue instanceof ImmutableChoiceValue) {
				int res = 0;
				for (final ImmutableValue choice : ((ImmutableChoiceValue) immutableValue).choices) {
					res += visit(choice);
				}
				return res;
			} else {
				return 1;
			}
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringBuilderValue other = (StringBuilderValue) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StringBuilderValue [values=" + values + "]";
	}
}
