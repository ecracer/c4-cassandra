package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.util.Collection;
import java.util.function.Function;

import com.google.common.collect.ImmutableMap;
import com.google.common.graph.ImmutableValueGraph;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;

public class IdTransactionGraph extends AbstractTransactionGraph<ProgramPointId, IdTransactionGraph, IdTransactionGraph.Builder> {

	protected IdTransactionGraph(
			ImmutableValueGraph<ProgramPointId, EdgeConstraint<ProgramPointId>> graph,
			ImmutableMap<ProgramPointId, EdgeConstraint<ProgramPointId>> sourceNodes,
			ImmutableMap<ProgramPointId, EdgeConstraint<ProgramPointId>> sinkNodes,
			boolean bypassPossible) {
		super(graph, sourceNodes, sinkNodes, bypassPossible);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static IdTransactionGraph createEmpty() {
		return builder().build();
	}

	@Override
	public Builder toBuilder() {
		return new Builder(this);
	}

	public static class Builder extends AbstractTransactionGraph.AbstractBuilder<ProgramPointId, IdTransactionGraph> {

		public Builder() {
			super();
		}

		public Builder(final IdTransactionGraph from) {
			super(from);
		}

		public <oldT> Builder(final AbstractTransactionGraph<oldT, ?, ?> from,
				final Function<oldT, Collection<ProgramPointId>> transformer) {
			super(from, transformer);
		}

		@Override
		public IdTransactionGraph build() {
			return new IdTransactionGraph(ImmutableValueGraph.copyOf(graph), ImmutableMap.copyOf(sourceNodes),
					ImmutableMap.copyOf(sinkNodes), bypassPossible);
		}
	}
}
