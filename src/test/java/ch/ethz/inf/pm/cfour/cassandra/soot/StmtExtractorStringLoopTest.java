package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;

public class StmtExtractorStringLoopTest extends AbstractSootTest {

	public StmtExtractorStringLoopTest() throws IOException {
		super("StringLoop", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final SessionExecuteInvoke nextCall = result.iterator().next();
		assertEquals(2, nextCall.statementArg.size());

		boolean hasBindedInList = false;
		boolean hasEmptyInList = false;

		for (final StatementValue stmt : nextCall.statementArg) {
			if ("SELECT * FROM test_table WHERE j IN (?)".equals(stmt.getQueryWithUnnamedBindMarkers())) {
				hasBindedInList = true;
				assertEquals(1, stmt.binds.size());
				final AbstractBind<ConcreteValue> nextBind = stmt.binds.get(0);
				assertTrue(nextBind.value instanceof UnknownMutableValue);
			} else if ("SELECT * FROM test_table WHERE j IN ()".equals(stmt.getQueryWithUnnamedBindMarkers())) {
				hasEmptyInList = true;
			} else {
				assertTrue(false);
			}
		}
		assertTrue(hasBindedInList);
		assertTrue(hasEmptyInList);
	}
}