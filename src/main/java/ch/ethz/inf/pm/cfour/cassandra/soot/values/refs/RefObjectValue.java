package ch.ethz.inf.pm.cfour.cassandra.soot.values.refs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.RefValueVisitor;
import ch.ethz.inf.pm.cfour.cassandra.util.Utils;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.FieldDesc;

public class RefObjectValue implements RefValue {

	private final String className;
	private final boolean isUnknown;
	private final ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> fields;
	private final ImmutableSet<FieldDesc> includedFields;
	private final ImmutableSet<FieldDesc> unknownFields;

	private RefObjectValue(final String className, final boolean isUnknown,
			final ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> fields, final ImmutableSet<FieldDesc> includedFields,
			final ImmutableSet<FieldDesc> unknownFields) {
		this.className = Objects.requireNonNull(className);
		this.isUnknown = isUnknown;
		this.fields = Objects.requireNonNull(fields);
		this.includedFields = Objects.requireNonNull(includedFields);
		this.unknownFields = Objects.requireNonNull(unknownFields);
	}

	public static RefObjectValue create(final String className, final Set<FieldDesc> includedFields) {
		return new RefObjectValue(className, false, ImmutableSetMultimap.of(), ImmutableSet.copyOf(includedFields), ImmutableSet.of());
	}

	public static RefObjectValue createUnknown(final String className, final Set<FieldDesc> includedFields) {
		return new RefObjectValue(className, true, ImmutableSetMultimap.of(), ImmutableSet.copyOf(includedFields), ImmutableSet.of());
	}

	@Override
	public <R> R apply(RefValueVisitor<R> visitor) {
		return visitor.visitRefObjectValue(this);
	}

	public String getClassName() {
		return className;
	}

	public boolean isUnknownObject() {
		return isUnknown;
	}

	public boolean isUnknown(final FieldDesc field) {
		return (isUnknown && (!includedFields.contains(field) || !fields.containsKey(field)))
				|| (!isUnknown && unknownFields.contains(field));
	}

	public boolean isUpdateable(final FieldDesc field) {
		return !isUnknown || includedFields.contains(field);
	}

	public ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> getFields() {
		return fields;
	}

	public boolean isFieldSet(final FieldDesc field) {
		if (isUnknown(field)) {
			throw new IllegalStateException();
		}
		return fields.containsKey(field);
	}

	public ImmutableSet<VarLocalOrImmutableValue> getField(final FieldDesc field) {
		if (!isFieldSet(field)) {
			throw new IllegalArgumentException();
		}
		return fields.get(field);
	}

	public RefObjectValue setField(final FieldDesc field, final Set<VarLocalOrImmutableValue> values) {
		if (!isUpdateable(field)) {
			throw new IllegalStateException();
		}
		if (isUnknown) {
			// we do not have any aliasing information for unknown objects
			// therefore, we only add values, which means that during the interprocedural analysis, effectivly
			// no fields can be set
			final Set<VarLocalOrImmutableValue> newValues = new HashSet<>(fields.get(field));
			newValues.addAll(values);
			return new RefObjectValue(className, isUnknown, Utils.multimapWithValues(fields, field, newValues), includedFields,
					Utils.setWithoutValue(unknownFields, field));
		} else {
			return new RefObjectValue(className, isUnknown, Utils.multimapWithValues(fields, field, values), includedFields,
					Utils.setWithoutValue(unknownFields, field));
		}
	}

	public RefObjectValue setFieldToUnknown(final FieldDesc field) {
		if (!isUpdateable(field)) {
			throw new IllegalStateException();
		}
		return new RefObjectValue(className, isUnknown, Utils.multimapWithoutKey(fields, field), includedFields,
				Utils.setWithElement(unknownFields, field));
	}

	public ImmutableSet<VarLocalOrImmutableValue> getAllFieldValues() {
		return ImmutableSet.copyOf(fields.values());
	}

	@Override
	public Set<VarLocalOrImmutableValue> getUsedVarLocalOrImmutableValues() {
		return getAllFieldValues();
	}

	@Override
	public RefObjectValue transformVarLocalOrImmutableValues(Function<VarLocalOrImmutableValue, VarLocalOrImmutableValue> transformer) {
		final ImmutableSetMultimap.Builder<FieldDesc, VarLocalOrImmutableValue> fieldsBuilder = ImmutableSetMultimap.builder();
		for (final Entry<FieldDesc, VarLocalOrImmutableValue> field : fields.entries()) {
			fieldsBuilder.put(field.getKey(), transformer.apply(field.getValue()));
		}
		return new RefObjectValue(className, isUnknown, fieldsBuilder.build(), includedFields, unknownFields);
	}

	public MergeResult mergeWith(final RefObjectValue other) {
		if (!className.equals(other.className) || !includedFields.equals(other.includedFields)) {
			throw new IllegalArgumentException("Cannot merge different objects");
		}

		boolean isUnknown = this.isUnknown || other.isUnknown;
		final Set<FieldDesc> unknownFields = new HashSet<>(this.unknownFields);
		unknownFields.addAll(other.unknownFields);
		if (isUnknown) {
			final Iterator<FieldDesc> it = unknownFields.iterator();
			while (it.hasNext()) {
				if (!includedFields.contains(it.next())) {
					it.remove();
				}
			}
		}

		final ImmutableSet.Builder<VarLocalOrImmutableValue> setToUnknownBuilder = ImmutableSet.builder();

		final ImmutableSetMultimap.Builder<FieldDesc, VarLocalOrImmutableValue> newFields = ImmutableSetMultimap.builder();
		for (final FieldDesc fieldKey : fields.keySet()) {
			if (!unknownFields.contains(fieldKey) && (!isUnknown || includedFields.contains(fieldKey))) {
				newFields.putAll(fieldKey, fields.get(fieldKey));
				if (!other.fields.containsKey(fieldKey)) {
					newFields.put(fieldKey, fieldKey.defaultValue);
				}
			} else {
				setToUnknownBuilder.addAll(fields.get(fieldKey));
			}
		}
		for (final FieldDesc fieldKey : other.fields.keySet()) {
			if (!unknownFields.contains(fieldKey) && (!isUnknown || includedFields.contains(fieldKey))) {
				newFields.putAll(fieldKey, other.fields.get(fieldKey));
				if (!fields.containsKey(fieldKey)) {
					newFields.put(fieldKey, fieldKey.defaultValue);
				}
			} else {
				setToUnknownBuilder.addAll(other.fields.get(fieldKey));
			}
		}
		return new MergeResult(setToUnknownBuilder.build(),
				new RefObjectValue(className, isUnknown, newFields.build(), includedFields, ImmutableSet.copyOf(unknownFields)));
	}

	public static class MergeResult {
		public final ImmutableSet<VarLocalOrImmutableValue> setToUnknown;
		public final RefObjectValue result;

		public MergeResult(final ImmutableSet<VarLocalOrImmutableValue> setToUnknown, final RefObjectValue result) {
			this.setToUnknown = Objects.requireNonNull(setToUnknown);
			this.result = Objects.requireNonNull(result);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		result = prime * result + ((includedFields == null) ? 0 : includedFields.hashCode());
		result = prime * result + (isUnknown ? 1231 : 1237);
		result = prime * result + ((unknownFields == null) ? 0 : unknownFields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefObjectValue other = (RefObjectValue) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		if (includedFields == null) {
			if (other.includedFields != null)
				return false;
		} else if (!includedFields.equals(other.includedFields))
			return false;
		if (isUnknown != other.isUnknown)
			return false;
		if (unknownFields == null) {
			if (other.unknownFields != null)
				return false;
		} else if (!unknownFields.equals(other.unknownFields))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RefObjectValue [className=" + className + ", isUnknown=" + isUnknown + ", fields=" + fields + ", includedFields="
				+ includedFields + ", unknownFields=" + unknownFields + "]";
	}
}
