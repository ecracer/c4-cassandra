package ch.ethz.inf.pm.cfour.cassandra.soot.reference;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.visitor.ReferenceVisitor;

public class ImmutableReference extends AbstractReference {

	private ImmutableReference(final ProgramPointId createdIn) {
		super(createdIn);
	}

	public static ImmutableReference create(final ProgramPointId createdIn) {
		return new ImmutableReference(createdIn);
	}

	@Override
	public <R> R apply(ReferenceVisitor<R> visitor) {
		return visitor.visitImmutableReference(this);
	}
}