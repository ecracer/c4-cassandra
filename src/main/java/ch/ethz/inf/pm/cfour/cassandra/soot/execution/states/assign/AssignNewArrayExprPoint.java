package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.AssignStmt;
import soot.jimple.IntConstant;
import soot.jimple.NewArrayExpr;

/**
 * Transformer for the assignment of a new array
 */
public class AssignNewArrayExprPoint extends AbstractAssignProgramPoint {

	private AssignNewArrayExprPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!(assign.getLeftOp() instanceof Local) || !(assign.getRightOp() instanceof NewArrayExpr)) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignNewArrayExprPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignNewArrayExprPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
		if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
			// NewArrayExpr
			final NewArrayExpr newArray = (NewArrayExpr) assign.getRightOp();
			final ArrayReference newRef = ArrayReference.create(id);
			final Integer size;
			if (newArray.getSize() instanceof IntConstant) {
				size = ((IntConstant) newArray.getSize()).value;
			} else {
				size = null;
			}
			if (size != null && size < 0) {
				state.setToBottom();
			} else {
				final ImmutableValue defaultElement = SootValUtils.getDefaultValue(newArray.getBaseType());
				final RefArrayValue value;
				if (size == null) {
					value = RefArrayValue.createWithUnknownLength(defaultElement);
				} else {
					value = RefArrayValue.create(size, defaultElement);
				}
				state.setArrayValue(leftLocal, newRef, value, id, oldState);
			}
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignNewArrayExprPoint(id, assign);
	}
}
