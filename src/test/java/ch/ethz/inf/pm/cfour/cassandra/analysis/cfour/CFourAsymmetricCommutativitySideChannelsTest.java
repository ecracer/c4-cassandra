package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourAsymmetricCommutativitySideChannelsTest extends AbstractCFourTest {

	public CFourAsymmetricCommutativitySideChannelsTest() throws IOException {
		super("CFourAsymCommuSideChannels");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("tracks", "id")
				.addNonPrimaryKeyColumn("tracks", "artist").addNonPrimaryKeyColumn("tracks", "track_name").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setAsymmetricCommutativityEnabled(true);
		options.setSideChannelsEnabled(false);
		optionsList.add(options);
		options = getInitialOptions();
		options.setAsymmetricCommutativityEnabled(false);
		options.setSideChannelsEnabled(false);
		optionsList.add(options);
		options = getInitialOptions();
		options.setAsymmetricCommutativityEnabled(true);
		options.setSideChannelsEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setAsymmetricCommutativityEnabled(false);
		options.setSideChannelsEnabled(true);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isAsymmetricCommutativityEnabled() && !options.isSideChannelsEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(1, result.getTotalVerifiedViolationsSize4());
		}
	}
}
