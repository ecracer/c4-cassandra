

import java.util.UUID;

import com.datastax.driver.core.Session;

public class FieldUUID {
	
	private UUID id = UUID.randomUUID();
	
	public void nextId(){
		id = UUID.randomUUID();
	}
	
	public void test(Session session) {
		session.execute("SELECT val FROM values WHERE id = ?", id);
	}
}