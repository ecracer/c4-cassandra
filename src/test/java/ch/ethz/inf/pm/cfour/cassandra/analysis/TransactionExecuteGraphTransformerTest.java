package ch.ethz.inf.pm.cfour.cassandra.analysis;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import ch.ethz.inf.pm.cfour.cassandra.beans.Method;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedUpdateStatement;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;

public class TransactionExecuteGraphTransformerTest {

	private void check(final StatementPart expected, final String stmt, final boolean treatUpdateAsInsert) {
		final Method tempMethod = Method.create("clz", "m", Collections.emptyList(), "void", "clz.m");
		final TransactionDescriptor tempDesc = TransactionDescriptor.create(tempMethod, false);

		final StatementValue statement = StatementValue.create(stmt, Collections.emptyList());
		final SessionExecuteInvoke firstInvoke = SessionExecuteInvoke
				.create(ProgramPointId.create(CallStack.create(tempMethod), new Object()), false, Collections.singleton(statement));

		final MutableGraph<SessionExecuteInvoke> graph = GraphBuilder.directed().allowsSelfLoops(true).build();
		graph.addNode(firstInvoke);
		final UnparsedTransactionGraph tgraph = UnparsedTransactionGraph.builder().addNode(firstInvoke)
				.addSourceNode(firstInvoke, AbstractTransactionGraph.EdgeConstraint.createEmpty()).addSinkNode(firstInvoke, AbstractTransactionGraph.EdgeConstraint.createEmpty())
				.setBypassPossible(false).build();

		final Options options = new Options();
		options.setTreatAllUpdatesAsStrict(!treatUpdateAsInsert);
		final Map<TransactionDescriptor, TransactionGraph> ret = new TransactionGraphTransformer(options)
				.transform(Collections.singletonMap(tempDesc, tgraph));

		final ParsedStatement expectedParsed;
		if (expected instanceof StatementPart.UpdatePart) {
			expectedParsed = ParsedUpdateStatement.create(statement.getQueryWithUnnamedBindMarkers(), (StatementPart.UpdatePart) expected);
		} else {
			expectedParsed = ParsedQueryStatement.create(statement.getQueryWithUnnamedBindMarkers(), (StatementPart.QueryPart) expected);
		}
		assertEquals(1, ret.get(tempDesc).nodes().size());
		assertEquals(expectedParsed, ret.get(tempDesc).nodes().iterator().next().statement);
	}

	@Test
	public void testInsertParsing() {
		check(StatementPart.UpsertPart.createInsert("table1", ImmutableList.of(ColumnValueUpdate.create("key", IntValue.create(3))), false),
				"INSERT INTO table1 (key) VALUES (3)", false);
	}

	@Test
	public void testUpdateParsing() {
		final List<Constraint> key2Constraint = Collections.singletonList(EQValConstraint.create("key", IntValue.create(2)));
		check(StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnValueUpdate.create("col1", IntValue.create(3))),
				key2Constraint, false, false), "UPDATE table1 SET COL1 = 3 WHERE key = 2", true);
		check(StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnValueUpdate.create("col1", IntValue.create(3))),
				key2Constraint, false, false).setCanInsert(false), "UPDATE table1 SET col1 = 3 WHERE key = 2", false);
		check(StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnValueUpdate.create("col1", IntValue.create(3))),
				key2Constraint, false, false).setCanInsert(false), "/*!STRICT!*/ UPDATE table1 SET col1 = 3 WHERE key = 2", true);
		check(StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnValueUpdate.create("col1", IntValue.create(3))),
				key2Constraint, false, false).setCanInsert(false), "/*!STRICT!*/ UPDATE table1 SET col1 = 3 WHERE key = 2", false);
	}

	@Test
	public void testSelectParsing() {
		final Constraints key2Constraint = Constraints.create(EQValConstraint.create("key", IntValue.create(2)));
		check(StatementPart.SelectColsPart.create("table1", ImmutableList.of(ColumnSelection.create("col1")), key2Constraint),
				"SELECT col1 FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectAllPart.create("table1", key2Constraint).setAllDisplayColumns(),
				"SELECT /*!DISPLAY * !*/ * FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectAllPart.create("table1", key2Constraint).setAllDisplayColumns(),
				"SELECT /*!DISPLAY col1, col2 !*/ * /*!DISPLAY * !*/ FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectAllPart.create("table1", key2Constraint).setDisplayColumns(ImmutableSet.of("col1")),
				"SELECT /*!DISPLAY col1 !*/ * FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectAllPart.create("table1", key2Constraint).setDisplayColumns(ImmutableSet.of("col1", "col2")),
				"SELECT /*!DISPLAY col1 col2!*/ * FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectAllPart.create("table1", key2Constraint).setDisplayColumns(ImmutableSet.of("col1", "col2")),
				"SELECT /*!DISPLAY COL1, col2 !*/ * FROM table1 WHERE key = 2", true);
		check(StatementPart.SelectColsPart
				.create("table1", ImmutableList.of(ColumnSelection.create("col1"), ColumnSelection.create("col2")), key2Constraint)
				.setDisplayColumns(ImmutableSet.of("col1")),
				"SELECT /*!DISPLAY COL1 !*/ col1 AS c1, COL2 AS c2 FROM table1 WHERE key = 2", true);
	}
}
