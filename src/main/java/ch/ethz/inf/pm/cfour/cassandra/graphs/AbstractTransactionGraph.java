package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.graph.EndpointPair;
import com.google.common.graph.Graphs;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

/**
 * Graphical generic representation of a graph with nodes of type T and edges of type EdgeConstraint&lt;T&gt;
 * 
 * @param <T>
 * @param <IMPL>
 * @param <BUILDER>
 */
public abstract class AbstractTransactionGraph<T, IMPL extends AbstractTransactionGraph<T, IMPL, BUILDER>, BUILDER extends AbstractTransactionGraph.AbstractBuilder<T, IMPL>> {

	protected final ImmutableValueGraph<T, EdgeConstraint<T>> graph;
	protected final ImmutableMap<T, EdgeConstraint<T>> sourceNodes;
	protected final ImmutableMap<T, EdgeConstraint<T>> sinkNodes;
	protected final boolean bypassPossible;

	protected AbstractTransactionGraph(final ImmutableValueGraph<T, EdgeConstraint<T>> graph,
			final ImmutableMap<T, EdgeConstraint<T>> sourceNodes, final ImmutableMap<T, EdgeConstraint<T>> sinkNodes,
			final boolean bypassPossible) {
		this.graph = Objects.requireNonNull(graph);
		this.sourceNodes = Objects.requireNonNull(sourceNodes);
		this.sinkNodes = Objects.requireNonNull(sinkNodes);
		this.bypassPossible = bypassPossible;
	}

	public ImmutableSet<T> sourceNodes() {
		return sourceNodes.keySet();
	}

	public EdgeConstraint<T> sourceConstraint(final T source) {
		return sourceNodes.get(source);
	}

	public ImmutableSet<T> sinkNodes() {
		return sinkNodes.keySet();
	}

	public EdgeConstraint<T> sinkConstraint(final T sink) {
		return sinkNodes.get(sink);
	}

	public boolean isBypassPossible() {
		return bypassPossible;
	}

	public Set<T> nodes() {
		return graph.nodes();
	}

	public Set<EndpointPair<T>> edges() {
		return graph.edges();
	}

	public EdgeConstraint<T> edgeConstraint(T nodeU, T nodeV) {
		return graph.edgeValue(nodeU, nodeV);
	}

	public Set<T> predecessors(T node) {
		return graph.predecessors(node);
	}

	public Set<T> successors(T node) {
		return graph.successors(node);
	}

	public abstract BUILDER toBuilder();

	public IMPL mergeWith(final IMPL other) {
		if (this == other) {
			return other;
		}
		final BUILDER builder = toBuilder();
		for (final T node : other.graph.nodes()) {
			builder.addNode(node);
		}
		for (final EndpointPair<T> edge : other.graph.edges()) {
			builder.addEdgeConstraint(edge.nodeU(), edge.nodeV(), other.graph.edgeValue(edge.nodeU(), edge.nodeV()));
		}
		for (final Entry<T, EdgeConstraint<T>> sourceNode : other.sourceNodes.entrySet()) {
			builder.addSourceNode(sourceNode.getKey(), sourceNode.getValue());
		}
		for (final Entry<T, EdgeConstraint<T>> sinkNode : other.sinkNodes.entrySet()) {
			builder.addSinkNode(sinkNode.getKey(), sinkNode.getValue());
		}
		if (other.bypassPossible) {
			builder.setBypassPossible(true);
		}
		return builder.build();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bypassPossible ? 1231 : 1237);
		// do not include as it is only based on reference equality
		//result = prime * result + ((graph == null) ? 0 : graph.hashCode());
		result = prime * result + ((sinkNodes == null) ? 0 : sinkNodes.hashCode());
		result = prime * result + ((sourceNodes == null) ? 0 : sourceNodes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractTransactionGraph<?, ?, ?> other = (AbstractTransactionGraph<?, ?, ?>) obj;
		if (bypassPossible != other.bypassPossible)
			return false;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!Graphs.equivalent(graph, other.graph))
			// use graph equivalent
			return false;
		if (sinkNodes == null) {
			if (other.sinkNodes != null)
				return false;
		} else if (!sinkNodes.equals(other.sinkNodes))
			return false;
		if (sourceNodes == null) {
			if (other.sourceNodes != null)
				return false;
		} else if (!sourceNodes.equals(other.sourceNodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractTransactionGraph [graph=" + graph + ", sourceNodes=" + sourceNodes + ", sinkNodes=" + sinkNodes
				+ ", bypassPossible=" + bypassPossible + "]";
	}

	public abstract static class AbstractBuilder<T, TYPE extends AbstractTransactionGraph<T, TYPE, ? extends AbstractBuilder<T, TYPE>>> {

		protected final MutableValueGraph<T, EdgeConstraint<T>> graph;
		protected final Map<T, EdgeConstraint<T>> sourceNodes;
		protected final Map<T, EdgeConstraint<T>> sinkNodes;
		protected boolean bypassPossible;

		public AbstractBuilder() {
			this.graph = ValueGraphBuilder.directed().allowsSelfLoops(true).build();
			this.sourceNodes = new HashMap<>();
			this.sinkNodes = new HashMap<>();
			this.bypassPossible = true;
		}

		public AbstractBuilder(final TYPE from) {
			this();
			initWithGraph(from, node -> Collections.singleton(node));
		}

		public <oldT> AbstractBuilder(final AbstractTransactionGraph<oldT, ?, ?> from, final Function<oldT, Collection<T>> transformer) {
			this();
			initWithGraph(from, transformer);
		}

		protected <oldT> void initWithGraph(final AbstractTransactionGraph<oldT, ?, ?> graph,
				final Function<oldT, Collection<T>> transformer) {
			for (final oldT oldNode : graph.graph.nodes()) {
				for (final T newNode : transformer.apply(oldNode)) {
					this.graph.addNode(newNode);
				}
			}
			for (final EndpointPair<oldT> edge : graph.graph.edges()) {
				final EdgeConstraint<T> newConstraint = graph.graph.edgeValue(edge.nodeU(), edge.nodeV()).transform(transformer);
				for (final T newNodeU : transformer.apply(edge.nodeU())) {
					for (final T newNodeV : transformer.apply(edge.nodeV())) {
						this.graph.putEdgeValue(newNodeU, newNodeV, newConstraint);
					}
				}
			}
			for (final Entry<oldT, EdgeConstraint<oldT>> oldSource : graph.sourceNodes.entrySet()) {
				final EdgeConstraint<T> newEdgeConstraint = oldSource.getValue().transform(transformer);
				for (final T newSourceNode : transformer.apply(oldSource.getKey())) {
					this.sourceNodes.put(newSourceNode, newEdgeConstraint);
				}
			}
			for (final Entry<oldT, EdgeConstraint<oldT>> oldSink : graph.sinkNodes.entrySet()) {
				final EdgeConstraint<T> newEdgeConstraint = oldSink.getValue().transform(transformer);
				for (final T newSinkNode : transformer.apply(oldSink.getKey())) {
					this.sinkNodes.put(newSinkNode, newEdgeConstraint);
				}
			}
			this.bypassPossible = graph.bypassPossible;
		}

		public abstract TYPE build();

		public AbstractBuilder<T, TYPE> addNode(final T node) {
			this.graph.addNode(node);
			return this;
		}

		public AbstractBuilder<T, TYPE> putNode(final T node) {
			// remove all edges
			this.graph.removeNode(node);
			this.graph.addNode(node);
			return this;
		}

		public AbstractBuilder<T, TYPE> addEdgeConstraint(final T nodeU, final T nodeV, final EdgeConstraint<T> edgeConstraint) {
			this.graph.putEdgeValue(nodeU, nodeV, edgeConstraint.mergeWith(this.graph.edgeValueOrDefault(nodeU, nodeV, edgeConstraint)));
			return this;
		}

		public AbstractBuilder<T, TYPE> putEdgeConstraint(final T nodeU, final T nodeV, final EdgeConstraint<T> edgeConstraint) {
			this.graph.putEdgeValue(nodeU, nodeV, edgeConstraint);
			return this;
		}

		public AbstractBuilder<T, TYPE> addSourceNode(final T source, final EdgeConstraint<T> sourceConstraint) {
			this.graph.addNode(source);
			if (sourceNodes.containsKey(source)) {
				sourceNodes.put(source, sourceConstraint.mergeWith(sourceNodes.get(source)));
			} else {
				sourceNodes.put(source, Objects.requireNonNull(sourceConstraint));
			}
			return this;
		}

		public AbstractBuilder<T, TYPE> putSourceNode(final T source, final EdgeConstraint<T> sourceConstraint) {
			this.graph.addNode(source);
			this.sourceNodes.put(source, Objects.requireNonNull(sourceConstraint));
			return this;
		}

		public AbstractBuilder<T, TYPE> addSinkNode(final T sink, final EdgeConstraint<T> sinkConstraint) {
			this.graph.addNode(sink);
			if (sinkNodes.containsKey(sink)) {
				sinkNodes.put(sink, sinkConstraint.mergeWith(sinkNodes.get(sink)));
			} else {
				sinkNodes.put(sink, Objects.requireNonNull(sinkConstraint));
			}
			return this;
		}

		public AbstractBuilder<T, TYPE> putSinkNode(final T sink, final EdgeConstraint<T> sinkConstraint) {
			this.graph.addNode(sink);
			this.sinkNodes.put(sink, Objects.requireNonNull(sinkConstraint));
			return this;
		}

		public AbstractBuilder<T, TYPE> setBypassPossible(final boolean bypassPossible) {
			this.bypassPossible = bypassPossible;
			return this;
		}
	}

	public static class EdgeConstraint<T> {

		@SuppressWarnings("rawtypes")
		private static final EdgeConstraint EMPTY_CONSTRAINT = new EdgeConstraint<>(ImmutableSet.of(), ImmutableSet.of());

		private final ImmutableSet<T> emptyResultNodes;
		private final ImmutableSet<T> nonEmptyResultNodes;

		private EdgeConstraint(final ImmutableSet<T> emptyResultNodes, final ImmutableSet<T> nonEmptyResultNodes) {
			if (!Collections.disjoint(Objects.requireNonNull(emptyResultNodes), Objects.requireNonNull(nonEmptyResultNodes))) {
				throw new IllegalArgumentException();
			}
			this.emptyResultNodes = emptyResultNodes;
			this.nonEmptyResultNodes = nonEmptyResultNodes;
		}

		@SuppressWarnings("unchecked")
		public static <T> EdgeConstraint<T> createEmpty() {
			return (EdgeConstraint<T>) EMPTY_CONSTRAINT;
		}

		public static <T> Builder<T> builder() {
			return new Builder<>();
		}

		public ImmutableSet<T> emptyResultNodes() {
			return emptyResultNodes;
		}

		public ImmutableSet<T> nonEmptyResultNodes() {
			return nonEmptyResultNodes;
		}

		public Builder<T> toBuilder() {
			return new Builder<T>().addEmptyResultNodes(emptyResultNodes).addNonEmptyResultNodes(nonEmptyResultNodes);
		}

		public EdgeConstraint<T> mergeWith(final EdgeConstraint<T> other) {
			if (this == other) {
				return this;
			}
			final Builder<T> builder = new Builder<>();
			for (final T emptyResultNode : emptyResultNodes) {
				if (other.emptyResultNodes.contains(emptyResultNode)) {
					builder.addEmptyResultNode(emptyResultNode);
				}
			}
			for (final T nonEmptyResultNode : nonEmptyResultNodes) {
				if (other.nonEmptyResultNodes.contains(nonEmptyResultNode)) {
					builder.addNonEmptyResultNode(nonEmptyResultNode);
				}
			}
			return builder.build();
		}

		public <newT> EdgeConstraint<newT> transform(final Function<T, Collection<newT>> transformer) {
			final Builder<newT> builder = new Builder<>();
			emptyResultNodes.forEach(n -> builder.addEmptyResultNodes(transformer.apply(n)));
			nonEmptyResultNodes.forEach(n -> builder.addNonEmptyResultNodes(transformer.apply(n)));
			return builder.build();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((emptyResultNodes == null) ? 0 : emptyResultNodes.hashCode());
			result = prime * result + ((nonEmptyResultNodes == null) ? 0 : nonEmptyResultNodes.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EdgeConstraint<?> other = (EdgeConstraint<?>) obj;
			if (emptyResultNodes == null) {
				if (other.emptyResultNodes != null)
					return false;
			} else if (!emptyResultNodes.equals(other.emptyResultNodes))
				return false;
			if (nonEmptyResultNodes == null) {
				if (other.nonEmptyResultNodes != null)
					return false;
			} else if (!nonEmptyResultNodes.equals(other.nonEmptyResultNodes))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "EdgeConstraint [emptyResultNodes=" + emptyResultNodes + ", nonEmptyResultNodes=" + nonEmptyResultNodes + "]";
		}

		public static class Builder<T> {

			private final ImmutableSet.Builder<T> emptyResultNodes = ImmutableSet.builder();
			private final ImmutableSet.Builder<T> nonEmptyResultNodes = ImmutableSet.builder();

			private Builder() {
			}

			public Builder<T> addEmptyResultNode(final T node) {
				emptyResultNodes.add(node);
				return this;
			}

			public Builder<T> addEmptyResultNodes(final Collection<T> nodes) {
				emptyResultNodes.addAll(nodes);
				return this;
			}

			public Builder<T> addNonEmptyResultNode(final T node) {
				nonEmptyResultNodes.add(node);
				return this;
			}

			public Builder<T> addNonEmptyResultNodes(final Collection<T> nodes) {
				nonEmptyResultNodes.addAll(nodes);
				return this;
			}

			public EdgeConstraint<T> build() {
				return new EdgeConstraint<T>(emptyResultNodes.build(), nonEmptyResultNodes.build());
			}
		}
	}
}
