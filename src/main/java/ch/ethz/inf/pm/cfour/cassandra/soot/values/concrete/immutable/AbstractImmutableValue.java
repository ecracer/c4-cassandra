package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ConcreteValueVisitor;

public abstract class AbstractImmutableValue implements ImmutableValue {

	@Override
	public <R> R apply(ConcreteValueVisitor<R> visitor) {
		return visitor.visitImmutableValue(this);
	}

	@Override
	public ImmutableValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (equals(newValue)) {
			return this;
		} else {
			return UnknownImmutableValue.create(updatedFrom);
		}
	}
}
