package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import soot.jimple.AssignStmt;

/**
 * Abstract transformer for an assign statement
 */
public abstract class AbstractAssignProgramPoint extends AbstractProgramPoint {

	protected AssignStmt assign;

	protected AbstractAssignProgramPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id);
		this.assign = Objects.requireNonNull(assign);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assign == null) ? 0 : assign.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAssignProgramPoint other = (AbstractAssignProgramPoint) obj;
		if (assign == null) {
			if (other.assign != null)
				return false;
		} else if (!assign.equals(other.assign))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractAssignProgramPoint [" + assign + "]";
	}
}
