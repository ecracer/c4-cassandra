package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import ch.ethz.inf.pm.cfour.ClientLocalVar;
import ch.ethz.inf.pm.cfour.GlobalVar;

public class VariableProvider {

	private int nextId = 0;

	private VariableProvider() {
	}

	public static VariableProvider create() {
		return new VariableProvider();
	}

	public GlobalVar newIntGlobalVar() {
		return newIntGlobalVar("u");
	}

	public GlobalVar newIntGlobalVar(final String prefix) {
		return CFourFactory.intGlobalVar(prefix + "_g_int_" + nextId++);
	}

	public GlobalVar newBoolGlobalVar() {
		return newBoolGlobalVar("u");
	}

	public GlobalVar newBoolGlobalVar(final String prefix) {
		return CFourFactory.boolGlobalVar(prefix + "_g_bool_" + nextId++);
	}

	public ClientLocalVar newIntClientLocalVar() {
		return newIntClientLocalVar("u");
	}

	public ClientLocalVar newIntClientLocalVar(final String prefix) {
		return CFourFactory.intClientLocalVar(prefix + "_c_int_" + nextId++);
	}

	public ClientLocalVar newBoolClientLocalVar() {
		return newBoolClientLocalVar("u");
	}

	public ClientLocalVar newBoolClientLocalVar(final String prefix) {
		return CFourFactory.boolClientLocalVar(prefix + "_c_bool_" + nextId++);
	}
}
