package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.BindMarkerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.EmptyResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StatementSingleResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

public abstract class AbstractImmutableValueVisitor<R> implements ImmutableValueVisitor<R> {

	@Override
	public R visit(final ImmutableValue value) {
		return value.apply(this);
	}

	public List<R> visit(final List<? extends ImmutableValue> values) {
		final List<R> result = new ArrayList<>();
		values.forEach(val -> result.add(visit(val)));
		return result;
	}

	public R visitDefault(final ImmutableValue value) {
		return null;
	}

	@Override
	public R visitBindMarkerValue(BindMarkerValue bindMarkerValue) {
		return visitDefault(bindMarkerValue);
	}

	@Override
	public R visitConsistencyLevelValue(ConsistencyLevelValue concistencyLevelValue) {
		return visitDefault(concistencyLevelValue);
	}

	@Override
	public R visitImmutableChoiceValue(ImmutableChoiceValue immutableChoiceValue) {
		return visitDefault(immutableChoiceValue);
	}

	@Override
	public R visitIntValue(IntValue intValue) {
		return visitDefault(intValue);
	}

	@Override
	public R visitEmptyResultValue(EmptyResultValue booleanValue) {
		return visitDefault(booleanValue);
	}

	@Override
	public R visitNullValue(NullValue nullValue) {
		return visitDefault(nullValue);
	}

	@Override
	public R visitStringValue(StringValue stringValue) {
		return visitDefault(stringValue);
	}

	@Override
	public R visitStringValueContainer(StringValueContainerValue stringValueContainerValue) {
		return visitDefault(stringValueContainerValue);
	}

	@Override
	public R visitUnknownImmutableValue(UnknownImmutableValue unknownImmutableValue) {
		return visitDefault(unknownImmutableValue);
	}

	@Override
	public R visitUUIDValue(UUIDValue uuidValue) {
		return visitDefault(uuidValue);
	}

	@Override
	public R visitClientImmutableValue(ClientImmutableValue value) {
		return visitDefault(value);
	}

	@Override
	public R visitStatementSingleResultValue(StatementSingleResultValue value) {
		return visitDefault(value);
	}
}
