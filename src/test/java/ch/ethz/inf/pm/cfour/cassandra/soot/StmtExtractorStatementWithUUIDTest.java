package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;

public class StmtExtractorStatementWithUUIDTest extends AbstractSootTest {

	public StmtExtractorStatementWithUUIDTest() throws IOException {
		super("StatementWithUUID", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(1, cqls.size());
		assertTrue(cqls.contains("INSERT INTO test_table (key1, key2, key3, key4) VALUES (?,?,?,?)"));

		final Set<StatementValue> statementSet = result.iterator().next().statementArg;
		assertEquals(1, statementSet.size());

		int uuids = 0;
		int widenedUUIDs = 0;
		int choiceValues = 0;

		for (final AbstractBind<ConcreteValue> bind : statementSet.iterator().next().binds) {
			if (bind.value instanceof UUIDValue) {
				if (((UUIDValue) bind.value).isWidened) {
					assertTrue(false);
				} else {
					uuids++;
				}
			} else if (bind.value instanceof ImmutableChoiceValue) {
				choiceValues++;
				final ImmutableChoiceValue choiceValue = (ImmutableChoiceValue) bind.value;
				for (final ImmutableValue val : choiceValue.choices) {
					if (((UUIDValue) val).isWidened) {
						widenedUUIDs++;
					} else {
						uuids++;
					}
				}
			} else {
				assertTrue(false);
			}
		}

		assertEquals(4, uuids);
		assertEquals(1, widenedUUIDs);
		assertEquals(1, choiceValues);
	}
}