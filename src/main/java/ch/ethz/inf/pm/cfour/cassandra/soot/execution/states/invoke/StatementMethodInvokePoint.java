package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.BatchStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.StringToStatementTransformer;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.Stmt;

public class StatementMethodInvokePoint extends AbstractInvokeProgramPoint {

	private StatementMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static StatementMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
                                                    final int methodId) {
		return new StatementMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
                                     final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
                                     final ExecutionState state, final ExecutionState oldState) {

		if (methodId == SootMethodUtils.PREP_STMT_BIND_NO_ARG || methodId == SootMethodUtils.PREP_STMT_BIND_1_ARG) {
			if (leftLocal != null) {
				if (methodId == SootMethodUtils.PREP_STMT_BIND_NO_ARG) {
					state.setBaseValue(leftLocal, BaseReference.create(id), resolveConcreteAndTransform(baseLocal, state, stmt -> s(stmt)),
							id, null);
				} else {
					final VarLocalOrImmutableValue binds = args.get(0);
					state.setBaseValue(leftLocal, BaseReference.create(id),
							resolveConcreteAndTransform(baseLocal,
									state, stmt -> transformToImmutableArray(binds, state, transformedBinds -> {
										if (stmt instanceof StatementValue) {
											return s(bindFromObjectArray((StatementValue) stmt, transformedBinds));
										} else {
											return s(stmt);
										}
									})),
							id, null);
				}
			}
			return true;

		} else if (methodId == SootMethodUtils.STMT_SET_CONSISTENCY_LEVEL || methodId == SootMethodUtils.STMT_SET_SERIAL_CONSISTENCY_LEVEL) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}
			transformAndPutValue(baseRefs, state, oldState, AbstractStatementValue.class, stmt -> {
				return resolveConcreteAndTransform(arg0, state, consistencyLevel -> {
					if (methodId == SootMethodUtils.STMT_SET_CONSISTENCY_LEVEL) {
						return s(stmt.setConsistencyLevel((ImmutableValue) consistencyLevel));
					} else {
						return s(stmt.setSerialConsistencyLevel((ImmutableValue) consistencyLevel));
					}
				});
			});
			return true;

		} else if (methodId == SootMethodUtils.STMT_GET_CONSISTENCY_LEVEL || methodId == SootMethodUtils.STMT_GET_SERIAL_CONSISTENCY_LEVEL) {
			if (leftLocal != null) {
				final Set<ImmutableValue> values = new HashSet<>();
				for (final AbstractReference baseRef0 : baseRefs) {
					final BaseReference baseRef = (BaseReference) baseRef0;
					for (final Value val : state.getBaseValue(baseRef)) {
						if (val instanceof AbstractStatementValue) {
							final AbstractStatementValue<?, ?> stmt = (AbstractStatementValue<?, ?>) val;
							values.add(methodId == SootMethodUtils.STMT_GET_CONSISTENCY_LEVEL ? stmt.consistencyLevel : stmt.serialConsistencyLevel);
						} else {
							state.setUnknown(leftLocal, id);
							return true;
						}
					}
				}
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), ImmutableChoiceValue.create(id, values), id, oldState);
			}
			return true;

		} else if (methodId == SootMethodUtils.BOUND_STMT_INIT) {

			if (state.isLocalInitialized(args.get(0))) {
				state.setBaseValue(baseLocal, BaseReference.create(id),
						resolveConcreteAndTransform(args.get(0), state, stmt -> {
							if (stmt instanceof StatementValue) {
								return s(stmt);
							} else if (stmt instanceof NullValue) {
								return Collections.emptySet();
							} else {
								return s(UnknownMutableValue.create());
							}
						}), id, null);
			} else {
				state.setToBottom();
			}

			return true;

		} else if (methodId == SootMethodUtils.BOUND_STMT_BIND_1_ARG) {
			final VarLocalOrImmutableValue binds = args.get(0);
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}

			transformAndPutValue(baseRefs, state, oldState, StatementValue.class, stmt -> {
				return transformToImmutableArray(binds, state,
						resolvedBinds -> s(bindFromObjectArray(stmt, resolvedBinds)));
			});
			return true;

		} else if (methodId == SootMethodUtils.BOUND_STMT_SET_WITH_IDX || methodId == SootMethodUtils.BOUND_STMT_SET_WITH_NAME
				|| methodId == SootMethodUtils.BOUND_STMT_SET_TO_NULL_WITH_IDX || methodId == SootMethodUtils.BOUND_STMT_SET_TO_NULL_WITH_NAME) {
			final VarLocalOrImmutableValue idx = args.get(0);
			final ConcreteValue value;
			if (methodId == SootMethodUtils.BOUND_STMT_SET_TO_NULL_WITH_IDX || methodId == SootMethodUtils.BOUND_STMT_SET_TO_NULL_WITH_NAME) {
				value = NullValue.create();
			} else {
				if (args.get(1) instanceof VarLocal) {
					final VarLocal argLocal = (VarLocal) args.get(1);
					if (argLocal.varType.equals(VarType.IMMUTABLE)) {
						value = ImmutableChoiceValue.create(id, state.getImmutableValues(argLocal));
					} else {
						value = UnknownMutableValue.create();
					}
				} else {
					value = (ImmutableValue) args.get(1);
				}
			}
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}

			transformAndPutValue(baseRefs, state, oldState, StatementValue.class, stmt -> {
				return resolveConcreteAndTransform(idx, state,
						resolvedIdx -> s(stmt.addBind(AbstractBind.UserBind.create(resolvedIdx, value))));
			});
			return true;

		} else if (methodId == SootMethodUtils.BOUND_STMT_SET_PARTITION_KEY_TOKEN) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}

			transformAndPutValue(baseRefs, state, oldState, StatementValue.class, stmt -> {
				return resolveConcreteAndTransform(arg0, state,
						resolvedArg0 -> s(stmt.addBind(AbstractBind.PartitionKeyTokenBind.create(resolvedArg0))));
			});
			return true;

		} else if (methodId == SootMethodUtils.BATCH_STMT_INIT) {
			state.setBaseValue(baseLocal, BaseReference.create(id), s(BatchStatementValue.create()), id, null);
			return true;

		} else if (methodId == SootMethodUtils.BATCH_STMT_ADD) {
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}

			transformAndPutValue(baseRefs, state, null, StatementValue.class, batchStmt0 -> {
				if (batchStmt0 instanceof BatchStatementValue) {
					final BatchStatementValue batchStmt = (BatchStatementValue) batchStmt0;
					return resolveConcreteAndTransform(args.get(0), state, stmt -> {
						// we add the same statement at most 2 times in a batch
						if (stmt instanceof StatementValue) {
							int stmtCount = 0;
							for (final StatementValue inBatch : batchStmt.statements) {
								if (inBatch.equals(stmt)) {
									stmtCount++;
								}
							}
							if (stmtCount >= 2) {
								return s(batchStmt);
							} else {
								return s(batchStmt.addStatement((StatementValue) stmt));
							}
						} else if (stmt instanceof NullValue) {
							return s(batchStmt);
						} else {
							return s(StatementValue.createUnknown());
						}
					});
				} else {
					return s(StatementValue.createUnknown());
				}
			});
			return true;

		} else if (methodId == SootMethodUtils.SIMPLE_STMT_INIT_NO_BINDS || methodId == SootMethodUtils.SIMPLE_STMT_INIT_WITH_BINDS) {
			final VarLocalOrImmutableValue arg0 = args.get(0);
			final VarLocalOrImmutableValue arg1;
			if (methodId == SootMethodUtils.SIMPLE_STMT_INIT_WITH_BINDS) {
				arg1 = args.get(1);
			} else {
				arg1 = null;
			}

			final Set<StatementValue> stmts = this.<StatementValue> resolveConcreteAndTransform(arg0, state,
					stmt -> {
						if (stmt instanceof NullValue) {
							return Collections.emptySet();
						} else if (arg1 == null) {
							return new StringToStatementTransformer(stmt, new ImmutableValue[0]).transform();
						} else {
							return this.<StatementValue> transformToImmutableArray(arg1, state,
									binds -> new StringToStatementTransformer(stmt, binds).transform());
						}
					});

			state.setBaseValue(baseLocal, BaseReference.create(id), stmts, id, oldState);
			return true;

		} else if (methodId == SootMethodUtils.STMT_GENERAL_SETTER_RETURNING_THIS || methodId == SootMethodUtils.BOUND_STMT_SET_ROUTING_KEY) {
			if (leftLocal != null) {
				state.setLocalFromOther(leftLocal, baseLocal);
			}
			return true;

		} else if (methodId == SootMethodUtils.STMT_GENERAL_GETTER || methodId == SootMethodUtils.BOUND_STMT_GET_SOME_VALUE) {
			if (leftLocal != null && !leftLocal.varType.equals(VarType.EXCLUDED)) {
				state.setUnknown(leftLocal, id);
			}
			return true;
		}

		return false;
	}

	private StatementValue bindFromObjectArray(final StatementValue stmt, final ImmutableValue[] objectArray) {
		if (objectArray != null) {
			return stmt.addFirstBinds(objectArray);
		}
		return stmt.clearUserBinds();
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new StatementMethodInvokePoint(id, left, invokeStmt, methodId);
	}
}
