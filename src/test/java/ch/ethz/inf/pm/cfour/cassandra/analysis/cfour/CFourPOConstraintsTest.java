package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourPOConstraintsTest extends AbstractCFourTest {

	public CFourPOConstraintsTest() throws IOException {
		super("CFourPOConstraints");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").addPrimaryKeyColumn("followers", "username")
				.addPrimaryKeyColumn("followers", "follower").addTableWithoutDelete("users").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setPoConstraintsEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setPoConstraintsEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isPoConstraintsEnabled()) {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
			assertEquals(0, result.getVerifiedViolationsSize3());
		} else {
			assertEquals(1, result.getVerifiedViolationsSize3());
			assertEquals(4, result.getTotalVerifiedViolationsSize4());
		}
	}
}
