package simpletwitter;

import com.datastax.driver.core.utils.UUIDs;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class SimpleTwitter {
	private final static String HOST = "127.0.0.1";

	private final DBAccess dbAccess;
	private final String username;
	
    public static void main(String[] args) throws IOException {
       
    	final String username = args[0];
    	final String password = args[1];
        final SimpleTwitter twitter = new SimpleTwitter(username, password);
        
        String command = null;
        String unsplittedParams = null;

        do {
            String input = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

            if(input.isEmpty()){
                twitter.showHelp();
            }
            else {
                String[] parameters = new String[0];
                int i = input.indexOf(" ");

                if (i > -1) {
                    command = input.substring(0, i);
                    unsplittedParams = input.substring(i + 1);
                    parameters = unsplittedParams.split(" ");
                } else
                    command = input;

                if (command.equalsIgnoreCase("ADDTWEET") && parameters.length == 1) {
                    twitter.addTweet(parameters[0]); 
                    } else if (command.equalsIgnoreCase("VIEWTWEETS") && parameters.length == 0) {
                        twitter.showUserline(null);
                } else if (command.equalsIgnoreCase("VIEWTWEETS") && parameters.length == 1) {
                    twitter.showUserline(parameters[0]);
                } else if (command.equalsIgnoreCase("HELP") ) {
                    twitter.showHelp();
                } else if (command.equalsIgnoreCase("EXIT")) {
                    System.out.println("Exiting...");
                } else
                    twitter.showHelp();
            }
            if (command == null){
                command = "";
            }
        } while(!command.equalsIgnoreCase("EXIT"));
    }
    
    private SimpleTwitter(final String username, final String password){
    	this.dbAccess = new DBAccess(HOST);
    	login(username, password);
    	this.username = username;
    }

    @Transaction
    private void login(final String username, final String password){
        ClientLocalValues.set("username", username);
    	final String passwordFromDb = dbAccess.getPassword(username);
    	if (passwordFromDb == null){
    		// new user
    		dbAccess.addUser(username, password);
    	} else if (!passwordFromDb.equals(password)){
    		throw new RuntimeException("Illegal password");
    	}
    }
    
    @Transaction(onlyForDisplaying = true)
    private void showHelp(){
        System.out.println("* ADDTWEET <tweet>");
        System.out.println("    adds a new tweet");
        System.out.println("* VIEWTWEETS");
        System.out.println("    view own tweets");
        System.out.println("* VIEWTWEETS <username>");
        System.out.println("    view tweets posted by another user");
        System.out.println("* HELP");
        System.out.println("    this message");
        System.out.println("* EXIT");
        System.out.println("    quit the program");
    }


    @Transaction
    public void addTweet(String tweet) {
        final UUID tweetId = UUIDs.random();
        final UUID timeId = UUIDs.timeBased();
        final String username = this.username;
        ClientLocalValues.set("username", username);
        
        dbAccess.addTweet(tweetId, username, tweet);
        dbAccess.addToUserline(username, timeId, tweetId);
    }

    @Transaction(onlyForDisplaying = true)
    public void showUserline(String username) {
        if (username == null){
        	username = this.username;
        }
        final List<UUID> tweetIds = dbAccess.getUserline(username);
        
        System.out.println("Tweets from " + username);
        for (Object[] tweet : dbAccess.getTweets(tweetIds)){
        	System.out.println();
        	System.out.println(tweet[2]);
        }
    }
}
