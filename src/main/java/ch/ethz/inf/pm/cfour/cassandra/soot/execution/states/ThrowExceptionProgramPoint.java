package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.Objects;

import soot.jimple.ThrowStmt;

/**
 * Marker for a throw statement
 */
public class ThrowExceptionProgramPoint extends AbstractProgramPoint {

	private final ThrowStmt throwStmt;

	private ThrowExceptionProgramPoint(final ProgramPointId id, final ThrowStmt throwStmt) {
		super(id);
		this.throwStmt = Objects.requireNonNull(throwStmt);
	}

	public static ThrowExceptionProgramPoint create(final CallStack callStack, final ThrowStmt throwStmt) {
		return new ThrowExceptionProgramPoint(ProgramPointId.create(callStack, throwStmt), throwStmt);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		// nop
		// point is used as a marker
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new ThrowExceptionProgramPoint(id, throwStmt);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((throwStmt == null) ? 0 : throwStmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThrowExceptionProgramPoint other = (ThrowExceptionProgramPoint) obj;
		if (throwStmt == null) {
			if (other.throwStmt != null)
				return false;
		} else if (!throwStmt.equals(other.throwStmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ThrowExceptionProgramPoint [" + throwStmt + "]";
	}
}
