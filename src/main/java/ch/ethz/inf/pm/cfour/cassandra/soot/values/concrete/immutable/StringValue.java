package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class StringValue extends AbstractImmutableValue {

	public final String string;

	private StringValue(final String string) {
		this.string = Objects.requireNonNull(string);
	}

	public static StringValue create(final String string) {
		return new StringValue(string);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitStringValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((string == null) ? 0 : string.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringValue other = (StringValue) obj;
		if (string == null) {
			if (other.string != null)
				return false;
		} else if (!string.equals(other.string))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StringValue [string=" + string + "]";
	}
}
