package ch.ethz.inf.pm.cfour.cassandra.analysis;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourResult;
import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.graphs.GraphToDotUtil;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.util.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.BasicSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;

/**
 * Checks the given program for serializability violations. The analysis consists of the following steps
 * <ol>
 * <li>Collect the unparsed transaction graphs using the {@link SootAnalysis}</li>
 * <li>Parse the transaction graphs using {@link TransactionGraphTransformer}</li>
 * <li>Collect the schema information using {@link SchemaInformationTransformer}</li>
 * <li>Run the basic serializability analysis {@link SerializabilityAnalysis}</li>
 * <li>Check the graphs using CFour {@link CFourSerializabilityAnalysis}</li>
 * </ol>
 */
public class SerializabilityAnalysis extends AbstractSerializabilityAnalysis {

	public final static String PHASE_SOOT = "Soot Analysis";
	public final static String PHASE_PARSING_TRANSACTION_GRAPHS = "Parsing the transaction graphs";
	public final static String PHASE_BASIC_SERIALIZABILITY = "Basic Serializability Analysis";
	public final static String PHASE_EC_CHECKER = "CFour Serializability Analysis";

	private final static Logger LOG = LogManager.getLogger(SerializabilityAnalysis.class);
	private CFourResult CFourResult = null;

	public SerializabilityAnalysis(final Options options) {
		super(options);
	}

	public Profiler run() {
		final SootAnalysis sootAnalysis = new SootAnalysis(options);
		final Profiler profiler = new Profiler();
		sootAnalysis.run(profiler);
		profiler.addSummaryPhase(PHASE_SOOT);
		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs = new TransactionGraphTransformer(
				options).transform(sootAnalysis.getTransactionGraphs());

		if (LOG.isDebugEnabled()) {
			for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
				LOG.debug("Transaction " + entry.getKey().toString());
				LOG.debug("------------------------------");
				for (final ParsedSessionExecuteInvoke sei : entry.getValue().nodes()) {
					LOG.debug("  " + sei.containingMethod.getShortName() + ":" + sei.number);
					LOG.debug("    " + sei.statement.getQuery());
				}
				LOG.debug("");
			}
		}
		if (outputDir != null) {
			writeTransactionExecuteGraphs(outputDir, transactionGraphs);
		}
		System.out.println("Number of transactions: " + transactionGraphs.size());
		// build schema info
		final SchemaInformation schemaInfo = new SchemaInformationTransformer(options).collect(transactionGraphs);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Used schema information: " + schemaInfo);
		}
		profiler.startNewPhase(PHASE_PARSING_TRANSACTION_GRAPHS);
		if (options.isBasicAnalysisEnabled()) {
			new BasicSerializabilityAnalysis(options).check(transactionGraphs, schemaInfo);
			profiler.startNewPhase(PHASE_BASIC_SERIALIZABILITY);
		}
		CFourResult = new CFourSerializabilityAnalysis(options).check(transactionGraphs, schemaInfo);
		profiler.startNewPhase(PHASE_EC_CHECKER);
		profiler.logProfilingInformation();
		return profiler;
	}

	private void writeTransactionExecuteGraphs(final File baseDir,
			final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionGraphs) {
		final File transactionsDir = new File(baseDir, "transactions");
		FileUtils.createEmptyDirectory(transactionsDir);
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs
				.entrySet()) {
			try {
				GraphToDotUtil.printTransactionGraph(
						new File(transactionsDir, entry.getKey().getShortName().replaceAll("\\W+", "") + ".dot"),
						entry.getKey().getShortSignature(), entry.getValue(), GraphToDotUtil.PARSED_EXECUTE_TO_KEY,
						GraphToDotUtil.PARSED_EXECUTE_TO_LABEL, GraphToDotUtil.EDGE_CONSTRAINT_TO_LABEL);
			} catch (IOException e) {
				LOG.error("Error writing dot file", e);
			}
		}
	}

	public CFourResult getCFourResult() {
		return CFourResult;
	}
}
