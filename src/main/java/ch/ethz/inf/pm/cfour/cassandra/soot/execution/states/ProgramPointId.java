package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.Objects;

/**
 * A unique location in a program. idInMethod is usually the statement.
 */
public class ProgramPointId {

	public final CallStack callStack;
	public final Object idInMethod;
	private final int hashCode;

	protected ProgramPointId(final CallStack callStack, final Object idInMethod) {
		this.callStack = Objects.requireNonNull(callStack);
		this.idInMethod = Objects.requireNonNull(idInMethod);
		this.hashCode = hashCodeInternal();
	}

	public static ProgramPointId create(final CallStack callStack, final Object idInMethod) {
		return new ProgramPointId(callStack, idInMethod);
	}

	public static ProgramPointId unique() {
		return new ProgramPointId(CallStack.EMPTY_STACK, new Object());
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	private int hashCodeInternal() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((callStack == null) ? 0 : callStack.hashCode());
		result = prime * result + ((idInMethod == null) ? 0 : idInMethod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramPointId other = (ProgramPointId) obj;
		if (hashCode != other.hashCode)
			return false;
		if (callStack == null) {
			if (other.callStack != null)
				return false;
		} else if (!callStack.equals(other.callStack))
			return false;
		if (idInMethod == null) {
			if (other.idInMethod != null)
				return false;
		} else if (!idInMethod.equals(other.idInMethod))
			return false;
		return true;
	}
}
