

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;

public class Transactions {

	private final Session session;
	
	public Transactions(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void method1(int i, A a){
		session.execute("SELECT * FROM method1_1");
		if (i > 10){
			session.execute("SELECT * FROM method1_2");
		} else {
			session.execute("SELECT * FROM method1_3");
		}
		a.dispatch();
	}
	
	@Transaction
	public void method2(int a){
		session.execute("SELECT * FROM method2_1");
		for (int i=0; i<a; i++){
			session.execute("SELECT * FROM method2_2");
		}
		session.execute("SELECT * FROM method2_3");
	}
	
	@Transaction
	public void method3(int a){
		if (a > 0){
			session.execute("SELECT * FROM method3_1");
			method3(a-1);
		}
	}
	
	@Transaction
	public void method4(int a){
		helper();
		session.execute("SELECT * FROM method4_1");
		helper();
	}
	
	@Transaction
	public void method5(){
		System.out.println("Im only printing...");
	}
	
	public void helper(){
		session.execute("SELECT * FROM method4_2");
	}
	
	public class A {
		
		public void dispatch(){
			session.execute("SELECT * FROM method1_4");
		}
	}
	
	public class B extends A {
		
		@Override
		public void dispatch(){
			session.execute("SELECT * FROM method1_5");
		}
	}
}
