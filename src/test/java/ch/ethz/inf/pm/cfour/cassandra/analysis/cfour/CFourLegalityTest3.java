package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import com.google.common.collect.ImmutableSet;

public class CFourLegalityTest3 extends AbstractCFourTest {

	public CFourLegalityTest3() throws IOException {
		super("CFourLegalitySpec3");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("chat_rooms", "room_name")
				.addNonPrimaryKeyColumn("chat_rooms", "participants").addTableWithoutDelete("chat_rooms")
				.addColumnsWritten("chat_rooms", ImmutableSet.of("room_name", "participants")).build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setLegalitySpecEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isLegalitySpecEnabled()) {
			assertEquals(0, result.getVerifiedViolationsSize2());
			assertEquals(0, result.getVerifiedViolationsSize3());
		} else {
			assertEquals(0, result.getVerifiedViolationsSize2());
			assertNotEquals(0, result.getVerifiedViolationsSize3());
		}
	}
}
