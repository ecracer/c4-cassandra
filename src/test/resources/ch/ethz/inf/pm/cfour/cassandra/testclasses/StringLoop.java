

import com.datastax.driver.core.Session;

public class StringLoop {
	
	public void test(Session session, int i, int j) {
		String inList = "";
		for (int k=0; k<i; k++){
			if (!inList.isEmpty()){
				inList += ", ";
			}
			inList += k;
		}
		String str = "SELECT * FROM test_table WHERE j IN (" + inList + ")";
		session.execute(str);
	}
}