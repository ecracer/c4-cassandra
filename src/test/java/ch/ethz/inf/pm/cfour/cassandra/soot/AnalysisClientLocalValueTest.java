package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisClientLocalValueTest extends AbstractSootTest {

	public AnalysisClientLocalValueTest() throws IOException {
		super("ClientLocalValue", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(
				new Options()).transform(executeGraphs);

		assertEquals(4, executeGraphs.size());

		final SchemaInformation schema = SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password")
				.addPrimaryKeyColumn("users2", "username").addNonPrimaryKeyColumn("users2", "password")
				.addPrimaryKeyColumn("settings", "username").addNonPrimaryKeyColumn("settings", "setting").build();

		final Options options = new Options();
		options.setClientLocalAreGlobalUnique(true);

		int violations = new CFourSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema).getVerifiedViolationsSize2();
		assertEquals(1, violations);

		options.setClientLocalAreGlobalUnique(false);
		violations = new CFourSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema).getVerifiedViolationsSize2();
		assertEquals(2, violations);
	}
}
