package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.IdentityStmt;

/**
 * Transformer for a catch(Exception e)
 */
public class CaughtExceptionProgramPoint extends AbstractProgramPoint {

	private final IdentityStmt idStmt;

	private CaughtExceptionProgramPoint(final ProgramPointId id, final IdentityStmt idStmt) {
		super(id);
		if (!(idStmt.getRightOp() instanceof CaughtExceptionRef)) {
			throw new IllegalArgumentException();
		}
		this.idStmt = Objects.requireNonNull(idStmt);
	}

	public static CaughtExceptionProgramPoint create(final CallStack callStack, final IdentityStmt idStmt) {
		return new CaughtExceptionProgramPoint(ProgramPointId.create(callStack, idStmt), idStmt);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) idStmt.getLeftOp(), id.callStack);
		if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
			state.setUnknown(leftLocal, id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new CaughtExceptionProgramPoint(id, idStmt);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((idStmt == null) ? 0 : idStmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CaughtExceptionProgramPoint other = (CaughtExceptionProgramPoint) obj;
		if (idStmt == null) {
			if (other.idStmt != null)
				return false;
		} else if (!idStmt.equals(other.idStmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CaughtExceptionProgramPoint [" + idStmt + "]";
	}
}
