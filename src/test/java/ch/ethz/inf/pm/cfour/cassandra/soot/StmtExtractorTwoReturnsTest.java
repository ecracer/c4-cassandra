package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorTwoReturnsTest extends AbstractSootTest {

	public StmtExtractorTwoReturnsTest() throws IOException {
		super("TwoReturns", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(4, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM test_table_1"));
		assertTrue(cqls.contains("SELECT * FROM test_table_2"));
		assertTrue(cqls.contains("SELECT * FROM test_table_1 WHERE j = ?"));
		assertTrue(cqls.contains("SELECT * FROM test_table_2 WHERE j = ?"));
	}
}