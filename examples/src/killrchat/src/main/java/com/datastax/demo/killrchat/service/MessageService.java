package com.datastax.demo.killrchat.service;

import com.datastax.demo.killrchat.entity.MessageEntity;
import com.datastax.demo.killrchat.model.MessageModel;
import com.datastax.demo.killrchat.model.LightUserModel;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import info.archinnov.achilles.persistence.PersistenceManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static com.datastax.demo.killrchat.entity.Schema.KILLRCHAT_USER;
import static java.lang.String.format;

@Service
public class MessageService {

    public static final String JOINING_MESSAGE = "%s joins the room";
    public static final String LEAVING_MESSAGE = "%s leaves the room";

    @Inject
    Session session;

    public MessageModel postNewMessage(LightUserModel author, String roomName, String messageContent) {
        final MessageEntity entity = new MessageEntity(roomName, UUIDs.timeBased(), author, messageContent);
        session.execute("INSERT INTO chat_room_messages (room_name, message_id, content, author, system_message) VALUES (?, ?, ?, ?, ?)",
        		entity.getRoomName(), entity.getMessageId(), entity.getContent(), entity.getAuthor(), entity.isSystemMessage());
        return entity.toModel();
    }

    public List<MessageModel> fetchNextMessagesForRoom(String roomName, UUID fromMessageId, int pageSize) {
        final Iterator<Row> rowIt = session.execute("SELECT room_name, message_id, content, author, system_message FROM chat_room_messages WHERE room_name = ? AND message_id >= ? LIMIT ?", 
        		roomName, fromMessageId, pageSize).iterator();
        final List<MessageModel> messages = new ArrayList<>();
        while (rowIt.hasNext()){
        	final Row row = rowIt.next();
        	final MessageModel message = new MessageModel();
        	message.setMessageId(row.getUUID("message_id"));
        	// TODO: add other columns
        	messages.add(message);
        }
        Collections.reverse(messages);
        return messages;
    }

    public MessageModel createJoiningMessage(String roomName, LightUserModel participant) {
        final MessageEntity entity = new MessageEntity(roomName, UUIDs.timeBased(), KILLRCHAT_USER.toLightModel(), format(JOINING_MESSAGE, participant.getFormattedName()), true);
        session.execute("INSERT INTO chat_room_messages (room_name, message_id, content, author, system_message) VALUES (?, ?, ?, ?, ?)",
        		entity.getRoomName(), entity.getMessageId(), entity.getContent(), entity.getAuthor(), entity.isSystemMessage());
        return entity.toModel();
    }

    public MessageModel createLeavingMessage(String roomName, LightUserModel participant) {
        final MessageEntity entity = new MessageEntity(roomName, UUIDs.timeBased(), KILLRCHAT_USER.toLightModel(), format(LEAVING_MESSAGE, participant.getFormattedName()), true);
        session.execute("INSERT INTO chat_room_messages (room_name, message_id, content, author, system_message) VALUES (?, ?, ?, ?, ?)",
        		entity.getRoomName(), entity.getMessageId(), entity.getContent(), entity.getAuthor(), entity.isSystemMessage());
        return entity.toModel();
    }
}
