package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.analysis.Profiler;
import ch.ethz.inf.pm.cfour.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.transaction.TransactionGraphCollector;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.ExecuteGraphTransformer;
import ch.ethz.inf.pm.cfour.cassandra.soot.fieldvalues.FieldValueCollector;
import ch.ethz.inf.pm.cfour.cassandra.soot.includedcollector.IncludedCollectorTransformer;
import soot.PackManager;
import soot.SootMethod;
import soot.Transform;

/**
 * This analysis builds the transaction graphs for a given program. Transactions have to be annotated in the source code
 * of the program or have to be supplied in the options.
 * 
 * The analysis executes the following steps using the Soot framework:
 * <ol>
 * <li>Collect information about which methods and fields are analyzed {@link IncludedCollectorTransformer}</li>
 * <li>Transform each method into its execute graph {@link ExecuteGraphTransformer}</li>
 * <li>Calculate the possible field values of each analyzed class (using the execute graphs)
 * {@link FieldValueCollector}</li>
 * <li>Build the transaction graphs (using the execute graphs) {@link TransactionGraphCollector}
 * </ol>
 */
public class SootAnalysis {

	private final SootOptions userOptions;
	private ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> transactionGraphs;

	public SootAnalysis(final SootOptions userOptions) {
		this.userOptions = Objects.requireNonNull(userOptions);
	}

	public ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> getTransactionGraphs() {
		if (transactionGraphs == null) {
			throw new IllegalStateException();
		}
		return transactionGraphs;
	}

	public void run(final Profiler profiler) {
		// Soot is statically configured --> may be executed only once concurrently
		synchronized (SootAnalysis.class) {
			final ImmutableSet<SootMethod> transactionMethods = SootSetup.setupSoot(this.userOptions);

			profiler.startNewPhase("soot class loading");

			final IncludedCollectorTransformer includedCollector = new IncludedCollectorTransformer();
			final ExecuteGraphTransformer executeGraphTransformer = new ExecuteGraphTransformer(includedCollector, userOptions);

			PackManager.v().getPack("wsop")
					.add(new Transform("wsop.includedCollector", includedCollector));

			// remove constantFolder
			// we do not use constant folding as it removes e.g. relations of return values and conditions in if statements
			PackManager.v().getPack("sop").remove("sop.cpf");
			PackManager.v().getPack("sop").add(new Transform("sop.executeGraphTransformer", executeGraphTransformer));

			PackManager.v().runPacks();

			final ImmutableMap<SootMethod, ExecuteGraph> executeGraphs = executeGraphTransformer
					.getExecuteGraphs();

			profiler.startNewPhase("soot transformations");

			final FieldValueCollector fieldValueCollector = new FieldValueCollector(executeGraphs,
					includedCollector.getIncludedStaticFields(), includedCollector.getIncludedFields(),
					includedCollector.getFieldsPerClass());

			profiler.startNewPhase("soot field value collection");

			final TransactionGraphCollector transactionGraphCollector = new TransactionGraphCollector(userOptions, executeGraphs,
					transactionMethods, fieldValueCollector.getInitialState());
			transactionGraphs = transactionGraphCollector.transactionGraphs;

			profiler.startNewPhase("soot transaction graph collection");
		}
	}
}
