import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.utils.UUIDs;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author feli
 */
public class TimeLine {

    private String username;
    private Query query;
    private ArrayList<Tweet> Tweets;


    public TimeLine(String username) {
        this.username = username;
        query = new Query();
        Tweets = new ArrayList<Tweet>();
    }

    public void loadTimeline() {
        this.loadTimeline(10);
    }

    public void loadTimeline(int limit) {
    	final String username = this.username;
    	ClientLocalValues.set("username", username);
        ResultSet set = query.getSession().execute(QueryBuilder.select().all()
                .from("timeline")
                .where(QueryBuilder.eq("username", username))
                .limit(limit)
        );

        List<Row> row = set.all();
        Tweets = new ArrayList<Tweet>();

        for (Row tuple : row) {
            Tweet twit = new Tweet();
            twit.setUsername(tuple.getString("username"));
            twit.setUuids(tuple.getUUID("tweet_id"));

            twit.Load();
            Tweets.add(twit);
        }
    }

    public String getTimeLine() {
        String retString = "";
        for (Tweet twit : Tweets) {
            retString += twit.getTweetString();
        }
        return retString;
    }

    public void putToTimeline(UUID tweet_id) {
    	final String username = this.username;
    	ClientLocalValues.set("username", username);
        ResultSet friends = query.getSession().execute(QueryBuilder.select("friend")
                .from("friends")
                .where(QueryBuilder.eq("username", username))
        );
//        List<Row> row = friends.all();
//        for (int i = 0; i < row.size(); i++) {
//            putToTimeline(row.get(i).getString("friend"), tweet_id);
//        }
        putToTimeline(username, tweet_id);
        for (Row tuple : friends) {
            putToTimeline(tuple.getString("friend"), tweet_id);
        }
    }

    public void putToTimeline(String friend, UUID tweet_id) {
        query.getSession().execute(QueryBuilder.insertInto("timeline")
                .value("tweet_id", tweet_id)
                .value("username", friend)
                .value("time", UUIDs.timeBased())
        );
    }
}
