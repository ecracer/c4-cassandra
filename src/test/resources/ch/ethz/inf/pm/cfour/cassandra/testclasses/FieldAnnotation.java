

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.datastax.driver.core.Session;

public class FieldAnnotation {
	
	@Autowired
	private Session session;

	public void test() {
		String str = "SELECT * FROM test_table";
		// must be executed, even though the session field is never assigned
		session.execute(str);
	}
	
	@Target(FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Autowired {

	}
}