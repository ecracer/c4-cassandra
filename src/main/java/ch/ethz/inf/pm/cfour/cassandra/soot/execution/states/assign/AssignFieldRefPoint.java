package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ObjectReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefObjectValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.FieldDesc;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Local;
import soot.jimple.AssignStmt;
import soot.jimple.FieldRef;
import soot.jimple.InstanceFieldRef;

/**
 * Transformer for reading xor writing a field
 */
public class AssignFieldRefPoint extends AbstractAssignProgramPoint {

	private AssignFieldRefPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!assign.containsFieldRef()) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignFieldRefPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignFieldRefPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final FieldDesc field = SootValUtils.transformField(assign.getFieldRef());
		final VarLocal baseLocal;
		if (!assign.getFieldRef().getField().isStatic() && assign.getFieldRef() instanceof InstanceFieldRef) {
			baseLocal = SootValUtils.transformLocal((Local) ((InstanceFieldRef) assign.getFieldRef()).getBase(), id.callStack);
		} else {
			baseLocal = null;
		}
		final VarLocal leftLocal;
		final VarLocalOrImmutableValue rightValue;
		if (assign.getLeftOp() instanceof FieldRef) {
			leftLocal = null;
			rightValue = SootValUtils.transformLocalOrImmutable(assign.getRightOp(), id);
		} else {
			leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
			if (SootTypeUtils.isConsistencyLevel(assign.getFieldRef().getField().getDeclaringClass().getType())) {
				rightValue = SootValUtils.transformConsistencyLevel(assign.getFieldRef());
			} else {
				rightValue = null;
			}
		}
		if (field.varType.equals(VarType.EXCLUDED) || (baseLocal != null && baseLocal.varType.equals(VarType.EXCLUDED))) {
			if (rightValue != null) {
				if (rightValue instanceof VarLocal && !((VarLocal) rightValue).varType.equals(VarType.EXCLUDED)) {
					state.setToUnknown(rightValue, id);
				}
			} else if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
				state.setUnknown(leftLocal, id);
			}
			return;
		}
		final Set<ObjectReference> baseRefs;
		if (baseLocal != null) {
			baseRefs = state.ensureNonNullPointsTo(baseLocal, ObjectReference.class);
			if (baseRefs.isEmpty()) {
				state.setToBottom();
				return;
			}
		} else {
			baseRefs = null;
		}
		if (leftLocal != null) {
			if (rightValue != null) {
				// Consistency Level
				state.setLocalFromPhi(leftLocal, Collections.singleton(rightValue), id);
			} else {
				setLocalFromField(leftLocal, field, baseRefs, state);
			}
		} else {
			setFieldFromRight(field, baseRefs, rightValue, state);
		}
	}

	public FieldDesc getAccessedField() {
		return SootValUtils.transformField(assign.getFieldRef());
	}

	public boolean isFieldDefinition() {
		return assign.getLeftOp() instanceof FieldRef;
	}

	private void setLocalFromField(final VarLocal leftLocal, final FieldDesc field, final Set<ObjectReference> baseRefs,
			final ExecutionState state) {
		if (leftLocal.varType.equals(VarType.EXCLUDED)) {
			if (baseRefs == null) {
				state.setStaticFieldToUnknown(field, id);
			} else {
				setFieldToUnknown(baseRefs, field, state);
			}
		} else if (baseRefs == null) {
			if (state.isStaticFieldSet(field)) {
				state.setLocalFromPhi(leftLocal, state.getStaticField(field), id);
			} else {
				state.setUnknown(leftLocal, id);
			}
		} else {
			boolean hasUnknown = false;
			final Set<VarLocalOrImmutableValue> rightVals = new HashSet<>();
			for (final ObjectReference objRef : baseRefs) {
				final RefObjectValue objVal = state.getObjectValue(objRef);
				if (objVal.isUnknown(field)) {
					hasUnknown = true;
					break;
				} else if (objVal.isFieldSet(field)) {
					rightVals.addAll(objVal.getField(field));
				} else if (field.hasAnnotation) {
					hasUnknown = true;
				} else {
					rightVals.add(field.defaultValue);
				}
			}
			if (hasUnknown) {
				setFieldToUnknown(baseRefs, field, state);
				state.setUnknown(leftLocal, id);
			} else {
				state.setLocalFromPhi(leftLocal, rightVals, id);
			}
		}
	}

	private void setFieldFromRight(final FieldDesc field, final Set<ObjectReference> baseRefs, final VarLocalOrImmutableValue rightValue,
			final ExecutionState state) {
		if (baseRefs == null) {
			if (rightValue instanceof VarLocal && ((VarLocal) rightValue).varType.equals(VarType.EXCLUDED)) {
				state.setStaticFieldToUnknown(field, id);
			} else {
				state.setStaticField(field, rightValue);
			}
		} else {
			for (final ObjectReference objRef : baseRefs) {
				final RefObjectValue objVal = state.getObjectValue(objRef);
				if (!objVal.isUpdateable(field)) {
					if (!(rightValue instanceof VarLocal) || !((VarLocal) rightValue).varType.equals(VarType.EXCLUDED)) {
						state.setToUnknown(rightValue, id);
					}
				} else if (rightValue instanceof VarLocal && ((VarLocal) rightValue).varType.equals(VarType.EXCLUDED)) {
					state.updateObjectValue(objRef, objVal.setFieldToUnknown(field), id);
				} else {
					state.updateObjectValue(objRef, objVal.setField(field, Collections.singleton(rightValue)), id);
				}
			}
		}
	}

	private void setFieldToUnknown(final Set<ObjectReference> baseRefs, final FieldDesc field, final ExecutionState state) {
		for (final ObjectReference objRef : baseRefs) {
			final RefObjectValue objVal = state.getObjectValue(objRef);
			if (!objVal.isUnknown(field) && objVal.isFieldSet(field)) {
				state.setToUnknown(objVal.getField(field), id);
			}
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignFieldRefPoint(id, assign);
	}
}
