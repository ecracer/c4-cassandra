package ch.ethz.inf.pm.cfour.cassandra.analysis.basic;

public enum EdgeValue {
	DEPENDENCY, ARBITRATION, ANTI_DEPENDENCY
}
