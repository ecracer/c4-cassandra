package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourStrictUpdateTest extends AbstractCFourTest {

	public CFourStrictUpdateTest() throws IOException {
		super("CFourStrictUpdate");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("chat_rooms", "room_name")
				.addNonPrimaryKeyColumn("chat_rooms", "participants")
				.addPrimaryKeyColumn("users", "login").addNonPrimaryKeyColumn("users", "chat_rooms").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setStrictUpdatesEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setStrictUpdatesEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isStrictUpdatesEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
		}
	}
}
