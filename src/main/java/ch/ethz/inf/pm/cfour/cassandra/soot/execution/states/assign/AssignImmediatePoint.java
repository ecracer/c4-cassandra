package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import java.util.Collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.EmptyResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.Immediate;
import soot.Local;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;

/**
 * Transformer for an assignment from an immediate (Local or Constant) to a local
 */
public class AssignImmediatePoint extends AbstractAssignProgramPoint {

	private final static Logger LOG = LogManager.getLogger(AssignImmediatePoint.class);

	private AssignImmediatePoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!(assign.getLeftOp() instanceof Local) || !(assign.getRightOp() instanceof Immediate)) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignImmediatePoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignImmediatePoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);

		if (assign.getRightOp() instanceof Local) {
			final VarLocal rightLocal = SootValUtils.transformLocal((Local) assign.getRightOp(), id.callStack);
			if (leftLocal.varType.equals(VarType.EXCLUDED)) {
				if (!rightLocal.varType.equals(VarType.EXCLUDED)) {
					state.setToUnknown(rightLocal, id);
				}
				return;
			} else if (!rightLocal.varType.equals(VarType.EXCLUDED) && !state.isLocalInitialized(rightLocal)) {
				state.setToBottom();
				return;
			}
			if (VarType.isAssignableTo(leftLocal.varType, rightLocal.varType)) {
				state.setLocalFromOther(leftLocal, rightLocal);
			} else {
				LOG.warn("Right var type cannot be assigned to left (" + assign.getRightOp().getType() + "->" +
						assign.getLeftOp().getType() + ")");
				state.setUnknown(leftLocal, id);
				if (!rightLocal.varType.equals(VarType.EXCLUDED)) {
					state.setToUnknown(rightLocal, id);
				}
			}
		} else if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
			ImmutableValue rightVal = SootValUtils.transformImmutableValue((Constant) assign.getRightOp(), id);
			if (SootTypeUtils.isBool(assign.getLeftOp().getType())) {
				// abstract a boolean assignment with an EmptyResult if possible
				if (rightVal instanceof IntValue) {
					boolean boolRight = !IntValue.create(0).equals(rightVal);
					if (!state.getMustHaveReturnedEmptyResults().isEmpty()) {
						if (boolRight) {
							rightVal = EmptyResultValue.create(state.getMustHaveReturnedEmptyResults().iterator().next());
						} else {
							rightVal = EmptyResultValue.create(state.getMustHaveReturnedEmptyResults().iterator().next())
									.setIsTrueWhenEmpty(false);
						}
					} else if (!state.getMustHaveReturnedNonEmptyResults().isEmpty()) {
						if (boolRight) {
							rightVal = EmptyResultValue.create(state.getMustHaveReturnedNonEmptyResults().iterator().next())
									.setIsTrueWhenEmpty(false);
						} else {
							rightVal = EmptyResultValue.create(state.getMustHaveReturnedNonEmptyResults().iterator().next());
						}
					}

				}
			}
			state.setLocalFromPhi(leftLocal, Collections.singleton(rightVal), id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignImmediatePoint(id, assign);
	}
}
