package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorArraysSimpleTest extends AbstractSootTest {

	public StmtExtractorArraysSimpleTest() throws IOException {
		super("ArraysSimple", "test1");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(2, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM test_1"));
		assertTrue(cqls.contains("SELECT * FROM test_1 WHERE col1 = 0 AND col2 = null"));
	}
}
