package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb;

import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.BindMarkerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBBindMarkerInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBBindMarkerInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBBindMarkerInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
                                                 final int methodId) {
		return new QBBindMarkerInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBBindMarkerInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
                                   final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == SootMethodUtils.QUERY_BUILDER_BINDMARKER) {
			if (leftLocal != null) {
				state.setImmutableValue(leftLocal, ImmutableReference.create(id), BindMarkerValue.createAnonymous(), id, oldState);
			}
			return true;
		} else if (methodId == SootMethodUtils.QUERY_BUILDER_BINDMARKER_NAME) {
			if (leftLocal != null) {
				final List<Object> targs = getArgs(args, invokeStmt.getInvokeExpr(), state);
				if (state.isBottom()) {
					// nop
				} else if (targs == null) {
					// one arg is unknown
					state.setUnknown(leftLocal, id);
				} else {
					state.setImmutableValue(leftLocal, ImmutableReference.create(id),
							ImmutableChoiceValue.create(id,
									transformSet(targs.get(0), StringValue.class, first -> BindMarkerValue.createNamed(first))),
							id, oldState);
				}
			}
			return true;
		}
		return false;
	}
}
