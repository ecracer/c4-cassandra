package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable;

import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ImmutableValueVisitor;
import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;

public class ImmutableChoiceValue extends AbstractImmutableValue implements ImmutableValue {

	public final ProgramPointId createdIn;
	public final ImmutableSet<ImmutableValue> choices;

	private ImmutableChoiceValue(final ProgramPointId createdIn, final ImmutableSet<ImmutableValue> choices) {
		this.createdIn = Objects.requireNonNull(createdIn);
		this.choices = Objects.requireNonNull(choices);
	}

	public static ImmutableValue create(final ProgramPointId createdIn, final Set<ImmutableValue> choices) {
		if (choices.isEmpty()) {
			throw new IllegalArgumentException();
		}
		if (choices.size() == 1) {
			return choices.iterator().next();
		}
		final ImmutableSet.Builder<ImmutableValue> choicesBuilder = ImmutableSet.builder();
		for (final ImmutableValue choice : choices) {
			if (choice instanceof ImmutableChoiceValue) {
				choicesBuilder.addAll(((ImmutableChoiceValue) choice).choices);
			} else {
				choicesBuilder.add(choice);
			}
		}
		final ImmutableSet<ImmutableValue> newChoices = choicesBuilder.build();
		for (final ImmutableValue val : newChoices) {
			if (val instanceof UnknownImmutableValue && ((UnknownImmutableValue) val).isWidened) {
				if (((UnknownImmutableValue) val).createdIn.equals(createdIn)) {
					return val;
				} else {
					return UnknownImmutableValue.create(createdIn);
				}
			}
		}
		return new ImmutableChoiceValue(createdIn, newChoices);
	}

	public boolean hasUnknownValue() {
		for (final ImmutableValue choice : choices) {
			if (choice instanceof UnknownImmutableValue) {
				return true;
			}
		}
		return false;
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitImmutableChoiceValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((choices == null) ? 0 : choices.hashCode());
		result = prime * result + ((createdIn == null) ? 0 : createdIn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImmutableChoiceValue other = (ImmutableChoiceValue) obj;
		if (choices == null) {
			if (other.choices != null)
				return false;
		} else if (!choices.equals(other.choices))
			return false;
		if (createdIn == null) {
			if (other.createdIn != null)
				return false;
		} else if (!createdIn.equals(other.createdIn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImmutableChoiceValue [createdIn=" + createdIn + ", choices=" + choices + "]";
	}
}
