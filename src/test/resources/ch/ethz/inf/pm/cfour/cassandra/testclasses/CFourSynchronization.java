

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import com.datastax.driver.core.Session;


public class CFourSynchronization {

	private final Session session;
	
	public CFourSynchronization(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void register(String username, String password){
		session.execute("SELECT username FROM users WHERE username = ?", username);
			session.execute("INSERT INTO users (username, password) VALUES (?, ?) IF NOT EXISTS", username, password);
	}
}
