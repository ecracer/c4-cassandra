package ch.ethz.inf.pm.cfour.cassandra.beans;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;

/**
 * Represents an execution of an event on Cassandra. The event is numbered so that it is unique. The statement is
 * parsed.
 */
public class ParsedSessionExecuteInvoke implements Comparable<ParsedSessionExecuteInvoke> {

	public final Method containingMethod;
	public final int number;
	public final boolean async;
	public final ParsedStatement statement;
	public final ConsistencyLevel consistencyLevel;
	public final ConsistencyLevel serialConsistencyLevel;

	private ParsedSessionExecuteInvoke(final Method containingMethod, final int number, final boolean async,
			final ParsedStatement statement, final ConsistencyLevel consistencyLevel, final ConsistencyLevel serialConsistencyLevel) {
		this.containingMethod = Objects.requireNonNull(containingMethod);
		this.number = number;
		this.async = async;
		this.statement = Objects.requireNonNull(statement);
		this.consistencyLevel = Objects.requireNonNull(consistencyLevel);
		this.serialConsistencyLevel = Objects.requireNonNull(serialConsistencyLevel);
	}

	public static ParsedSessionExecuteInvoke create(final Method containingMethod, final int number,
			final boolean async, final ParsedStatement stmt, final ConsistencyLevel consistencyLevel,
			final ConsistencyLevel serialConsistencyLevel) {
		return new ParsedSessionExecuteInvoke(containingMethod, number, async, stmt, consistencyLevel, serialConsistencyLevel);
	}

	public String getSignature() {
		return number + "_" + containingMethod.getShortName();
	}

	@Override
	public int compareTo(ParsedSessionExecuteInvoke o) {
		return ((Integer) number).compareTo(o.number);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (async ? 1231 : 1237);
		result = prime * result + ((consistencyLevel == null) ? 0 : consistencyLevel.hashCode());
		result = prime * result + ((containingMethod == null) ? 0 : containingMethod.hashCode());
		result = prime * result + number;
		result = prime * result + ((serialConsistencyLevel == null) ? 0 : serialConsistencyLevel.hashCode());
		result = prime * result + ((statement == null) ? 0 : statement.hashCode());
		return result;
	}

	public boolean equalsIgnoringStatementQueryAndNumber(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParsedSessionExecuteInvoke other = (ParsedSessionExecuteInvoke) obj;
		if (async != other.async)
			return false;
		if (consistencyLevel != other.consistencyLevel)
			return false;
		if (containingMethod == null) {
			if (other.containingMethod != null)
				return false;
		} else if (!containingMethod.equals(other.containingMethod))
			return false;
		if (serialConsistencyLevel != other.serialConsistencyLevel)
			return false;
		if (statement == null) {
			if (other.statement != null)
				return false;
		} else if (!statement.equalsIgnoringQuery(other.statement))
			return false;
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParsedSessionExecuteInvoke other = (ParsedSessionExecuteInvoke) obj;
		if (async != other.async)
			return false;
		if (consistencyLevel != other.consistencyLevel)
			return false;
		if (containingMethod == null) {
			if (other.containingMethod != null)
				return false;
		} else if (!containingMethod.equals(other.containingMethod))
			return false;
		if (number != other.number)
			return false;
		if (serialConsistencyLevel != other.serialConsistencyLevel)
			return false;
		if (statement == null) {
			if (other.statement != null)
				return false;
		} else if (!statement.equals(other.statement))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParsedSessionExecuteInvoke [stmt=" + statement + "]";
	}

	public static enum ConsistencyLevel {
		ALL, ANY, EACH_QUORUM, LOCAL_ONE, LOCAL_QUORUM, LOCAL_SERIAL, ONE, QUORUM, SERIAL, THREE, TWO, DEFAULT, UNKNOWN;

		public static ConsistencyLevel of(final ConsistencyLevelValue other) {
			return ConsistencyLevel.valueOf(other.level.toString());
		}
	}
}
