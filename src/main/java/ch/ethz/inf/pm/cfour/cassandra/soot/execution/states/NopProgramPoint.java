package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

/**
 * General marker
 */
public class NopProgramPoint extends AbstractProgramPoint {

	private NopProgramPoint(final ProgramPointId programPointId) {
		super(programPointId);
	}

	public static NopProgramPoint create(final CallStack callStack) {
		return new NopProgramPoint(ProgramPointId.create(callStack, new Object()));
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		// nop
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(final ProgramPointId id) {
		return new NopProgramPoint(id);
	}

	@Override
	public String toString() {
		return "NopProgramPoint [" + id.idInMethod + "]";
	}
}
