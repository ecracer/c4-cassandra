package ch.ethz.inf.pm.cfour.cassandra;

import ch.ethz.inf.pm.cfour.Parameters;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions;

public class Options extends SootOptions {

	private String graphOutputFolder = null;
	private String createSchemaFile = null;
	private String violationClassificationFile = null;
	private boolean basicAnalysisEnabled = true;
	private boolean cFourEnabled = true;
	private boolean encodeBatchStatementPermutations = false;
	private boolean valueAnalysisEnabled = true;
	private boolean pruneDisplayCodeEnabled = true;
	private boolean strictUpdatesEnabled = true;
	private boolean treatAllUpdatesAsStrict = false;
	private boolean clientLocalAreGlobalUnique = false;
	private boolean sideChannelsEnabled = false;
	private boolean poConstraintsEnabled = true;
	// cfour options
	private boolean resultGeneralizationCheckEnabled = false;
	private boolean staticSubsetMinimalityEnabled = false;
	private boolean globalAndEventConstraintsEnabled = true;
	private boolean absorptionEnabled = true;
	private boolean singlePOPathEnabled = true;
	private boolean commutativityEnabled = true;
	private boolean asymmetricCommutativityEnabled = true;
	private boolean synchronizingOperationsEnabled = true;
	private boolean legalitySpecEnabled = true;

	public String getGraphOutputFolder() {
		return graphOutputFolder;
	}

	public void setGraphOutputFolder(String graphOutputFolder) {
		this.graphOutputFolder = graphOutputFolder;
	}

	public String getCreateSchemaFile() {
		return createSchemaFile;
	}

	public void setCreateSchemaFile(String createSchemaFile) {
		this.createSchemaFile = createSchemaFile;
	}

	public String getViolationClassificationFile() {
		return violationClassificationFile;
	}

	public void setViolationClassificationFile(String violationClassificationFile) {
		this.violationClassificationFile = violationClassificationFile;
	}

	public boolean isBasicAnalysisEnabled() {
		return basicAnalysisEnabled;
	}

	public void setBasicAnalysisEnabled(boolean basicAnalysisEnabled) {
		this.basicAnalysisEnabled = basicAnalysisEnabled;
	}

	public boolean isCFourEnabled() {
		return cFourEnabled;
	}

	public void setCFourEnabled(boolean cFourEnabled) {
		this.cFourEnabled = cFourEnabled;
	}

	public boolean isEncodeBatchStatementPermutations() {
		return encodeBatchStatementPermutations;
	}

	public void setEncodeBatchStatementPermutations(boolean encodeBatchStatementPermutations) {
		this.encodeBatchStatementPermutations = encodeBatchStatementPermutations;
	}

	public boolean isValueAnalysisEnabled() {
		return valueAnalysisEnabled;
	}

	public void setValueAnalysisEnabled(boolean valueAnalysisEnabled) {
		this.valueAnalysisEnabled = valueAnalysisEnabled;
	}

	public boolean isPruneDisplayCodeEnabled() {
		return pruneDisplayCodeEnabled;
	}

	public void setPruneDisplayCodeEnabled(boolean pruneDisplayCodeEnabled) {
		this.pruneDisplayCodeEnabled = pruneDisplayCodeEnabled;
	}

	public boolean isStrictUpdatesEnabled() {
		return strictUpdatesEnabled;
	}

	public void setStrictUpdatesEnabled(boolean strictUpdatesEnabled) {
		this.strictUpdatesEnabled = strictUpdatesEnabled;
	}

	public boolean isTreatAllUpdatesAsStrict() {
		return treatAllUpdatesAsStrict;
	}

	public void setTreatAllUpdatesAsStrict(boolean treatAllUpdatesAsStrict) {
		this.treatAllUpdatesAsStrict = treatAllUpdatesAsStrict;
	}

	public boolean isClientLocalAreGlobalUnique() {
		return clientLocalAreGlobalUnique;
	}

	public void setClientLocalAreGlobalUnique(boolean clientLocalAreGlobalUnique) {
		this.clientLocalAreGlobalUnique = clientLocalAreGlobalUnique;
	}

	public boolean isSideChannelsEnabled() {
		return sideChannelsEnabled;
	}

	public void setSideChannelsEnabled(boolean sideChannelsEnabled) {
		this.sideChannelsEnabled = sideChannelsEnabled;
	}

	public boolean isPoConstraintsEnabled() {
		return poConstraintsEnabled;
	}

	public void setPoConstraintsEnabled(boolean poConstraintsEnabled) {
		this.poConstraintsEnabled = poConstraintsEnabled;
	}

	public boolean isResultGeneralizationCheckEnabled() {
		return resultGeneralizationCheckEnabled;
	}

	public void setResultGeneralizationCheckEnabled(boolean resultGeneralizationCheckEnabled) {
		this.resultGeneralizationCheckEnabled = resultGeneralizationCheckEnabled;
	}

	public boolean isStaticSubsetMinimalityEnabled() {
		return staticSubsetMinimalityEnabled;
	}

	public void setStaticSubsetMinimalityEnabled(boolean staticSubsetMinimalityEnabled) {
		this.staticSubsetMinimalityEnabled = staticSubsetMinimalityEnabled;
	}

	public boolean isGlobalAndEventConstraintsEnabled() {
		return globalAndEventConstraintsEnabled;
	}

	public void setGlobalAndEventConstraintsEnabled(boolean globalAndEventConstraintsEnabled) {
		this.globalAndEventConstraintsEnabled = globalAndEventConstraintsEnabled;
	}

	public boolean isAbsorptionEnabled() {
		return absorptionEnabled;
	}

	public void setAbsorptionEnabled(boolean absorptionEnabled) {
		this.absorptionEnabled = absorptionEnabled;
	}

	public boolean isSinglePOPathEnabled() {
		return singlePOPathEnabled;
	}

	public void setSinglePOPathEnabled(boolean singlePOPathEnabled) {
		this.singlePOPathEnabled = singlePOPathEnabled;
	}

	public boolean isCommutativityEnabled() {
		return commutativityEnabled;
	}

	public void setCommutativityEnabled(boolean commutativityEnabled) {
		this.commutativityEnabled = commutativityEnabled;
	}

	public boolean isAsymmetricCommutativityEnabled() {
		return asymmetricCommutativityEnabled;
	}

	public void setAsymmetricCommutativityEnabled(boolean asymmetricCommutativityEnabled) {
		this.asymmetricCommutativityEnabled = asymmetricCommutativityEnabled;
	}

	public boolean isSynchronizingOperationsEnabled() {
		return synchronizingOperationsEnabled;
	}

	public void setSynchronizingOperationsEnabled(boolean synchronizingOperationsEnabled) {
		this.synchronizingOperationsEnabled = synchronizingOperationsEnabled;
	}

	public boolean isLegalitySpecEnabled() {
		return legalitySpecEnabled;
	}

	public void setLegalitySpecEnabled(boolean legalitySpecEnabled) {
		this.legalitySpecEnabled = legalitySpecEnabled;
	}

	@Override
	public String toString() {
		return "Options [graphOutputFolder=" + graphOutputFolder + ", createSchemaFile=" + createSchemaFile
				+ ", violationClassificationFile=" + violationClassificationFile + ", basicAnalysisEnabled=" + basicAnalysisEnabled
				+ ", cFourEnabled=" + cFourEnabled + ", encodeBatchStatementPermutations=" + encodeBatchStatementPermutations
				+ ", valueAnalysisEnabled=" + valueAnalysisEnabled + ", pruneDisplayCodeEnabled=" + pruneDisplayCodeEnabled
				+ ", strictUpdatesEnabled=" + strictUpdatesEnabled + ", treatAllUpdatesAsStrict=" + treatAllUpdatesAsStrict
				+ ", clientLocalAreGlobalUnique=" + clientLocalAreGlobalUnique + ", sideChannelsEnabled=" + sideChannelsEnabled
				+ ", poConstraintsEnabled=" + poConstraintsEnabled + ", resultGeneralizationCheckEnabled="
				+ resultGeneralizationCheckEnabled + ", staticSubsetMinimalityEnabled=" + staticSubsetMinimalityEnabled
				+ ", globalAndEventConstraintsEnabled=" + globalAndEventConstraintsEnabled + ", absorptionEnabled=" + absorptionEnabled
				+ ", singlePOPathEnabled=" + singlePOPathEnabled + ", commutativityEnabled=" + commutativityEnabled
				+ ", asymmetricCommutativityEnabled=" + asymmetricCommutativityEnabled + ", synchronizingOperationsEnabled="
				+ synchronizingOperationsEnabled + ", legalitySpecEnabled=" + legalitySpecEnabled + ", toString()=" + super.toString()
				+ "]";
	}

	public Parameters toParam() {

		Parameters p = new Parameters(
				false,
				false,
				isResultGeneralizationCheckEnabled(),
				false,
				isStaticSubsetMinimalityEnabled(),
				isGlobalAndEventConstraintsEnabled(),
				isAbsorptionEnabled(),
				isSinglePOPathEnabled(),
				isCommutativityEnabled(),
				isAsymmetricCommutativityEnabled(),
				true,
				false,
				isLegalitySpecEnabled(),
				true,
				isSynchronizingOperationsEnabled(),
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				true,
				true,
				true,
				true,
				false,
				scala.Option.apply(null),
				scala.Option.apply(null)
		);
		return p;
	}
}
