package ch.ethz.inf.pm.cfour.cassandra.soot.reference.visitor;

import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ObjectReference;

public class AbstractReferenceVisitor<R> implements ReferenceVisitor<R> {

	@Override
	public R visit(AbstractReference reference) {
		return reference.apply(this);
	}

	protected R visitDefault(AbstractReference reference) {
		return null;
	}

	@Override
	public R visitImmutableReference(ImmutableReference immutableReference) {
		return visitDefault(immutableReference);
	}

	@Override
	public R visitBaseReference(BaseReference baseReference) {
		return visitDefault(baseReference);
	}

	@Override
	public R visitArrayReference(ArrayReference arrayReference) {
		return visitDefault(arrayReference);
	}

	@Override
	public R visitObjectReference(ObjectReference objectReference) {
		return visitDefault(objectReference);
	}
}
