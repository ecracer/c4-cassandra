

import com.datastax.driver.core.Session;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.ConsistencyLevel;

public class BatchStatementLoop {
	
	public void test(Session session) {
		final BatchStatement stmt = new BatchStatement();
		for (int i=0; i<20; i++){
			stmt.add(new SimpleStatement("INSERT INTO table1 (key) VALUES (?)", i));
		}
		session.execute(stmt);
	}
}