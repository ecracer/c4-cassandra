package ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts;

import java.util.List;
import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import com.google.common.collect.ImmutableList;

/**
 * Base class for the different constraints (e.g. =, IN)
 */
public abstract class Constraint {

	// If new classes are added --> Check GraphBuilder & CommutativityAbsorptionExprGenerator

	public final String column;

	protected Constraint(final String column) {
		this.column = Objects.requireNonNull(column);
	}

	public abstract String getLabel();

	public abstract ConcreteValue getValue();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column == null) ? 0 : column.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Constraint other = (Constraint) obj;
		if (column == null) {
			if (other.column != null)
				return false;
		} else if (!column.equals(other.column))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	public static class EQConstraint extends Constraint {

		private EQConstraint(final String column) {
			super(column);
		}

		public static EQConstraint create(final String column) {
			return new EQConstraint(column);
		}

		@Override
		public String getLabel() {
			return column + "_EQ_" + "?";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}
	}

	public static class EQValConstraint extends EQConstraint {

		public final ConcreteValue value;

		private EQValConstraint(final String column, final ConcreteValue value) {
			super(column);
			this.value = Objects.requireNonNull(value);
		}

		public static EQValConstraint create(final String column, final ConcreteValue value) {
			return new EQValConstraint(column, value);
		}

		@Override
		public String getLabel() {
			return column + "_EQ_" + Math.abs(value.hashCode());
		}

		@Override
		public ConcreteValue getValue() {
			return value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			EQValConstraint other = (EQValConstraint) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}

	public static class INConstraint extends Constraint {

		private INConstraint(final String column) {
			super(column);
		}

		public static INConstraint create(final String column) {
			return new INConstraint(column);
		}

		@Override
		public String getLabel() {
			return column + "_IN_" + "?";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}
	}

	public static class INValsConstraint extends INConstraint {

		public final ImmutableList<ConcreteValue> values;

		private INValsConstraint(final String column, final ImmutableList<ConcreteValue> values) {
			super(column);
			this.values = Objects.requireNonNull(values);
		}

		public static INValsConstraint create(final String column, final List<ConcreteValue> values) {
			return new INValsConstraint(column, ImmutableList.copyOf(values));
		}

		@Override
		public String getLabel() {
			return column + "_IN_" + Math.abs(values.hashCode());
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((values == null) ? 0 : values.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			INValsConstraint other = (INValsConstraint) obj;
			if (values == null) {
				if (other.values != null)
					return false;
			} else if (!values.equals(other.values))
				return false;
			return true;
		}
	}

	public static class UnknownConstraint extends Constraint {

		private UnknownConstraint(final String column) {
			super(column);
		}

		public static UnknownConstraint create(final String column) {
			return new UnknownConstraint(column);
		}

		@Override
		public String getLabel() {
			return column + "_UNK";
		}

		@Override
		public ConcreteValue getValue() {
			return null;
		}
	}
}