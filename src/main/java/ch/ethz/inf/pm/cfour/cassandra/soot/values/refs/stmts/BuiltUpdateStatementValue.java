package ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;

public class BuiltUpdateStatementValue extends AbstractBuiltStatementValue {

	public final String table;
	public final ImmutableList<String> assignments;
	public final ImmutableList<String> clauses;
	public final ImmutableList<String> conditions;
	public final boolean ifExists;

	private BuiltUpdateStatementValue(final String table) {
		super(ImmutableList.of());
		this.table = Objects.requireNonNull(table);
		this.assignments = ImmutableList.of();
		this.clauses = ImmutableList.of();
		this.conditions = ImmutableList.of();
		this.ifExists = false;
	}

	public static BuiltUpdateStatementValue create(final StringValue keyspace, final StringValue table) {
		if (keyspace != null) {
			return new BuiltUpdateStatementValue(keyspace.string + "." + table.string);
		} else {
			return new BuiltUpdateStatementValue(table.string);
		}
	}

	private BuiltUpdateStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
                                      final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel, final String table,
                                      final ImmutableList<String> assignments, final ImmutableList<String> clauses, final ImmutableList<String> conditions,
                                      final boolean ifExists) {
		super(binds, consistencyLevel, serialConsistencyLevel);
		this.table = Objects.requireNonNull(table);
		this.assignments = Objects.requireNonNull(assignments);
		this.clauses = Objects.requireNonNull(clauses);
		this.conditions = Objects.requireNonNull(conditions);
		this.ifExists = ifExists;
	}

	public BuiltUpdateStatementValue addAssignment(final BuiltStatementAssignmentValue assignment) {
		final String[] bindMarkers = new String[assignment.values.size()];
		final ImmutableList.Builder<AbstractBind<VarLocalOrImmutableValue>> builder = ImmutableList.builder();
		builder.addAll(binds);
		for (int i = 0; i < bindMarkers.length; i++) {
			final String bindMarker = getNewBindName(i);
			bindMarkers[i] = ":" + bindMarker;
			builder.add(AbstractBind.SystemBind.create(bindMarker, assignment.values.get(i)));
		}
		return new BuiltUpdateStatementValue(
				builder.build(),
				consistencyLevel,
				serialConsistencyLevel,
				table,
				ImmutableList.<String> builder().addAll(assignments).add(assignment.getAssignmentWithNamedBinds(bindMarkers)).build(),
				clauses,
				conditions,
				ifExists);
	}

	public BuiltUpdateStatementValue addWhere(final BuiltStatementClauseValue clause) {
		final String bindMarker = getNewBindName();
		return new BuiltUpdateStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds)
						.add(AbstractBind.SystemBind.create(bindMarker, clause.value)).build(),
				consistencyLevel,
				serialConsistencyLevel,
				table,
				assignments,
				ImmutableList.<String> builder().addAll(clauses).add(clause.getClauseWithNamedBind(":" + bindMarker)).build(),
				conditions,
				ifExists);
	}

	public BuiltUpdateStatementValue addCondition(final BuiltStatementClauseValue clause) {
		final String bindMarker = getNewBindName();
		return new BuiltUpdateStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds)
						.add(AbstractBind.SystemBind.create(bindMarker, clause.value)).build(),
				consistencyLevel,
				serialConsistencyLevel,
				table,
				assignments,
				clauses,
				ImmutableList.<String> builder().addAll(conditions).add(clause.getClauseWithNamedBind(":" + bindMarker)).build(),
				ifExists);
	}

	public BuiltUpdateStatementValue setIfExists(final boolean ifExists) {
		return new BuiltUpdateStatementValue(binds, consistencyLevel, serialConsistencyLevel, table, assignments, clauses, conditions,
				ifExists);
	}

	@Override
	protected AbstractBuiltStatementValue createNewInternal(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		return new BuiltUpdateStatementValue(binds, consistencyLevel, serialConsistencyLevel, table, assignments, clauses, conditions,
				ifExists);
	}

	@Override
	public String getStatement() {
		final StringBuilder sb = new StringBuilder("UPDATE ");
		sb.append(table);
		if (!assignments.isEmpty()) {
			sb.append(" SET ");
			sb.append(String.join(", ", assignments));
		}
		if (!clauses.isEmpty()) {
			sb.append(" WHERE ");
			sb.append(String.join(" AND ", clauses));
		}
		if (!conditions.isEmpty()) {
			sb.append(" IF ");
			sb.append(String.join(" AND ", conditions));
		}
		if (ifExists) {
			sb.append(" IF EXISTS");
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assignments == null) ? 0 : assignments.hashCode());
		result = prime * result + ((clauses == null) ? 0 : clauses.hashCode());
		result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
		result = prime * result + (ifExists ? 1231 : 1237);
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltUpdateStatementValue other = (BuiltUpdateStatementValue) obj;
		if (assignments == null) {
			if (other.assignments != null)
				return false;
		} else if (!assignments.equals(other.assignments))
			return false;
		if (clauses == null) {
			if (other.clauses != null)
				return false;
		} else if (!clauses.equals(other.clauses))
			return false;
		if (conditions == null) {
			if (other.conditions != null)
				return false;
		} else if (!conditions.equals(other.conditions))
			return false;
		if (ifExists != other.ifExists)
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}
}
