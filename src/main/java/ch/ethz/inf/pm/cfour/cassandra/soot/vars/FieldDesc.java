package ch.ethz.inf.pm.cfour.cassandra.soot.vars;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;

public class FieldDesc {

	public final String className;
	public final String fieldName;
	public final boolean isStatic;
	public final VarType varType;
	public final ImmutableValue defaultValue;
	// if a field has an annotation, it can be filled from anywhere (e.g. @Autowired from Spring-Framework). 
	// In this case, the field is not included in the analysis (i.e. it is set to unknown) 
	public final boolean hasAnnotation;

	private FieldDesc(final String className, final String fieldName, final boolean isStatic, final VarType varType,
			final ImmutableValue defaultValue, final boolean hasAnnotation) {
		this.className = Objects.requireNonNull(className);
		this.fieldName = Objects.requireNonNull(fieldName);
		this.isStatic = isStatic;
		this.varType = Objects.requireNonNull(varType);
		this.defaultValue = Objects.requireNonNull(defaultValue);
		this.hasAnnotation = hasAnnotation;
	}

	public static FieldDesc create(final String className, final String fieldName, final boolean isStatic, final VarType varType,
			final ImmutableValue defaultValue, final boolean hasAnnotation) {
		return new FieldDesc(className, fieldName, isStatic, varType, defaultValue, hasAnnotation);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + (hasAnnotation ? 1231 : 1237);
		result = prime * result + (isStatic ? 1231 : 1237);
		result = prime * result + ((varType == null) ? 0 : varType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldDesc other = (FieldDesc) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (hasAnnotation != other.hasAnnotation)
			return false;
		if (isStatic != other.isStatic)
			return false;
		if (varType != other.varType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return (isStatic ? "static " : "") + className + " " + fieldName;
	}
}
