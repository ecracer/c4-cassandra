package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisSingleTransactionTest extends AbstractSootTest {

	public AnalysisSingleTransactionTest() throws IOException {
		super("SingleTransaction", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final Options options = new Options();

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(options)
				.transform(executeGraphs);

		assertEquals(1, executeGraphs.size());

		final SchemaInformation schema = SchemaInformation.create();

		int violations = new CFourSerializabilityAnalysis(options).check(transactionExecuteGraphs, schema)
				.getTotalVerifiedViolationsSize4();
		assertEquals(1, violations);
	}
}
