package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.AssignStmt;
import soot.jimple.CastExpr;
import soot.jimple.Constant;

/**
 * Transformer for an assignment having a cast on the right side
 */
public class AssignCastExprPoint extends AbstractAssignProgramPoint {

	private AssignCastExprPoint(final ProgramPointId id, final AssignStmt assign) {
		super(id, assign);
		if (!(assign.getLeftOp() instanceof Local) || !(assign.getRightOp() instanceof CastExpr)) {
			throw new IllegalArgumentException();
		}
	}

	public static AssignCastExprPoint create(final CallStack callStack, final AssignStmt assign) {
		return new AssignCastExprPoint(ProgramPointId.create(callStack, assign), assign);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		final VarLocal leftLocal = SootValUtils.transformLocal((Local) assign.getLeftOp(), id.callStack);
		final CastExpr cast = (CastExpr) assign.getRightOp();
		if (cast.getOp() instanceof Local) {
			final VarLocal rightLocal = SootValUtils.transformLocal((Local) cast.getOp(), id.callStack);
			if (rightLocal.varType.equals(VarType.EXCLUDED)) {
				if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
					state.setUnknown(leftLocal, id);
				}
				return;
			} else if (!state.isLocalInitialized(rightLocal)) {
				state.setToBottom();
				return;
			} else if (leftLocal.varType.equals(VarType.EXCLUDED)) {
				state.setToUnknown(rightLocal, id);
				return;
			} else if (VarType.isAssignableTo(leftLocal.varType, rightLocal.varType)) {
				state.setLocalFromOther(leftLocal, rightLocal);
			} else {
				boolean setToUnknown = false;
				if (leftLocal.varType.equals(VarType.IMMUTABLE)) {
					final Set<ImmutableValue> immutableValues = new HashSet<>();
					for (final AbstractReference rightRef : state.getPointsTo(rightLocal)) {
						if (rightRef instanceof ImmutableReference) {
							immutableValues.add(state.getImmutableValue((ImmutableReference) rightRef));
						} else {
							setToUnknown = true;
							break;
						}
					}
					if (!setToUnknown) {
						state.setLocalFromPhi(leftLocal, immutableValues, id);
					}
				} else {
					setToUnknown = true;
				}
				if (setToUnknown) {
					state.setUnknown(leftLocal, id);
					if (!rightLocal.varType.equals(VarType.EXCLUDED)) {
						state.setToUnknown(rightLocal, id);
					}
				}
			}
		} else if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
			final ImmutableValue rightVal = SootValUtils.transformImmutableValue((Constant) cast.getOp(), id);
			state.setLocalFromPhi(leftLocal, Collections.singleton(rightVal), id);
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AssignCastExprPoint(id, assign);
	}
}
