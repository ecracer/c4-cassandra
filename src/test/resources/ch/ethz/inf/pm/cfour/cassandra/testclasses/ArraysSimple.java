

import com.datastax.driver.core.Session;

public class ArraysSimple {
	
	public void test1(Session session) {
		final int[][] keys = new int[2][];
		keys[0] = new int[1];
		final String[] queries = new String[3];
		queries[0] = "SELECT * FROM test_1";
		queries[2] = "WHERE " + "col1" + " = " + keys[0][0] + " AND col2 = " + keys[1];
		queries[1] = queries[0] + " " + queries[2];
		session.execute(queries[0]);
		session.execute(queries[1]);
	}
}