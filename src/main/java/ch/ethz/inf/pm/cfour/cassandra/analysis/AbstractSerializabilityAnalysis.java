package ch.ethz.inf.pm.cfour.cassandra.analysis;

import java.io.File;
import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.Options;

public abstract class AbstractSerializabilityAnalysis {

	protected final Options options;
	protected final File outputDir;

	protected AbstractSerializabilityAnalysis(final Options options) {
		this.options = Objects.requireNonNull(options);
		if (options.getGraphOutputFolder() != null) {
			final File baseFile = new File(options.getGraphOutputFolder());
			if (!baseFile.exists()) {
				baseFile.mkdirs();
			}
			outputDir = baseFile;
		} else {
			outputDir = null;
		}
	}
}
