package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.EmptyResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.BindMarkerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StatementSingleResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValueContainerValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;

public interface ImmutableValueVisitor<R> {

	R visit(final ImmutableValue value);

	R visitBindMarkerValue(final BindMarkerValue bindMarkerValue);

	R visitConsistencyLevelValue(final ConsistencyLevelValue concistencyLevelValue);

	R visitImmutableChoiceValue(final ImmutableChoiceValue immutableChoiceValue);

	R visitIntValue(final IntValue intValue);

	R visitEmptyResultValue(final EmptyResultValue booleanValue);

	R visitNullValue(final NullValue nullValue);

	R visitStringValue(final StringValue stringValue);

	R visitStringValueContainer(final StringValueContainerValue stringValueContainerValue);

	R visitUnknownImmutableValue(final UnknownImmutableValue unknownImmutableValue);

	R visitUUIDValue(final UUIDValue uuidValue);

	R visitClientImmutableValue(final ClientImmutableValue value);

	R visitStatementSingleResultValue(final StatementSingleResultValue value);
}
