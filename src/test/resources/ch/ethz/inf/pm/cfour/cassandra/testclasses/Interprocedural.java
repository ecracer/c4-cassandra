

import com.datastax.driver.core.Session;


public class Interprocedural {

	public void test1(final Session session, int k){
		final String[] queryParts = new String[2];
		queryParts[0] = getQuery(k);
		queryParts[1] = "k = 1";
		fillArg(queryParts);
		session.execute(queryParts[0] + queryParts[1]);
	}
	
	private String getQuery(int k){
		if (k > 100){			
			return "SELECT * FROM test1 WHERE ";
		} else {
			return "SELECT * FROM test2 WHERE ";
		}
	}
	
	private void fillArg(String[] queryParts){
		for (int i=50; i<100; i++){
			queryParts[1] = "k = " + i;
		}
	}
}
