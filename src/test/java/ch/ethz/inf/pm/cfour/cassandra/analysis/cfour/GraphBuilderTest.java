package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.FALSE;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.TRUE;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.and;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.boolInsertsNewRowsArgLeft;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.boolUpdatesExistingRowsArgLeft;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.intArgLeft;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.intArgRight;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.not;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.unequal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.Encoder;
import ch.ethz.inf.pm.cfour.Main;
import ch.ethz.inf.pm.cfour.Unfolder;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnCounterUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate.SetOp;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnUnknownValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.INConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.INValsConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.UnknownConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.cfour.output.Statistics;

public class GraphBuilderTest {

	@Test
	public void testSelectSelect() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create());
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);

		graphBuilder.addEvent(select, "q1");
		graphBuilder.addEvent(select, "q2");
		graphBuilder.addTransactionOrderEdge("q1", "q2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testSelectUpdate() {
		final UnknownImmutableValue key = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, "1"));
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("table1",
				ImmutableList.of(ColumnValueUpdate.create("key", key), ColumnUnknownValueUpdate.create("val")), false);
		final GraphBuilder graphBuilder = new GraphBuilder(
				SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "val").build(), select,
				insert);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(select, "t1"),
				graphBuilder.addEvent(insert, "t1"));

		assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build(), new Options().toParam()).size());
	}

	@Test
	public void testSelectUpdateDisplayCols() {
		final UnknownImmutableValue key = UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, "1"));
		final StatementPart.QueryPart select = StatementPart.SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)))
				.setDisplayColumns(Collections.singleton("set_value"));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnSetUpdate.create("set_value", SetOp.ADD)), ImmutableList.of(EQValConstraint.create("key", key)),
				true, true);
		final GraphBuilder graphBuilder = new GraphBuilder(
				SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "set_value").build(),
				select,
				insert);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(select, "t1"),
				graphBuilder.addEvent(insert, "t1"));

		assertEquals(0, Unfolder.unfoldAndCheck(graphBuilder.build(), new Options().toParam()).size());
	}

	@Test
	public void testSelectUpdateUsingUUIDs() {
		for (int i = 0; i < 2; i++) {
			final UUIDValue key = i == 0
					? UUIDValue.createRandom(ProgramPointId.unique())
					: UUIDValue.createFromString(ProgramPointId.unique());
			final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create(EQValConstraint.create("key", key)));
			final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("table1",
					ImmutableList.of(ColumnValueUpdate.create("key", key), ColumnUnknownValueUpdate.create("val")), false);
			final GraphBuilder graphBuilder = new GraphBuilder(
					SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").addNonPrimaryKeyColumn("table1", "val").build(),
					select,
					insert);

			graphBuilder.addProgramOrderEdge(
					graphBuilder.addEvent(select, "t1"),
					graphBuilder.addEvent(insert, "t1"));

			assertEquals(i, Unfolder.unfoldAndCheck(graphBuilder.build(), new Options().toParam()).size());
		}
	}

	@Test
	public void testUpdateSelect() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create());
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select, update), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t1"),
				graphBuilder.addEvent(select, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t2"),
				graphBuilder.addEvent(select, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isDefined());
	}

	@Test
	public void testUpdateSelectUsingEmptyIN() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create(INValsConstraint.create("key", ImmutableList.of())));
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);
		assertEquals(graphBuilder.getCommutativityExpr(select, select), TRUE);
		assertEquals(graphBuilder.getCommutativityExpr(select, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t1"),
				graphBuilder.addEvent(select, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update, "t2"),
				graphBuilder.addEvent(select, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testSelectUpdateDifferentTransaction() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create());
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);

		graphBuilder.addEvent(update, "tu1");
		graphBuilder.addEvent(update, "tu2");
		graphBuilder.addEvent(select, "tq1");
		graphBuilder.addEvent(select, "tq2");

		graphBuilder.addTransactionOrderEdge("tu1", "tq1");
		graphBuilder.addTransactionOrderEdge("tu2", "tq2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isDefined());
	}

	@Test
	public void testSelectUpdateDifferentTables() {
		final StatementPart.SelectAllPart select1 = StatementPart.SelectAllPart.create("table1", Constraints.create());
		final StatementPart.UpsertPart update1 = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final StatementPart.SelectAllPart select2 = StatementPart.SelectAllPart.create("table2", Constraints.create());
		final StatementPart.UpdatePart update2 = StatementPart.UpsertPart.createUpdate("table2", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select1, update1, select2, update2);

		assertEquals(graphBuilder.getCommutativityExpr(select1, select2), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update1), TRUE);
		assertEquals(graphBuilder.getCommutativityExpr(select1, update2), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update1, update1), TRUE);
		assertNotEquals(graphBuilder.getAbsorptionExpr(update2, update2), TRUE);
		assertEquals(graphBuilder.getAbsorptionExpr(update1, update2), FALSE);
		assertEquals(graphBuilder.getAbsorptionExpr(update2, update1), FALSE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select1, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(select2, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testSelectUpdateDifferentRows() {
		final StatementPart.SelectAllPart select1 = StatementPart.SelectAllPart.create("table1",
				Constraints.create(EQValConstraint.create("key", StringValue.create("str1"))));
		final StatementPart.UpsertPart update1 = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(EQValConstraint.create("key", StringValue.create("str1"))), false, false);
		final StatementPart.SelectAllPart select2 = StatementPart.SelectAllPart.create("table1",
				Constraints.create(EQValConstraint.create("key", StringValue.create("str2"))));
		final StatementPart.UpsertPart update2 = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(EQValConstraint.create("key", StringValue.create("str2"))), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").build(),
				select1, update1, select2, update2);

		assertEquals(graphBuilder.getCommutativityExpr(select1, select2), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update1), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(select1, update2), unequal(intArgLeft(1), intArgRight(1)));
		assertEquals(graphBuilder.getAbsorptionExpr(update1, update2),
				and(not(boolInsertsNewRowsArgLeft()), not(boolUpdatesExistingRowsArgLeft())));
		assertEquals(graphBuilder.getAbsorptionExpr(update2, update1),
				and(not(boolInsertsNewRowsArgLeft()), not(boolUpdatesExistingRowsArgLeft())));

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select1, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(select2, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testInsertUpdate() {
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("key"), ColumnUnknownValueUpdate.create("col1")), false);
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.builder().addPrimaryKeyColumn("table1", "key").build(), insert,
				update);

		assertNotEquals(graphBuilder.getCommutativityExpr(insert, insert), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(insert, update), TRUE);
		assertNotEquals(graphBuilder.getCommutativityExpr(update, update), TRUE);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(insert, "t1"),
				graphBuilder.addEvent(update, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(insert, "t2"),
				graphBuilder.addEvent(update, "t2"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testCircularDependencies() {
		final StatementPart.UpsertPart update1 = StatementPart.UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final StatementPart.UpsertPart update2 = StatementPart.UpsertPart.createUpdate("table2",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final StatementPart.UpsertPart update3 = StatementPart.UpsertPart.createUpdate("table3",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);

		final StatementPart.SelectAllPart select1 = StatementPart.SelectAllPart.create("table1", Constraints.create(UnknownConstraint.create("key")));
		final StatementPart.SelectAllPart select2 = StatementPart.SelectAllPart.create("table2", Constraints.create(UnknownConstraint.create("key")));

		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), update1, update2, update3, select1, select2);

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select2, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(update3, "t2"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update3, "t3"),
				graphBuilder.addEvent(select1, "t3"));

		graphBuilder.addTransactionOrderEdge("t1", "t2");

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isDefined());
	}

	@Test
	public void testCircularDependenciesUsingCounter() {
		final StatementPart.UpsertPart update1 = StatementPart.UpsertPart.createUpdate("table1",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final StatementPart.UpsertPart update2 = StatementPart.UpsertPart.createUpdate("table2",
				ImmutableList.of(ColumnUnknownValueUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);
		final StatementPart.UpsertPart update3 = StatementPart.UpsertPart.createUpdate("table3",
				ImmutableList.of(ColumnCounterUpdate.create("val")), ImmutableList.of(UnknownConstraint.create("key")), false, false);

		final StatementPart.SelectAllPart select1 = StatementPart.SelectAllPart.create("table1", Constraints.create(UnknownConstraint.create("key")));
		final StatementPart.SelectAllPart select2 = StatementPart.SelectAllPart.create("table2", Constraints.create(UnknownConstraint.create("key")));

		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), update1, update2, update3, select1, select2);

		assertEquals(TRUE, graphBuilder.getCommutativityExpr(update3, update3));

		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update1, "t1"),
				graphBuilder.addEvent(select2, "t1"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update2, "t2"),
				graphBuilder.addEvent(update3, "t2"));
		graphBuilder.addProgramOrderEdge(
				graphBuilder.addEvent(update3, "t3"),
				graphBuilder.addEvent(select1, "t3"));

		assertTrue(Encoder.findViolations(graphBuilder.build(), ScalaUtils.emptyOption(), new Options().toParam()).isEmpty());
	}

	@Test
	public void testUsingUnfolder() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("table1", Constraints.create());
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("table1", Collections.singletonList(ColumnUnknownValueUpdate.create("col1")),
				Collections.singletonList(UnknownConstraint.create("key")), false, false);
		final GraphBuilder graphBuilder = new GraphBuilder(SchemaInformation.create(), select, update);

		graphBuilder.addEvent(update, "t1");
		graphBuilder.addEvent(select, "t2");

		graphBuilder.addTransactionOrderEdge("t1", "t1");
		graphBuilder.addTransactionOrderEdge("t1", "t2");
		graphBuilder.addTransactionOrderEdge("t2", "t1");
		graphBuilder.addTransactionOrderEdge("t2", "t2");

		assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build(), new Options().toParam()).size());
	}

	@Test
	public void testAsymetric() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("users", Constraints.create(EQConstraint.create("username")));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("users",
				ImmutableList.of(ColumnUnknownValueUpdate.create("username"), ColumnUnknownValueUpdate.create("rooms")),
				true);
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("users", Collections.singletonList(ColumnUnknownValueUpdate.create("rooms")),
				Collections.singletonList(EQConstraint.create("username")), false, false).setCanInsert(false);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").build(), select, insert, update);

			graphBuilder.addEvent(update, "t1");
			graphBuilder.addProgramOrderEdge(graphBuilder.addEvent(select, "t2"), graphBuilder.addEvent(insert, "t2"));

			options.setAsymmetricCommutativityEnabled(i == 0);
			options.setSynchronizingOperationsEnabled(false);
			assertEquals(1 + i, Unfolder.unfoldAndCheck(graphBuilder.build(),options.toParam()).size());
		}
	}

	@Test
	public void testAsymetricUUIDs() {
		// Select cannot know random UUID created in the insert
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("tracks", Constraints.create(INConstraint.create("id")));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("tracks",
				ImmutableList.of(ColumnValueUpdate.create("id", UUIDValue.createRandom(ProgramPointId.unique())),
						ColumnUnknownValueUpdate.create("genre")),
				false);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			options.setSideChannelsEnabled(i != 0);
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("tracks", "id").addNonPrimaryKeyColumn("tracks", "genre").build(),
					insert, select);

			graphBuilder.addEvent(insert, "t1");
			graphBuilder.addEvent(select, "t2");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			assertEquals(i, Unfolder.unfoldAndCheck(graphBuilder.build(), options.toParam()).size());
		}
	}

	@Test
	public void testAsymetricUUIDs2() {
		for (int i = 0; i <= 1; i++) {
			final StatementPart.SelectAllPart select;
			if (i == 0) {
				//Could be range query which may include new track
				select = StatementPart.SelectAllPart.create("tracks", Constraints.create(UnknownConstraint.create("id")));
			} else {
				//Could also include new track
				select = StatementPart.SelectAllPart.create("tracks", Constraints.create(EQConstraint.create("genre")));
			}
			final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("tracks",
					ImmutableList.of(ColumnValueUpdate.create("id", UUIDValue.createRandom(ProgramPointId.unique())),
							ColumnUnknownValueUpdate.create("genre")),
					false);

			final Options options = new Options();
			options.setSideChannelsEnabled(false);
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("tracks", "id").addNonPrimaryKeyColumn("tracks", "genre").build(),
					insert, select);

			graphBuilder.addEvent(insert, "t1");
			graphBuilder.addEvent(select, "t2");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			assertEquals(1, Unfolder.unfoldAndCheck(graphBuilder.build(), options.toParam()).size());
		}
	}

	@Test
	public void testSynchronizingOperations() {
		final StatementPart.SelectAllPart select = StatementPart.SelectAllPart.create("users",
				Constraints.create(EQValConstraint.create("username", ClientImmutableValue.create("username"))));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("users",
				ImmutableList.of(ColumnValueUpdate.create("username", ClientImmutableValue.create("username")),
						ColumnUnknownValueUpdate.create("rooms")),
				true);
		final StatementPart.UpsertPart update = StatementPart.UpsertPart.createUpdate("users", ImmutableList.of(ColumnUnknownValueUpdate.create("rooms")),
				ImmutableList.of(EQConstraint.create("username")),
				true, true);

		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "rooms")
							.addTableWithoutDelete("users").build(),
					insert, select, update);

			graphBuilder.addEvent(update, "t1");
			graphBuilder.addProgramOrderEdge(
					graphBuilder.addEvent(select, "t2"),
					graphBuilder.addEvent(insert, "t2"));

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");

			options.setSynchronizingOperationsEnabled(i==0);

			assertEquals(i * 2, Unfolder.unfoldAndCheck(graphBuilder.build(), options.toParam()).size());
		}
	}

	@Test
	public void testInsertIfNotExists() {
		final StatementPart.SelectColsPart select = StatementPart.SelectColsPart.create("users", ImmutableList.of(ColumnSelection.create("username")),
				Constraints.create(EQValConstraint.create("username", ClientImmutableValue.create("username"))));
		final StatementPart.UpsertPart insert = StatementPart.UpsertPart.createInsert("users",
				ImmutableList.of(ColumnValueUpdate.create("username", ClientImmutableValue.create("username")),
						ColumnUnknownValueUpdate.create("password")),
				true);
		final StatementPart.UpsertPart updateDifferent = StatementPart.UpsertPart.createUpdate("users", ImmutableList.of(ColumnUnknownValueUpdate.create("room_names")),
				ImmutableList.of(EQValConstraint.create("username", ClientImmutableValue.create("username"))),
				false, false);

		for (int i = 0; i < 2; i++) {
			final Options options = new Options();
			final GraphBuilder graphBuilder = new GraphBuilder(options,
					SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "password")
							.addNonPrimaryKeyColumn("users", "room_names").addTableWithoutDelete("users").build(),
					insert, select, updateDifferent);

			graphBuilder.addEvent(updateDifferent, "t1");
			graphBuilder.addEvent(select, "t2");
			graphBuilder.addEvent(insert, "t3");

			graphBuilder.addTransactionOrderEdge("t1", "t1");
			graphBuilder.addTransactionOrderEdge("t1", "t2");
			graphBuilder.addTransactionOrderEdge("t1", "t3");
			graphBuilder.addTransactionOrderEdge("t2", "t1");
			graphBuilder.addTransactionOrderEdge("t2", "t2");
			graphBuilder.addTransactionOrderEdge("t2", "t3");
			graphBuilder.addTransactionOrderEdge("t3", "t1");
			graphBuilder.addTransactionOrderEdge("t3", "t2");
			graphBuilder.addTransactionOrderEdge("t3", "t3");

			options.setAsymmetricCommutativityEnabled(i==0);
			options.setSynchronizingOperationsEnabled(false);

			Unfolder.unfoldAndCheck(graphBuilder.build(), options.toParam());
			assertEquals(3 * i, Statistics.totalVerifiedViolationsSize4());
		}
	}
}
