package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ConcreteValueVisitor;

public interface ConcreteValue extends Value {

	<R> R apply(final ConcreteValueVisitor<R> visitor);

	ConcreteValue widenWith(final Value newValue, final ProgramPointId updatedFrom);
}
