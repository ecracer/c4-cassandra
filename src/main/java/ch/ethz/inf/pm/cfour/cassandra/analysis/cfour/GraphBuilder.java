package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.and;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.event;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.graph;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.or;
import static ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourFactory.poEdge;

import java.util.*;

import ch.ethz.inf.pm.cfour.*;
import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ClientImmutableValue;

/**
 * Helper class for transforming the transaction graphs to a graph that can be used when calling CFour. System
 * Specification is built on construction. Afterwards, events and edges can be added using the methods.
 */
public class GraphBuilder {

	private static final Logger LOG = LogManager.getLogger(GraphBuilder.class);

	private final Options options;
	private final VariableProvider varProvider;
	private final ClientLocalVar clientIdVar;
	private final ImmutableList<Operation> operations;
	private final ImmutableBiMap<StatementPart, String> partToOperationId;
	private final ImmutableMap<String, Expr> commutativitySpecs;
	private final ImmutableMap<String, Expr> absorptionSpecs;
	private final ImmutableMap<String, Expr> asymmetricCommutativity;
	private final ImmutableMap<String, Expr> synchronizationSpec;
	private final ImmutableMap<String, Expr> legalitySpec;

	private final Map<String, Event> events = new TreeMap<>();
	private final List<PoEdge> programOrder = new ArrayList<>();
	private final List<ToEdge> transactionOrder = new ArrayList<>();
	private final Map<String, BiMap<Integer, ConcreteValue>> transactionLocalValues = new HashMap<>();
	private final SetMultimap<String, Integer> transactionLocalValuesUsedMultipleTimes = HashMultimap.create();
	private final Map<String, ClientLocalVar> clientLocalVariables = new HashMap<>();

	public GraphBuilder(final SchemaInformation schemaInfo, final StatementPart... parts) {
		this(new Options(), schemaInfo, parts);
	}

	public GraphBuilder(final Options options, final SchemaInformation schemaInfo, final StatementPart... parts) {
		this(options, schemaInfo, new HashSet<>(Arrays.asList(parts)), null);
	}

	private final Comparator<String> opIdComparator = new Comparator<String>() {

		@Override
		public int compare(String op1Id, String op2Id) {
			final StatementPart part1 = partToOperationId.inverse().get(op1Id);
			final StatementPart part2 = partToOperationId.inverse().get(op2Id);
			if (part1 instanceof StatementPart.UpdatePart && part2 instanceof StatementPart.QueryPart) {
				return -1;
			} else if (part2 instanceof StatementPart.UpdatePart && part1 instanceof StatementPart.QueryPart) {
				return 1;
			} else {
				return op1Id.compareTo(op2Id);
			}
		}
	};

	public GraphBuilder(final Options options, final SchemaInformation schemaInfo, final Map<StatementPart, String> parts) {
		this(options, schemaInfo, parts.keySet(), parts);
	}

	private GraphBuilder(final Options options, final SchemaInformation schemaInfo, final Set<StatementPart> parts,
			final Map<StatementPart, String> queries) {
		this.options = Objects.requireNonNull(options);
		varProvider = VariableProvider.create();
		clientIdVar = varProvider.newIntClientLocalVar("clientId");
		final CommutativityAbsorptionExprGenerator commutativityAbsorptionUtil = CommutativityAbsorptionExprGenerator.create(schemaInfo,
				options, varProvider);

		final ImmutableList.Builder<Operation> operationsBuilder = ImmutableList.builder();

		final String formatStr = "%0" + (int) Math.floor(Math.log10(parts.size() + 3) + 1) + "d_";

		int opCount = 0;
		final ImmutableBiMap.Builder<StatementPart, String> partToOperationIdBuilder = ImmutableBiMap.builder();
		// known parts
		for (final StatementPart part : parts) {
			final String operation = String.format(formatStr + part.getCQLCommand(), opCount++);
			operationsBuilder.add(part instanceof StatementPart.UpdatePart ? new Update(operation) : new Query(operation));
			partToOperationIdBuilder.put(part, operation);
			if (queries != null && LOG.isDebugEnabled()) {
				LOG.debug(operation + " was created from " + queries.get(part));
			}
		}
		operations = operationsBuilder.build();
		partToOperationId = partToOperationIdBuilder.build();

		final List<String> sortedOperationIds = new ArrayList<>(partToOperationId.values());
		Collections.sort(sortedOperationIds, opIdComparator);

		final ImmutableMap.Builder<String, Expr> commutativitySpecsBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> absorptionSpecsBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> asymmetricCommutativityBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> synchronizationSpecBuilder = ImmutableMap.builder();
		final ImmutableMap.Builder<String, Expr> legalitySpecBuilder = ImmutableMap.builder();
		for (int i = 0; i < sortedOperationIds.size(); i++) {
			final String op1Id = sortedOperationIds.get(i);
			final StatementPart part1 = partToOperationId.inverse().get(op1Id);
			for (int j = i; j < sortedOperationIds.size(); j++) {
				final String op2Id = sortedOperationIds.get(j);
				final StatementPart part2 = partToOperationId.inverse().get(op2Id);
				// commutativity: symmetric --> only one has to be specified
				commutativitySpecsBuilder.put(op1Id + "," + op2Id, commutativityAbsorptionUtil.getCommutativityExpr(part1, part2));
				// asymmetricCommutativity	
				final Expr asymmetric12 = commutativityAbsorptionUtil.getAsymetricCommutativityExpr(part1, part2);
				if (asymmetric12 != null) {
					asymmetricCommutativityBuilder.put(op1Id + "," + op2Id, asymmetric12);
				}
				if (!op1Id.equals(op2Id)) {
					final Expr asymmetric21 = commutativityAbsorptionUtil.getAsymetricCommutativityExpr(part2, part1);
					if (asymmetric21 != null) {
						asymmetricCommutativityBuilder.put(op2Id + "," + op1Id, asymmetric21);
					}
				}
				if (part1 instanceof StatementPart.UpdatePart && part2 instanceof StatementPart.UpdatePart) {
					// absorption: specify both ways
					absorptionSpecsBuilder.put(op1Id + "," + op2Id, commutativityAbsorptionUtil.getAbsorptionExpr(part1, part2));
					if (part1 != part2) {
						absorptionSpecsBuilder.put(op2Id + "," + op1Id, commutativityAbsorptionUtil.getAbsorptionExpr(part2, part1));
					}
				}
				// synchronization: specify both ways
				final Expr synchronizing12 = commutativityAbsorptionUtil.getSynchronizingOperationExpr(part1, part2);
				if (synchronizing12 != null) {
					synchronizationSpecBuilder.put(op1Id + "," + op2Id, synchronizing12);
				}
				final Expr synchronizing21 = commutativityAbsorptionUtil.getSynchronizingOperationExpr(part2, part1);
				if (synchronizing21 != null && part1 != part2) {
					synchronizationSpecBuilder.put(op2Id + "," + op1Id, synchronizing21);
				}
				// legality along CA
				final Expr legality12 = commutativityAbsorptionUtil.getLegalityExpr(part1, part2);
				if (legality12 != null) {
					legalitySpecBuilder.put(op1Id + "," + op2Id, legality12);
				}
				final Expr legality21 = commutativityAbsorptionUtil.getLegalityExpr(part2, part1);
				if (legality21 != null && part1 != part2) {
					legalitySpecBuilder.put(op2Id + "," + op1Id, legality21);
				}
			}
		}
		this.commutativitySpecs = commutativitySpecsBuilder.build();
		this.absorptionSpecs = absorptionSpecsBuilder.build();
		this.asymmetricCommutativity = asymmetricCommutativityBuilder.build();
		this.synchronizationSpec = synchronizationSpecBuilder.build();
		this.legalitySpec = legalitySpecBuilder.build();
	}

	public SystemSpecification getSpecification() {
		return new SystemSpecification(ScalaUtils.from(operations), ScalaUtils.from(commutativitySpecs),
				ScalaUtils.from(absorptionSpecs), ScalaUtils.from(asymmetricCommutativity), ScalaUtils.from(Collections.emptyMap()),
				ScalaUtils.from(synchronizationSpec), ScalaUtils.from(legalitySpec),
				ScalaUtils.from(new HashMap<>()),
				ScalaUtils.from(new HashMap<>()),
				ScalaUtils.from(new LinkedList<>()),
				ScalaUtils.from(new HashMap<>())
				);
	}

	public Expr getCommutativityExpr(final StatementPart part1, final StatementPart part2) {
		final String id1 = partToOperationId.get(part1);
		final String id2 = partToOperationId.get(part2);
		if (opIdComparator.compare(id1, id2) < 0) {
			return commutativitySpecs.get(id1 + "," + id2);
		} else {
			return commutativitySpecs.get(id2 + "," + id1);
		}
	}

	public Expr getAbsorptionExpr(final StatementPart part1, final StatementPart part2) {
		final String id1 = partToOperationId.get(part1);
		final String id2 = partToOperationId.get(part2);
		return absorptionSpecs.get(id1 + "," + id2);
	}

	public String addEvent(final StatementPart part, final String transactionId) {
		return addEvent(part, transactionId, null);
	}

	public String addEvent(final StatementPart part, final String transactionId, final ParsedSessionExecuteInvoke sessionExecuteInvoke) {
		final String eventId = "e" + events.size() + "_" + part.getLabel();
		final List<Expr> eventConstraint = new ArrayList<>();
		eventConstraint.add(CFourFactory.equal(CFourFactory.intClientIdArgLeft(), clientIdVar));
		final Set<Integer> constraintIdxs = new HashSet<>(part.getPossibleConstraintIdxs().values());
		final Set<Integer> columnIdxs = part instanceof StatementPart.UpsertPart
				? new HashSet<>(((StatementPart.UpsertPart) part).components.getPossiblyUpdatedColumnIdxs().values())
				: Collections.emptySet();
		if (!constraintIdxs.isEmpty() || !columnIdxs.isEmpty()) {
			for (final Integer constraintIdx : constraintIdxs) {
				handleValue(eventId, transactionId, constraintIdx, part.getConstraint(constraintIdx).getValue(), part, eventConstraint);
			}
			if (part instanceof StatementPart.UpsertPart) {
				final StatementPart.UpsertPart upsPart = (StatementPart.UpsertPart) part;
				for (final Integer columnIdx : columnIdxs) {
					if (!constraintIdxs.contains(columnIdx)) {
						handleValue(eventId, transactionId, columnIdx, upsPart.components.getColumnUpdate(columnIdx).getValue(),
								part, eventConstraint);
					}
				}
			}
		}
		if (part instanceof StatementPart.UpsertPart) {
			final StatementPart.UpsertPart upsert = (StatementPart.UpsertPart) part;
			if (upsert.ifExists) {
				eventConstraint.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()));
			} else if (upsert.ifNotExists) {
				eventConstraint.add(CFourFactory.not(CFourFactory.boolUpdatesExistingRowsArgLeft()));
			} else if (!upsert.canInsert) {
				// non-LWT that cannot insert must update rows
				eventConstraint.add(CFourFactory.not(CFourFactory.boolInsertsNewRowsArgLeft()));
				eventConstraint.add(CFourFactory.boolUpdatesExistingRowsArgLeft());
			} else {
				// non-LWT has to insert a new row or update an existing one
				eventConstraint.add(CFourFactory.or(CFourFactory.boolInsertsNewRowsArgLeft(), CFourFactory.boolUpdatesExistingRowsArgLeft()));
			}
		}
		events.put(eventId, CFourFactory.event(eventId, transactionId, partToOperationId.get(part), CFourFactory.and(eventConstraint)));
		return eventId;
	}

	public String addSkipEvent(final String label, final String transactionId) {
		final String eventId = "e" + events.size() + "_" + label;
		events.put(eventId, CFourFactory.skip(eventId, transactionId));
		return eventId;
	}

	public void addTransactionOrderEdge(final String fromTransactionId, final String toTransactionId) {
		transactionOrder.add(CFourFactory.toEdge(fromTransactionId, toTransactionId));
	}

	public void addProgramOrderEdge(final String fromEventId, final String toEventId) {
		addProgramOrderEdge(fromEventId, toEventId, null, AbstractTransactionGraph.EdgeConstraint.createEmpty());
	}

	public void addProgramOrderEdge(final String fromEventId, final String toEventId,
			final ParsedSessionExecuteInvoke fromEvent, final AbstractTransactionGraph.EdgeConstraint<ParsedSessionExecuteInvoke> sessionExecute) {

		final List<Expr> poConstraint = new ArrayList<>();
		poConstraint.add(CFourFactory.TRUE);
		if (options.isPoConstraintsEnabled() && fromEvent != null && sessionExecute != null) {
			if (sessionExecute.emptyResultNodes().contains(fromEvent)) {
				poConstraint.add(CFourFactory.boolReturnsEmptyResultArgLeft());
			} else if (sessionExecute.nonEmptyResultNodes().contains(fromEvent)) {
				poConstraint.add(CFourFactory.not(CFourFactory.boolReturnsEmptyResultArgLeft()));
			}
		}
		programOrder.add(CFourFactory.poEdge(fromEventId, toEventId, CFourFactory.and(poConstraint)));
	}

	private int getTransactionValueArgIdx(final StatementPart part, final int offset) {
		if (part == null) {
			// skip event
			return offset;
		} else {
			return (part.getMaxIdx() < 100 ? 100 : part.getMaxIdx() + 1) + offset;
		}
	}

	private void handleValue(final String eventId, final String transactionId, final int idx, final ConcreteValue value,
			final StatementPart part, final List<Expr> eventConstraint) {
		if (options.isValueAnalysisEnabled() && value != null) {
			if (value instanceof ClientImmutableValue) {
				// encode client equality
				final String id = ((ClientImmutableValue) value).id;
				if (!clientLocalVariables.containsKey(id)) {
					clientLocalVariables.put(id, varProvider.newIntClientLocalVar());
				}
				eventConstraint.add(CFourFactory.equal(CFourFactory.intArgLeft(idx), clientLocalVariables.get(id)));
			}
			if (value instanceof UUIDValue && ((UUIDValue) value).isRandom) {
				if (!transactionLocalValues.containsKey(transactionId)) {
					eventConstraint.add(CFourFactory.equal(CFourFactory.intArgLeft(idx), CFourFactory.intFreshUniqueVar()));
				}
			}
			if ((value instanceof UnknownImmutableValue && !((UnknownImmutableValue) value).isWidened) ||
					(value instanceof UUIDValue && !((UUIDValue) value).isWidened)) {
				// we have to encode equality of UnknownValues along the ProgramOrder in a single transaction
				if (!transactionLocalValues.containsKey(transactionId)) {
					transactionLocalValues.put(transactionId, HashBiMap.create());
				}
				final BiMap<Integer, ConcreteValue> transactionVals = transactionLocalValues.get(transactionId);
				final int offset;
				if (!transactionVals.containsValue(value)) {
					offset = transactionVals.size();
					transactionVals.put(offset, value);
				} else {
					offset = transactionVals.inverse().get(value);
					transactionLocalValuesUsedMultipleTimes.put(transactionId, offset);
				}
				eventConstraint.add(CFourFactory.equal(CFourFactory.intArgLeft(idx),
						CFourFactory.intArgLeft(getTransactionValueArgIdx(part, offset))));
			}
		}
	}

	public AbstractHistory build() {
		return CFourFactory.graph(new ArrayList<>(events.values()), transformProgramOrder(), transactionOrder, getSpecification(),
				buildGlobalConstraint());
	}

	public List<PoEdge> transformProgramOrder() {
		if (transactionLocalValuesUsedMultipleTimes.isEmpty()) {
			return programOrder;
		}
		final List<PoEdge> transformedPO = new ArrayList<>(programOrder.size());
		for (final PoEdge oldEdge : programOrder) {
			final Event sourceEvent = events.get(oldEdge.sourceID());
			if (transactionLocalValuesUsedMultipleTimes.containsKey(sourceEvent.txn())) {
				final Event targetEvent = events.get(oldEdge.targetID());
				final StatementPart sourcePart = partToOperationId.inverse().get(sourceEvent.operationID());
				final StatementPart targetPart = partToOperationId.inverse().get(targetEvent.operationID());
				final List<Expr> poConstraint = new ArrayList<>();
				poConstraint.add(oldEdge.constraint());
				for (int i : transactionLocalValuesUsedMultipleTimes.get(sourceEvent.txn())) {
					poConstraint.add(CFourFactory.equal(CFourFactory.intArgLeft(getTransactionValueArgIdx(sourcePart, i)),
							CFourFactory.intArgRight(getTransactionValueArgIdx(targetPart, i))));
				}
				transformedPO.add(CFourFactory.poEdge(oldEdge.sourceID(), oldEdge.targetID(), CFourFactory.and(poConstraint)));
			} else {
				transformedPO.add(oldEdge);
			}
		}
		return transformedPO;
	}

	private Expr buildGlobalConstraint() {
		final List<Expr> globalConstraint = new ArrayList<>();
		globalConstraint.add(CFourFactory.unequal(clientIdVar, CFourFactory.asOtherClientLocalVar(clientIdVar)));
		if (options.isClientLocalAreGlobalUnique()) {
			for (final ClientLocalVar clv : clientLocalVariables.values()) {
				globalConstraint.add(CFourFactory.unequal(clv, CFourFactory.asOtherClientLocalVar(clv)));
			}
		}
		return CFourFactory.and(globalConstraint);
	}

	@Override
	public String toString() {
		return build().toString();
	}
}
