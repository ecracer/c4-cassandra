package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.lang.annotation.Annotation;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import soot.tagkit.AbstractHost;
import soot.tagkit.AnnotationElem;
import soot.tagkit.AnnotationIntElem;
import soot.tagkit.AnnotationTag;
import soot.tagkit.Tag;
import soot.tagkit.VisibilityAnnotationTag;

public class SootAnnotationUtils {

	public static boolean hasAnnotation(final AbstractHost host) {
		return getAnnotationTag(host) != null;
	}

	public static Transaction getTransactionAnnotation(final AbstractHost host) {
		final VisibilityAnnotationTag annotationTag = getAnnotationTag(host);
		if (annotationTag != null) {
			for (final AnnotationTag annotation : annotationTag.getAnnotations()) {
				if (Transaction.SOOT_NAME.equals(annotation.getType())) {
					boolean onlyForDisplaying = false;
					for (final AnnotationElem element : annotation.getElems()) {
						if ("onlyForDisplaying".equals(element.getName()) && element instanceof AnnotationIntElem) {
							if (((AnnotationIntElem) element).getValue() != 0) {
								onlyForDisplaying = true;
							}
						}
					}
					final boolean finalOnlyForDisplaying = onlyForDisplaying;
					return new Transaction() {

						@Override
						public Class<? extends Annotation> annotationType() {
							return Transaction.class;
						}

						@Override
						public boolean onlyForDisplaying() {
							return finalOnlyForDisplaying;
						}
					};
				}
			}
		}
		return null;
	}

	private static VisibilityAnnotationTag getAnnotationTag(final AbstractHost host) {
		final Tag tag = host.getTag("VisibilityAnnotationTag");
		if (tag != null) {
			return (VisibilityAnnotationTag) tag;
		} else {
			return null;
		}
	}

}
