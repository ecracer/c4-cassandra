package ch.ethz.inf.pm.cfour.cassandra.soot.values;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;

/**
 * Abstract class for a statement value, i.e. a query with consistency-levels and binds
 */
public abstract class AbstractStatementValue<T, B extends AbstractStatementValue<T, B>> implements Value {

	public final ImmutableList<AbstractBind<T>> binds;

	public final ImmutableValue consistencyLevel;
	/**
	 * Serial consistency level (see
	 * {@link https://docs.datastax.com/en/drivers/java/2.0/com/datastax/driver/core/Statement.html#setSerialConsistencyLevel-com.datastax.driver.core.ConsistencyLevel-})
	 */
	public final ImmutableValue serialConsistencyLevel;

	protected AbstractStatementValue(final ImmutableList<AbstractBind<T>> binds) {
		this(binds, NullValue.create(), NullValue.create());
	}

	protected AbstractStatementValue(final ImmutableList<AbstractBind<T>> binds, final ImmutableValue consistencyLevel,
			final ImmutableValue serialConsistencyLevel) {
		this.binds = Objects.requireNonNull(binds);
		this.consistencyLevel = checkConsistencyLevel(consistencyLevel);
		this.serialConsistencyLevel = checkConsistencyLevel(serialConsistencyLevel);
	}

	private static ImmutableValue checkConsistencyLevel(final ImmutableValue consistencyLevel) {
		if (consistencyLevel == null
				|| !(consistencyLevel instanceof UnknownImmutableValue || consistencyLevel instanceof ConsistencyLevelValue
						|| consistencyLevel instanceof NullValue)) {
			throw new IllegalArgumentException();
		}
		return consistencyLevel;
	}

	public B addBind(final AbstractBind<T> bind) {
		final ImmutableList.Builder<AbstractBind<T>> builder = ImmutableList.builder();
		builder.addAll(binds);
		builder.add(Objects.requireNonNull(bind));
		return createNewInternal(builder.build(), consistencyLevel, serialConsistencyLevel);
	}

	public B addFirstBinds(final T[] value) {
		final ImmutableList.Builder<AbstractBind<T>> builder = ImmutableList.builder();
		builder.addAll(binds);
		for (int i = 0; i < value.length; i++) {
			builder.add(AbstractBind.UserBind.<T> create(IntValue.create(i), value[i]));
		}
		return createNewInternal(builder.build(), consistencyLevel, serialConsistencyLevel);
	}

	public B clearUserBinds() {
		final ImmutableList.Builder<AbstractBind<T>> newBinds = ImmutableList.builder();
		for (final AbstractBind<T> bind : binds) {
			if (bind instanceof AbstractBind.SystemBind) {
				newBinds.add(bind);
			}
		}
		return createNewInternal(newBinds.build(), consistencyLevel, serialConsistencyLevel);
	}

	public B setConsistencyLevel(final ImmutableValue consistencyLevel) {
		return createNewInternal(binds, consistencyLevel, serialConsistencyLevel);
	}

	public B setSerialConsistencyLevel(final ImmutableValue serialConsistencyLevel) {
		return createNewInternal(binds, consistencyLevel, serialConsistencyLevel);
	}

	protected abstract B createNewInternal(final ImmutableList<AbstractBind<T>> binds, final ImmutableValue consistencyLevel,
			final ImmutableValue serialConsistencyLevel);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((binds == null) ? 0 : binds.hashCode());
		result = prime * result + ((consistencyLevel == null) ? 0 : consistencyLevel.hashCode());
		result = prime * result + ((serialConsistencyLevel == null) ? 0 : serialConsistencyLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractStatementValue<?, ?> other = (AbstractStatementValue<?, ?>) obj;
		if (binds == null) {
			if (other.binds != null)
				return false;
		} else if (!binds.equals(other.binds))
			return false;
		if (consistencyLevel == null) {
			if (other.consistencyLevel != null)
				return false;
		} else if (!consistencyLevel.equals(other.consistencyLevel))
			return false;
		if (serialConsistencyLevel == null) {
			if (other.serialConsistencyLevel != null)
				return false;
		} else if (!serialConsistencyLevel.equals(other.serialConsistencyLevel))
			return false;
		return true;
	}
}
