package ch.ethz.inf.pm.cfour.cassandra.soot.fieldvalues;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.DefaultProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.AnalyzedMethodInvokePoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ObjectReference;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.NopProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.assign.AssignFieldRefPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.FieldDesc;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.ForwardFlowAnalysis;

/**
 * This analysis runs the fixpoint analysis on a single execution graph. AnalyzedMethodInvokeTransformers are bridged
 * using the Default Transformer. This analysis tracks if values from the initial flow are modified. It collects also
 * all immutableValues that are assigned to a static field.
 *
 */
public class FieldValueAnalysis extends ForwardFlowAnalysis<AbstractProgramPoint, ExecutionState> {

	/** Initial CallStack */
	private final CallStack callStack;
	private final ExecutionState entryFlow;
	private final Set<AbstractReference> changedRefsBuilder = new HashSet<>();
	private final SetMultimap<FieldDesc, ImmutableValue> staticFieldValuesBuilder = HashMultimap.create();
	private final boolean isStaticInitializer;

	public FieldValueAnalysis(final CallStack callStack, final ExecuteGraph executionGraph,
			final ExecutionState entryFlow, final boolean isStaticInitializer) {
		super(new SootGraph(executionGraph));
		this.callStack = Objects.requireNonNull(callStack);
		this.entryFlow = Objects.requireNonNull(entryFlow);
		this.isStaticInitializer = isStaticInitializer;
		for (final java.util.Map.Entry<FieldDesc, VarLocalOrImmutableValue> entry : entryFlow.getStaticFieldMap().entries()) {
			if (!(entry.getValue() instanceof ImmutableValue)) {
				throw new IllegalArgumentException();
			}
			staticFieldValuesBuilder.put(entry.getKey(), (ImmutableValue) entry.getValue());
		}
		doAnalysis();
	}

	public ExecutionState getFinalFlow() {
		ExecutionState ret = null;
		for (final AbstractProgramPoint unit : graph.getTails()) {
			if (ret == null) {
				ret = getFlowAfter(unit);
			} else {
				ret.mergeWith(getFlowAfter(unit), ret);
			}
		}
		if (ret.isBottom()) {
			final Iterator<AbstractProgramPoint> it = graph.iterator();
			while (it.hasNext()) {
				ret.mergeWith(getFlowAfter(it.next()), ret);
			}
		}
		return ret != null ? ret : entryFlow;
	}

	public Set<AbstractReference> getChangedRefs() {
		return changedRefsBuilder;
	}

	public SetMultimap<FieldDesc, ImmutableValue> getStaticFieldValues() {
		return staticFieldValuesBuilder;
	}

	@Override
	protected ExecutionState entryInitialFlow() {
		return entryFlow.clone();
	}

	@Override
	protected ExecutionState newInitialFlow() {
		return new ExecutionState();
	}

	@Override
	protected void merge(ExecutionState in1, ExecutionState in2, ExecutionState out) {
		in1.mergeWith(in2, out);
	}

	@Override
	protected void copy(ExecutionState source, ExecutionState dest) {
		source.copyTo(dest);
	}

	@Override
	protected void flowThrough(ExecutionState in, AbstractProgramPoint d, ExecutionState out) {
		final ExecutionState oldOut = getFlowAfter(d).clone();
		in.copyTo(out);
		if (!out.isBottom()) {
			if (d instanceof AnalyzedMethodInvokePoint) {
				final AbstractProgramPoint defaultPoint = DefaultProgramPoint.create(callStack,
						((AnalyzedMethodInvokePoint) d).invokeStmt);
				if (defaultPoint != null) {
					defaultPoint.transform(out, oldOut);
				}
			} else if (!(d instanceof AnalyzedMethodInvokePoint.MethodReturnPoint)) {
				d.createWithCallstack(callStack).transform(out, oldOut);
			}
			if (!out.isBottom()) {
				computeChangedRefs(out);
				// Handle static fields: if a non-immutable-value is assigned to a static field, remove the static
				// field from the analysis --> We only consider immutable values for static fields
				if (d instanceof AssignFieldRefPoint) {
					final AssignFieldRefPoint assignFieldRef = (AssignFieldRefPoint) d;
					final FieldDesc accessedField = assignFieldRef.getAccessedField();
					if (staticFieldValuesBuilder.containsKey(accessedField)) {
						if (assignFieldRef.isFieldDefinition()) {
							final Set<ImmutableValue> newVals = new HashSet<>();
							for (final VarLocalOrImmutableValue newVal : out.getStaticField(accessedField)) {
								if (newVal instanceof ImmutableValue) {
									newVals.add((ImmutableValue) newVal);
								} else {
									staticFieldValuesBuilder.removeAll(accessedField);
									return;
								}
							}
							if (isStaticInitializer) {
								staticFieldValuesBuilder.replaceValues(accessedField, newVals);
							} else {
								staticFieldValuesBuilder.putAll(accessedField, newVals);
							}
						}
					}
				}
			}
		}
	}

	private void computeChangedRefs(ExecutionState out) {
		for (final BaseReference baseRef : entryFlow.getBaseValueMap().keySet()) {
			if (!entryFlow.getBaseValueMap().get(baseRef).equals(out.getBaseValueMap().get(baseRef))) {
				changedRefsBuilder.add(baseRef);
			}
		}
		for (final ArrayReference arrayRef : entryFlow.getArrayValueMap().keySet()) {
			if (!entryFlow.getArrayValueMap().get(arrayRef).equals(out.getArrayValueMap().get(arrayRef))) {
				changedRefsBuilder.add(arrayRef);
			}
		}
		for (final ObjectReference objectRef : entryFlow.getObjectValueMap().keySet()) {
			if (!entryFlow.getObjectValueMap().get(objectRef).equals(out.getObjectValueMap().get(objectRef))) {
				changedRefsBuilder.add(objectRef);
			}
		}
	}

	/**
	 * Directed Graph that can be used by the fixed point analysis provided by soot. Additional NopProgramPoints are
	 * inserted as head and tail. Also, a methodInvokePoint is connected with its corresponding methodReturnPoint
	 * 
	 * @author Arthur Kurath
	 */
	private static class SootGraph implements DirectedGraph<AbstractProgramPoint> {

		private final ExecuteGraph executionGraph;
		private final AbstractProgramPoint head;
		private final AbstractProgramPoint tail;

		public SootGraph(final ExecuteGraph executionGraph) {
			this.executionGraph = executionGraph;
			this.head = NopProgramPoint.create(CallStack.EMPTY_STACK);
			this.tail = NopProgramPoint.create(CallStack.EMPTY_STACK);
		}

		@Override
		public List<AbstractProgramPoint> getHeads() {
			return Collections.singletonList(head);
		}

		@Override
		public List<AbstractProgramPoint> getTails() {
			return Collections.singletonList(tail);
		}

		@Override
		public List<AbstractProgramPoint> getPredsOf(AbstractProgramPoint s) {
			if (s == head) {
				return Collections.emptyList();
			} else if (s == tail) {
				final List<AbstractProgramPoint> ret = new ArrayList<>(executionGraph.sinkNodes());
				if (executionGraph.isBypassPossible()) {
					ret.add(head);
				}
				return ret;
			} else if (s instanceof AnalyzedMethodInvokePoint.MethodReturnPoint) {
				return Collections.singletonList(((AnalyzedMethodInvokePoint.MethodReturnPoint) s).correspondingMethodInvoke);
			} else {
				final List<AbstractProgramPoint> ret = new ArrayList<>(executionGraph.predecessors(s));
				if (executionGraph.sourceNodes().contains(s)) {
					ret.add(head);
				}
				return ret;
			}
		}

		@Override
		public List<AbstractProgramPoint> getSuccsOf(AbstractProgramPoint s) {
			if (s == tail) {
				return Collections.emptyList();
			} else if (s == head) {
				final List<AbstractProgramPoint> ret = new ArrayList<>(executionGraph.sourceNodes());
				if (executionGraph.isBypassPossible()) {
					ret.add(tail);
				}
				return ret;
			} else if (s instanceof AnalyzedMethodInvokePoint) {
				return Collections.singletonList(((AnalyzedMethodInvokePoint) s).methodReturnPoint);
			} else {
				final List<AbstractProgramPoint> ret = new ArrayList<>(executionGraph.successors(s));
				if (executionGraph.sinkNodes().contains(s)) {
					ret.add(tail);
				}
				return ret;
			}
		}

		@Override
		public int size() {
			return executionGraph.nodes().size() + 2;
		}

		@Override
		public Iterator<AbstractProgramPoint> iterator() {
			final List<AbstractProgramPoint> allNodes = new ArrayList<>();
			allNodes.add(head);
			allNodes.add(tail);
			allNodes.addAll(executionGraph.nodes());
			return allNodes.iterator();
		}
	}
}
