package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisEntryMethodTest extends AbstractSootTest {

	public AnalysisEntryMethodTest() throws IOException {
		super("EntryMethod", "entry");
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraph) {
		assertEquals(1, executeGraph.size());
		final UnparsedTransactionGraph graph = executeGraph.values().iterator().next();
		assertTrue(graph.isBypassPossible());
		assertEquals(0, graph.nodes().size());
		assertEquals(0, graph.edges().size());
		assertEquals(0, graph.sourceNodes().size());
		assertEquals(0, graph.sinkNodes().size());
	}
}
