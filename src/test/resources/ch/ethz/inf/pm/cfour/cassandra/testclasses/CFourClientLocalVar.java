

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class CFourClientLocalVar {
	
	private final Session session;
	
	public CFourClientLocalVar(final Session session){
		this.session = session;
	}
	
	@Transaction
	// KillrChat: UserResource#createUser
	public void registerUser(String username, String password){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO users (username, password) VALUES (?, ?)", username, password);
	}
	
	@Transaction
	// KillrChat: RememberMeResource#getRemembermeUser
	public void getUser(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT * FROM users WHERE username = ?", username);
	}
}
