package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.Collections;
import java.util.Objects;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.beans.Method;

/**
 * Represents a call stack. Method is the current method, the stack contains the stmts that lead to the execution of the
 * current method.
 */
public class CallStack {

	public final static CallStack EMPTY_STACK = create(
			Method.create("java.lang.Object", "<init>", Collections.emptyList(), "void", "void java.lang.Object.<init>()"));

	public final Method method;
	public final ImmutableList<Object> callStack;

	private CallStack(final Method method, final ImmutableList<Object> callStack) {
		this.method = Objects.requireNonNull(method);
		this.callStack = Objects.requireNonNull(callStack);
	}

	public static CallStack create(final Method method) {
		return new CallStack(method, ImmutableList.of());
	}

	public CallStack push(final Object nextCall, final Method newMethod) {
		if (callStack.contains(nextCall)) {
			return new CallStack(
					newMethod,
					callStack.subList(0, callStack.indexOf(nextCall) + 1));
		} else {
			return new CallStack(
					newMethod,
					ImmutableList.<Object> builder().addAll(callStack).add(nextCall).build());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((callStack == null) ? 0 : callStack.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CallStack other = (CallStack) obj;
		if (callStack == null) {
			if (other.callStack != null)
				return false;
		} else if (!callStack.equals(other.callStack))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return method.toString() + " || " + callStack.size() + " ||";
	}
}
