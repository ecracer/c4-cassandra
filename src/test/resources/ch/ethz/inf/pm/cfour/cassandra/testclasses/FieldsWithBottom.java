

import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;

public class FieldsWithBottom {
	
	private Session session;
	private PreparedStatement query1;
	private PreparedStatement query2;
	private StringBuilder temp;
	
	public FieldsWithBottom(Session session){
		this.session = session;
		query1 = session.prepare("SELECT val1 FROM table1");
		query2 = session.prepare("SELECT val1 FROM table2");
	}
	
	public void test1(Session session) {
		session.execute(query1.bind());
	}
	
	public void test2(Session session) {
		session.execute(query2.bind());
	}
	
	private void throwsException(){
		query1 = session.prepare("SELECT val2 FROM table1");
		temp.append("SELECT val2 FROM table2");
		query2 = session.prepare("SELECT val2 FROM table2");
	}
}