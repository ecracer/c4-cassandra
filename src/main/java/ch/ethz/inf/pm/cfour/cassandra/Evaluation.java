package ch.ethz.inf.pm.cfour.cassandra;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.cfour.cassandra.analysis.Profiler;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.analysis.cfour.CFourResult;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions.TransactionMethod;

/**
 * Evaluates all the programs supplied as arguments using different options. Use the following structure for arguments:
 * <ol>
 * <li>Path to a file where the results should be written</li>
 * <li>First project to evaluate, of the form
 * <i>projectName</i>;<i>projectJar</i>;<i>createSchemaFile</i>?;<i>excludedTransaction</i>*</li>
 * <li>Second project to evalute</li>
 * <li>...</li>
 * </ol>
 * Example:<br>
 * "output\evaluation.csv" "cassandra-twitter;projects\cassandra-twitter.jar;projects\cassandra-twitter-schema.cql"
 * "cassatwitter;projects\cassatwitter.jar;projects\cassatwitter-schema.cql"
 */
public class Evaluation {

	private static final Logger LOG = LogManager.getLogger(Evaluation.class);

	public static void main(String[] args) throws IOException {
		// first argument: resultFile
		// second ... n: projectName;projectJar;createSchema?;excludedTransaction...
		if (args.length <= 1) {
			throw new IllegalArgumentException();
		}
		final File result = new File(args[0]);
		final ImmutableSet.Builder<ProjectUnderEvaluation> projectBuilder = ImmutableSet.builder();
		for (int i = 1; i < args.length; i++) {
			final String[] projectDesc = args[i].split(";");
			if (projectDesc.length < 3) {
				throw new IllegalArgumentException();
			}
			final ImmutableSet.Builder<String> excludedTransactionMethods = ImmutableSet.builder();
			for (int j = 3; j < projectDesc.length; j++) {
				excludedTransactionMethods.add(projectDesc[j]);
			}
			projectBuilder.add(new ProjectUnderEvaluation(projectDesc[0], new File(projectDesc[1]),
					projectDesc[2].isEmpty() ? null : new File(projectDesc[2]), excludedTransactionMethods.build()));
		}
		new Evaluation(result, projectBuilder.build()).evaluate();
	}

	private final File resultFile;
	private final ImmutableSet<ProjectUnderEvaluation> projects;

	public Evaluation(final File resultFile, final ImmutableSet<ProjectUnderEvaluation> projects) {
		this.resultFile = Objects.requireNonNull(resultFile);
		this.projects = Objects.requireNonNull(projects);
	}

	public void evaluate() throws IOException {
		final BufferedWriter resultOut = new BufferedWriter(new FileWriter(resultFile));
		try {
			for (final ProjectUnderEvaluation project : projects) {
				evaluate(project, resultOut);
			}
		} finally {
			resultOut.close();
		}
	}

	private void evaluate(final ProjectUnderEvaluation project, final BufferedWriter resultOut) throws IOException {
		for (int i = 0; i <= 1; i++) {
			final Options options = new Options();
			options.addClasspath(project.jarFile.getAbsolutePath());
			for (final String excludedTransaction : project.excludedTransactions) {
				options.addExcludedTransactionMethod(TransactionMethod.parse(excludedTransaction));
			}
			options.setBasicAnalysisEnabled(false);

			options.setValueAnalysisEnabled(true);
			options.setStrictUpdatesEnabled(true);
			options.setTreatAllUpdatesAsStrict(false);
			options.setClientLocalValuesEnabled(true);
			options.setClientLocalAreGlobalUnique(false);
			options.setSideChannelsEnabled(false);
			options.setPoConstraintsEnabled(true);

			options.setGlobalAndEventConstraintsEnabled(true);
			options.setAbsorptionEnabled(true);
			options.setSinglePOPathEnabled(true);
			options.setCommutativityEnabled(true);
			options.setAsymmetricCommutativityEnabled(true);
			options.setSynchronizingOperationsEnabled(true);
			options.setLegalitySpecEnabled(true);

			options.setCreateSchemaFile(project.createSchemaFile.getAbsolutePath());

			final boolean displayCodeEnabled = i == 0;
			final String projectName = displayCodeEnabled ? project.name : (project.name + "-with-display");
			options.setPruneDisplayCodeEnabled(displayCodeEnabled);

			checkAndPrint("var", projectName, options, resultOut);

			options.setSynchronizingOperationsEnabled(false);
			checkAndPrint("var-without-synchronization", projectName, options, resultOut);
			options.setSynchronizingOperationsEnabled(true);

			options.setStrictUpdatesEnabled(false);
			checkAndPrint("var-without-strict-updates", projectName, options, resultOut);
			options.setStrictUpdatesEnabled(true);

			options.setAbsorptionEnabled(false);
			checkAndPrint("var-without-absorption", projectName, options, resultOut);
			options.setAbsorptionEnabled(true);

			options.setSinglePOPathEnabled(false);
			checkAndPrint("var-without-process", projectName, options, resultOut);
			options.setSinglePOPathEnabled(true);

			options.setCommutativityEnabled(false);
			options.setAsymmetricCommutativityEnabled(false);
			checkAndPrint("var-without-commutativity", projectName, options, resultOut);
			options.setCommutativityEnabled(true);
			checkAndPrint("var-without-asymmetric-commutativity", projectName, options, resultOut);
			options.setAsymmetricCommutativityEnabled(true);

			options.setGlobalAndEventConstraintsEnabled(false);
			checkAndPrint("var-without-global-event-constraints", projectName, options, resultOut);
			options.setGlobalAndEventConstraintsEnabled(true);

			options.setPoConstraintsEnabled(false);
			checkAndPrint("var-without-po-constraints", projectName, options, resultOut);
			options.setPoConstraintsEnabled(true);

			options.setCreateSchemaFile(null);
			checkAndPrint("var-without-schema", projectName, options, resultOut);
			options.setCreateSchemaFile(project.createSchemaFile.getAbsolutePath());

			options.setLegalitySpecEnabled(false);
			checkAndPrint("var-without-legality-spec", projectName, options, resultOut);
			options.setLegalitySpecEnabled(true);

			options.setSideChannelsEnabled(true);
			checkAndPrint("var-with-side-channels", projectName, options, resultOut);
			options.setSideChannelsEnabled(false);

			options.setClientLocalAreGlobalUnique(true);
			checkAndPrint("var-with-global-uniqueness", projectName, options, resultOut);
			options.setClientLocalAreGlobalUnique(false);

			options.setClientLocalValuesEnabled(false);
			checkAndPrint("without-local-var", projectName, options, resultOut);

			options.setValueAnalysisEnabled(false);
			checkAndPrint("without-value-analysis", projectName, options, resultOut);

			options.setValueAnalysisEnabled(false);
			options.setStrictUpdatesEnabled(false);
			options.setTreatAllUpdatesAsStrict(false);
			options.setClientLocalValuesEnabled(false);
			options.setClientLocalAreGlobalUnique(false);
			options.setSideChannelsEnabled(true);
			options.setPoConstraintsEnabled(false);

			options.setGlobalAndEventConstraintsEnabled(false);
			options.setAbsorptionEnabled(false);
			options.setSinglePOPathEnabled(false);
			options.setCommutativityEnabled(false);
			options.setAsymmetricCommutativityEnabled(false);
			options.setSynchronizingOperationsEnabled(false);
			options.setLegalitySpecEnabled(false);

			options.setCreateSchemaFile(null);
			checkAndPrint("without-enhancements", projectName, options, resultOut);

			options.setCreateSchemaFile(project.createSchemaFile.getAbsolutePath());
			checkAndPrint("only-schema", projectName, options, resultOut);
			options.setCommutativityEnabled(true);
			checkAndPrint("only-schema-commutativity", projectName, options, resultOut);
			options.setStrictUpdatesEnabled(true);
			checkAndPrint("only-schema-commutativity-strict-updates", projectName, options, resultOut);
		}
	}

	private void checkAndPrint(final String testName, final String projectName, final Options options, final BufferedWriter resultOut)
			throws IOException {
		for (int i = 0; i < 3; i++) {
			try {
				final SerializabilityAnalysis analysis = new SerializabilityAnalysis(options);
				final Profiler profiler = analysis.run();
				final CFourResult result = analysis.getCFourResult();
				final StringBuilder sb = new StringBuilder();
				sb.append(testName).append(";").append(projectName).append(";").append(result.getPossibleViolationsSize2()).append(";")
						.append(result.getVerifiedViolationsSize2()).append(";").append(result.getPossibleViolationsSize3()).append(";")
						.append(result.getVerifiedViolationsSize3()).append(";").append(result.getPossibleViolationsSize4()).append(";")
						.append(result.getVerifiedViolationsSize4()).append(";").append(result.getTotalPossibleViolationsSize4())
						.append(";")
						.append(result.getTotalVerifiedViolationsSize4()).append(";")
						.append(result.getVerifiedViolationsSize2() + result.getVerifiedViolationsSize3()
								+ result.getVerifiedViolationsSize4())
						.append(";").append(result.getPossibleViolationsSize2()).append("/").append(result.getVerifiedViolationsSize2())
						.append(" | ").append(result.getPossibleViolationsSize3()).append("/").append(result.getVerifiedViolationsSize3())
						.append(" | ").append(result.getPossibleViolationsSize4()).append("/").append(result.getVerifiedViolationsSize4())
						.append(";").append(String.format("%.3f", profiler.getRuntimeForPhase(SerializabilityAnalysis.PHASE_SOOT)))
						.append(";").append(String.format("%.3f", profiler.getRuntimeForPhase(SerializabilityAnalysis.PHASE_EC_CHECKER)));
				resultOut.write(sb.toString());
				resultOut.write("\n");
				resultOut.flush();
				return;
			} catch (Exception e) {
				LOG.error("Error evaluating " + testName + " on " + projectName, e);
			}
		}
		resultOut.write(new StringBuilder().append(testName).append(";").append(projectName).append(";").append("ERROR").toString());
		resultOut.write("\n");
		resultOut.flush();
	}

	private static class ProjectUnderEvaluation {
		public final String name;
		public final File jarFile;
		public final File createSchemaFile;
		public final ImmutableSet<String> excludedTransactions;

		public ProjectUnderEvaluation(final String name, final File jarFile, final File createSchemaFile,
				final ImmutableSet<String> excludedTransactions) {
			this.name = Objects.requireNonNull(name);
			this.jarFile = Objects.requireNonNull(jarFile);
			this.createSchemaFile = createSchemaFile;
			this.excludedTransactions = Objects.requireNonNull(excludedTransactions);
		}
	}
}
