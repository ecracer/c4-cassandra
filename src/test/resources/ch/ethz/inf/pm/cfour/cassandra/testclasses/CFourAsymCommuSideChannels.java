

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import java.util.UUID;

import com.datastax.driver.core.Session;


public class CFourAsymCommuSideChannels {

	private final Session session;
	
	public CFourAsymCommuSideChannels(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from Playlist (TrackServlet#doPost)
	public void star(String trackId){
		session.execute("SELECT artist, track_name FROM tracks WHERE id = ?", UUID.fromString(trackId));
		// star this track
	}
	
	@Transaction
	// from Playlist (TrackServlet#doPost)
	public void addTrack(String artist, String trackName){
		session.execute("INSERT INTO tracks (id, artist, track_name) VALUES (?, ?, ?)", UUID.randomUUID(), artist, trackName);
	}
}
