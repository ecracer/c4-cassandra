package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorStringAppendInLoopTest extends AbstractSootTest {

	public StmtExtractorStringAppendInLoopTest() throws IOException {
		super("StringAppendInLoop", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(0, result.size());
	}
}