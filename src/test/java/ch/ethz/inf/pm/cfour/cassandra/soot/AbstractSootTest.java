package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.tools.*;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.analysis.Profiler;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

@Category(SootTests.class)
public abstract class AbstractSootTest {

	private final static String CASSANDRA_DRIVER = "cassandra-driver-core-3.0.0.jar";
	private final static String ANNOTATIONS = "serializability-annotations.jar";

	protected final String className;
	protected final String[] entryMethod;
	protected final File tempDir;
	protected final File jarFile;

	public AbstractSootTest(final String className, final String... entryMethod)
			throws IOException {
		this.className = Objects.requireNonNull(className);
		this.entryMethod = Objects.requireNonNull(entryMethod);
		this.tempDir = Files.createTempDirectory("soot").toFile();
		final File[] sourceFiles = new File[3];
		sourceFiles[0] = new File(tempDir, className + ".java");
		copySourceFile(className + ".java", sourceFiles[0]);
		sourceFiles[1] = new File(tempDir, CASSANDRA_DRIVER);
		copySourceFile(CASSANDRA_DRIVER, sourceFiles[1]);
		sourceFiles[2] = new File(tempDir, ANNOTATIONS);
		copySourceFile(ANNOTATIONS, sourceFiles[2]);
		final File[] classFiles = compileFile(className, sourceFiles);
		jarFile = new File(tempDir, className + ".jar");
		createZipFile(jarFile, classFiles);
	}

	private void copySourceFile(final String fileName, final File targetFile) throws IOException {
		final BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(targetFile));
		final InputStream src = AbstractSootTest.class
				.getResourceAsStream("../testclasses/" + fileName);
		try {
			int readBytes;
			byte[] buffer = new byte[2048];
			while ((readBytes = src.read(buffer)) != -1) {
				output.write(buffer, 0, readBytes);
			}
		} finally {
			output.close();
			src.close();
		}
	}

	private File[] compileFile(final String className, final File... files) {
		final StringBuilder classPath = new StringBuilder();
		final List<File> sourceFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().endsWith(".java")) {
				sourceFiles.add(files[i]);
			} else {
				if (classPath.length() > 0) {
					classPath.append(File.pathSeparator);
				}
				classPath.append(files[i].getAbsolutePath());
			}
		}
		final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		final StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		final Iterable<? extends JavaFileObject> compUnits = fileManager
				.getJavaFileObjects(sourceFiles.toArray(new File[sourceFiles.size()]));
		final List<String> options = new ArrayList<>();
		options.add("-d");
		options.add(this.tempDir.getAbsolutePath());
		if (classPath.length() > 0) {
			options.add("-classpath");
			options.add(classPath.toString());
		}
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		compiler.getTask(null, fileManager, diagnostics, options, null, compUnits).call();

		for (Diagnostic diagnostic : diagnostics.getDiagnostics())
			System.out.format("Error on line %d in %s%n",
					diagnostic.getLineNumber(),
					diagnostic.getSource().toString());

		final List<File> classFiles = new ArrayList<>();
		for (final File f : this.tempDir.listFiles()) {
			if (f.getName().endsWith(".class")) {
				classFiles.add(f);
			}
		}
		return classFiles.toArray(new File[classFiles.size()]);
	}

	private void createZipFile(final File out, final File... in) throws IOException {
		final ZipOutputStream outStream = new ZipOutputStream(new FileOutputStream(out));
		for (int i = 0; i < in.length; i++) {
			final FileInputStream inStream = new FileInputStream(in[i]);
			try {
				outStream.putNextEntry(new ZipEntry(in[i].getName()));
				int next;
				while ((next = inStream.read()) != -1) {
					outStream.write(next);
				}
			} finally {
				inStream.close();
			}
		}
		outStream.close();
	}

	public void onFinish() {
		for (final File tempFile : tempDir.listFiles()) {
			tempFile.delete();
		}
		tempDir.delete();
	}

	protected Options getInitialOptions() {
		final Options sootOptions = new Options();
		sootOptions.addClasspath(jarFile.getAbsolutePath());
		for (int i = 0; i < entryMethod.length; i++) {
			sootOptions.addTransactionMethod(new SootOptions.TransactionMethod(className, entryMethod[i], null));
		}
		return sootOptions;
	}

	protected List<Options> createOptions() {
		return Collections.singletonList(getInitialOptions());
	}

	@Test
	public void test() {
		for (final Options sootOptions : createOptions()) {
			final SootAnalysis analysis = new SootAnalysis(sootOptions);
			analysis.run(new Profiler());
			checkTransactionExecuteGraph(sootOptions, analysis.getTransactionGraphs());
			final Set<SessionExecuteInvoke> sessionExecuteCalls = new HashSet<>();
			for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> transactionEntry : analysis.getTransactionGraphs()
					.entrySet()) {
				sessionExecuteCalls.addAll(transactionEntry.getValue().nodes());
			}
			checkSessionExecuteCalls(sessionExecuteCalls);
		}
	}

	protected void checkSessionExecuteCalls(final Set<SessionExecuteInvoke> executeCalls) {

	}

	protected void checkTransactionExecuteGraph(final Options options,
			final ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraph) {

		checkTransactionExecuteGraph(executeGraph);
	}

	protected void checkTransactionExecuteGraph(final ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraph) {

	}

	protected Set<String> getCQLs(final SessionExecuteInvoke executeInvoke) {
		final Set<String> cqls = new HashSet<>();
		executeInvoke.statementArg.forEach(stmt -> cqls.add(stmt.getQueryWithUnnamedBindMarkers()));
		return cqls;
	}
}
