package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourClientLocalGlobalUniqueVarTest extends AbstractCFourTest {

	public CFourClientLocalGlobalUniqueVarTest() throws IOException {
		super("CFourClientLocalGlobalUniqueVar");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("playlist_tracks", "username")
				.addPrimaryKeyColumn("playlist_tracks", "playlist_name").addPrimaryKeyColumn("playlist_tracks", "sequence_no").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setClientLocalValuesEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setClientLocalValuesEnabled(true);
		options.setClientLocalAreGlobalUnique(true);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isClientLocalAreGlobalUnique()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(1, result.getTotalVerifiedViolationsSize4());
		}
	}
}
