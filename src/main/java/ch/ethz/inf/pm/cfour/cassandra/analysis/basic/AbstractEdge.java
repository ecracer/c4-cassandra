package ch.ethz.inf.pm.cfour.cassandra.analysis.basic;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;

public abstract class AbstractEdge {

	public final TransactionDescriptor t1;
	public final TransactionDescriptor t2;

	protected AbstractEdge(final TransactionDescriptor t1, final TransactionDescriptor t2) {
		this.t1 = Objects.requireNonNull(t1);
		this.t2 = Objects.requireNonNull(t2);
	}

	public TransactionDescriptor getOtherTransaction(final TransactionDescriptor t) {
		if (t.equals(t1)) {
			return t2;
		} else {
			return t1;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((t1 == null) ? 0 : t1.hashCode());
		result = prime * result + ((t2 == null) ? 0 : t2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEdge other = (AbstractEdge) obj;
		if (t1 == null) {
			if (other.t1 != null)
				return false;
		} else if (!t1.equals(other.t1))
			return false;
		if (t2 == null) {
			if (other.t2 != null)
				return false;
		} else if (!t2.equals(other.t2))
			return false;
		return true;
	}

	public static class ArbitrationEdge extends AbstractEdge {

		private ArbitrationEdge(final TransactionDescriptor t1, final TransactionDescriptor t2) {
			// arbitration edges have no direction, so use order in order to keep edges with same nodes equal
			super(t1.getUniqueDescription().compareTo(t2.getUniqueDescription()) < 0 ? t1 : t2,
					t1.getUniqueDescription().compareTo(t2.getUniqueDescription()) < 0 ? t2 : t1);
		}

		public static ArbitrationEdge create(final TransactionDescriptor t1, final TransactionDescriptor t2) {
			return new ArbitrationEdge(t1, t2);
		}

		@Override
		public String toString() {
			return "ARB " + t1 + " " + t2;
		}
	}

	public static class DependencyEdge extends AbstractEdge {

		private DependencyEdge(final TransactionDescriptor t1, final TransactionDescriptor t2) {
			super(t1, t2);
		}

		public static DependencyEdge create(final TransactionDescriptor t1, final TransactionDescriptor t2) {
			return new DependencyEdge(t1, t2);
		}

		public boolean isAntiDependency(final TransactionDescriptor fromT) {
			if (fromT.equals(t2)) {
				return true;
			} else if (fromT.equals(t1)) {
				return false;
			} else {
				throw new IllegalArgumentException();
			}
		}

		@Override
		public String toString() {
			return "DEP " + t1 + " " + t2;
		}
	}
}
