package simpletwitter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class DBAccess {

	private final Session session;
	
	private final PreparedStatement selectUser;
	private final PreparedStatement insertUser;
	
	private final PreparedStatement selectTweet;
	private final PreparedStatement insertTweet;
	
	private final PreparedStatement selectUserline;
	private final PreparedStatement insertUserline;
	
	public DBAccess(final String host){
		this.session = Cluster.builder().addContactPoint(host).build().connect();
		
		this.selectUser = session.prepare("SELECT password FROM users WHERE username = ? ");
		this.insertUser = session.prepare("INSERT INTO users (username, password) VALUES (?, ?)");
		
		this.selectTweet = session.prepare("SELECT tweet_id, username, body FROM tweets WHERE tweet_id IN ? ");
		this.insertTweet = session.prepare("INSERT INTO tweets (tweet_id, username, body) VALUES (?, ?, ?) ");
		
		this.selectUserline = session.prepare("SELECT tweet_id FROM userline WHERE username = ? ");
		this.insertUserline = session.prepare("INSERT INTO userline (username, time, tweet_id) VALUES (?, ?, ?) ");
	}
	
	public boolean isNewUser(final String username){
		return session.execute(selectUser.bind(username)).isExhausted();
	}
	
	public String getPassword(final String username){
		final Row result = session.execute(selectUser.bind(username)).one();
		return result == null ? null : result.getString(0);
	}
	
	public void addUser(final String username, final String password){
		session.execute(insertUser.bind().
				setString(0, username).
				setString(1, password));
	}
	
	public List<Object[]> getTweets(final List<UUID> tweetIds){
		final List<Row> rows = session.execute(selectTweet.bind(tweetIds)).all();
		final List<Object[]> tweets = new ArrayList<Object[]>(rows.size());
		for (final Row row : rows){
			tweets.add(new Object[]{row.getUUID(0), row.getString(1), row.getString(2)});
		}
		return tweets;
	}
	
	public void addTweet(final UUID tweetId, final String username, final String body){
		session.execute(insertTweet.bind().setUUID(0, tweetId).setString(1, username).setString(2, body));
	}
	
	public List<UUID> getUserline(final String username){
		final List<Row> rows = session.execute(selectUserline.bind(username)).all();
		final List<UUID> tweetIds = new ArrayList<UUID>(rows.size());
		for (final Row row : rows){
			tweetIds.add(row.getUUID(0));
		}
		return tweetIds;
	}
	
	public void addToUserline(final String username, final UUID time, final UUID tweetId){
		session.execute(insertUserline.bind().setString(0, username).setUUID(1, time).setUUID(2, tweetId));
	}	
}
