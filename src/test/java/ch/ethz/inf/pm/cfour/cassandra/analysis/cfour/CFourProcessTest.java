package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourProcessTest extends AbstractCFourTest {

	public CFourProcessTest() throws IOException {
		super("CFourProcess");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setSinglePOPathEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setSinglePOPathEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isSinglePOPathEnabled()) {
			assertEquals(0, result.getVerifiedViolationsSize2());
			assertEquals(0, result.getVerifiedViolationsSize3());
			assertEquals(1, result.getVerifiedViolationsSize4());
		} else {
			assertEquals(1, result.getVerifiedViolationsSize2());
			assertEquals(0, result.getVerifiedViolationsSize3());
			assertEquals(0, result.getVerifiedViolationsSize4());
		}
	}
}
