package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map.Entry;

import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisResultIfElseTest extends AbstractSootTest {

	public AnalysisResultIfElseTest() throws IOException {
		super("ResultIfElse", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(
			ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {
		assertEquals(1, executeGraphs.size());
		for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> entry : executeGraphs.entrySet()) {
			if ("method1".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod1(entry.getValue());
			} else {
				assertTrue(false);
			}
		}
	}

	private void checkMethod1(final UnparsedTransactionGraph graph1) {
		assertEquals(4, graph1.nodes().size());
		SessionExecuteInvoke invoke1 = null;
		SessionExecuteInvoke invoke2 = null;
		SessionExecuteInvoke invoke3 = null;
		SessionExecuteInvoke invoke4 = null;
		for (SessionExecuteInvoke invoke : graph1.nodes()) {
			assertEquals(1, invoke.statementArg.size());
			final String cql = invoke.statementArg.iterator().next().query;
			if ("SELECT * FROM users WHERE username = 'Bob'".equals(cql)) {
				invoke1 = invoke;
			} else if ("INSERT INTO users (username) VALUES ('Bob2')".equals(cql)) {
				invoke2 = invoke;
			} else if ("INSERT INTO users (username) VALUES ('Bob')".equals(cql)) {
				invoke3 = invoke;
			} else if ("SELECT * FROM users WHERE username = 'Bob2'".equals(cql)) {
				invoke4 = invoke;
			} else {
				assertTrue(false);
			}
		}
		assertNotNull(invoke1);
		assertNotNull(invoke2);
		assertNotNull(invoke3);
		assertNotNull(invoke4);

		final UnparsedTransactionGraph expected = UnparsedTransactionGraph.builder().addNode(invoke1).addNode(invoke2).addNode(invoke3)
				.addNode(invoke4).putSourceNode(invoke1, AbstractTransactionGraph.EdgeConstraint.createEmpty()).putSinkNode(invoke4, AbstractTransactionGraph.EdgeConstraint.createEmpty())
				.putEdgeConstraint(invoke1, invoke2, AbstractTransactionGraph.EdgeConstraint.<SessionExecuteInvoke> builder().addNonEmptyResultNode(invoke1).build())
				.putEdgeConstraint(invoke1, invoke3, AbstractTransactionGraph.EdgeConstraint.<SessionExecuteInvoke> builder().addEmptyResultNode(invoke1).build())
				.putEdgeConstraint(invoke2, invoke4, AbstractTransactionGraph.EdgeConstraint.createEmpty())
				.putEdgeConstraint(invoke3, invoke4, AbstractTransactionGraph.EdgeConstraint.createEmpty()).setBypassPossible(false).build();

		assertEquals(expected, graph1);
	}
}
