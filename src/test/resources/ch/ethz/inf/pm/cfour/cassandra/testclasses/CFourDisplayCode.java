

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class CFourDisplayCode {
	
	private final Session session;
	
	public CFourDisplayCode(final Session session){
		this.session = session;
	}
	
	@Transaction
	// CurrencyExchange: TradeService#saveTrade
	public void saveTrade(final BigDecimal amountSell, BigDecimal amountBuy, Date timePlaced){
		session.execute("INSERT INTO trades (id, amountsell, amountbuy, timeplaced) VALUES (?, ?, ?, ?)", UUID.randomUUID(), amountSell, amountBuy, timePlaced);
	}
	
	@Transaction(onlyForDisplaying = true)
	// CurrencyExchange: TradeService#getListTradesFromDate
	public void getListTradesFromDate(final Date fromDate){
		session.execute("SELECT * FROM trades WHERE timeplaced > ?", fromDate);
	}
}
