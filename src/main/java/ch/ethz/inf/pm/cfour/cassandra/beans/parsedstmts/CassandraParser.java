package ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.apache.cassandra.config.ColumnDefinition;
import org.apache.cassandra.cql3.AbstractMarker;
import org.apache.cassandra.cql3.CQL3Type;
import org.apache.cassandra.cql3.ColumnIdentifier;
import org.apache.cassandra.cql3.Constants;
import org.apache.cassandra.cql3.CqlLexer;
import org.apache.cassandra.cql3.CqlParser;
import org.apache.cassandra.cql3.MultiColumnRelation;
import org.apache.cassandra.cql3.Operation;
import org.apache.cassandra.cql3.Operator;
import org.apache.cassandra.cql3.Relation;
import org.apache.cassandra.cql3.Sets;
import org.apache.cassandra.cql3.SingleColumnRelation;
import org.apache.cassandra.cql3.Term;
import org.apache.cassandra.cql3.Term.Raw;
import org.apache.cassandra.cql3.TokenRelation;
import org.apache.cassandra.cql3.VariableSpecifications;
import org.apache.cassandra.cql3.WhereClause;
import org.apache.cassandra.cql3.selection.RawSelector;
import org.apache.cassandra.cql3.selection.Selectable;
import org.apache.cassandra.cql3.selection.Selectable.WithCast;
import org.apache.cassandra.cql3.statements.BatchStatement;
import org.apache.cassandra.cql3.statements.CFStatement;
import org.apache.cassandra.cql3.statements.CreateTableStatement;
import org.apache.cassandra.cql3.statements.DeleteStatement;
import org.apache.cassandra.cql3.statements.ModificationStatement;
import org.apache.cassandra.cql3.statements.SelectStatement;
import org.apache.cassandra.cql3.statements.UpdateStatement;
import org.apache.cassandra.utils.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnCounterUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnPartUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnUnknownValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnValueUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQValConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.INConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.INValsConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.UnknownConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedBatchUpdateStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedUpdateStatement;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind.UserBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StatementSingleResultValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;

public class CassandraParser {

	private final static Logger LOG = LogManager.getLogger(CassandraParser.class);

	private final Map<StatementValue, ParsedStatement> statementCache = new HashMap<>();

	public ParsedStatement parseStatementValue(final StatementValue stmt) {
		if (statementCache.containsKey(stmt)) {
			return statementCache.get(stmt);
		}
		if (!stmt.query.isEmpty()) {
			statementCache.put(stmt, null);
			try {
				final ANTLRStringStream inputStream = new ANTLRStringStream(stmt.query);
				final CqlLexer lexer = new CqlLexer(inputStream);
				final CommonTokenStream token = new CommonTokenStream(lexer);
				final CqlParser parser = new CqlParser(token);
				final org.apache.cassandra.cql3.statements.ParsedStatement query = parser.query();
				if (query != null && query instanceof CFStatement) {
					final CFStatement parsedStmt = (CFStatement) query;
					final List<AbstractBind<ConcreteValue>> nonUsedBinds = new ArrayList<>(stmt.binds);
					final Map<Integer, ConcreteValue> bindMap = getBinds(parsedStmt, nonUsedBinds);
					final String cql = stmt.getQueryWithUnnamedBindMarkers();
					final ParsedStatement ret;
					if (parsedStmt instanceof UpdateStatement.ParsedInsertJson) {
						throw new RuntimeException("InsertJson-Queries are not supported");
					}
					if (parsedStmt instanceof SelectStatement.RawStatement) {
						ret = ParsedQueryStatement.create(cql,
								getQueryPart((SelectStatement.RawStatement) parsedStmt, nonUsedBinds, bindMap));
					} else if (parsedStmt instanceof BatchStatement.Parsed) {
						ret = ParsedBatchUpdateStatement.create(cql,
								getUpdateParts((BatchStatement.Parsed) parsedStmt, nonUsedBinds, bindMap));
					} else if (parsedStmt instanceof UpdateStatement.ParsedInsert || parsedStmt instanceof UpdateStatement.ParsedUpdate
							|| parsedStmt instanceof DeleteStatement.Parsed) {
						ret = ParsedUpdateStatement.create(cql,
								getUpdatePart((ModificationStatement.Parsed) parsedStmt, nonUsedBinds, bindMap));
					} else {
						ret = null;
					}
					if (ret != null) {
						statementCache.put(stmt, ret);
						return ret;
					}
				}
			} catch (final Exception e) {
				LOG.error("Error parsing query " + stmt.query + ". Use Unknown...", e);
			}
		}
		throw new RuntimeException("Could not parse " + stmt);
	}

	private ImmutableMap<Integer, ConcreteValue> getBinds(final CFStatement parsedStmt,
			List<AbstractBind<ConcreteValue>> nonUsedBinds)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Field f = VariableSpecifications.class.getDeclaredField("variableNames");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<ColumnIdentifier> vars = (List<ColumnIdentifier>) f.get(parsedStmt.getBoundVariables());
		final ImmutableMap.Builder<Integer, ConcreteValue> bindMap = ImmutableMap.builder();
		if (LOG.isDebugEnabled()) {
			LOG.debug("varNames: " + vars);
			LOG.debug("Parsed query: " + parsedStmt);
		}
		int nextUserBindIdx = 0;
		int idx = 0;
		for (final ColumnIdentifier var : vars) {
			boolean isSystemBind = false;
			if (var != null) {
				final ListIterator<AbstractBind<ConcreteValue>> bindIt = nonUsedBinds.listIterator(nonUsedBinds.size());
				while (bindIt.hasPrevious()) {
					final AbstractBind<ConcreteValue> bind = bindIt.previous();
					if (bind instanceof SystemBind && ((SystemBind<?>) bind).name.equalsIgnoreCase(var.toString())) {
						bindMap.put(idx, bind.value);
						isSystemBind = true;
						bindIt.remove();
						break;
					}
				}
			}
			if (!isSystemBind) {
				// read list backwards --> binds that are added later overwrite previous binds
				final ListIterator<AbstractBind<ConcreteValue>> bindIt = nonUsedBinds.listIterator(nonUsedBinds.size());
				boolean found = false;
				while (bindIt.hasPrevious()) {
					final AbstractBind<ConcreteValue> nextBind = bindIt.previous();
					if (nextBind instanceof UserBind) {
						final UserBind<ConcreteValue> nextUserBind = (UserBind<ConcreteValue>) nextBind;
						if (nextUserBind.idx instanceof IntValue && ((IntValue) nextUserBind.idx).number == nextUserBindIdx) {
							if (!found) {
								bindMap.put(idx, nextUserBind.value);
								found = true;
							}
							bindIt.remove();
						} else if (nextUserBind.idx instanceof StringValue && var != null
								&& var.toString().equalsIgnoreCase(((StringValue) nextUserBind.idx).string)) {
							if (!found) {
								bindMap.put(idx, nextUserBind.value);
								found = true;
							}
							bindIt.remove();
						}
					}
				}
				nextUserBindIdx++;
			}
			idx++;
		}
		return bindMap.build();
	}

	private StatementPart.QueryPart getQueryPart(final SelectStatement.RawStatement parsedStatement,
                                                 final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {
		final String table = getTable(parsedStatement);
		final List<Constraint> constraints = getConstraints(parsedStatement, nonUsedBinds, bindMap);
		if (parsedStatement.selectClause.isEmpty()) {
			return StatementPart.SelectAllPart.create(table, Constraints.create(constraints));
		} else {
			final List<ColumnSelection> cols = new ArrayList<>();
			for (final RawSelector selector : parsedStatement.selectClause) {
				cols.add(getColumnSelection(selector.selectable));
			}
			return StatementPart.SelectColsPart.create(table, cols, Constraints.create(constraints));
		}
	}

	private ColumnSelection getColumnSelection(final Selectable.Raw selectable) throws Exception {
		if (selectable instanceof ColumnDefinition.Raw) {
			return ColumnSelection.create(((ColumnDefinition.Raw) selectable).rawText().toLowerCase());
		} else if (selectable instanceof WithCast.Raw) {
			Field f = WithCast.Raw.class.getDeclaredField("arg");
			f.setAccessible(true);
			return getColumnSelection((Selectable.Raw) f.get(selectable));
		} else {
			throw new RuntimeException("Unknown Selectable " + selectable);
		}
	}

	private Set<StatementPart.UpdatePart> getUpdateParts(final BatchStatement.Parsed parsedStatement,
                                                         final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final Field f = BatchStatement.Parsed.class.getDeclaredField("parsedStatements");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<ModificationStatement.Parsed> batchStatements = (List<ModificationStatement.Parsed>) f.get(parsedStatement);
		final Set<StatementPart.UpdatePart> parts = new HashSet<>();
		for (final ModificationStatement.Parsed batchStmt : batchStatements) {
			parts.add(getUpdatePart(batchStmt, nonUsedBinds, bindMap));
		}
		return parts;
	}

	private StatementPart.UpdatePart getUpdatePart(final ModificationStatement.Parsed parsedStatement,
                                                   final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final String table = getTable(parsedStatement);
		if (parsedStatement instanceof UpdateStatement.ParsedInsert) {
			return getUpdatePartForInsert((UpdateStatement.ParsedInsert) parsedStatement, table, nonUsedBinds, bindMap);
		} else if (parsedStatement instanceof UpdateStatement.ParsedUpdate) {
			return getUpdatePartForUpdate((UpdateStatement.ParsedUpdate) parsedStatement, table, nonUsedBinds, bindMap);
		} else if (parsedStatement instanceof DeleteStatement.Parsed) {
			return getUpdatePartForDelete((DeleteStatement.Parsed) parsedStatement, table, nonUsedBinds, bindMap);
		}
		throw new RuntimeException("Cannot extract parts from " + parsedStatement);
	}

	private StatementPart.UpdatePart getUpdatePartForInsert(final UpdateStatement.ParsedInsert parsedInsert, final String table,
                                                            final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final Field f = UpdateStatement.ParsedInsert.class.getDeclaredField("columnNames");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<ColumnDefinition.Raw> columnNames = (List<ColumnDefinition.Raw>) f.get(parsedInsert);
		final Field f2 = UpdateStatement.ParsedInsert.class.getDeclaredField("columnValues");
		f2.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<Term.Raw> columnValues = (List<Term.Raw>) f2.get(parsedInsert);
		if (columnNames.size() != columnValues.size()) {
			throw new RuntimeException("NUmber of columnNames must macth number of columnValues");
		}
		final List<ColumnUpdate> columnUpdates = new ArrayList<>();
		final Iterator<ColumnDefinition.Raw> colIt = columnNames.iterator();
		final Iterator<Term.Raw> valIt = columnValues.iterator();
		while (colIt.hasNext()) {
			final String col = colIt.next().rawText().toLowerCase();
			final ConcreteValue val = getValue(valIt.next(), col, nonUsedBinds, bindMap);
			if (val != null) {
				columnUpdates.add(ColumnValueUpdate.create(col, val));
			} else {
				columnUpdates.add(ColumnUnknownValueUpdate.create(col));
			}
		}
		return StatementPart.UpsertPart.createInsert(table, columnUpdates, isIfNotExists(parsedInsert));
	}

	private StatementPart.UpdatePart getUpdatePartForUpdate(final UpdateStatement.ParsedUpdate parsedUpdate, final String table,
                                                            final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final Field f = UpdateStatement.ParsedUpdate.class.getDeclaredField("updates");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<Pair<ColumnDefinition.Raw, Operation.RawUpdate>> updates = (List<Pair<ColumnDefinition.Raw, Operation.RawUpdate>>) f
				.get(parsedUpdate);

		final List<ColumnUpdate> cols = new ArrayList<>();
		for (Pair<ColumnDefinition.Raw, Operation.RawUpdate> update : updates) {
			final String col = update.left.rawText().toLowerCase();
			if (update.right instanceof Operation.SetValue) {
				final Field valF = Operation.SetValue.class.getDeclaredField("value");
				valF.setAccessible(true);
				final ConcreteValue val = getValue((Term.Raw) valF.get(update.right), col, nonUsedBinds, bindMap);
				if (val != null) {
					cols.add(ColumnValueUpdate.create(col, val));
				} else {
					cols.add(ColumnUnknownValueUpdate.create(col));
				}
			} else if (update.right instanceof Operation.Addition || update.right instanceof Operation.Substraction) {
				final Field valF = update.right.getClass().getDeclaredField("value");
				valF.setAccessible(true);
				final Term.Raw valRaw = (Term.Raw) valF.get(update.right);
				if (valRaw instanceof AbstractMarker.Raw) {
					final ConcreteValue val = getValue(valRaw, col, nonUsedBinds, bindMap);
					if (val instanceof IntValue) {
						cols.add(ColumnCounterUpdate.create(col));
					} else {
						cols.add(ColumnPartUpdate.create(col));
					}
				} else if (valRaw instanceof Constants.Literal) {
					cols.add(ColumnCounterUpdate.create(col));
				} else if (valRaw instanceof Sets.Literal) {
					final Field elementsF = Sets.Literal.class.getDeclaredField("elements");
					elementsF.setAccessible(true);
					@SuppressWarnings("unchecked")
					final List<Term.Raw> elements = (List<Raw>) elementsF.get(valRaw);
					if (elements.size() == 1) {
						final ColumnSetUpdate.SetOp op = update.right instanceof Operation.Addition
								? ColumnSetUpdate.SetOp.ADD
								: ColumnSetUpdate.SetOp.REMOVE;
						final ConcreteValue val = getValue(elements.get(0), col, nonUsedBinds, bindMap);
						if (val == null) {
							cols.add(ColumnSetUpdate.create(col, op));
						} else {
							cols.add(ColumnSetValueUpdate.create(col, op, val));
						}
					} else {
						cols.add(ColumnPartUpdate.create(col));
					}
				} else {
					cols.add(ColumnPartUpdate.create(col));
				}
			} else {
				cols.add(ColumnPartUpdate.create(col));
			}
		}
		return StatementPart.UpsertPart.createUpdate(table, cols, getConstraints(parsedUpdate, nonUsedBinds, bindMap),
				isIfExists(parsedUpdate) || hasConditions(parsedUpdate), isIfExists(parsedUpdate));
	}

	private StatementPart.UpdatePart getUpdatePartForDelete(final DeleteStatement.Parsed parsedDelete, final String table,
                                                            final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final Field f = DeleteStatement.Parsed.class.getDeclaredField("deletions");
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		final List<Operation.RawDeletion> deletions = (List<Operation.RawDeletion>) f.get(parsedDelete);
		if (deletions.isEmpty()) {
			final List<Constraint> constraints = getConstraints(parsedDelete, nonUsedBinds, bindMap);
			return StatementPart.DeletePart.create(table, Constraints.create(constraints), isIfExists(parsedDelete) || hasConditions(parsedDelete),
					isIfExists(parsedDelete));
		} else {
			final List<ColumnUpdate> cols = new ArrayList<>();
			for (Operation.RawDeletion deletion : deletions) {
				final String col = deletion.affectedColumn().rawText().toLowerCase();
				if (deletion instanceof Operation.ColumnDeletion) {
					cols.add(ColumnValueUpdate.create(col, NullValue.create()));
				} else {
					cols.add(ColumnPartUpdate.create(col));
				}
			}
			return StatementPart.UpsertPart.createDelete(table, cols, getConstraints(parsedDelete, nonUsedBinds, bindMap),
					isIfExists(parsedDelete) || hasConditions(parsedDelete), isIfExists(parsedDelete));
		}
	}

	private static String getTable(final CFStatement parsedStatement) {
		final String cf = parsedStatement.columnFamily();
		final String table = cf.substring(cf.indexOf(".") + 1).trim().toLowerCase();
		if (table == null || table.isEmpty() || table.indexOf(".") >= 0) {
			throw new RuntimeException("Illegal Column Family");
		}
		return table;
	}

	private static boolean isIfExists(final ModificationStatement.Parsed stmt)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Field f = ModificationStatement.Parsed.class.getDeclaredField("ifExists");
		f.setAccessible(true);
		return f.getBoolean(stmt);
	}

	private static boolean isIfNotExists(final ModificationStatement.Parsed stmt)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Field f = ModificationStatement.Parsed.class.getDeclaredField("ifNotExists");
		f.setAccessible(true);
		return f.getBoolean(stmt);
	}

	private static boolean hasConditions(final ModificationStatement.Parsed stmt)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Field f = ModificationStatement.Parsed.class.getDeclaredField("conditions");
		f.setAccessible(true);
		return !((List<?>) f.get(stmt)).isEmpty();
	}

	private List<Constraint> getConstraints(final CFStatement parsedStatement0, final List<AbstractBind<ConcreteValue>> nonUsedBinds,
			final Map<Integer, ConcreteValue> bindMap) throws Exception {
		if (parsedStatement0 instanceof SelectStatement.RawStatement || parsedStatement0 instanceof UpdateStatement.ParsedUpdate
				|| parsedStatement0 instanceof DeleteStatement.Parsed) {
			final WhereClause whereClause;
			if (parsedStatement0 instanceof SelectStatement.RawStatement) {
				whereClause = ((SelectStatement.RawStatement) parsedStatement0).whereClause;
			} else if (parsedStatement0 instanceof UpdateStatement.ParsedUpdate) {
				final Field f = UpdateStatement.ParsedUpdate.class.getDeclaredField("whereClause");
				f.setAccessible(true);
				whereClause = (WhereClause) f.get(parsedStatement0);
			} else {
				final Field f = DeleteStatement.Parsed.class.getDeclaredField("whereClause");
				f.setAccessible(true);
				whereClause = (WhereClause) f.get(parsedStatement0);
			}
			if (!whereClause.expressions.isEmpty()) {
				throw new RuntimeException("Custom index expressions are not supported");
			}
			final List<Constraint> constraints = new ArrayList<>();
			for (final Relation relation : whereClause.relations) {
				if (relation instanceof MultiColumnRelation) {
					for (final ColumnDefinition.Raw col : ((MultiColumnRelation) relation).getEntities()) {
						constraints.add(UnknownConstraint.create(col.rawText().toLowerCase()));
					}
				} else if (relation instanceof TokenRelation) {
					final Field f = TokenRelation.class.getDeclaredField("entities");
					f.setAccessible(true);
					@SuppressWarnings("unchecked")
					final List<ColumnDefinition.Raw> cols = (List<ColumnDefinition.Raw>) f.get(relation);
					for (final ColumnDefinition.Raw col : cols) {
						constraints.add(UnknownConstraint.create(col.rawText().toLowerCase()));
					}
				} else {
					final SingleColumnRelation rel = (SingleColumnRelation) relation;
					if (rel.getMapKey() != null) {
						constraints.add(UnknownConstraint.create(rel.getEntity().rawText().toLowerCase()));
					} else if (rel.operator() == Operator.IN && rel.getInValues() != null) {
						constraints.add(getInConstraint(rel.getEntity(), rel.getInValues(), nonUsedBinds, bindMap));
					} else {
						constraints.add(getConstraint(rel.getEntity(), rel.operator(), rel.getValue(), nonUsedBinds, bindMap));
					}
				}
			}
			return constraints;
		}
		throw new RuntimeException("Unknown statement" + parsedStatement0);
	}

	private Constraint getConstraint(final ColumnDefinition.Raw col, final Operator op, final Term.Raw value,
			final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final ConcreteValue concVal = getValue(value, col.rawText().toLowerCase(), nonUsedBinds, bindMap);
		if (op == Operator.EQ) {
			if (concVal != null) {
				return EQValConstraint.create(col.rawText().toLowerCase(), concVal);
			} else {
				return EQConstraint.create(col.rawText().toLowerCase());
			}
		} else if (op == Operator.IN) {
			return INConstraint.create(col.rawText().toLowerCase());
		}
		return UnknownConstraint.create(col.rawText().toLowerCase());
	}

	private Constraint getInConstraint(final ColumnDefinition.Raw col, final List<? extends Term.Raw> values,
			final List<AbstractBind<ConcreteValue>> nonUsedBinds, final Map<Integer, ConcreteValue> bindMap) throws Exception {

		final List<ConcreteValue> inValues = new ArrayList<>();
		for (final Term.Raw val : values) {
			if (val == null) {
				// parsing problem
				return INConstraint.create(col.rawText().toLowerCase());
			}
			final ConcreteValue concVal = getValue(val, col.rawText().toLowerCase(), nonUsedBinds, bindMap);
			if (concVal == null) {
				// value is not comparable, e.g. UnknownMutableValue or widenenedImmutableValue
				return INConstraint.create(col.rawText().toLowerCase());
			} else {
				inValues.add(concVal);
			}
		}
		return INValsConstraint.create(col.rawText().toLowerCase(), inValues);
	}

	private ConcreteValue getValue(final Term.Raw value, final String column, final List<AbstractBind<ConcreteValue>> nonUsedBinds,
			final Map<Integer, ConcreteValue> bindMap) throws Exception {

		if (value instanceof Constants.Literal) {
			final Constants.Literal constLit = (Constants.Literal) value;
			final Field typeField = Constants.Literal.class.getDeclaredField("type");
			typeField.setAccessible(true);
			final Constants.Type constType = (Constants.Type) typeField.get(constLit);
			if (constType == Constants.Type.INTEGER) {
				return IntValue.create(Integer.parseInt(constLit.getRawText()));
			} else if (constType == Constants.Type.STRING) {
				return StringValue.create(constLit.getRawText());
			} else if (constType == Constants.Type.BOOLEAN) {
				if ("TRUE".equalsIgnoreCase(constLit.getRawText())) {
					return IntValue.create(1);
				} else {
					return IntValue.create(0);
				}
			} else {
				// we use the string representation of the value for creating an ID
				return UnknownImmutableValue.create(ProgramPointId.create(CallStack.EMPTY_STACK, constLit.getRawText()));
			}
		} else if (value instanceof AbstractMarker.Raw) {
			final Field idxField = AbstractMarker.Raw.class.getDeclaredField("bindIndex");
			idxField.setAccessible(true);
			final int bindIdx = idxField.getInt(value);
			ConcreteValue ret = null;
			if (bindMap.containsKey(bindIdx)) {
				ret = bindMap.get(bindIdx);
			} else if (column != null) {
				final ListIterator<AbstractBind<ConcreteValue>> bindIt = nonUsedBinds.listIterator(nonUsedBinds.size());
				while (bindIt.hasPrevious()) {
					final AbstractBind<ConcreteValue> nextBind = bindIt.previous();
					if (nextBind instanceof UserBind && ((UserBind<?>) nextBind).idx instanceof StringValue
							&& ((StringValue) ((UserBind<?>) nextBind).idx).string.equalsIgnoreCase(column)) {
						if (ret == null) {
							ret = ((UserBind<ConcreteValue>) nextBind).value;
						}
						bindIt.remove();
					}
				}
			}
			if (ret != null && ret instanceof StatementSingleResultValue) {
				return transformStatementSingleResultValue((StatementSingleResultValue) ret);
			}
			return ret;
		}
		return null;
	}

	private ConcreteValue transformStatementSingleResultValue(StatementSingleResultValue statementResult) {
		final Set<ImmutableValue> possibleValues = new HashSet<>();
		for (final StatementValue stmt : statementResult.resultSet.resultFrom.statementArg) {
			final ParsedStatement parsed = parseStatementValue(stmt);
			if (parsed == null || !(parsed instanceof ParsedQueryStatement)) {
				return null;
			}
			final ParsedQueryStatement query = (ParsedQueryStatement) parsed;
			String col = null;
			if (statementResult.idx instanceof IntValue && query.queryPart instanceof StatementPart.SelectColsPart) {
				final int idx = ((IntValue) statementResult.idx).number;
				final StatementPart.SelectColsPart selectCols = (StatementPart.SelectColsPart) query.queryPart;
				if (idx < 0 || idx >= selectCols.getSelectedColumns().size()) {
					return null;
				}
				col = selectCols.getSelectedColumns().get(idx).column;
			} else if (statementResult.idx instanceof StringValue) {
				col = ((StringValue) statementResult.idx).string;
			}
			if (col != null) {
				final ImmutableListMultimap<String, Integer> constraintIdxs = query.queryPart.getDefiniteConstraintIdxs();
				if (constraintIdxs.containsKey(col)) {
					for (final int constraintIdx : constraintIdxs.get(col)) {
						final Constraint constraint = query.queryPart.getConstraint(constraintIdx);
						if (constraint instanceof EQValConstraint
								&& ((EQValConstraint) constraint).value instanceof ImmutableValue) {
							possibleValues.add((ImmutableValue) ((EQValConstraint) constraint).value);
						} else {
							return null;
						}
					}
				}
			}
		}
		if (possibleValues.isEmpty()) {
			return null;
		}
		return ImmutableChoiceValue.create(ProgramPointId.unique(), possibleValues);
	}

	public static ParsedCreateTableStatement tryParseCreateTable(final String stmt) {
		try {
			final ANTLRStringStream inputStream = new ANTLRStringStream(stmt);
			final CqlLexer lexer = new CqlLexer(inputStream);
			final CommonTokenStream token = new CommonTokenStream(lexer);
			final CqlParser parser = new CqlParser(token);
			final org.apache.cassandra.cql3.statements.ParsedStatement parsed = parser.query();
			if (parsed instanceof CreateTableStatement.RawStatement) {
				final CreateTableStatement.RawStatement createTableStmt = (CreateTableStatement.RawStatement) parsed;
				final String table = getTable(createTableStmt);
				final Field fdefs = CreateTableStatement.RawStatement.class.getDeclaredField("definitions");
				fdefs.setAccessible(true);
				@SuppressWarnings("unchecked")
				final Map<ColumnIdentifier, CQL3Type.Raw> defs = (Map<ColumnIdentifier, CQL3Type.Raw>) fdefs.get(createTableStmt);
				final Field fpks = CreateTableStatement.RawStatement.class.getDeclaredField("keyAliases");
				fpks.setAccessible(true);
				@SuppressWarnings("unchecked")
				final List<List<ColumnIdentifier>> pks = (List<List<ColumnIdentifier>>) fpks.get(createTableStmt);
				final Field fpks2 = CreateTableStatement.RawStatement.class.getDeclaredField("columnAliases");
				fpks2.setAccessible(true);
				@SuppressWarnings("unchecked")
				final List<ColumnIdentifier> pks2 = (List<ColumnIdentifier>) fpks2.get(createTableStmt);
				if (pks.size() != 1) {
					return null;
				}
				final Set<String> primaryKeyColumns = new HashSet<>();
				for (final ColumnIdentifier pkCol : pks.get(0)) {
					primaryKeyColumns.add(pkCol.toString().toLowerCase());
				}
				for (final ColumnIdentifier pkCol : pks2) {
					primaryKeyColumns.add(pkCol.toString().toLowerCase());
				}
				final Set<String> nonPrimaryKeyColumns = new HashSet<>();
				for (final ColumnIdentifier nonPkCol : defs.keySet()) {
					final String nonPkColStr = nonPkCol.toString().toLowerCase();
					if (!primaryKeyColumns.contains(nonPkColStr)) {
						nonPrimaryKeyColumns.add(nonPkColStr);
					}
				}
				return ParsedCreateTableStatement.create(table, primaryKeyColumns, nonPrimaryKeyColumns);
			}
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Error parsing " + stmt, e);
			}
		}
		return null;
	}
}
