package ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts;

import java.util.Objects;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind.SystemBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;

public class BuiltInsertStatementValue extends AbstractBuiltStatementValue {

	public final String table;
	public final ImmutableList<String> columns;
	public final ImmutableList<String> bindMarkers;
	public final boolean ifNotExists;

	private BuiltInsertStatementValue(final String table) {
		super(ImmutableList.of());
		this.table = Objects.requireNonNull(table);
		this.columns = ImmutableList.of();
		this.bindMarkers = ImmutableList.of();
		this.ifNotExists = false;
	}

	public static BuiltInsertStatementValue create(final StringValue keyspace, final StringValue table) {
		if (keyspace != null) {
			return new BuiltInsertStatementValue(keyspace.string + "." + table.string);
		} else {
			return new BuiltInsertStatementValue(table.string);
		}
	}

	private BuiltInsertStatementValue(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel, final String table,
			final ImmutableList<String> columns, final ImmutableList<String> bindMarkers, final boolean ifNotExists) {
		super(binds, consistencyLevel, serialConsistencyLevel);
		this.table = table;
		this.columns = columns;
		this.bindMarkers = bindMarkers;
		this.ifNotExists = ifNotExists;
	}

	@Override
	protected AbstractBuiltStatementValue createNewInternal(final ImmutableList<AbstractBind<VarLocalOrImmutableValue>> binds,
			final ImmutableValue consistencyLevel, final ImmutableValue serialConsistencyLevel) {
		return new BuiltInsertStatementValue(binds, consistencyLevel, serialConsistencyLevel, table, columns, bindMarkers, ifNotExists);
	}

	public BuiltInsertStatementValue addColumnValue(final StringValue column, final VarLocalOrImmutableValue value) {
		final String bindmarker = getNewBindName();
		return new BuiltInsertStatementValue(
				ImmutableList.<AbstractBind<VarLocalOrImmutableValue>> builder().addAll(binds).add(SystemBind.create(bindmarker, value))
						.build(),
				consistencyLevel,
				serialConsistencyLevel,
				table,
				ImmutableList.<String> builder().addAll(columns).add(column.string).build(),
				ImmutableList.<String> builder().addAll(bindMarkers).add(":" + bindmarker).build(),
				ifNotExists);
	}

	public BuiltInsertStatementValue setIfNotExists(final boolean ifNotExists) {
		return new BuiltInsertStatementValue(binds, consistencyLevel, serialConsistencyLevel, table, columns, bindMarkers, ifNotExists);
	}

	@Override
	public String getStatement() {
		final StringBuilder sb = new StringBuilder("INSERT INTO ");
		sb.append(table);
		sb.append(" (").append(String.join(", ", columns)).append(") ");
		sb.append("VALUES");
		sb.append(" (").append(String.join(", ", bindMarkers)).append(")");
		if (ifNotExists) {
			sb.append(" IF NOT EXISTS");
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bindMarkers == null) ? 0 : bindMarkers.hashCode());
		result = prime * result + ((columns == null) ? 0 : columns.hashCode());
		result = prime * result + (ifNotExists ? 1231 : 1237);
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuiltInsertStatementValue other = (BuiltInsertStatementValue) obj;
		if (bindMarkers == null) {
			if (other.bindMarkers != null)
				return false;
		} else if (!bindMarkers.equals(other.bindMarkers))
			return false;
		if (columns == null) {
			if (other.columns != null)
				return false;
		} else if (!columns.equals(other.columns))
			return false;
		if (ifNotExists != other.ifNotExists)
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}
}
