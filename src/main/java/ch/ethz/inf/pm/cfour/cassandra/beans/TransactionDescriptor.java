package ch.ethz.inf.pm.cfour.cassandra.beans;

import java.util.Objects;

/**
 * Unique representation of a transaction
 */
public class TransactionDescriptor implements Comparable<TransactionDescriptor> {

	public final Method entryMethod;
	public final boolean isOnlyForDisplaying;

	private TransactionDescriptor(final Method entryMethod, final boolean isOnlyForDisplaying) {
		this.entryMethod = Objects.requireNonNull(entryMethod);
		this.isOnlyForDisplaying = isOnlyForDisplaying;
	}

	public static TransactionDescriptor create(final Method entryMethod, final boolean isOnlyForDisplaying) {
		return new TransactionDescriptor(entryMethod, isOnlyForDisplaying);
	}

	public String getUniqueDescription() {
		return entryMethod.signature;
	}

	public String getShortName() {
		return entryMethod.getShortName();
	}

	public String getShortSignature() {
		return entryMethod.getShortSignature();
	}

	@Override
	public int compareTo(TransactionDescriptor o) {
		return getUniqueDescription().compareTo(o.getUniqueDescription());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entryMethod == null) ? 0 : entryMethod.hashCode());
		result = prime * result + (isOnlyForDisplaying ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionDescriptor other = (TransactionDescriptor) obj;
		if (entryMethod == null) {
			if (other.entryMethod != null)
				return false;
		} else if (!entryMethod.equals(other.entryMethod))
			return false;
		if (isOnlyForDisplaying != other.isOnlyForDisplaying)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return (isOnlyForDisplaying ? "Display " : "") + "Transaction [" + entryMethod.getShortName() + "]";
	}
}
