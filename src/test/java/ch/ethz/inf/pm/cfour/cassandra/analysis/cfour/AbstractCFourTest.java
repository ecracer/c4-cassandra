package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.AbstractSootTest;

public abstract class AbstractCFourTest extends AbstractSootTest {

	public AbstractCFourTest(final String className) throws IOException {
		super(className);
	}

	protected abstract void fillOptionsList(final List<Options> optionsList);

	@Override
	protected Options getInitialOptions() {
		final Options options = super.getInitialOptions();
		options.setCreateSchemaFile("supplied");
		return options;
	}

	protected abstract SchemaInformation buildSchemaInformation();

	@Override
	protected List<Options> createOptions() {
		final List<Options> options = new ArrayList<>();
		fillOptionsList(options);
		return options;
	}

	@Override
	protected void checkTransactionExecuteGraph(final Options options,
			final ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(
				options).transform(executeGraphs);

		final CFourResult result = new CFourSerializabilityAnalysis(options).check(transactionExecuteGraphs,
				"supplied".equals(options.getCreateSchemaFile()) ? buildSchemaInformation() : SchemaInformation.create());

		checkResult(options, result);
	}

	protected abstract void checkResult(final Options options, final CFourResult result);
}
