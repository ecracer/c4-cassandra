package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefObjectValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.AbstractBuiltStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltStatementAssignmentValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;

public interface RefValueVisitor<R> {

	R visit(final RefValue value);

	R visitRefArrayValue(final RefArrayValue arrayValue);

	R visitRefObjectValue(final RefObjectValue objectValue);

	R visitAbstractBuiltStatementValue(final AbstractBuiltStatementValue statementValue);

	R visitBuiltStatmentClauseValue(final BuiltStatementClauseValue clauseValue);

	R visitBuiltStatmentAssignmentValue(final BuiltStatementAssignmentValue assignmentValue);
}
