import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.querybuilder.QueryBuilder;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * User class is an implementation for twitter
 * Created by jojo on 11/11/15.
 */
public class User {
    private String Username;
    private String Password;
    private String TableName;
    private Query query;

    /**
     * Constructor of a User class
     *
     * @param uname
     * @param pass
     * @throws NoSuchAlgorithmException
     */
    public User(String uname, String pass) throws NoSuchAlgorithmException {
        Username = uname;
        Password = md5(pass);
        TableName = "users";
        query = new Query(CassaMain.serverIP, CassaMain.keyspace);
    }

    /**
     * md5(String) is used to hashed a string from input to a md5 hash.
     *
     * @param input
     * @return String
     * @throws NoSuchAlgorithmException
     */
    public String md5(String input) throws NoSuchAlgorithmException {
        if (input.isEmpty()) {
            return "";
        } else {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        }
    }

    /**
     * Save() is used to save new user in the database
     *
     * @return boolean
     */
    public boolean save() {
        if (Username.isEmpty() || Password.isEmpty() || !loadUsername()) {
            return false;
        } else {
        	final String Username = this.Username;
        	ClientLocalValues.set("username", Username);
            query.getSession().execute(QueryBuilder.insertInto(TableName)
                    .value("username", Username)
                    .value("password", Password));
            return true;
        }
    }

    /**
     * Search a username in database , true if found
     *
     * @return boolean
     */
    public boolean loadUsername() {
    	final String Username = this.Username;
    	ClientLocalValues.set("username", Username);
        if (Username.isEmpty()) {
            return false;
        } else {
            boolean ret = true;
            ResultSet set = query.getSession().execute(QueryBuilder.select().all()
                    .from(TableName)
                    .where(QueryBuilder.eq("username", Username)));
            for (Row tuple : set) {
                ret = tuple.getString("username").equals(Username);
            }
            return ret;
        }
    }

    /**
     * Load() is used to check if the user and password are correct.
     *
     * @return boolean
     */
    public boolean load() {
    	final String Username = this.Username;
    	ClientLocalValues.set("username", Username);
        if (Username.isEmpty() || Password.isEmpty()) {
            return false;
        } else {
            boolean ret = false;
            ResultSet set = query.getSession().execute(QueryBuilder.select().all()
                    .from(TableName)
                    .where(QueryBuilder.eq("username", Username))
                    .and(QueryBuilder.eq("password", Password)));
            for (Row tuple : set) {
                ret = tuple.getString("username").equals(Username) && tuple.getString("password").equals(Password);
            }
            return ret;
        }
    }

    /**
     * Add a friend to friendlist and follower list
     *
     * @param friendUsername
     * @return
     */
    public boolean friend(String friendUsername) {
    	final String username = this.Username;
		ClientLocalValues.set("username", username);
        query.getSession().execute(QueryBuilder.insertInto("friends")
                .value("username", username)
                .value("friend", friendUsername)
                .value("since", new Date()));

        query.getSession().execute(QueryBuilder.insertInto("followers")
                .value("username", username)
                .value("follower", friendUsername)
                .value("since", new Date()));
        return true;
    }

    public String getFriends() {
    	final String username = this.Username;
    	ClientLocalValues.set("username", username);
        String resString = "";
        ResultSet friends = query.getSession().execute(QueryBuilder.select("friend")
                .from("friends")
                .where(QueryBuilder.eq("username", username))
        );

        List<Row> row = friends.all();
        for (int i = 0; i < row.size(); i++) {
            resString += row.get(i).getString("friend") + "\n";
        }

        return resString;
    }

    public String getUsername() {
    	final String username = this.Username;
    	ClientLocalValues.set("username", username);
        return username;
    }
}
