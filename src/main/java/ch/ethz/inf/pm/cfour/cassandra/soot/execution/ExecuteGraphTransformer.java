package ch.ethz.inf.pm.cfour.cassandra.soot.execution;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.soot.includedcollector.IncludedCollectorTransformer;
import soot.Body;
import soot.BodyTransformer;
import soot.SootMethod;

/**
 * Transformation from method to execute graph. A node ({@link AbstractProgramPoint}) in the execute graph abstracts the
 * effects of the statement on the {@link ExecutionState}. An edge represents control-flow edge in the original method.
 */
public class ExecuteGraphTransformer extends BodyTransformer {

	private final IncludedCollectorTransformer includedTransformer;
	private final SootOptions options;
	private final ImmutableMap.Builder<SootMethod, ExecuteGraph> executeGraphsBuilder = ImmutableMap.builder();
	private Set<SootMethod> includedMethods = null;

	public ExecuteGraphTransformer(final IncludedCollectorTransformer includedTransformer, final SootOptions options) {
		this.includedTransformer = Objects.requireNonNull(includedTransformer);
		this.options = Objects.requireNonNull(options);
	}

	@Override
	protected void internalTransform(final Body body, final String phaseName, @SuppressWarnings("rawtypes") final Map options) {
		if (includedMethods == null) {
			includedMethods = includedTransformer.getIncludedMethods();
		}
		final SootMethod method = body.getMethod();
		// Soot adds method during the analysis. We only include methods we found in the first search
		if (includedMethods.contains(method)) {
			final ExecuteGraph executeGraph = new ExecuteGraphAnalysis(includedMethods, method, this.options,
					body).getExecuteGraph();
			executeGraphsBuilder.put(method, executeGraph);
		}
	}

	public ImmutableMap<SootMethod, ExecuteGraph> getExecuteGraphs() {
		return executeGraphsBuilder.build();
	}
}
