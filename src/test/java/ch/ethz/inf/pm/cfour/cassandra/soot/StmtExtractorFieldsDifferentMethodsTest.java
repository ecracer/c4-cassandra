package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsDifferentMethodsTest extends AbstractSootTest {

	public StmtExtractorFieldsDifferentMethodsTest() throws IOException {
		super("FieldsDifferentMethods", "execute");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(3, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			assertEquals(1, cqls.size());
			assertEquals("", cqls.iterator().next());
		}
	}
}
