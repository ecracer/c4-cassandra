package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;

public class StmtExtractorFieldsAliasingTest extends AbstractSootTest {

	public StmtExtractorFieldsAliasingTest() throws IOException {
		super("FieldsAliasing", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());
		for (final SessionExecuteInvoke call : result) {
			assertEquals(1, call.statementArg.size());
			final StatementValue statement = call.statementArg.iterator().next();
			assertEquals("SELECT * FROM table1 WHERE name = ?", statement.getQueryWithUnnamedBindMarkers());
			assertEquals(1, statement.binds.size());
			final ImmutableChoiceValue val = (ImmutableChoiceValue) statement.binds.get(0).value;
			assertEquals(3, val.choices.size());
			assertTrue(val.choices.contains(StringValue.create("alice")));
			assertTrue(val.choices.contains(StringValue.create("bob")));
			assertTrue(val.choices.contains(NullValue.create()));
		}
	}
}
