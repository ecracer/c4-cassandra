package com.cilesizemre.twissandra.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cilesizemre.twissandra.controller.service.user.UserService;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

@RestController
@RequestMapping(value = "/user/")
public class UserRestService {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/add")
	@Transaction
	public void addUser(@RequestParam String username, @RequestParam String password) {
		ClientLocalValues.set("username", username);
		userService.addUser(username, password);
	}
	
	@RequestMapping(value = "/follow")
	@Transaction
	public void followUser(@RequestParam String username, @RequestParam String friend) {
		ClientLocalValues.set("username", username);
		userService.followUser(username, friend);
	}
	
}
