package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldEscapeTest extends AbstractSootTest {

	public StmtExtractorFieldEscapeTest() throws IOException {
		super("FieldEscape", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(4, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			switch (nextCall.executedFrom.callStack.method.methodName) {
				case "test1" :
				case "test2" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains(""));
					break;
				case "test3" :
					assertEquals(2, cqls.size());
					assertTrue(cqls.contains(""));
					assertTrue(cqls.contains("SELECT * FROM test2"));
					break;
				case "test4" :
					assertEquals(2, cqls.size());
					assertTrue(cqls.contains(""));
					assertTrue(cqls.contains("SELECT * FROM test4"));
					break;
				default :
					assertTrue(false);
			}
		}
	}
}
