package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

public interface ValueVisitor<R> extends ConcreteValueVisitor<R>, RefValueVisitor<R> {

}
