package com.datastax.demo.killrchat.service;


import javax.inject.Inject;

import com.datastax.demo.killrchat.entity.UserEntity;
import com.datastax.demo.killrchat.exceptions.RememberMeDoesNotExistException;
import com.datastax.demo.killrchat.exceptions.UserAlreadyExistsException;
import com.datastax.demo.killrchat.exceptions.UserNotFoundException;
import com.datastax.demo.killrchat.model.UserModel;
import com.datastax.demo.killrchat.security.utils.SecurityUtils;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import info.archinnov.achilles.exception.AchillesLightWeightTransactionException;
import info.archinnov.achilles.persistence.PersistenceManager;
import info.archinnov.achilles.type.OptionsBuilder;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class UserService {

    @Inject
    Session session;

    public void createUser(UserModel model) {
        final ResultSet rs = session.execute("INSERT INTO users (login, bio, email, chat_rooms, lastname, firstname, pass) VALUES (?, ?, ?, ?, ?, ?, ?) IF NOT EXISTS",
        		model.getLogin(), model.getBio(), model.getEmail(), model.getChatRooms(), model.getLastname(), model.getPassword());
        if (!rs.wasApplied()){
        	 throw new UserAlreadyExistsException(format("The user with the login '%s' already exists", model.getLogin()));
        }
    }

    public UserEntity findByLogin(String login) {
    	final Row row = session.execute("SELECT login, bio, email, chat_rooms, lastname, firstname, pass /*!DISPLAY chat_rooms !*/ FROM users WHERE login = ?", login).one();
    	if (row == null) {
            throw new UserNotFoundException(format("Cannot find user with login '%s'", login));
        }
    	final UserEntity ret = new UserEntity();
    	ret.setLogin(row.getString("login"));
    	//TODO: fill entity
        return ret;
    }

    public UserModel fetchRememberMeUser() {
        final String login = SecurityUtils.getCurrentLogin();
        ClientLocalValues.set("login", login);
        if ("anonymousUser".equals(login)) {
            throw new RememberMeDoesNotExistException(format("There is no remember me information for the login '%s'", login));
        }
        return findByLogin(login).toModel();
    }
}
