package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;

import ch.ethz.inf.pm.cfour.Parameters;
import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.AbstractSerializabilityAnalysis;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.util.FileUtils;
import ch.ethz.inf.pm.cfour.cassandra.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;
import com.google.common.graph.EndpointPair;

import ch.ethz.inf.pm.cfour.AbstractHistory;
import ch.ethz.inf.pm.cfour.Main;
import ch.ethz.inf.pm.cfour.Unfolder;
import ch.ethz.inf.pm.cfour.Unfolder.Violation;
import ch.ethz.inf.pm.cfour.Unfolder.Violation2;
import ch.ethz.inf.pm.cfour.Unfolder.Violation3;
import ch.ethz.inf.pm.cfour.Unfolder.Violation4;
import ch.ethz.inf.pm.cfour.Unfolder.ViolationSet;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.output.DumpGraph;
import ch.ethz.inf.pm.cfour.output.LabeledGraph;
import ch.ethz.inf.pm.cfour.output.Statistics;
import scala.None;
import scala.Option;

/**
 * Checks the given transactions for serializability violations calling CFour
 * <ol>
 * <li>Transforms the transaction graphs to the input of CFour using {@link GraphBuilder}</li>
 * <li>Invocation of CFour</li>
 * <li>Writes the results to the output folder if set</li>
 * </ol>
 */
public class CFourSerializabilityAnalysis extends AbstractSerializabilityAnalysis {

	private final static Logger LOG = LogManager.getLogger(CFourSerializabilityAnalysis.class);

	private final ViolationClassifier classifier;

	public CFourSerializabilityAnalysis(final Options options) {
		super(options);
		this.classifier = new ViolationClassifier(options);
	}

	public CFourResult check(final Map<TransactionDescriptor, TransactionGraph> transactionGraphs,
							 final SchemaInformation schemaInformation) {

		final BiMap<TransactionDescriptor, String> transactionLabels = buildTransactionLabels(transactionGraphs.keySet());
		final AbstractHistory allTransactionGraph = buildSingleClientTransactionGraph(transactionGraphs, transactionLabels, schemaInformation);
		if (outputDir != null) {
			final File cFourGraphDir = new File(outputDir, "ECGraph");
			FileUtils.createEmptyDirectory(cFourGraphDir);
			try {
				Files.write(Paths.get(cFourGraphDir.getAbsolutePath(), outputDir.getName() + ".json"),
						allTransactionGraph.toJSON().getBytes(), StandardOpenOption.CREATE);
			} catch (IOException e) {
				LOG.error("Error writing json graph file", e);
			}
		}

		final CFourResult result = new CFourResult();
		if (options.isCFourEnabled()) {
			final Set<Violation> violations = new HashSet<>();

			final long cFourRuntime;
			synchronized (Main.class) {
				LOG.debug("Checking serializability");
				Parameters p = options.toParam();
				final long cFourStart = System.currentTimeMillis();
				final ViolationSet violationSet = Unfolder.unfoldAndCheck(allTransactionGraph, p);
				cFourRuntime = System.currentTimeMillis() - cFourStart;
				violations.addAll(ScalaUtils.from(violationSet.set()));
				result.setPossibleViolationsSize2(Statistics.possibleViolationsSize2());
				result.setVerifiedViolationsSize2(Statistics.verifiedViolationsSize2());
				result.setPossibleViolationsSize3(Statistics.possibleViolationsSize3());
				result.setVerifiedViolationsSize3(Statistics.verifiedViolationsSize3());
				result.setPossibleViolationsSize4(Statistics.possibleViolationsSize4());
				result.setVerifiedViolationsSize4(Statistics.verifiedViolationsSize4());
				result.setTotalPossibleViolationsSize4(Statistics.totalPossibleViolationsSize4());
				result.setTotalVerifiedViolationsSize4(Statistics.totalVerifiedViolationsSize4());
			}

			final Map<String, LabeledGraph<String, String>> shortNameToGraphMap = getShortNameToGraphMap(violations);
			final SortedSetMultimap<ViolationClassifier.Classification, String> classificationToShortNameMap = TreeMultimap.create();
			for (final String shortName : shortNameToGraphMap.keySet()) {
				classificationToShortNameMap.put(classifier.classificationOf(shortName), shortName);
			}

			System.out.println("Found " + classificationToShortNameMap.size() + " violations");

			final File cFourViolationsDir;
			if (outputDir != null) {
				cFourViolationsDir = new File(outputDir, "ECViolations");
				FileUtils.createEmptyDirectory(cFourViolationsDir);
				for (final ViolationClassifier.Classification classification : classificationToShortNameMap.keySet()) {
					FileUtils.createEmptyDirectory(new File(cFourViolationsDir, classification.getShortName()));
				}
			} else {
				cFourViolationsDir = null;
			}

			final StringBuilder classificationOutput = new StringBuilder();
			for (final ViolationClassifier.Classification classification : classificationToShortNameMap.keySet()) {
				final SortedSet<String> shortNames = classificationToShortNameMap.get(classification);
				final int violationsPerClassification = shortNames.size();
				final int violationsPerClassificationPercent = (int) Math
						.round(((double) (100 * shortNames.size())) / ((double) classificationToShortNameMap.size()));
				System.out.println(classification + ": " + violationsPerClassification + " (" + violationsPerClassificationPercent + "%)");
				classificationOutput.append(classification).append(";").append(violationsPerClassification).append(";")
						.append(violationsPerClassificationPercent).append("%\n");
				for (final String shortName : shortNames) {
					if (cFourViolationsDir == null) {
						System.out.println(shortName);
					} else {
						try {
							System.out.println(Files.write(
									Paths.get(cFourViolationsDir.getAbsolutePath(),
											classifier.classificationOf(shortName).getShortName(),
											shortName + ".html"),
									DumpGraph.getString(shortNameToGraphMap.get(shortName)).getBytes(), StandardOpenOption.CREATE));
						} catch (IOException e) {
							LOG.error("Error writing file", e);
						}
					}
				}
			}
			classificationOutput.append("NONE").append(";").append(result.getPossibleViolationsSize4() - result.getVerifiedViolationsSize4())
					.append(";").append("\n");
			if (cFourViolationsDir != null) {
				try {
					Files.write(
							Paths.get(cFourViolationsDir.getAbsolutePath(), "violations.csv"),
							classificationOutput.toString().getBytes(), StandardOpenOption.CREATE);
					Files.write(
							Paths.get(cFourViolationsDir.getAbsolutePath(), "runtime.csv"),
							new StringBuilder().append(cFourRuntime).append(";").append(result.getPossibleViolationsSize2()
									+ result.getPossibleViolationsSize3() + result.getPossibleViolationsSize4()).append(";")
									.append(result.getVerifiedViolationsSize2() + result.getVerifiedViolationsSize3()
											+ result.getVerifiedViolationsSize4())
									.toString().getBytes(),
							StandardOpenOption.CREATE);
				} catch (IOException e) {
					LOG.error("Error writing file", e);
				}
			}
		}
		return result;
	}

	private ImmutableMap<String, LabeledGraph<String, String>> getShortNameToGraphMap(final Set<Violation> violations) {

		final ImmutableMap.Builder<String, LabeledGraph<String, String>> shortNameToGraphBuilder = ImmutableMap.builder();
		for (final Violation violation0 : violations) {
			final String shortName;
			if (violation0 instanceof Violation2) {
				final Violation2 violation = (Violation2) violation0;
				shortName = "2_" + violation.c1() + "_" + violation.c2();
			} else if (violation0 instanceof Violation3) {
				final Violation3 violation = (Violation3) violation0;
				shortName = "3_" + violation.c1a() + "_" + violation.c1b() + "_" + violation.c2();
			} else {
				final Violation4 violation = (Violation4) violation0;
				shortName = "4_" + violation.c1a() + "_" + violation.c1b() + "_" + violation.c2a() + "_" + violation.c2b();
			}
			shortNameToGraphBuilder.put(shortName, violation0.g());
		}
		return shortNameToGraphBuilder.build();
	}

	private ImmutableBiMap<TransactionDescriptor, String> buildTransactionLabels(Set<TransactionDescriptor> keySet) {
		final BiMap<TransactionDescriptor, String> transactionLabels = HashBiMap.create();
		for (final TransactionDescriptor tdesc : new TreeSet<>(keySet)) {
			final String transactionId = tdesc.entryMethod.className + "_" + tdesc.entryMethod.methodName;
			String uniqueTransactionId = transactionId;
			for (int i = 1; transactionLabels.containsValue(uniqueTransactionId); i++) {
				uniqueTransactionId = transactionId + "_" + i;
			}
			transactionLabels.put(tdesc, uniqueTransactionId);
		}
		return ImmutableBiMap.copyOf(transactionLabels);
	}

	private AbstractHistory buildSingleClientTransactionGraph(final Map<TransactionDescriptor, TransactionGraph> transactionGraphs,
			final Map<TransactionDescriptor, String> transactionLabels, final SchemaInformation schemaInformation) {

		final Map<StatementPart, String> parts = new HashMap<>();
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionGraphs.entrySet()) {
			for (final ParsedSessionExecuteInvoke sei : entry.getValue().nodes()) {
				for (final StatementPart part : sei.statement.getStatementParts()) {
					parts.put(part, sei.statement.query);
				}
			}
		}
		// we use parts that read nor write as entry / exit
		final GraphBuilder graphBuilder = new GraphBuilder(options, schemaInformation, parts);

		final Set<String> emittedTransactions = new HashSet<>();

		for (final Entry<TransactionDescriptor, TransactionGraph> transaction : transactionGraphs.entrySet()) {
			if (!transaction.getValue().nodes().isEmpty()) {
				final String transactionId = transactionLabels.get(transaction.getKey());

				graphBuilder.addTransactionOrderEdge(transactionId, transactionId);
				for (final String emittedTransaction : emittedTransactions) {
					graphBuilder.addTransactionOrderEdge(emittedTransaction, transactionId);
					graphBuilder.addTransactionOrderEdge(transactionId, emittedTransaction);
				}

				emittedTransactions.add(transactionId);

				emitTransaction(transactionId, transaction.getValue(), graphBuilder);
			}
		}
		return graphBuilder.build();
	}

	private void emitTransaction(final String transactionId, final TransactionGraph transactionGraph, final GraphBuilder graphBuilder) {

		// each Statement contains 1:n entryEvents and 1:n exitEvents --> an edge in the transaction graph 
		// is represented using entry and exit events of source and dest node
		final SetMultimap<ParsedSessionExecuteInvoke, String> entryEvents = TreeMultimap.create();
		final SetMultimap<ParsedSessionExecuteInvoke, String> exitEvents = TreeMultimap.create();

		for (final ParsedSessionExecuteInvoke sei : transactionGraph.nodes()) {
			final Consumer<List<StatementPart>> sessionExecuteInvokeEncode = orderedParts -> {
				String lastId = null;
				for (int i = 0; i < orderedParts.size(); i++) {
					final StatementPart part = orderedParts.get(i);
					final String nextId = graphBuilder.addEvent(part, transactionId, sei);
					if (lastId != null) {
						graphBuilder.addProgramOrderEdge(lastId, nextId);
					}
					if (i == 0) {
						entryEvents.put(sei, nextId);
					}
					if (i == orderedParts.size() - 1) {
						exitEvents.put(sei, nextId);
					}
					lastId = nextId;
				}
			};
			if (options.isEncodeBatchStatementPermutations()) {
				Utils.consumeAllPermutations(sei.statement.getStatementParts(), sessionExecuteInvokeEncode);
			} else {
				sessionExecuteInvokeEncode.accept(new ArrayList<>(sei.statement.getStatementParts()));
			}
		}

		final String transactionEntryId = graphBuilder.addSkipEvent("ENTRY", transactionId);
		for (final ParsedSessionExecuteInvoke source : transactionGraph.sourceNodes()) {
			for (final String sourceEntry : entryEvents.get(source)) {
				graphBuilder.addProgramOrderEdge(transactionEntryId, sourceEntry);
			}
		}

		for (final EndpointPair<ParsedSessionExecuteInvoke> executionEdge : transactionGraph.edges()) {
			final AbstractTransactionGraph.EdgeConstraint<ParsedSessionExecuteInvoke> edgeConstraint = transactionGraph.edgeConstraint(executionEdge.nodeU(),
					executionEdge.nodeV());
			for (final String exit : exitEvents.get(executionEdge.nodeU())) {
				for (final String entry : entryEvents.get(executionEdge.nodeV())) {
					graphBuilder.addProgramOrderEdge(exit, entry, executionEdge.nodeU(), edgeConstraint);
				}
			}
		}

		final String transactionExitId = graphBuilder.addSkipEvent("EXIT", transactionId);
		for (final ParsedSessionExecuteInvoke sink : transactionGraph.sinkNodes()) {
			for (final String sinkExit : exitEvents.get(sink)) {
				graphBuilder.addProgramOrderEdge(sinkExit, transactionExitId);
			}
		}
	}
}
