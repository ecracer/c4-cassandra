package ch.ethz.inf.pm.cfour.cassandra.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.CassandraParser;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.BatchStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedBatchUpdateStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedUpdateStatement;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;

/**
 * Transforms the transaction graphs from the unparsed version
 * ({@link SessionExecuteInvoke}) to the parsed version
 * ({@link ParsedSessionExecuteInvoke})
 * <ul>
 * <li>Splits nodes that represent multiple CQL statement</li>
 * <li>Parses the CQL-strings into {@link StatementPart}
 * <li>Applies display and strict update hints</li>
 * </ul>
 */
public class TransactionGraphTransformer {

	private final static Logger LOG = LogManager.getLogger(TransactionGraphTransformer.class);

	private final Options options;
	private final CassandraParser parser = new CassandraParser();

	private int nextId = 1;

	public TransactionGraphTransformer(final Options options) {
		this.options = Objects.requireNonNull(options);
	}

	public ImmutableMap<TransactionDescriptor, TransactionGraph> transform(
			Map<TransactionDescriptor, UnparsedTransactionGraph> transactionExecuteGraphs) {

		final ImmutableMap.Builder<TransactionDescriptor, TransactionGraph> transformedBuilder = ImmutableMap.builder();

		for (final Entry<TransactionDescriptor, UnparsedTransactionGraph> entry : transactionExecuteGraphs.entrySet()) {
			transformedBuilder.put(entry.getKey(), transformGraph(entry.getKey(), entry.getValue()));
		}

		return transformedBuilder.build();
	}

	private TransactionGraph transformGraph(final TransactionDescriptor transactionDescriptor,
			final UnparsedTransactionGraph graph) {
		final SetMultimap<SessionExecuteInvoke, ParsedSessionExecuteInvoke> transformedNodes = HashMultimap.create();
		for (final SessionExecuteInvoke node : graph.nodes()) {
			transformedNodes.putAll(node, parseSessionExecuteInvoke(node, transactionDescriptor.isOnlyForDisplaying));
		}
		return TransactionGraph.createFrom(graph, transformedNodes);
	}

	private ImmutableSet<ParsedSessionExecuteInvoke> parseSessionExecuteInvoke(final SessionExecuteInvoke executeInvoke,
			final boolean onlyDisplayCode) {

		final Set<ParsedSessionExecuteInvoke> sessionExecutes = new HashSet<>();
		for (final StatementValue stmt : executeInvoke.statementArg) {
			ParsedStatement parsedStatement = parseStatement(stmt, onlyDisplayCode);

			final ParsedSessionExecuteInvoke sessionExecute = ParsedSessionExecuteInvoke.create(
					executeInvoke.executedFrom.callStack.method,
					nextId++,
					executeInvoke.async,
					parsedStatement,
					parseConsistencyLevel(stmt.consistencyLevel),
					parseConsistencyLevel(stmt.serialConsistencyLevel));

			boolean similarExists = false;
			for (final ParsedSessionExecuteInvoke otherSessionExecute : sessionExecutes) {
				if (otherSessionExecute.equalsIgnoringStatementQueryAndNumber(sessionExecute)) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Ignoring query " + parsedStatement.query + " as " + otherSessionExecute.statement.query
								+ " already exists");
					}
					similarExists = true;
					break;
				}
			}
			if (!similarExists) {
				sessionExecutes.add(sessionExecute);
			}
		}
		return ImmutableSet.copyOf(sessionExecutes);
	}

	private ParsedStatement parseStatement(final StatementValue stmt, final boolean onlyDisplayCode) {
		if (stmt instanceof BatchStatementValue) {
			final BatchStatementValue batchStatement = (BatchStatementValue) stmt;
			final Set<StatementPart.UpdatePart> updateParts = new HashSet<>();
			for (final StatementValue singleStmt : batchStatement.statements) {
				final ParsedStatement singleParsed = parseStatement(singleStmt, onlyDisplayCode);
				if (singleParsed instanceof ParsedUpdateStatement) {
					updateParts.add(((ParsedUpdateStatement) singleParsed).writePart);
				} else if (singleParsed instanceof ParsedBatchUpdateStatement) {
					updateParts.addAll(((ParsedBatchUpdateStatement) singleParsed).updateParts);
				} else {
					throw new RuntimeException("Illegal Statement");
				}
			}

			return ParsedBatchUpdateStatement.create(batchStatement.query, updateParts);
		} else {
			ParsedStatement parsedStatement = parser.parseStatementValue(stmt);

			if (options.isStrictUpdatesEnabled() || options.isTreatAllUpdatesAsStrict()) {
				parsedStatement = applyStrictUpdateHint(parsedStatement);
			}

			if (options.isPruneDisplayCodeEnabled()) {
				if (onlyDisplayCode && parsedStatement instanceof ParsedQueryStatement) {
					final ParsedQueryStatement parsedQueryStatement = (ParsedQueryStatement) parsedStatement;
					parsedStatement = ParsedQueryStatement.create(parsedQueryStatement.query,
							parsedQueryStatement.queryPart.setAllDisplayColumns());
				} else {
					parsedStatement = applyDisplayColumnHint(parsedStatement);
				}
			}
			return parsedStatement;
		}
	}

	private ParsedSessionExecuteInvoke.ConsistencyLevel parseConsistencyLevel(final ImmutableValue consistencyLevel) {
		if (consistencyLevel instanceof NullValue) {
			return ParsedSessionExecuteInvoke.ConsistencyLevel.DEFAULT;
		} else if (consistencyLevel instanceof ConsistencyLevelValue) {
			return ParsedSessionExecuteInvoke.ConsistencyLevel.of((ConsistencyLevelValue) consistencyLevel);
		} else {
			return ParsedSessionExecuteInvoke.ConsistencyLevel.UNKNOWN;
		}
	}

	// Matches /*!STRICT!*/ hint to declare a strict update
	private final static Pattern STRICT_UPDATE_PATTERN = Pattern.compile("\\/\\*!STRICT!\\*\\/", Pattern.CASE_INSENSITIVE);

	private ParsedStatement applyStrictUpdateHint(final ParsedStatement parsedStatement) {
		if (parsedStatement instanceof ParsedUpdateStatement) {
			final ParsedUpdateStatement parsedUpdateStatement = (ParsedUpdateStatement) parsedStatement;
			return ParsedUpdateStatement.create(parsedUpdateStatement.query,
					transformUpdatePart(parsedUpdateStatement.writePart, parsedStatement.query));

		} else if (parsedStatement instanceof ParsedBatchUpdateStatement) {
			final ParsedBatchUpdateStatement parsedBatch = (ParsedBatchUpdateStatement) parsedStatement;
			final Set<StatementPart.UpdatePart> updates = new HashSet<>();
			for (final StatementPart.UpdatePart update : parsedBatch.updateParts) {
				updates.add(transformUpdatePart(update, parsedBatch.query));
			}
			return ParsedBatchUpdateStatement.create(parsedBatch.query, updates);
		}
		return parsedStatement;
	}

	private StatementPart.UpdatePart transformUpdatePart(final StatementPart.UpdatePart part, final String query) {
		if (part instanceof StatementPart.UpsertPart && "UPDATE".equals(part.getCQLCommand())) {
			if (options.isTreatAllUpdatesAsStrict()) {
				return ((StatementPart.UpsertPart) part).setCanInsert(false);
			} else if (options.isStrictUpdatesEnabled() && STRICT_UPDATE_PATTERN.matcher(query).find()) {
				return ((StatementPart.UpsertPart) part).setCanInsert(false);
			}
		}
		return part;
	}

	// Matches either /*!DISPLAY * !*/ or /*!DISPLAY col1, col2 !*/ capturing the COL-Expr in the first group
	private final static Pattern DISPLAY_COLS_PATTERN = Pattern.compile("\\/\\*!DISPLAY ((?:\\*)|(?:(?:[a-zA-Z_0-9]+),? ?)+) ?!\\*\\/",
			Pattern.CASE_INSENSITIVE);

	private ParsedStatement applyDisplayColumnHint(final ParsedStatement parsedStatement) {
		final Matcher m;
		if (!(parsedStatement instanceof ParsedQueryStatement) || !(m = DISPLAY_COLS_PATTERN.matcher(parsedStatement.query)).find()) {
			return parsedStatement;
		}

		final List<String> displayCols = new ArrayList<>();
		do {
			if (m.group(1).equals("*")) {
				return ParsedQueryStatement.create(parsedStatement.query,
						((ParsedQueryStatement) parsedStatement).queryPart.setAllDisplayColumns());
			} else {
				final String[] matchedDisplayCols = m.group(1).split("(?:, ?| )");
				for (final String dc : matchedDisplayCols) {
					if (!dc.trim().isEmpty()) {
						displayCols.add(dc.trim().toLowerCase());
					}
				}
			}
		} while (m.find());

		return ParsedQueryStatement.create(parsedStatement.query,
				((ParsedQueryStatement) parsedStatement).queryPart.setDisplayColumns(ImmutableSet.copyOf(displayCols)));
	}
}
