package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.EdgeValue;
import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.Graph;
import com.google.common.graph.Network;
import com.google.common.graph.ValueGraph;

import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.AbstractEdge;
import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.AbstractEdge.ArbitrationEdge;
import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.AbstractEdge.DependencyEdge;
import ch.ethz.inf.pm.cfour.cassandra.analysis.basic.TransactionExecution;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph.EdgeConstraint;
import soot.SootMethod;

/**
 * Helper for outputting dot files that represent some graph
 */
public class GraphToDotUtil {

	private final static Logger LOG = LogManager.getLogger(GraphToDotUtil.class);

	public final static Function<ParsedSessionExecuteInvoke, String> PARSED_EXECUTE_TO_KEY = (exec -> {
		return exec.getSignature();
	});

	public final static Function<ParsedSessionExecuteInvoke, String> PARSED_EXECUTE_TO_LABEL = (exec -> {
		final StringBuilder sb = new StringBuilder();
		sb.append(exec.getSignature()).append(": ").append(exec.statement.getQuery());
		return sb.toString();
	});

	public final static Function<EdgeConstraint<ParsedSessionExecuteInvoke>, String> EDGE_CONSTRAINT_TO_LABEL = (edgeConstraint -> {
		final StringBuilder sb = new StringBuilder();
		for (final ParsedSessionExecuteInvoke emptyResult : edgeConstraint.emptyResultNodes()) {
			sb.append("empty:").append(emptyResult.getSignature());
		}
		for (final ParsedSessionExecuteInvoke nonEmptyResult : edgeConstraint.nonEmptyResultNodes()) {
			sb.append("nonempty:").append(nonEmptyResult.getSignature());
		}
		return sb.toString();
	});

	public static void printTransactionGraph(final File dotFile, final String title, final TransactionGraph graph,
			final Function<ParsedSessionExecuteInvoke, String> keyTransformer,
			final Function<ParsedSessionExecuteInvoke, String> labelTransformer,
			final Function<EdgeConstraint<ParsedSessionExecuteInvoke>, String> edgeTransformer)
			throws IOException {
		final PrintWriter writer = new PrintWriter(dotFile);
		try {
			writer.println("digraph G {");
			graph.sourceNodes().forEach(n -> writer.println("  source -> \"" + keyTransformer.apply(n) + "\" [label=\""
					+ edgeTransformer.apply(graph.sourceConstraint(n)) + "\"];"));
			for (final ParsedSessionExecuteInvoke node : graph.nodes()) {
				for (final ParsedSessionExecuteInvoke succ : graph.successors(node)) {
					writer.println("  \"" + keyTransformer.apply(node) + "\"  -> \"" + keyTransformer.apply(succ) + "\" [label=\""
							+ edgeTransformer.apply(graph.edgeConstraint(node, succ)) + "\"];");
				}
			}
			graph.sinkNodes().forEach(n -> writer.println("  \"" + keyTransformer.apply(n) + "\" -> sink [label=\""
					+ edgeTransformer.apply(graph.sinkConstraint(n)) + "\"];"));
			writer.println();
			for (final ParsedSessionExecuteInvoke node : graph.nodes()) {
				writer.println("  \"" + keyTransformer.apply(node) + "\" [label=\"" + labelTransformer.apply(node) + "\"];");
			}
			writer.println();
			writer.println("  labelloc=\"t\";");
			writer.println("  label=\"" + title + "\";");
			writer.println("}");
		} finally {
			writer.close();
		}
		createPDFFromDotIfPossible(dotFile);
	}

	public static void printBasicSDSGGraph(final File dotFile, final String title,
			final Network<TransactionDescriptor, AbstractEdge> basicSDSG) throws IOException {
		final PrintWriter writer = new PrintWriter(dotFile);
		try {
			writer.println("digraph G {");
			for (final TransactionDescriptor transaction : basicSDSG.nodes()) {
				writer.println("  \"" + transaction.getUniqueDescription() + "\" [label=\"" + transaction.getShortName() + "\"];");
			}
			for (final AbstractEdge edge : basicSDSG.edges()) {
				if (edge instanceof DependencyEdge) {
					final DependencyEdge dep = (DependencyEdge) edge;
					final String t1desc = dep.t1.getUniqueDescription();
					final String t2desc = dep.t2.getUniqueDescription();
					writer.println("  \"" + t1desc + "\" -> \"" + t2desc + "\" [label=\"DEP\", color=\"green\"];");
				} else {
					// arbitration goes in both directions
					final ArbitrationEdge arb = (ArbitrationEdge) edge;
					final String t1desc = arb.t1.getUniqueDescription();
					final String t2desc = arb.t2.getUniqueDescription();
					writer.println("  \"" + t1desc + "\" -> \"" + t2desc + "\" [label=\"ARB\", color=\"blue\", dir=\"both\"];");
				}
			}
			writer.println();
			writer.println("  labelloc=\"t\";");
			writer.println("  label=\"" + title + "\";");
			writer.println("}");
		} finally {
			writer.close();
		}
		createPDFFromDotIfPossible(dotFile);
	}

	public static void printBasicCriticalCycleGraph(final File dotFile, final String title,
			final ValueGraph<TransactionExecution, EdgeValue> criticalCycle) throws IOException {
		final PrintWriter writer = new PrintWriter(dotFile, "UTF-8");
		try {
			writer.println("digraph G {");
			for (final TransactionExecution transaction : criticalCycle.nodes()) {
				writer.println("  \"" + transaction.id + "\" [label=\"" + transaction.transaction.getShortName() + "\"];");
			}
			for (final EndpointPair<TransactionExecution> edge : criticalCycle.edges()) {
				final EdgeValue edgeVal = criticalCycle.edgeValue(edge.nodeU(), edge.nodeV());
				if (edgeVal == EdgeValue.DEPENDENCY) {
					writer.println("  \"" + edge.nodeU().id + "\" -> \"" + edge.nodeV().id + "\" [label=\"DEP\", color=\"green\"];");
				} else if (edgeVal == EdgeValue.ANTI_DEPENDENCY) {
					writer.println("  \"" + edge.nodeU().id + "\" -> \"" + edge.nodeV().id + "\" [label=\"ANTI\", color=\"blue\"];");
				} else if (edgeVal == EdgeValue.ARBITRATION) {
					writer.println("  \"" + edge.nodeU().id + "\" -> \"" + edge.nodeV().id + "\" [label=\"ARB\", color=\"red\"];");
				} else {
					throw new RuntimeException("Illegal edge value");
				}
			}
			writer.println();
			writer.println("  labelloc=\"t\";");
			writer.println("  label=\"" + title + "\";");
			writer.println("}");
		} finally {
			writer.close();
		}
		createPDFFromDotIfPossible(dotFile);
	}

	public static void logGraphToTemp(final File dir, final SootMethod method, final Graph<?> graph) {
		try {
			final File dotFile = new File(dir, method.getName().replaceAll("\\W+", "") + ".dot");
			printDotFileFromGraph(dotFile, method.getName(), graph);
		} catch (Exception e) {
			LOG.error("Graph dump was not written", e);
		}
	}

	public static void printDotFileFromGraph(final File dotFile, final String title, final Graph<? extends Object> graph)
			throws IOException {
		final PrintWriter writer = new PrintWriter(dotFile);
		try {
			final Map<Object, Integer> idMap = new HashMap<>();
			writer.println("digraph G {");
			for (final Object node : graph.nodes()) {
				if (!idMap.containsKey(node)) {
					idMap.put(node, idMap.size());
				}
				for (final Object succ : graph.successors(node)) {
					if (!idMap.containsKey(succ)) {
						idMap.put(succ, idMap.size());
					}
					writer.println("  \"" + idMap.get(node) + "\"  -> \"" + idMap.get(succ) + "\";");
				}
			}
			for (final Object node : graph.nodes()) {
				writer.println("  \"" + idMap.get(node) + "\" [label=\"" + node.toString().replaceAll("\"", "") + "\"];");
			}
			writer.println();
			writer.println("  labelloc=\"t\";");
			writer.println("  label=\"" + title + "\";");
			writer.println("}");
		} finally {
			writer.close();
		}
		createPDFFromDotIfPossible(dotFile);
	}

	public static void createPDFFromDotIfPossible(final File dotFile) throws IOException {
		final String graphvizHome = System.getenv("GRAPHVIZ_HOME");
		if (graphvizHome != null && !graphvizHome.isEmpty()) {
			final File graphvizHomeFile = new File(graphvizHome);
			final File dotExec = new File(new File(graphvizHomeFile, "bin"), "dot.exe");
			if (dotExec.exists()) {
				Runtime.getRuntime().exec(new String[]{dotExec.getAbsolutePath(), "-Tpdf", "-O", dotFile.getAbsolutePath()});
			}
		}
	}
}