package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourDisplayCode2Test extends AbstractCFourTest {

	public CFourDisplayCode2Test() throws IOException {
		super("CFourDisplayCode2");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "password")
				.addNonPrimaryKeyColumn("users", "playlists").addPrimaryKeyColumn("playlist_tracks", "username")
				.addPrimaryKeyColumn("playlist_tracks", "playlist").addPrimaryKeyColumn("playlist_tracks", "track_id").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setPruneDisplayCodeEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setPruneDisplayCodeEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isPruneDisplayCodeEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(4, result.getTotalVerifiedViolationsSize4());
		}
	}
}
