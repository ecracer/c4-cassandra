package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Map.Entry;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart;
import com.google.common.collect.ImmutableMap;

import ch.ethz.inf.pm.cfour.cassandra.analysis.TransactionGraphTransformer;
import ch.ethz.inf.pm.cfour.cassandra.beans.TransactionDescriptor;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ParsedStatement.ParsedQueryStatement;
import ch.ethz.inf.pm.cfour.cassandra.graphs.TransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;

public class AnalysisTransactionsOnlyDisplayingTest extends AbstractSootTest {

	public AnalysisTransactionsOnlyDisplayingTest() throws IOException {
		super("TransactionsOnlyDisplaying", new String[0]);
	}

	@Override
	protected void checkTransactionExecuteGraph(ImmutableMap<TransactionDescriptor, UnparsedTransactionGraph> executeGraphs) {

		final ImmutableMap<TransactionDescriptor, TransactionGraph> transactionExecuteGraphs = new TransactionGraphTransformer(
				new Options()).transform(executeGraphs);

		assertEquals(3, executeGraphs.size());
		for (final Entry<TransactionDescriptor, TransactionGraph> entry : transactionExecuteGraphs.entrySet()) {
			if ("method1".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod1(entry.getValue());
			} else if ("method2".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod2(entry.getValue());
			} else if ("method3".equals(entry.getKey().entryMethod.methodName)) {
				checkMethod3(entry.getValue());
			} else {
				assertTrue(false);
			}
		}
	}

	private void checkMethod1(final TransactionGraph graph1) {
		assertEquals(1, graph1.nodes().size());
		final StatementPart.QueryPart queryPart = ((ParsedQueryStatement) graph1.nodes().iterator().next().statement).queryPart;
		assertEquals(true, queryPart.areAllDisplayColumns());
	}

	private void checkMethod2(final TransactionGraph graph2) {
		assertEquals(1, graph2.nodes().size());
		final StatementPart.QueryPart queryPart = ((ParsedQueryStatement) graph2.nodes().iterator().next().statement).queryPart;
		assertEquals(false, queryPart.areAllDisplayColumns());
	}

	private void checkMethod3(final TransactionGraph graph3) {
		assertEquals(1, graph3.nodes().size());
		final StatementPart.QueryPart queryPart = ((ParsedQueryStatement) graph3.nodes().iterator().next().statement).queryPart;
		assertEquals(false, queryPart.areAllDisplayColumns());
	}
}
