import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.utils.UUIDs;

import java.util.UUID;

/**
 * Class tweet is used to create and view tweets
 * Created by jojo on 12/11/15.
 */
public class Tweet {

    private String uuid;
    private UUID uuids;
    private String Username;
    private String Body;
    private Query query;

    /**
     * Empty constructor
     */
    public Tweet() {
        query = new Query(CassaMain.serverIP, CassaMain.keyspace);
    }

    /**
     * Constructor for new tweet.
     *
     * @param user
     * @param body
     */
    public Tweet(String user, String body) {

        uuids = UUIDs.random();
        uuid = uuids.toString();
        Username = user;
        Body = body;
        query = new Query(CassaMain.serverIP, CassaMain.keyspace);
    }

    /**
     * Get UUID of a tweet
     *
     * @return uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Set UUID of a tweet
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Get Username who tweet
     *
     * @return username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * Set Username who tweet
     *
     * @param username
     */
    public void setUsername(String username) {
        Username = username;
    }

    /**
     * Get the body of a tweet
     *
     * @return body
     */
    public String getBody() {
        return Body;
    }

    /**
     * Set body of a tweet
     *
     * @param body
     */
    public void setBody(String body) {
        Body = body;
    }

    /**
     * Save tweet to database
     * I.S. uuid, username and body is not empty.
     * F.S. tweet saved to the database and return true
     *
     * @return boolean
     */
    public boolean Save() {
        if (uuid.isEmpty() || Username.isEmpty() || Body.isEmpty()) {
            return false;
        } else {
            query.getSession().execute(QueryBuilder.insertInto("tweets")
                    .value("tweet_id", uuids)
                    .value("username", Username)
                    .value("body", Body));

            return true;
        }
    }

    public UUID getUuids() {
        return uuids;
    }

    public void setUuids(UUID uuid) {
        this.uuids = uuid;
        this.uuid = uuids.toString();
    }

    /**
     * Load tweet from database
     * I.S. uuid and username is not empty
     * F.S tweet loaded from the database
     *
     * @return
     */
    public boolean Load() {
        if (uuid.isEmpty()) {
            return false;
        } else {
            ResultSet set = query.getSession().execute(QueryBuilder.select().all()
                    .from("tweets")
                    .where(QueryBuilder.eq("tweet_id", uuids)));
            for (Row tuple : set) {
                Username = tuple.getString("username");
                Body = tuple.getString("body");
            }
            return true;
        }
    }

    public String getTweetString() {
        String retString = "";
        retString += "@" + this.Username + "\n";
        retString += "TweetID: " + this.uuids + "\n";
        retString += "Tweet: " + this.Body + "\n";

        retString += "-----------------------------------\n";
        return retString;
    }
}
