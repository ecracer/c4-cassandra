package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.graphs.ExecuteGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefObjectValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.AbstractBuiltStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.graphs.AbstractTransactionGraph.EdgeConstraint;
import ch.ethz.inf.pm.cfour.cassandra.graphs.IdTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.graphs.UnparsedTransactionGraph;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootTypeUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ArrayReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.BaseReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ObjectReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.visitor.ReferenceVisitor;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UUIDValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefArrayValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.RefValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltStatementAssignmentValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.FieldDesc;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import soot.SootMethod;

/**
 * State which is transformed along the {@link ExecuteGraph}<br>
 * All parts used in the flow must be <b>immutable</b> (allows efficient copying of state)
 */
public class ExecutionState {

	private final static Logger LOG = LogManager.getLogger(ExecutionState.class);
	private final static ProgramPointId MERGE_PROGRAM_POINT = ProgramPointId.create(CallStack.EMPTY_STACK, new Object());

	// Field information used for building unknown objects
	// Is never changed
	private ImmutableSetMultimap<String, FieldDesc> includedFields;
	private ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> possibleFieldValues;

	// whether this state is reachable
	private boolean isBottom;

	// Information for building the transaction graph 
	private IdTransactionGraph transactionGraph;
	private ImmutableMap<ProgramPointId, SessionExecuteInvoke> executeInvokeMap;
	private ImmutableSet<ProgramPointId> lastExecuteInvokes;
	private EdgeConstraint<ProgramPointId> currentPathConstraint;
	private boolean mustInvokeExecute;

	// Frames on the call stack
	private ImmutableSetMultimap<CallStack, MethodCallState> methodCallStates;
	private ImmutableSetMultimap<CallStack, VarLocalOrImmutableValue> methodCallReturnVals;

	// Information about the static fields
	private ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> staticFieldMap;
	private ImmutableSet<FieldDesc> staticMayBeNotSet;

	// Map from local variable to reference 
	private ImmutableSetMultimap<VarLocal, AbstractReference> pointsToMap;

	// References to concrete values
	private ImmutableMap<ImmutableReference, ImmutableValue> immutableValueMap;
	private ImmutableSetMultimap<BaseReference, Value> baseValueMap;
	private ImmutableMap<ArrayReference, RefArrayValue> arrayValueMap;
	private ImmutableMap<ObjectReference, RefObjectValue> objectValueMap;

	public ExecutionState() {
		clear();
	}

	public ExecutionState(final ImmutableSetMultimap<String, FieldDesc> includedFields,
			final ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> possibleFieldValues,
			final ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> staticFieldMap,
			final ImmutableSetMultimap<VarLocal, AbstractReference> pointsToMap,
			final ImmutableMap<ImmutableReference, ImmutableValue> immutableValueMap,
			final ImmutableSetMultimap<BaseReference, Value> baseValueMap,
			final ImmutableMap<ArrayReference, RefArrayValue> arrayValueMap,
			final ImmutableMap<ObjectReference, RefObjectValue> objectValueMap) {
		this();
		this.includedFields = Objects.requireNonNull(includedFields);
		this.possibleFieldValues = Objects.requireNonNull(possibleFieldValues);
		this.isBottom = false;
		this.staticFieldMap = Objects.requireNonNull(staticFieldMap);
		this.pointsToMap = Objects.requireNonNull(pointsToMap);
		this.immutableValueMap = Objects.requireNonNull(immutableValueMap);
		this.baseValueMap = Objects.requireNonNull(baseValueMap);
		this.arrayValueMap = Objects.requireNonNull(arrayValueMap);
		this.objectValueMap = Objects.requireNonNull(objectValueMap);
	}

	public SetMultimap<String, FieldDesc> getIncludedFields() {
		return includedFields;
	}

	public SetMultimap<FieldDesc, VarLocalOrImmutableValue> getPossibleFieldValues() {
		return possibleFieldValues;
	}

	public void addInitialMethodCallState(final CallStack initialStack, final SootMethod method) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		final VarLocal baseLocal;
		if (method.isStatic()) {
			baseLocal = null;
		} else {
			final ProgramPointId updatedFrom = ProgramPointId.create(CallStack.EMPTY_STACK, method.getSignature());
			baseLocal = VarLocal.create(updatedFrom.callStack, "b0", method.getDeclaringClass().getName(),
					SootTypeUtils.resolveVarType(method.getDeclaringClass().getType()));
			if (method.isConstructor()) {
				setObjectValue(baseLocal, ObjectReference.create(updatedFrom),
						RefObjectValue.create(baseLocal.typeName, includedFields.get(baseLocal.typeName)), updatedFrom, null);
			} else {
				setUnknown(baseLocal, updatedFrom);
			}
		}
		final List<VarLocal> parameters = new ArrayList<>(method.getParameterCount());
		for (int i = 0; i < method.getParameterCount(); i++) {
			final VarLocal param = VarLocal.create(CallStack.EMPTY_STACK, "p" + i, method.getParameterType(i).getEscapedName(),
					SootTypeUtils.resolveVarType(method.getParameterType(i)));
			if (!param.varType.equals(VarType.EXCLUDED)) {
				setUnknown(param, ProgramPointId.create(CallStack.EMPTY_STACK, method.getSignature() + i));
			}
			parameters.add(param);
		}
		setMethodCallState(initialStack, baseLocal, parameters);
	}

	public void setToBottom() {
		clear();
	}

	public boolean isBottom() {
		return isBottom;
	}

	/*
	 * Session Execute Graph
	 */

	public void addSessionExecuteInvoke(final SessionExecuteInvoke sessionExecuteInvoke) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		Objects.requireNonNull(sessionExecuteInvoke);
		final IdTransactionGraph.Builder builder = transactionGraph.toBuilder();
		if (!mustInvokeExecute) {
			builder.addSourceNode(sessionExecuteInvoke.executedFrom, currentPathConstraint);
		}
		builder.putNode(sessionExecuteInvoke.executedFrom);
		for (final ProgramPointId lastExecuteInvoke : lastExecuteInvokes) {
			builder.putEdgeConstraint(lastExecuteInvoke, sessionExecuteInvoke.executedFrom, currentPathConstraint);
		}
		builder.setBypassPossible(false);
		this.transactionGraph = builder.build();
		this.lastExecuteInvokes = ImmutableSet.of(sessionExecuteInvoke.executedFrom);
		this.executeInvokeMap = Utils.mapWithValue(executeInvokeMap, sessionExecuteInvoke.executedFrom, sessionExecuteInvoke);
		this.currentPathConstraint = EdgeConstraint.createEmpty();
		this.mustInvokeExecute = true;
	}

	public UnparsedTransactionGraph getFinalTransactionGraph() {
		final IdTransactionGraph.Builder resBuilder = transactionGraph.toBuilder();
		for (final ProgramPointId lastExecuteInvoke : lastExecuteInvokes) {
			resBuilder.addSinkNode(lastExecuteInvoke, currentPathConstraint);
		}
		return UnparsedTransactionGraph.createFrom(resBuilder.build(), executeInvokeMap);
	}

	public void addMustHaveReturnedEmptyResultConstraint(final ProgramPointId resultFrom) {
		this.currentPathConstraint = currentPathConstraint.toBuilder().addEmptyResultNode(resultFrom).build();
	}

	public void addMustHaveReturnedNonEmptyResultConstraint(final ProgramPointId resultFrom) {
		this.currentPathConstraint = currentPathConstraint.toBuilder().addNonEmptyResultNode(resultFrom).build();
	}

	public ImmutableSet<ProgramPointId> getMustHaveReturnedEmptyResults() {
		return currentPathConstraint.emptyResultNodes();
	}

	public ImmutableSet<ProgramPointId> getMustHaveReturnedNonEmptyResults() {
		return currentPathConstraint.nonEmptyResultNodes();
	}

	/*
	 * MethodCallState
	 */

	public void setMethodCallState(final CallStack callStack, final VarLocal baseLocal,
			final List<? extends VarLocalOrImmutableValue> parameters) {

		if (this.isBottom) {
			throw new IllegalStateException();
		}
		final MethodCallState callState = MethodCallState.create(baseLocal, ImmutableList.copyOf(parameters));

		if (methodCallStates.containsKey(callStack)) {
			final ImmutableSetMultimap.Builder<CallStack, MethodCallState> methodCallStatesBuilder = ImmutableSetMultimap.builder();
			for (final CallStack key : methodCallStates.keySet()) {
				if (key.equals(callStack)) {
					methodCallStatesBuilder.put(callStack, callState);
				} else {
					methodCallStatesBuilder.putAll(key, methodCallStates.get(key));
				}
			}

		} else {
			methodCallStates = ImmutableSetMultimap.<CallStack, MethodCallState> builder().putAll(methodCallStates)
					.put(callStack, callState).build();
		}
	}

	public ImmutableSet<VarLocal> getBaseLocal(final CallStack callStack) {
		final Set<MethodCallState> callStates = methodCallStates.get(callStack);
		if (callStates.isEmpty()) {
			throw new RuntimeException(callStack.method + " has no call state");
		}
		final ImmutableSet.Builder<VarLocal> baseLocals = ImmutableSet.builder();
		for (final MethodCallState callState : callStates) {
			baseLocals.add(callState.baseLocal);
		}
		return baseLocals.build();
	}

	public ImmutableSet<VarLocalOrImmutableValue> getParameter(final CallStack callStack, final int index) {
		final Set<MethodCallState> callStates = methodCallStates.get(callStack);
		if (callStates.isEmpty()) {
			throw new RuntimeException(callStack.method + " has no call state");
		}
		final ImmutableSet.Builder<VarLocalOrImmutableValue> params = ImmutableSet.builder();
		for (MethodCallState callState : callStates) {
			if (index < 0 || index >= callState.parameters.size()) {
				throw new IllegalArgumentException();
			}
			params.add(callState.parameters.get(index));
		}
		return params.build();
	}

	public void setMethodCallReturnVal(final CallStack callStack, final VarLocalOrImmutableValue returnVal) {

		if (this.isBottom) {
			throw new IllegalStateException();
		}
		if (methodCallReturnVals.containsKey(callStack)) {
			final ImmutableSetMultimap.Builder<CallStack, VarLocalOrImmutableValue> methodCallReturnValsBuilder = ImmutableSetMultimap
					.builder();
			for (final CallStack key : methodCallReturnVals.keySet()) {
				if (key.equals(callStack)) {
					methodCallReturnValsBuilder.put(callStack, returnVal);
				} else {
					methodCallReturnValsBuilder.putAll(key, methodCallReturnVals.get(key));
				}
			}

		} else {
			methodCallReturnVals = ImmutableSetMultimap.<CallStack, VarLocalOrImmutableValue> builder().putAll(methodCallReturnVals)
					.put(callStack, returnVal).build();
		}
	}

	public ImmutableSet<VarLocalOrImmutableValue> getReturnVals(final CallStack callStack) {
		return methodCallReturnVals.get(callStack);
	}

	/*
	 * StaticField
	 */
	public boolean isStaticFieldSet(final FieldDesc field) {
		return !staticMayBeNotSet.contains(field) && staticFieldMap.containsKey(field);
	}

	public void setStaticField(final FieldDesc field, final VarLocalOrImmutableValue right) {
		staticFieldMap = Utils.multimapWithValue(staticFieldMap, Objects.requireNonNull(field), Objects.requireNonNull(right));
		staticMayBeNotSet = Utils.setWithoutValue(staticMayBeNotSet, field);
	}

	public void setStaticFieldToUnknown(final FieldDesc field, final ProgramPointId updatedFrom) {
		if (staticFieldMap.containsKey(field)) {
			setToUnknown(staticFieldMap.get(field), updatedFrom);
		}
		staticFieldMap = Utils.multimapWithoutKey(staticFieldMap, field);
	}

	public ImmutableSet<VarLocalOrImmutableValue> getStaticField(final FieldDesc field) {
		if (!isStaticFieldSet(field)) {
			throw new IllegalArgumentException();
		}
		return staticFieldMap.get(field);
	}

	public ImmutableSetMultimap<FieldDesc, VarLocalOrImmutableValue> getStaticFieldMap() {
		return staticFieldMap;
	}

	/*
	 * VarLocal
	 */
	public boolean isLocalInitialized(final VarLocalOrImmutableValue local) {
		if (local instanceof VarLocal) {
			if (((VarLocal) local).varType.equals(VarType.EXCLUDED)) {
				throw new IllegalArgumentException();
			}
			return pointsToMap.containsKey(local);
		} else {
			return true;
		}
	}

	public ImmutableSet<AbstractReference> getPointsTo(final VarLocal local) {
		if (!isLocalInitialized(local) || local.varType.equals(VarType.EXCLUDED)) {
			throw new IllegalArgumentException();
		}
		return pointsToMap.get(local);
	}

	public ImmutableSetMultimap<VarLocal, AbstractReference> getPointsToMap() {
		return pointsToMap;
	}

	public void setLocalFromOther(final VarLocal localToSet, final VarLocal other) {
		if (!isLocalInitialized(other) || other.varType.equals(VarType.EXCLUDED)) {
			throw new IllegalArgumentException();
		}
		if (!VarType.isAssignableTo(localToSet.varType, other.varType)) {
			throw new IllegalArgumentException();
		}
		setPointsTo(localToSet, getPointsTo(other));
	}

	public void setLocalFromPhi(final VarLocal leftLocal, final Set<? extends VarLocalOrImmutableValue> phiVals,
			final ProgramPointId updatedFrom) {
		final Set<VarLocal> phiLocals = new HashSet<>();
		final Set<ImmutableValue> phiImmutables = new HashSet<>();
		boolean hasExcluded = false;
		for (final VarLocalOrImmutableValue phiVal : phiVals) {
			if (phiVal instanceof VarLocal) {
				final VarLocal phiLocal = (VarLocal) phiVal;
				if (phiLocal.varType.equals(VarType.EXCLUDED)) {
					hasExcluded = true;
				} else if (isLocalInitialized(phiLocal)) {
					phiLocals.add(phiLocal);
				}
			} else {
				phiImmutables.add((ImmutableValue) phiVal);
			}
		}
		if (phiLocals.size() == 0 && phiImmutables.size() == 0 && !hasExcluded) {
			setToBottom();
			return;
		} else if (hasExcluded || leftLocal.varType.equals(VarType.EXCLUDED)) {
			if (!leftLocal.varType.equals(VarType.EXCLUDED)) {
				setUnknown(leftLocal, updatedFrom);
			}
			setToUnknown(phiLocals, updatedFrom);
			return;
		} else {
			for (final VarLocal phiLocal : phiLocals) {
				if (!VarType.isAssignableTo(leftLocal.varType, phiLocal.varType)) {
					LOG.warn("Not all phiLocals can be assigned to the left local");
					setUnknown(leftLocal, updatedFrom);
					for (final VarLocal iphiLocal : phiLocals) {
						if (!iphiLocal.varType.equals(VarType.EXCLUDED)) {
							setToUnknown(iphiLocal, updatedFrom);
						}
					}
					return;
				}
			}
			if (leftLocal.varType.equals(VarType.IMMUTABLE)) {
				for (final VarLocal phiLocal : phiLocals) {
					phiImmutables.addAll(getImmutableValues(phiLocal));
				}
				setImmutableValue(leftLocal, ImmutableReference.create(updatedFrom),
						ImmutableChoiceValue.create(updatedFrom, phiImmutables), updatedFrom, null);
			} else {
				boolean hasNull = false;
				final Set<ImmutableValue> immutableValues = new HashSet<>();
				for (final ImmutableValue val : phiImmutables) {
					if (val instanceof NullValue) {
						hasNull = true;
					} else if (leftLocal.varType.equals(VarType.OBJECT)) {
						immutableValues.add(val);
					} else {
						throw new RuntimeException("Illegal immutable value");
					}
				}
				final ImmutableSet.Builder<AbstractReference> refsBuilder = ImmutableSet.builder();
				for (final VarLocal phiLocal : phiLocals) {
					refsBuilder.addAll(getPointsTo(phiLocal));
				}
				if (hasNull) {
					if (leftLocal.varType.equals(VarType.OBJECT)) {
						refsBuilder.add(BaseReference.NULL_REF);
					} else if (leftLocal.varType.equals(VarType.INCLUDED)) {
						refsBuilder.add(ObjectReference.NULL_REF);
					} else if (leftLocal.varType.equals(VarType.ARRAY)) {
						refsBuilder.add(ArrayReference.NULL_REF);
					} else {
						throw new IllegalStateException();
					}
				}
				if (!immutableValues.isEmpty()) {
					final ImmutableReference immuRef = ImmutableReference.create(updatedFrom);
					setImmutableRef(immuRef, ImmutableChoiceValue.create(updatedFrom, immutableValues), updatedFrom, null);
					refsBuilder.add(immuRef);
				}

				final ImmutableSet<AbstractReference> refs = refsBuilder.build();
				if (!refs.isEmpty()) {
					setPointsTo(leftLocal, refs);
				}
			}
		}
	}

	private void setPointsTo(final VarLocal local, final Set<? extends AbstractReference> refs) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		pointsToMap = Utils.multimapWithValues(pointsToMap, local, refs);
	}

	private void setPointsTo(final VarLocal local, final AbstractReference ref) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		pointsToMap = Utils.multimapWithValue(pointsToMap, local, ref);
	}

	public boolean isNullReference(final AbstractReference ref) {
		return ref == BaseReference.NULL_REF || ref == ObjectReference.NULL_REF || ref == ArrayReference.NULL_REF;
	}

	public <T extends AbstractReference> ImmutableSet<T> ensureNonNullPointsTo(final VarLocal local, final Class<T> refClass) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		if (!isLocalInitialized(local) || local.varType.equals(VarType.EXCLUDED)) {
			throw new IllegalArgumentException();
		}
		final ImmutableSet.Builder<T> result = ImmutableSet.builder();
		boolean removed = false;
		for (final AbstractReference ref : pointsToMap.get(local)) {
			if (isNullReference(ref)) {
				removed = true;
			} else {
				if (refClass.isAssignableFrom(ref.getClass())) {
					@SuppressWarnings("unchecked")
					final T refT = (T) ref;
					result.add(refT);
				}
			}
		}
		final ImmutableSet<T> nonNullRefs = result.build();
		if (removed) {
			setPointsTo(local, nonNullRefs);
		}
		return nonNullRefs;
	}

	/*
	 * ImmutableValue
	 */
	public void setImmutableValue(final VarLocal local, final ImmutableReference ref, final ImmutableValue value,
			final ProgramPointId updatedFrom, final ExecutionState oldState) {
		if (!local.varType.equals(VarType.IMMUTABLE) && !local.varType.equals(VarType.OBJECT)) {
			throw new IllegalArgumentException();
		}
		if (value instanceof NullValue && local.varType.equals(VarType.OBJECT)) {
			throw new IllegalArgumentException();
		}
		setImmutableRef(ref, value, updatedFrom, oldState);
		setPointsTo(local, ref);
	}

	private void setImmutableRef(final ImmutableReference ref, final ImmutableValue value, final ProgramPointId updatedFrom,
			final ExecutionState oldState) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		final ImmutableValue newVal;
		if (value instanceof UUIDValue && immutableValueMap.containsKey(ref)) {
			final ImmutableValue curValue = immutableValueMap.get(ref);
			if (curValue instanceof UUIDValue && ((UUIDValue) curValue).isWidened) {
				return;
			} else {
				newVal = curValue.widenWith(value, updatedFrom);
			}
		} else if (value instanceof UnknownImmutableValue && immutableValueMap.containsKey(ref)) {
			final ImmutableValue curValue = immutableValueMap.get(ref);
			if (curValue instanceof UnknownImmutableValue && ((UnknownImmutableValue) curValue).isWidened) {
				return;
			} else {
				newVal = curValue.widenWith(value, updatedFrom);
			}
		} else if (oldState != null && oldState.immutableValueMap.containsKey(ref) && !value.equals(oldState.immutableValueMap.get(ref))) {
			newVal = oldState.immutableValueMap.get(ref).widenWith(value, updatedFrom);
		} else {
			newVal = value;
		}
		immutableValueMap = Utils.mapWithValue(immutableValueMap, ref, newVal);
	}

	public ImmutableSet<ImmutableValue> getImmutableValues(final VarLocal local) {
		if (!isLocalInitialized(local) || !local.varType.equals(VarType.IMMUTABLE)) {
			throw new IllegalArgumentException();
		}
		final ImmutableSet.Builder<ImmutableValue> resultBuilder = ImmutableSet.builder();
		for (final AbstractReference ref : getPointsTo(local)) {
			if (!(ref instanceof ImmutableReference)) {
				throw new RuntimeException("Expected Immutable Reference");
			}
			resultBuilder.add(getImmutableValue((ImmutableReference) ref));
		}
		return resultBuilder.build();
	}

	public ImmutableValue getImmutableValue(final ImmutableReference ref) {
		if (!immutableValueMap.containsKey(ref)) {
			throw new IllegalArgumentException();
		}
		return immutableValueMap.get(ref);
	}

	public ImmutableMap<ImmutableReference, ImmutableValue> getImmutableValueMap() {
		return immutableValueMap;
	}

	/*
	 * BaseValue
	 */
	public void setBaseValue(final VarLocal local, final BaseReference ref, final Set<? extends Value> values,
			final ProgramPointId updatedFrom, final ExecutionState oldState) {
		if (!local.varType.equals(VarType.OBJECT)) {
			throw new IllegalArgumentException();
		}
		updateBaseValue(ref, values, updatedFrom, oldState);
		setPointsTo(local, ref);
	}

	public void setBaseValueWithNull(final VarLocal local, final BaseReference ref, final Set<? extends Value> values,
			final ProgramPointId updatedFrom, final ExecutionState oldState) {
		if (!local.varType.equals(VarType.OBJECT)) {
			throw new IllegalArgumentException();
		}
		updateBaseValue(ref, values, updatedFrom, oldState);
		setPointsTo(local, ImmutableSet.of(ref, BaseReference.NULL_REF));
	}

	public void updateBaseValue(final BaseReference ref, final Set<? extends Value> values, final ProgramPointId updatedFrom,
			final ExecutionState oldState) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		for (final Value value : values) {
			if (value instanceof ImmutableValue || value instanceof RefArrayValue || value instanceof RefObjectValue) {
				throw new IllegalArgumentException();
			}
		}
		final Set<? extends Value> newVals;
		if (oldState != null && oldState.baseValueMap.containsKey(ref)) {
			final Set<Value> oldVals = oldState.baseValueMap.get(ref);
			if (values.equals(oldVals)) {
				newVals = oldVals;
			} else if (oldVals.size() == 1 && oldVals.iterator().next() instanceof ConcreteValue) {
				final Set<Value> tNewVals = new HashSet<>();
				final ConcreteValue oldVal = (ConcreteValue) oldVals.iterator().next();
				for (final Value val : values) {
					tNewVals.add((Value) oldVal.widenWith(val, updatedFrom));
				}
				newVals = tNewVals;
			} else {
				if (values.size() != 1 || !(values.iterator().next() instanceof UnknownMutableValue)) {
					setToUnknown(ref, updatedFrom);
					newVals = getBaseValue(ref);
				} else {
					newVals = values;
				}
			}
		} else {
			newVals = values;
		}
		baseValueMap = Utils.multimapWithValues(baseValueMap, ref, newVals);
	}

	public ImmutableSet<Value> getBaseValue(final BaseReference ref) {
		if (ref == BaseReference.NULL_REF || !baseValueMap.containsKey(ref)) {
			throw new IllegalArgumentException();
		}
		return baseValueMap.get(ref);
	}

	public ImmutableSetMultimap<BaseReference, Value> getBaseValueMap() {
		return baseValueMap;
	}

	/*
	 * ArrayValue
	 */
	public void setArrayValue(final VarLocal local, final ArrayReference ref, final RefArrayValue value,
			final ProgramPointId updatedFrom, final ExecutionState oldState) {
		updateArrayValue(ref, value, updatedFrom, oldState);
		updateArrayRefs(local, Collections.singleton(ref));
	}

	public void updateArrayValue(final ArrayReference ref, final RefArrayValue value, final ProgramPointId updatedFrom) {
		updateArrayValue(ref, value, updatedFrom, null);
	}

	private void updateArrayValue(final ArrayReference ref, final RefArrayValue value, final ProgramPointId updatedFrom,
			final ExecutionState oldState) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		final RefArrayValue newValue;
		if (oldState != null && oldState.arrayValueMap.containsKey(ref)) {
			final RefArrayValue oldVal = oldState.arrayValueMap.get(ref);
			if (value.equals(oldVal)) {
				newValue = value;
			} else {
				if (!value.isUnknown()) {
					setToUnknown(ref, updatedFrom);
					newValue = getArrayValue(ref);
				} else {
					newValue = value;
				}
			}
		} else {
			newValue = value;
		}
		arrayValueMap = Utils.mapWithValue(arrayValueMap, ref, newValue);
	}

	public RefArrayValue getArrayValue(final ArrayReference ref) {
		if (ref == ArrayReference.NULL_REF || !arrayValueMap.containsKey(ref)) {
			throw new IllegalArgumentException();
		}
		return arrayValueMap.get(ref);
	}

	public void updateArrayRefs(final VarLocal local, final Set<ArrayReference> refs) {
		if (!local.varType.equals(VarType.OBJECT) && !local.varType.equals(VarType.ARRAY)) {
			throw new IllegalArgumentException();
		}
		setPointsTo(local, refs);
	}

	public ImmutableMap<ArrayReference, RefArrayValue> getArrayValueMap() {
		return arrayValueMap;
	}

	/*
	 * ObjectValue
	 */
	public void setObjectValue(final VarLocal local, final ObjectReference ref, final RefObjectValue value,
			final ProgramPointId updatedFrom, final ExecutionState oldState) {
		updateObjectValue(ref, value, updatedFrom, oldState);
		updateObjectRefs(local, Collections.singleton(ref));
	}

	public void updateObjectValue(final ObjectReference ref, final RefObjectValue value, final ProgramPointId updatedFrom) {
		updateObjectValue(ref, value, updatedFrom, null);
	}

	private void updateObjectValue(final ObjectReference ref, final RefObjectValue value, final ProgramPointId updatedFrom,
			final ExecutionState oldState) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		final RefObjectValue newValue;
		if (oldState != null && oldState.objectValueMap.containsKey(ref)) {
			final RefObjectValue oldVal = oldState.objectValueMap.get(ref);
			if (value.equals(oldVal)) {
				newValue = value;
			} else {
				if (!value.isUnknownObject()) {
					setToUnknown(ref, updatedFrom);
					newValue = getObjectValue(ref);
				} else {
					newValue = value;
				}
			}
		} else {
			newValue = value;
		}
		objectValueMap = Utils.mapWithValue(objectValueMap, ref, newValue);
	}

	public RefObjectValue getObjectValue(final ObjectReference ref) {
		if (ref == ObjectReference.NULL_REF || !objectValueMap.containsKey(ref)) {
			throw new IllegalArgumentException();
		}
		return objectValueMap.get(ref);
	}

	public void updateObjectRefs(final VarLocal local, final Set<ObjectReference> refs) {
		if (!local.varType.equals(VarType.OBJECT) && !local.varType.equals(VarType.INCLUDED)) {
			throw new IllegalArgumentException();
		}
		setPointsTo(local, refs);
	}

	public ImmutableMap<ObjectReference, RefObjectValue> getObjectValueMap() {
		return objectValueMap;
	}

	/*
	 * Unknown
	 */

	private RefObjectValue createUnknownObject(final String className) {
		if (includedFields.containsKey(className)) {
			RefObjectValue refObjectValue = RefObjectValue.createUnknown(className, includedFields.get(className));
			for (final FieldDesc field : includedFields.get(className)) {
				if (possibleFieldValues.containsKey(field)) {
					refObjectValue = refObjectValue.setField(field, possibleFieldValues.get(field));
				} else {
					throw new RuntimeException("Missing possible values");
				}
			}
			return refObjectValue;
		} else {
			return RefObjectValue.createUnknown(className, Collections.emptySet());
		}
	}

	private RefObjectValue createUnknownObject(final RefObjectValue template) {
		if (template.isUnknownObject()) {
			throw new IllegalArgumentException();
		}
		if (includedFields.containsKey(template.getClassName())) {
			RefObjectValue refObjectValue = RefObjectValue.createUnknown(template.getClassName(),
					includedFields.get(template.getClassName()));
			for (final FieldDesc field : includedFields.get(template.getClassName())) {
				if (possibleFieldValues.containsKey(field)) {
					if (template.isUnknown(field)) {
						refObjectValue.setFieldToUnknown(field);
					} else {
						final Set<VarLocalOrImmutableValue> possibleValues = new HashSet<>(possibleFieldValues.get(field));
						if (template.isFieldSet(field)) {
							possibleValues.addAll(template.getField(field));
						} else {
							possibleValues.add(field.defaultValue);
						}
						refObjectValue = refObjectValue.setField(field, possibleValues);
					}
				} else {
					throw new RuntimeException("Missing possible values");
				}
			}
			return refObjectValue;
		} else {
			return RefObjectValue.createUnknown(template.getClassName(), Collections.emptySet());
		}
	}

	public void setUnknown(final VarLocal local, final ProgramPointId updatedFrom) {
		if (local.varType.equals(VarType.IMMUTABLE)) {
			setImmutableValue(local, ImmutableReference.create(updatedFrom), UnknownImmutableValue.create(updatedFrom), updatedFrom, null);
		} else if (local.varType.equals(VarType.OBJECT)) {
			setBaseValue(local, BaseReference.create(updatedFrom), Collections.singleton(UnknownMutableValue.create()), updatedFrom, null);
		} else if (local.varType.equals(VarType.INCLUDED)) {
			setObjectValue(local, ObjectReference.create(updatedFrom), createUnknownObject(local.typeName), updatedFrom, null);
		} else if (local.varType.equals(VarType.ARRAY)) {
			setArrayValue(local, ArrayReference.create(updatedFrom), RefArrayValue.createUnknown(), updatedFrom, null);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void setToUnknown(final Set<? extends VarLocalOrImmutableValue> locals, final ProgramPointId updatedFrom) {
		final Set<AbstractReference> refs = new HashSet<>();
		for (final VarLocalOrImmutableValue local : locals) {
			if (local instanceof VarLocal && isLocalInitialized(local)) {
				refs.addAll(getPointsTo((VarLocal) local));
			}
		}
		setToUnknownImpl(refs, updatedFrom);
	}

	public void setToUnknown(final VarLocalOrImmutableValue local, final ProgramPointId updatedFrom) {
		if (local instanceof VarLocal) {
			setToUnknownImpl(pointsToMap.get((VarLocal) local), updatedFrom);
		}
	}

	public void setToUnknown(final AbstractReference ref, final ProgramPointId updatedFrom) {
		setToUnknownImpl(Collections.singleton(ref), updatedFrom);
	}

	private void setToUnknownImpl(final Set<? extends AbstractReference> refs, final ProgramPointId updatedFrom) {
		if (this.isBottom) {
			throw new IllegalStateException();
		}
		if (refs.isEmpty()) {
			return;
		}
		final ReferenceVisitor<Void> updater = new ReferenceVisitor<Void>() {

			@Override
			public Void visit(AbstractReference reference) {
				return reference.apply(this);
			}

			@Override
			public Void visitImmutableReference(ImmutableReference immutableReference) {
				return null;
			}

			@Override
			public Void visitBaseReference(BaseReference baseReference) {
				if (baseReference != BaseReference.NULL_REF) {
					final Set<Value> oldValues = getBaseValue(baseReference);
					updateBaseValue(baseReference, Collections.singleton(UnknownMutableValue.create()), updatedFrom, null);
					for (final Value oldValue : oldValues) {
						if (oldValue instanceof RefValue) {
							if (oldValue instanceof AbstractBuiltStatementValue) {
								for (final AbstractBind<VarLocalOrImmutableValue> bind : ((AbstractBuiltStatementValue) oldValue).binds) {
									setToUnknown(bind.value, updatedFrom);
								}
							} else if (oldValue instanceof BuiltStatementAssignmentValue) {
								setToUnknown(new HashSet<>(((BuiltStatementAssignmentValue) oldValue).values), updatedFrom);
							} else if (oldValue instanceof BuiltStatementClauseValue) {
								setToUnknown(((BuiltStatementClauseValue) oldValue).value, updatedFrom);
							} else {
								throw new RuntimeException("Unexpected RefValue");
							}
						}
					}
				}
				return null;
			}

			@Override
			public Void visitArrayReference(ArrayReference arrayReference) {
				if (arrayReference != ArrayReference.NULL_REF) {
					final RefArrayValue refArray = getArrayValue(arrayReference);
					if (!refArray.isUnknown()) {
						updateArrayValue(arrayReference, refArray.toUnknown(), updatedFrom);
						setToUnknown(refArray.getAllElements(), updatedFrom);
					}
				}
				return null;
			}

			@Override
			public Void visitObjectReference(ObjectReference objectReference) {
				if (objectReference != ObjectReference.NULL_REF) {
					final RefObjectValue refObject = getObjectValue(objectReference);
					if (!refObject.isUnknownObject()) {
						updateObjectValue(objectReference, createUnknownObject(refObject), updatedFrom);
						setToUnknown(refObject.getAllFieldValues(), updatedFrom);
					}
				}
				return null;
			}
		};

		for (final AbstractReference ref : refs) {
			updater.visit(ref);
		}
	}

	/*
	 * Value-Resolving
	 */
	private final ReferenceVisitor<ImmutableSet<Value>> valueResolver = new ReferenceVisitor<ImmutableSet<Value>>() {

		@Override
		public ImmutableSet<Value> visit(AbstractReference reference) {
			return reference.apply(this);
		}

		@Override
		public ImmutableSet<Value> visitImmutableReference(ImmutableReference immutableReference) {
			return ImmutableSet.of(getImmutableValue(immutableReference));
		}

		@Override
		public ImmutableSet<Value> visitBaseReference(BaseReference baseReference) {
			return getBaseValue(baseReference);
		}

		@Override
		public ImmutableSet<Value> visitArrayReference(ArrayReference arrayReference) {
			return ImmutableSet.of(getArrayValue(arrayReference));
		}

		@Override
		public ImmutableSet<Value> visitObjectReference(ObjectReference objectReference) {
			return ImmutableSet.of(getObjectValue(objectReference));
		}
	};

	public ImmutableSet<Value> getValue(final AbstractReference reference) {
		return valueResolver.visit(reference);
	}

	private final ReferenceVisitor<ImmutableSet<ConcreteValue>> concreteValueTransformer = new ReferenceVisitor<ImmutableSet<ConcreteValue>>() {

		@Override
		public ImmutableSet<ConcreteValue> visit(AbstractReference reference) {
			return reference.apply(this);
		}

		@Override
		public ImmutableSet<ConcreteValue> visitImmutableReference(ImmutableReference immutableReference) {
			return ImmutableSet.of(getImmutableValue(immutableReference));
		}

		@Override
		public ImmutableSet<ConcreteValue> visitBaseReference(BaseReference baseReference) {
			final ImmutableSet.Builder<ConcreteValue> retBuilder = ImmutableSet.builder();
			for (final Value val : getBaseValue(baseReference)) {
				if (val instanceof ConcreteValue) {
					retBuilder.add((ConcreteValue) val);
				} else if (val instanceof AbstractBuiltStatementValue) {
					final AbstractBuiltStatementValue stmt = (AbstractBuiltStatementValue) val;
					final List<AbstractBind<ConcreteValue>> concreteBinds = new ArrayList<>();
					for (final AbstractBind<VarLocalOrImmutableValue> bind : stmt.binds) {
						if (bind.value instanceof ImmutableValue) {
							concreteBinds.add(bind.createNew((ImmutableValue) bind.value));
						} else {
							final VarLocal bindLocal = (VarLocal) bind.value;
							if (bindLocal.varType.equals(VarType.IMMUTABLE)) {
								final Set<ImmutableValue> bindValues = getImmutableValues(bindLocal);
								if (bindValues.size() == 1) {
									concreteBinds.add(bind.createNew(bindValues.iterator().next()));
								} else {
									concreteBinds.add(bind.createNew(UnknownMutableValue.create()));
								}
							} else {
								concreteBinds.add(bind.createNew(UnknownMutableValue.create()));
							}
						}
					}
					retBuilder.add(
							StatementValue.create(stmt.getStatement(), concreteBinds, stmt.consistencyLevel, stmt.serialConsistencyLevel));
				} else {
					retBuilder.add(UnknownMutableValue.create());
				}
			}
			return retBuilder.build();
		}

		@Override
		public ImmutableSet<ConcreteValue> visitArrayReference(ArrayReference arrayReference) {
			return ImmutableSet.of(UnknownMutableValue.create());
		}

		@Override
		public ImmutableSet<ConcreteValue> visitObjectReference(ObjectReference objectReference) {
			return ImmutableSet.of(UnknownMutableValue.create());
		}
	};

	public ImmutableSet<ConcreteValue> getConcreteValue(final AbstractReference reference) {
		return concreteValueTransformer.visit(reference);
	}

	public void mergeWith(final ExecutionState other, final ExecutionState result) {
		Objects.requireNonNull(other);
		Objects.requireNonNull(result);
		if (result == this && result == other) {
			return;
		}

		final ExecutionState flowToMerge;
		if (result == this) {
			flowToMerge = other;
		} else if (result == other) {
			flowToMerge = this;
		} else {
			result.clear();
			copyTo(result);
			flowToMerge = other;
		}

		if (other.isBottom) {
			return;
		} else if (result.isBottom) {
			other.copyTo(result);
			return;
		}

		if (result.includedFields != flowToMerge.includedFields || result.possibleFieldValues != flowToMerge.possibleFieldValues) {
			throw new RuntimeException("Field information must not change");
		}

		result.isBottom = result.isBottom && flowToMerge.isBottom;

		result.transactionGraph = result.transactionGraph.mergeWith(flowToMerge.transactionGraph);
		result.executeInvokeMap = Utils.mergeMap(result.executeInvokeMap, flowToMerge.executeInvokeMap, key -> {
			return result.executeInvokeMap.get(key).mergeWith(flowToMerge.executeInvokeMap.get(key));
		});
		result.lastExecuteInvokes = Utils.mergeSet(result.lastExecuteInvokes, flowToMerge.lastExecuteInvokes);
		result.currentPathConstraint = result.currentPathConstraint.mergeWith(flowToMerge.currentPathConstraint);
		result.mustInvokeExecute = result.mustInvokeExecute && flowToMerge.mustInvokeExecute;

		result.methodCallStates = Utils.mergeMultimap(result.methodCallStates, flowToMerge.methodCallStates);
		result.methodCallReturnVals = Utils.mergeMultimap(result.methodCallReturnVals, flowToMerge.methodCallReturnVals);

		result.staticMayBeNotSet = Utils.mergeSet(result.staticMayBeNotSet, flowToMerge.staticMayBeNotSet);
		result.staticMayBeNotSet = Utils.mergeSet(result.staticMayBeNotSet,
				Utils.getMissingKeys(result.staticFieldMap, flowToMerge.staticFieldMap));
		result.staticFieldMap = Utils.mergeMultimap(result.staticFieldMap, flowToMerge.staticFieldMap);
		result.pointsToMap = Utils.mergeMultimap(result.pointsToMap, flowToMerge.pointsToMap);

		result.immutableValueMap = Utils.mergeMap(result.immutableValueMap, flowToMerge.immutableValueMap, key -> {
			final ImmutableValue v1 = result.immutableValueMap.get(key);
			if (v1 instanceof UnknownImmutableValue && ((UnknownImmutableValue) v1).isWidened) {
				return v1;
			}
			final ImmutableValue v2 = flowToMerge.immutableValueMap.get(key);
			if (v2 instanceof UnknownImmutableValue && ((UnknownImmutableValue) v2).isWidened) {
				return v2;
			}
			final ImmutableValue choice = ImmutableChoiceValue.create(key.createdIn, ImmutableSet.of(v1, v2));
			return choice;
		});
		result.baseValueMap = Utils.mergeMultimap(result.baseValueMap, flowToMerge.baseValueMap);
		result.arrayValueMap = Utils.mergeMap(result.arrayValueMap, flowToMerge.arrayValueMap,
				key -> result.arrayValueMap.get(key).mergeWith(flowToMerge.arrayValueMap.get(key)));

		final Set<VarLocalOrImmutableValue> setToUnknown = new HashSet<>();
		result.objectValueMap = Utils.mergeMap(result.objectValueMap, flowToMerge.objectValueMap,
				key -> {
					final RefObjectValue.MergeResult merge = result.objectValueMap.get(key).mergeWith(flowToMerge.objectValueMap.get(key));
					setToUnknown.addAll(merge.setToUnknown);
					return merge.result;
				});
		result.setToUnknown(setToUnknown, MERGE_PROGRAM_POINT);
	}

	public void copyTo(final ExecutionState result) {
		Objects.requireNonNull(result);
		if (result == this) {
			return;
		}

		result.includedFields = includedFields;
		result.possibleFieldValues = possibleFieldValues;

		result.isBottom = isBottom;

		result.transactionGraph = transactionGraph;
		result.executeInvokeMap = executeInvokeMap;
		result.lastExecuteInvokes = lastExecuteInvokes;
		result.currentPathConstraint = currentPathConstraint;
		result.mustInvokeExecute = mustInvokeExecute;

		result.methodCallStates = methodCallStates;
		result.methodCallReturnVals = methodCallReturnVals;

		result.staticFieldMap = staticFieldMap;
		result.staticMayBeNotSet = staticMayBeNotSet;
		result.pointsToMap = pointsToMap;

		result.immutableValueMap = immutableValueMap;
		result.baseValueMap = baseValueMap;
		result.arrayValueMap = arrayValueMap;
		result.objectValueMap = objectValueMap;
	}

	@Override
	public ExecutionState clone() {
		final ExecutionState ret = new ExecutionState();
		copyTo(ret);
		return ret;
	}

	public void clear() {
		includedFields = ImmutableSetMultimap.of();
		possibleFieldValues = ImmutableSetMultimap.of();

		isBottom = true;

		transactionGraph = IdTransactionGraph.createEmpty();
		executeInvokeMap = ImmutableMap.of();
		lastExecuteInvokes = ImmutableSet.of();
		currentPathConstraint = EdgeConstraint.createEmpty();
		mustInvokeExecute = false;

		methodCallStates = ImmutableSetMultimap.of();
		methodCallReturnVals = ImmutableSetMultimap.of();

		staticFieldMap = ImmutableSetMultimap.of();
		staticMayBeNotSet = ImmutableSet.of();
		pointsToMap = ImmutableSetMultimap.of();

		immutableValueMap = ImmutableMap.of();
		baseValueMap = ImmutableSetMultimap.of();
		arrayValueMap = ImmutableMap.of();
		objectValueMap = ImmutableMap.of();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrayValueMap == null) ? 0 : arrayValueMap.hashCode());
		result = prime * result + ((baseValueMap == null) ? 0 : baseValueMap.hashCode());
		result = prime * result + ((immutableValueMap == null) ? 0 : immutableValueMap.hashCode());
		result = prime * result + (isBottom ? 1231 : 1237);
		result = prime * result + ((lastExecuteInvokes == null) ? 0 : lastExecuteInvokes.hashCode());
		result = prime * result + ((methodCallReturnVals == null) ? 0 : methodCallReturnVals.hashCode());
		result = prime * result + ((methodCallStates == null) ? 0 : methodCallStates.hashCode());
		result = prime * result + (mustInvokeExecute ? 1231 : 1237);
		result = prime * result + ((objectValueMap == null) ? 0 : objectValueMap.hashCode());
		result = prime * result + ((pointsToMap == null) ? 0 : pointsToMap.hashCode());
		result = prime * result + ((staticFieldMap == null) ? 0 : staticFieldMap.hashCode());
		result = prime * result + ((staticMayBeNotSet == null) ? 0 : staticMayBeNotSet.hashCode());
		result = prime * result + ((transactionGraph == null) ? 0 : transactionGraph.hashCode());
		result = prime * result + ((executeInvokeMap == null) ? 0 : executeInvokeMap.hashCode());
		result = prime * result + ((currentPathConstraint == null) ? 0 : currentPathConstraint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExecutionState other = (ExecutionState) obj;
		if (arrayValueMap == null) {
			if (other.arrayValueMap != null)
				return false;
		} else if (!arrayValueMap.equals(other.arrayValueMap))
			return false;
		if (baseValueMap == null) {
			if (other.baseValueMap != null)
				return false;
		} else if (!baseValueMap.equals(other.baseValueMap))
			return false;
		if (immutableValueMap == null) {
			if (other.immutableValueMap != null)
				return false;
		} else if (!immutableValueMap.equals(other.immutableValueMap))
			return false;
		if (isBottom != other.isBottom)
			return false;
		if (lastExecuteInvokes == null) {
			if (other.lastExecuteInvokes != null)
				return false;
		} else if (!lastExecuteInvokes.equals(other.lastExecuteInvokes))
			return false;
		if (methodCallReturnVals == null) {
			if (other.methodCallReturnVals != null)
				return false;
		} else if (!methodCallReturnVals.equals(other.methodCallReturnVals))
			return false;
		if (methodCallStates == null) {
			if (other.methodCallStates != null)
				return false;
		} else if (!methodCallStates.equals(other.methodCallStates))
			return false;
		if (mustInvokeExecute != other.mustInvokeExecute)
			return false;
		if (objectValueMap == null) {
			if (other.objectValueMap != null)
				return false;
		} else if (!objectValueMap.equals(other.objectValueMap))
			return false;
		if (pointsToMap == null) {
			if (other.pointsToMap != null)
				return false;
		} else if (!pointsToMap.equals(other.pointsToMap))
			return false;
		if (staticFieldMap == null) {
			if (other.staticFieldMap != null)
				return false;
		} else if (!staticFieldMap.equals(other.staticFieldMap))
			return false;
		if (staticMayBeNotSet == null) {
			if (other.staticMayBeNotSet != null)
				return false;
		} else if (!staticMayBeNotSet.equals(other.staticMayBeNotSet))
			return false;
		if (transactionGraph == null) {
			if (other.transactionGraph != null)
				return false;
		} else if (!transactionGraph.equals(other.transactionGraph))
			return false;
		if (executeInvokeMap == null) {
			if (other.executeInvokeMap != null)
				return false;
		} else if (!executeInvokeMap.equals(other.executeInvokeMap))
			return false;
		if (currentPathConstraint == null) {
			if (other.currentPathConstraint != null)
				return false;
		} else if (!currentPathConstraint.equals(other.currentPathConstraint))
			return false;
		if (possibleFieldValues == null) {
			if (other.possibleFieldValues != null)
				return false;
		} else if (!possibleFieldValues.equals(other.possibleFieldValues))
			return false;
		if (includedFields == null) {
			if (other.includedFields != null)
				return false;
		} else if (!includedFields.equals(other.includedFields))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (isBottom) {
			return "ExecutionState [Bottom]";
		} else if (pointsToMap.size() < 200) {
			final StringBuilder sb = new StringBuilder("\n  ");
			for (final VarLocal var : pointsToMap.keySet()) {
				sb.append(var).append(": ");
				for (final AbstractReference ref : pointsToMap.get(var)) {
					if (isNullReference(ref)) {
						sb.append("<null>\n ");
					} else {
						for (final Value val : getValue(ref)) {
							sb.append(val).append("\n  ");
						}
					}
				}
			}
			return sb.toString();
		} else {
			return "ExecutionState [...]";
		}
	}

	public static class MethodCallState {

		public final VarLocal baseLocal;
		public final ImmutableList<VarLocalOrImmutableValue> parameters;

		private MethodCallState(final VarLocal baseLocal, final ImmutableList<VarLocalOrImmutableValue> parameters) {
			if (baseLocal != null && baseLocal.varType.equals(VarType.EXCLUDED)) {
				throw new IllegalArgumentException();
			}
			this.baseLocal = baseLocal;
			this.parameters = Objects.requireNonNull(parameters);
		}

		public static MethodCallState create(final VarLocal baseLocal, final ImmutableList<VarLocalOrImmutableValue> parameters) {
			return new MethodCallState(baseLocal, parameters);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((baseLocal == null) ? 0 : baseLocal.hashCode());
			result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MethodCallState other = (MethodCallState) obj;
			if (baseLocal == null) {
				if (other.baseLocal != null)
					return false;
			} else if (!baseLocal.equals(other.baseLocal))
				return false;
			if (parameters == null) {
				if (other.parameters != null)
					return false;
			} else if (!parameters.equals(other.parameters))
				return false;
			return true;
		}
	}
}
