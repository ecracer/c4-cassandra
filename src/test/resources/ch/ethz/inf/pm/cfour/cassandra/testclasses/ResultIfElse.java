

import com.datastax.driver.core.Session;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;


public class ResultIfElse {
	
	@Transaction
	public void method1(Session session) {
		if (isUserExist(session)){
			session.execute("INSERT INTO users (username) VALUES ('Bob2')");
		} else {
			session.execute("INSERT INTO users (username) VALUES ('Bob')");
		}
		session.execute("SELECT * FROM users WHERE username = 'Bob2'");
	}
	
	private boolean isUserExist(Session session){
		return !session.execute("SELECT * FROM users WHERE username = 'Bob'").isExhausted();
	}
}