package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ConcreteValueVisitor;

public class UnknownMutableValue implements ConcreteValue {

	public final static UnknownMutableValue INSTANCE = new UnknownMutableValue();

	private UnknownMutableValue() {
	}

	public static UnknownMutableValue create() {
		return INSTANCE;
	}

	@Override
	public <R> R apply(ConcreteValueVisitor<R> visitor) {
		return visitor.visitUnknownMutableValue(this);
	}

	@Override
	public ConcreteValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		return this;
	}

	@Override
	public int hashCode() {
		return 514354;
	}

	@Override
	public boolean equals(Object obj) {
		// singleton
		if (this == obj)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "UnknownMutableValue";
	}
}
