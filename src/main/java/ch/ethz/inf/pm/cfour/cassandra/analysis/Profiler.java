package ch.ethz.inf.pm.cfour.cassandra.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Profiler {

	private final static Logger LOG = LogManager.getLogger(Profiler.class);

	private long lastTimeMillis = System.currentTimeMillis();
	private final List<String> timeInfo = new ArrayList<>();
	private final Map<String, Double> phaseRuntimes = new HashMap<>();

	public void startNewPhase(final String oldPhase) {
		final double runtime = ((double) (System.currentTimeMillis() - lastTimeMillis)) / 1000f;
		phaseRuntimes.put(oldPhase, runtime);
		timeInfo.add(oldPhase + " took " + String.format("%.3f", runtime) + "s");
		lastTimeMillis = System.currentTimeMillis();
	}

	public void addSummaryPhase(final String phase) {
		double runtimes = 0d;
		for (final Double runtime : phaseRuntimes.values()) {
			runtimes += runtime;
		}
		phaseRuntimes.put(phase, runtimes);
		timeInfo.add(phase + " took " + String.format("%.3f", runtimes) + "s");
	}

	public double getRuntimeForPhase(final String phase) {
		return phaseRuntimes.getOrDefault(phase, 0d);
	}

	public void logProfilingInformation() {
		logProfilingInformation(Level.INFO);
	}

	public void logProfilingInformation(Level logLevel) {
		if (LOG.isEnabled(logLevel)) {
			for (final String time : timeInfo) {
				LOG.log(logLevel, time);
			}
		}
	}
}
