package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;

public class CFourSchemaTest extends AbstractCFourTest {

	public CFourSchemaTest() throws IOException {
		super("CFourSchema");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username")
				.addNonPrimaryKeyColumn("users", "password").build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		// use schema information
		optionsList.add(options);
		options = getInitialOptions();
		options.setSynchronizingOperationsEnabled(false);
		options.setCreateSchemaFile(null);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.getCreateSchemaFile() != null) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(2, result.getTotalVerifiedViolationsSize4());
		}
	}
}
