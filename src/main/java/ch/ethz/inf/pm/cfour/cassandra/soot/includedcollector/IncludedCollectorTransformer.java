package ch.ethz.inf.pm.cfour.cassandra.soot.includedcollector;

import java.util.Iterator;
import java.util.Map;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import ch.ethz.inf.pm.cfour.cassandra.soot.vars.FieldDesc;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;

/**
 * Collects all methods that are analyzed and all the fields from the analyzed classes. Builds a multimap that assigns
 * to each class a set of fields that objects of this class have (including fields from superclasses)
 */
public class IncludedCollectorTransformer extends SceneTransformer {

	private final static Logger LOG = LogManager.getLogger(IncludedCollectorTransformer.class);

	private final ImmutableSet.Builder<SootMethod> includedMethodsBuilder = ImmutableSet.builder();
	private final ImmutableSet.Builder<FieldDesc> includedStaticFieldsBuilder = ImmutableSet.builder();
	private final ImmutableSet.Builder<FieldDesc> includedFieldsBuilder = ImmutableSet.builder();
	private final ImmutableSetMultimap.Builder<String, FieldDesc> fieldsPerClassBuilder = ImmutableSetMultimap.builder();

	public ImmutableSet<SootMethod> getIncludedMethods() {
		return includedMethodsBuilder.build();
	}

	public ImmutableSet<FieldDesc> getIncludedStaticFields() {
		return includedStaticFieldsBuilder.build();
	}

	public ImmutableSet<FieldDesc> getIncludedFields() {
		return includedFieldsBuilder.build();
	}

	public ImmutableSetMultimap<String, FieldDesc> getFieldsPerClass() {
		return fieldsPerClassBuilder.build();
	}

	@Override
	protected void internalTransform(final String phaseName, @SuppressWarnings("rawtypes") final Map options) {
		final Iterator<SootClass> clazzIt = Scene.v().getApplicationClasses().snapshotIterator();
		int classes = 0;
		int methods = 0;
		while (clazzIt.hasNext()) {
			classes++;
			final SootClass clazz = clazzIt.next();
			for (final SootMethod method : clazz.getMethods()) {
				if (method.isConcrete()) {
					includedMethodsBuilder.add(method);
					methods++;
				}
			}
			SootClass current = clazz;
			while (current != null) {
				for (final SootField field : current.getFields()) {
					final FieldDesc fieldDesc = SootValUtils.transformField(field);
					if (!fieldDesc.hasAnnotation && !fieldDesc.varType.equals(VarType.EXCLUDED)) {
						if (fieldDesc.isStatic) {
							includedStaticFieldsBuilder.add(fieldDesc);
						} else {
							includedFieldsBuilder.add(fieldDesc);
							fieldsPerClassBuilder.put(clazz.getName(), fieldDesc);
						}
					}
				}
				if (current.hasSuperclass() && Scene.v().getApplicationClasses().contains(current.getSuperclass())) {
					current = current.getSuperclass();
				} else {
					break;
				}
			}
		}
		if (LOG.isInfoEnabled()) {
			LOG.info("Included classes: " + classes);
			LOG.info("Included methods: " + methods);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Included static fields: " + includedStaticFieldsBuilder.build());
			LOG.debug("Included fields: " + includedFieldsBuilder.build());
			LOG.debug("Included fields per class: " + fieldsPerClassBuilder.build());
		}
	}
}
