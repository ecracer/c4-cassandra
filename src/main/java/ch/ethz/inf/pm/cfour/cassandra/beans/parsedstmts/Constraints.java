package ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;

/**
 * List of constraints (i.e. representation of the WHERE part of a statement)
 */
public class Constraints {

	private ImmutableList<Constraint> constraints;

	private Constraints(final ImmutableList<Constraint> constraints) {
		this.constraints = constraints;
	}

	public static Constraints create(final List<Constraint> constraints) {
		return new Constraints(ImmutableList.copyOf(Objects.requireNonNull(constraints)));
	}

	public static Constraints create(final Constraint... constraints) {
		return new Constraints(ImmutableList.copyOf(Objects.requireNonNull(constraints)));
	}

	public static Builder builder() {
		return new Builder();
	}

	public int getConstraintSize() {
		return constraints.size();
	}

	public Constraint getConstraint(final int idx) {
		return constraints.get(idx);
	}

	public ImmutableListMultimap<String, Integer> getConstraintIdxsPerColumn() {

		final ImmutableListMultimap.Builder<String, Integer> retBuilder = ImmutableListMultimap.builder();
		for (int i = 0; i < constraints.size(); i++) {
			retBuilder.put(constraints.get(i).column, i);
		}
		return retBuilder.build();
	}

	public String getLabel() {
		if (constraints == null) {
			return "unknown";
		} else if (constraints.isEmpty()) {
			return "";
		} else {
			final StringBuilder sb = new StringBuilder();
			final Iterator<Constraint> it = constraints.iterator();
			sb.append(it.next().getLabel());
			while (it.hasNext()) {
				sb.append("_AND_").append(it.next().getLabel());
			}
			return sb.toString();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((constraints == null) ? 0 : constraints.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Constraints other = (Constraints) obj;
		if (constraints == null) {
			if (other.constraints != null)
				return false;
		} else if (!constraints.equals(other.constraints))
			return false;
		return true;
	}

	public static class Builder {

		private final ImmutableList.Builder<Constraint> constraints = ImmutableList.builder();
		private int nextIdx = 0;

		private Builder() {
		}

		public int add(final Constraint constraint) {
			constraints.add(Objects.requireNonNull(constraint));
			return nextIdx++;
		}

		public Constraints build() {
			return new Constraints(constraints.build());
		}
	}
}
