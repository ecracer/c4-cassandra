package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ImmutableValueVisitor;

public class ConsistencyLevelValue extends AbstractImmutableValue implements ImmutableValue {

	public final Level level;

	private ConsistencyLevelValue(final Level level) {
		this.level = Objects.requireNonNull(level);
	}

	public static ConsistencyLevelValue create(final Level level) {
		return new ConsistencyLevelValue(level);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitConsistencyLevelValue(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsistencyLevelValue other = (ConsistencyLevelValue) obj;
		if (level != other.level)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return level.toString();
	}

	public static enum Level {
		ALL, ANY, EACH_QUORUM, LOCAL_ONE, LOCAL_QUORUM, LOCAL_SERIAL, ONE, QUORUM, SERIAL, THREE, TWO
	}
}
