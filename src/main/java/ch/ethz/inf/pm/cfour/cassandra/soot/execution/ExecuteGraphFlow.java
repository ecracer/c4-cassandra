package ch.ethz.inf.pm.cfour.cassandra.soot.execution;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;

/**
 * Flow for the {@link ExecuteGraphAnalysis}
 */
public class ExecuteGraphFlow {

	private Set<AbstractProgramPoint> lastProgramPoints = new HashSet<>();

	public void addLastProgramPoint(final AbstractProgramPoint lastElement) {
		lastProgramPoints.add(Objects.requireNonNull(lastElement));
	}

	public Set<AbstractProgramPoint> getLastProgramPoints() {
		return Collections.unmodifiableSet(lastProgramPoints);
	}

	public void mergeWith(final ExecuteGraphFlow other, final ExecuteGraphFlow result) {
		Objects.requireNonNull(other);
		Objects.requireNonNull(result);
		if (result == this && result == other) {
			return;
		}
		final ExecuteGraphFlow flowToMerge;
		if (result == this) {
			flowToMerge = other;
		} else if (result == other) {
			flowToMerge = this;
		} else {
			result.clear();
			copyTo(result);
			flowToMerge = other;
		}

		result.lastProgramPoints.addAll(flowToMerge.lastProgramPoints);
	}

	public void copyTo(final ExecuteGraphFlow result) {
		Objects.requireNonNull(result);
		if (result == this) {
			return;
		}
		result.clear();
		result.mergeWith(this, result);
	}

	public void clear() {
		lastProgramPoints.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lastProgramPoints == null) ? 0 : lastProgramPoints.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExecuteGraphFlow other = (ExecuteGraphFlow) obj;
		if (lastProgramPoints == null) {
			if (other.lastProgramPoints != null)
				return false;
		} else if (!lastProgramPoints.equals(other.lastProgramPoints))
			return false;
		return true;
	}

}
