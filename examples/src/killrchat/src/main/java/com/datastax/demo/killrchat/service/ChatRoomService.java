package com.datastax.demo.killrchat.service;

import com.datastax.demo.killrchat.entity.ChatRoomEntity;
import com.datastax.demo.killrchat.entity.MessageEntity;
import com.datastax.demo.killrchat.entity.UserEntity;
import com.datastax.demo.killrchat.exceptions.ChatRoomAlreadyExistsException;
import com.datastax.demo.killrchat.exceptions.ChatRoomDoesNotExistException;
import com.datastax.demo.killrchat.exceptions.IncorrectRoomException;
import com.datastax.demo.killrchat.model.ChatRoomModel;
import com.datastax.demo.killrchat.model.LightUserModel;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Sets;
import info.archinnov.achilles.exception.AchillesLightWeightTransactionException;
import info.archinnov.achilles.persistence.Batch;
import info.archinnov.achilles.persistence.PersistenceManager;
import info.archinnov.achilles.type.OptionsBuilder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.datastax.demo.killrchat.entity.Schema.CHATROOMS;
import static com.datastax.demo.killrchat.entity.Schema.CHATROOM_MESSAGES;
import static com.datastax.demo.killrchat.entity.Schema.KEYSPACE;
import static com.datastax.driver.core.querybuilder.QueryBuilder.batch;
import static com.datastax.driver.core.querybuilder.QueryBuilder.bindMarker;
import static com.datastax.driver.core.querybuilder.QueryBuilder.delete;
import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.select;
import static info.archinnov.achilles.type.OptionsBuilder.ifEqualCondition;
import static info.archinnov.achilles.type.OptionsBuilder.ifExists;
import static info.archinnov.achilles.type.OptionsBuilder.ifNotExists;
import static java.lang.String.format;

@Service
public class ChatRoomService {

    private static final Delete.Where DELETE_ROOM_MESSAGES = delete().from(KEYSPACE, CHATROOM_MESSAGES).where(QueryBuilder.eq("room_name", bindMarker("roomName")));

    public static final String DELETION_MESSAGE = "The room '%s' has been removed by '%s'";


    @Inject
    Session session;


    public void createChatRoom(String roomName, String banner, LightUserModel creator) {
        final Set<LightUserModel> participantsList = Sets.newHashSet(creator);
        final String creatorLogin = creator.getLogin();
        final ChatRoomEntity room = new ChatRoomEntity(roomName, creator, new Date(), banner, participantsList);
        boolean applied = session.execute("INSERT INTO chat_rooms (room_name, create_date, creator_login, participants, banner, creator) VALUES (?, ?, ?, ?, ?, ?) IF NOT EXISTS", 
        		room.getRoomName(), room.getCreationDate(), room.getCreatorLogin(), room.getParticipants(), room.getBanner(), room.getCreator()).wasApplied();
        if (!applied){
            throw new ChatRoomAlreadyExistsException(format("The room '%s' already exists", roomName));
        }

        session.execute("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms + {?} WHERE login = ?", roomName, creatorLogin);
    }

    public ChatRoomModel findRoomByName(String roomName) {
    	final Row row = session.execute("SELECT room_name, create_date, creator_login, participants, banner, creator FROM chat_rooms WHERE room_name = ?", roomName).one();
        if (row == null) {
            throw new ChatRoomDoesNotExistException(format("Chat room '%s' does not exists", roomName));
        }
        final ChatRoomModel chatRoom = new ChatRoomModel();
        chatRoom.setRoomName(row.getString("room_name"));;
        //TODO add other
        return chatRoom;
    }

    public List<ChatRoomModel> listChatRooms(int fetchSize) {
    	final Iterator<Row> rowIt = session.execute("SELECT  room_name, create_date, creator_login, participants, banner, creator FROM chat_rooms LIMIT ?").iterator();
    	final List<ChatRoomModel> chatRooms = new ArrayList<>();
    	while (rowIt.hasNext()){
    		final Row row = rowIt.next();
    		 final ChatRoomModel chatRoom = new ChatRoomModel();
    	        chatRoom.setRoomName(row.getString("room_name"));;
    	        //TODO add other
    	       chatRooms.add(chatRoom);
    	}
        return chatRooms;
    }

    public void addUserToRoom(String roomName, LightUserModel participant) {
        boolean applied = session.execute("UPDATE chat_rooms SET participants = participants + {?} WHERE room_name = ? IF EXISTS", participant, roomName).wasApplied();
        if (!applied){
            throw new ChatRoomDoesNotExistException(format("The chat room '%s' does not exist", roomName));
        }

        session.execute("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms + {?} WHERE login = ?", roomName, participant.getLogin());
    }

    public void removeUserFromRoom(String roomName, LightUserModel participant) {
        final BatchStatement batch = new BatchStatement();

        batch.add(new SimpleStatement("UPDATE chat_rooms SET participants = participants - {?} WHERE room_name = ?", participant, roomName));
        batch.add(new SimpleStatement("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms - {?} WHERE login = ?", roomName, participant.getLogin()));
        
        session.execute(batch);
    }

    public String deleteRoomWithParticipants(String creatorLogin, String roomName, Set<String> participants) {
    	boolean applied = session.execute("DELETE FROM chat_rooms WHERE room_name = ? IF creator_login = ?", roomName, creatorLogin).wasApplied();
    	if (!applied){
    		throw new IncorrectRoomException("Incorrect room");
    	}
    	
    	session.execute("DELETE FROM chat_room_messages WHERE room_name = ?", roomName);
        // Delete all chat messages from room
    	
    	final BatchStatement batch = new BatchStatement();
    	for (final String participantLogin : participants){
    		batch.add(new SimpleStatement("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms - {?} WHERE login = ?", roomName, participantLogin));
    	}
    	session.execute(batch);

        return String.format(DELETION_MESSAGE, roomName, creatorLogin);
    }

}
