package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Options needed for the soot phase
 */
public class SootOptions {
	private final List<String> classpath = new ArrayList<>();
	private final List<TransactionMethod> transactionMethods = new ArrayList<>();
	private final List<TransactionMethod> excludedTransactionMethods = new ArrayList<>();
	private boolean soundThrowAnalysisEnabled = false;
	private boolean clientLocalValuesEnabled = true;

	public String getClasspathAsString(final String... additionalEntries) {
		final StringBuilder builder = new StringBuilder();
		final ArrayList<String> completeClasspath = new ArrayList<>(this.classpath);
		if (additionalEntries != null && additionalEntries.length > 0) {
			completeClasspath.addAll(Arrays.asList(additionalEntries));
		}
		completeClasspath.forEach(cp -> {
			if (builder.length() != 0) {
				builder.append(File.pathSeparator);
			}
			builder.append(cp);
		});
		return builder.toString();
	}

	public List<String> getClasspath() {
		return Collections.unmodifiableList(classpath);
	}

	public SootOptions addClasspath(final String classpath) {
		this.classpath.add(Objects.requireNonNull(classpath));
		return this;
	}

	public List<TransactionMethod> getTransactionMethods() {
		return Collections.unmodifiableList(transactionMethods);
	}

	public SootOptions addTransactionMethod(final TransactionMethod transactionMethod) {
		Objects.requireNonNull(transactionMethod);
		this.transactionMethods.add(transactionMethod);
		return this;
	}

	public List<TransactionMethod> getExcludedTransactionMethods() {
		return Collections.unmodifiableList(excludedTransactionMethods);
	}

	public SootOptions addExcludedTransactionMethod(final TransactionMethod excludedTransactionMethod) {
		this.excludedTransactionMethods.add(Objects.requireNonNull(excludedTransactionMethod));
		return this;
	}

	public boolean isSoundThrowAnalysisEnabled() {
		return soundThrowAnalysisEnabled;
	}

	public void setSoundThrowAnalysisEnabled(boolean soundThrowAnalysisEnabled) {
		this.soundThrowAnalysisEnabled = soundThrowAnalysisEnabled;
	}

	public boolean isClientLocalValuesEnabled() {
		return clientLocalValuesEnabled;
	}

	public void setClientLocalValuesEnabled(boolean clientLocalValuesEnabled) {
		this.clientLocalValuesEnabled = clientLocalValuesEnabled;
	}

	@Override
	public String toString() {
		return "SootOptions [classpath=" + classpath + ", transactionMethods=" + transactionMethods + ", excludedTransactionMethods="
				+ excludedTransactionMethods + ", soundThrowAnalysisEnabled=" + soundThrowAnalysisEnabled + ", clientLocalValuesEnabled="
				+ clientLocalValuesEnabled + "]";
	}

	public static class TransactionMethod {

		public final String className;
		public final String methodName;
		public final String[] parameterClassNames;

		public TransactionMethod(final String className, final String methodName, final String[] parameterClassNames) {
			Objects.requireNonNull(className);
			Objects.requireNonNull(methodName);
			this.className = className;
			this.methodName = methodName;
			this.parameterClassNames = parameterClassNames;
		}

		/**
		 * Parses a method of the form full.qualified.className#methodName or
		 * full.qualified.className#methodName(int,Object)
		 * 
		 * @param transactionMethod
		 * @return
		 */
		public static TransactionMethod parse(final String transactionMethod) {
			final Pattern methodMatcher = Pattern.compile("^([^#()]+)#([^#()]+)(\\(([^#()]*)\\))?$");
			final Matcher matcher = methodMatcher.matcher(transactionMethod);
			if (matcher.matches()) {
				if (matcher.group(3) == null) {
					return new SootOptions.TransactionMethod(matcher.group(1), matcher.group(2), null);
				} else {
					final String[] params = matcher.group(4).isEmpty() ? new String[0] : matcher.group(4).split(",");
					for (int i = 0; i < params.length; i++) {
						params[i] = params[i].trim();
					}
					return new SootOptions.TransactionMethod(matcher.group(1), matcher.group(2), params);
				}
			} else {
				throw new IllegalArgumentException(
						"Illegal method format " + transactionMethod + ". Use the format full.qualified.className#methodName");
			}
		}

		@Override
		public String toString() {
			return className + "#" + methodName
					+ (parameterClassNames != null ? "(" + String.join(", ", parameterClassNames) + ")" : "");
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((className == null) ? 0 : className.hashCode());
			result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
			result = prime * result + Arrays.hashCode(parameterClassNames);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TransactionMethod other = (TransactionMethod) obj;
			if (className == null) {
				if (other.className != null)
					return false;
			} else if (!className.equals(other.className))
				return false;
			if (methodName == null) {
				if (other.methodName != null)
					return false;
			} else if (!methodName.equals(other.methodName))
				return false;
			if (!Arrays.equals(parameterClassNames, other.parameterClassNames))
				return false;
			return true;
		}
	}
}
