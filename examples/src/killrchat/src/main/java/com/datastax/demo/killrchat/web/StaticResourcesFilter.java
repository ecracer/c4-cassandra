package com.datastax.demo.killrchat.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class StaticResourcesFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Nothing to initialize 
	}

	@Override
	public void destroy() {
		// Nothing to destroy
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String contextPath = ((HttpServletRequest) request).getContextPath();
		String requestURI = httpRequest.getRequestURI();
		requestURI = StringUtils.substringAfter(requestURI, contextPath);
		if (StringUtils.equals("/", requestURI)) {
			requestURI = "/index.html";
		}
		String newURI = "/web" + requestURI;
		request.getRequestDispatcher(newURI).forward(request, response);
	}
}
