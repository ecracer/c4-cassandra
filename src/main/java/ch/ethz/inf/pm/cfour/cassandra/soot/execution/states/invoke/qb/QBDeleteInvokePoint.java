package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke.qb;

import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_ALL;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_COLUMN;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_FROM_TABLE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_FROM_TABLE_KEYSPACE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_IF_EXISTS;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_LIST_ELT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_MAP_ELT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_ONLY_IF;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_ONLY_IF_AND_CLAUSE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_ONLY_IF_CLAUSE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_SET_ELT;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_WHERE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_WHERE_AND_CLAUSE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.DELETE_WHERE_CLAUSE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.QUERY_BUILDER_DELETE;
import static ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils.QUERY_BUILDER_DELETE_COLS;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.AbstractReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltDeleteStatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.refs.stmts.BuiltStatementClauseValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class QBDeleteInvokePoint extends AbstractQBInvokeProgramPoint {

	private QBDeleteInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId) {
		super(id, left, invokeStmt, methodId);
	}

	public static QBDeleteInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
                                             final int methodId) {
		return new QBDeleteInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new QBDeleteInvokePoint(id, left, invokeStmt, methodId);
	}

	@Override
	public boolean transformStatic(final VarLocal leftLocal, final List<VarLocalOrImmutableValue> args, final StaticInvokeExpr invokeExpr,
			final int methodId, final ExecutionState state, final ExecutionState oldState) {

		if (methodId == QUERY_BUILDER_DELETE) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> s(BuiltDeleteStatementValue.create(null)));
			return true;
		} else if (methodId == QUERY_BUILDER_DELETE_COLS) {
			transformQueryBuilderStaticInvoke(leftLocal, args, invokeStmt, state, oldState,
					targs -> s(BuiltDeleteStatementValue.create((StringValue[]) targs.get(0))));
			return true;
		}
		return false;
	}

	@Override
	public boolean transformInstance(final VarLocal leftLocal, final VarLocal baseLocal, final Set<AbstractReference> baseRefs,
			final List<VarLocalOrImmutableValue> args, final InstanceInvokeExpr invokeExpr, final int methodId,
			final ExecutionState state, final ExecutionState oldState) {

		if (methodId == DELETE_ALL) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == DELETE_COLUMN) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class, first -> val.addColumn(first)));
			return true;
		} else if (methodId == DELETE_LIST_ELT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.addCollectionElementColumn(first, (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == DELETE_MAP_ELT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.addCollectionElementColumn(first, (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == DELETE_SET_ELT) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.addCollectionElementColumn(first, (VarLocalOrImmutableValue) targs.get(1))));
			return true;
		} else if (methodId == DELETE_FROM_TABLE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), StringValue.class,
							first -> val.from(null, first)));
			return true;
		} else if (methodId == DELETE_FROM_TABLE_KEYSPACE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> this.<Value, StringValue> transformSetS(targs.get(0), StringValue.class,
							first -> transformSet(targs.get(1), StringValue.class, second -> val.from(first, second))));
			return true;
		} else if (methodId == DELETE_IF_EXISTS) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> s(val.setIfExists(true)));
			return true;
		} else if (methodId == DELETE_ONLY_IF) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == DELETE_ONLY_IF_CLAUSE || methodId == DELETE_ONLY_IF_AND_CLAUSE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementClauseValue.class, first -> val.addCondition(first)));
			return true;
		} else if (methodId == DELETE_WHERE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> s(val));
			return true;
		} else if (methodId == DELETE_WHERE_CLAUSE || methodId == DELETE_WHERE_AND_CLAUSE) {
			transformQueryBuilderInstanceInvoke(leftLocal, baseLocal, baseRefs, args, invokeStmt, state, oldState,
					BuiltDeleteStatementValue.class,
					(val, targs) -> transformSet(targs.get(0), BuiltStatementClauseValue.class, first -> val.addWhere(first)));
			return true;
		}
		return false;
	}
}
