

import com.datastax.driver.core.Session;

public class FieldEscape {
	
	private StringBuilder queryBuilder;
	private StringBuilder queryBuilder2;
	private String plainQuery;
	private String plainQuery2;
	private Object[] holder;
	
	public FieldEscape(){
		queryBuilder = new StringBuilder("SELECT * FROM test1");
		plainQuery = "SELECT * FROM test2";
		queryBuilder2 = new StringBuilder("SELECT * FROM test3");
		plainQuery2 = "SELECT * FROM test4";
		holder = new Object[2];
		holder[0] = queryBuilder;
		holder[1] = plainQuery;
	}
	
	public void escaper(final Object[] holder, final StringBuilder sb, final String str) {
		// do something
	}
	
	public void letEscape() {
		escaper(holder, queryBuilder2, plainQuery2);
	}
	
	public void test(Session session) {
		letEscape();
		test1(session);
		test2(session);
		test3(session);
		test4(session);
	}
	
	private void test1(Session session){
		session.execute(queryBuilder.toString());
	}
	private void test2(Session session){
		session.execute(queryBuilder2.toString());
	}
	private void test3(Session session){
		session.execute(plainQuery);
	}
	private void test4(Session session){
		session.execute(plainQuery2);
	}
}