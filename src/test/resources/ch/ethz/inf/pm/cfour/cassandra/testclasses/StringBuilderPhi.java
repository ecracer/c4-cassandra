

import com.datastax.driver.core.Session;

public class StringBuilderPhi {
	
	public void test(Session session, int i, int j) {
		final StringBuilder sb;
		if (i == 0){
			sb = new StringBuilder("SELECT * FROM test_table_1");
		} else {
			sb = new StringBuilder("SELECT * FROM test_table_2");
		}
		sb.append(" WHERE j = " + j);
		session.execute(sb.toString());
	}
}