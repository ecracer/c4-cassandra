

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Row;

public class ClientLocalValueQueryResult {

	private final Session session;
	
	public ClientLocalValueQueryResult(final Session session){
		this.session = session;
	}
	
	@Transaction
	public void addTrack(String username, String track){
		ClientLocalValues.set("username", username);
		final Row row = session.execute("SELECT * FROM users WHERE username = ?", username).one();
		final String usernameFromDb = row.getString("username");
		session.execute("UPDATE users SET tracks = tracks + {?} WHERE username = ?", track, usernameFromDb);
	}
	
	@Transaction
	public void addTrack2(String username, String track){
		ClientLocalValues.set("username", username);
		final Row row = session.execute("SELECT username, tracks FROM users2 WHERE username = ?", username).one();
		final String usernameFromDb = row.getString(0);
		session.execute("UPDATE users2 SET tracks = tracks + {?} WHERE username = ?", track, usernameFromDb);
	}
	
	@Transaction
	public void addTrack3(String username, String track){
		ClientLocalValues.set("username", username);
		final Row row = session.execute("SELECT * FROM users3 WHERE username = ?", username).one();
		final String usernameFromDb = row.getString(0);
		session.execute("UPDATE users3 SET tracks = tracks + {?} WHERE username = ?", track, usernameFromDb);
	}
}
