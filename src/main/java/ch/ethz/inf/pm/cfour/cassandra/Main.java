package ch.ethz.inf.pm.cfour.cassandra;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.inf.pm.cfour.cassandra.analysis.SerializabilityAnalysis;

/**
 * Checks a program for serializability violations
 */
public class Main {

	private static final Logger LOG = LogManager.getLogger(Main.class);

	public static void main(String[] args) {
		try {
			final Options options = new Options();
			String currentArg = "";
			for (int i = 0; i < args.length; i++) {
				switch (args[i]) {
					case "-cp" :
						currentArg = "cp";
						break;
					case "-tm" :
						currentArg = "tm";
						break;
					case "-excludedTm" :
						currentArg = "excludedTm";
						break;
					case "-outputDir" :
						currentArg = "outputDir";
						break;
					case "-schemaFile" :
						currentArg = "schemaFile";
						break;
					case "-violationClassificationFile" :
						currentArg = "violationClassificationFile";
						break;
					case "-enableSoundThrowAnalysis" :
						options.setSoundThrowAnalysisEnabled(true);
						break;
					case "-disableBasicAnalysis" :
						options.setBasicAnalysisEnabled(false);
						break;
					case "-disableCFour" :
						options.setCFourEnabled(false);
						break;
					case "-encodeBatchStatementPermutations" :
						options.setEncodeBatchStatementPermutations(true);
						break;
					case "-disableValueAnalysis" :
						options.setValueAnalysisEnabled(false);
						break;
					case "-disablePruningDisplayCode" :
						options.setPruneDisplayCodeEnabled(false);
						break;
					case "-disableStrictUpdates" :
						options.setStrictUpdatesEnabled(false);
						break;
					case "-treatAllUpdatesAsStrict" :
						options.setTreatAllUpdatesAsStrict(true);
						break;
					case "-disableClientLocals" :
						options.setClientLocalValuesEnabled(false);
						break;
					case "-clientLocalsAreGlobalUnique" :
						options.setClientLocalAreGlobalUnique(true);
						break;
					case "-enableSideChannels" :
						options.setSideChannelsEnabled(true);
						break;
					case "-disablePOConstraints" :
						options.setPoConstraintsEnabled(false);
						break;
					case "-enableResultGeneralizationCheck" :
						options.setResultGeneralizationCheckEnabled(true);
						break;
					case "-enableStaticSubsetMinimality" :
						options.setStaticSubsetMinimalityEnabled(true);
						break;
					case "-disableGlobalAndEventConstraints" :
						options.setGlobalAndEventConstraintsEnabled(false);
						break;
					case "-disableAbsorption" :
						options.setAbsorptionEnabled(false);
						break;
					case "-disableSinglePOPath" :
						options.setSinglePOPathEnabled(false);
						break;
					case "-disableCommutativity" :
						options.setCommutativityEnabled(false);
						break;
					case "-disableAsymmetricCommutativity" :
						options.setAsymmetricCommutativityEnabled(false);
						break;
					case "-disableSynchronizingOperations" :
						options.setSynchronizingOperationsEnabled(false);
						break;
					case "-disableLegalitySpec" :
						options.setLegalitySpecEnabled(false);
						break;
					default :
						switch (currentArg) {
							case "cp" :
								options.addClasspath(args[i]);
								break;
							case "tm" :
								options.addTransactionMethod(SootOptions.TransactionMethod.parse(args[i]));
								break;
							case "excludedTm" :
								options.addExcludedTransactionMethod(SootOptions.TransactionMethod.parse(args[i]));
								break;
							case "outputDir" :
								options.setGraphOutputFolder(args[i]);
								currentArg = "";
								break;
							case "schemaFile" :
								options.setCreateSchemaFile(args[i]);
								currentArg = "";
								break;
							case "violationClassificationFile" :
								options.setViolationClassificationFile(args[i]);
								currentArg = "";
								break;
							default :
								LOG.warn("Ignoring " + args[i] + " as it is not prepended with an option marker.");
								break;
						}
				}
			}

			LOG.info("The following options are set: " + options);
			new SerializabilityAnalysis(options).run();
		} catch (Exception e) {
			LOG.error("Error", e);
		}
	}
}
