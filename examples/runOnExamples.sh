#!/usr/bin/env bash

# Runs the analysis on the projects specified below
# Output is stored in the output folder

# Build the project using maven
mvn -f ../pom.xml -DskipTests clean package

# Projects for which the analysis should run
# Format: projectId;projectName;additionalArguments
projects[0]='cassandra-lock'
projects[1]='cassandra-twitter'
projects[2]='cassatwitter'
projects[3]='cassieq-core;cassieq-core;-excludedTm io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#ackMessage io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#createQueue io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#deleteQueue io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getAccountQueues io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getMessage io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getQueueDefinition io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#getQueueStatistics io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#putMessage io.paradoxical.cassieq.discoverable.resources.api.v1.QueueResource#updateMessage io.paradoxical.cassieq.workers.QueueDeleter#delete'
projects[4]='currency-exchange'
projects[5]='datastax-queueing'
projects[6]='killrchat'
projects[7]='playlist'
projects[8]='roomstore'
projects[9]='shopping-cart'
projects[10]='simple-twitter'
projects[11]='twissandra'


# Configurations which should be executed for each project
# Format: suffix;options
# Default configuration
configs[0]='-filtered'
# include display code
configs[1]='-unfiltered;-disablePruningDisplayCode'

mkdir -p output

for project in "${projects[@]}"
do
  projectParts=(${project//;/ })
  projectId=${projectParts[0]}
  projectName=${projectParts[1]-${projectParts[0]}}
  additionalArguments=${projectParts[@]:2}
  
  echo Processing $projectId...
  echo =================================
  
  echo $additionalArguments
  
  for config in "${configs[@]}"
  do
    configParts=(${config//;/ })
    configSuffix=${configParts[0]}
    configArgs=${configParts[@]:1}
    
    rm -rf output/$projectName$configSuffix
    java -jar ../target/cfour-cassandra-0.0.1-SNAPSHOT-jar-with-dependencies.jar -cp jars/$projectId.jar -schemaFile schema/$projectId-schema.cql -violationClassificationFile classifications/$projectId-violations.txt -outputDir output/$projectName$configSuffix $configArgs $additionalArguments -disableCFour
  done
done
