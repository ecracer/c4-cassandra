package ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts;

import java.util.Iterator;
import java.util.Objects;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;

/**
 * Holder for updates and constraints, where it may not clear whether a column is updated or used to constrain the row
 */
public class UpsertComponents {

	private final ImmutableList<UpsertComponent> upsertComponents;
	private final ImmutableList<ColumnUpdate> columnUpdates;
	private final Constraints constraints;

	private UpsertComponents(final ImmutableList<UpsertComponent> upsertComponents, final ImmutableList<ColumnUpdate> columnUpdates,
			final Constraints constraints) {
		if (upsertComponents == null || upsertComponents.isEmpty()) {
			throw new IllegalArgumentException();
		}
		this.upsertComponents = upsertComponents;
		this.columnUpdates = Objects.requireNonNull(columnUpdates);
		this.constraints = Objects.requireNonNull(constraints);
	}

	public static Builder builder() {
		return new Builder();
	}

	public ImmutableListMultimap<String, Integer> getPossiblyUpdatedColumnIdxs() {
		final ImmutableListMultimap.Builder<String, Integer> possiblyUpdatedColumnIdx = ImmutableListMultimap.builder();
		for (int i = 0; i < upsertComponents.size(); i++) {
			final UpsertComponent component = upsertComponents.get(i);
			if (component.columnUpdateIdx != null) {
				possiblyUpdatedColumnIdx.put(component.column, i);
			}
		}
		return possiblyUpdatedColumnIdx.build();
	}

	public ColumnUpdate getColumnUpdate(final int idx) {
		return columnUpdates.get(upsertComponents.get(idx).columnUpdateIdx);
	}

	public ImmutableListMultimap<String, Integer> getPossibleConstraintIdxs() {
		final ImmutableListMultimap.Builder<String, Integer> possibleConstraintIdxs = ImmutableListMultimap.builder();
		for (int i = 0; i < upsertComponents.size(); i++) {
			final UpsertComponent component = upsertComponents.get(i);
			if (component.constraintIdx != null) {
				possibleConstraintIdxs.put(component.column, i);
			}
		}
		return possibleConstraintIdxs.build();
	}

	public ImmutableListMultimap<String, Integer> getDefiniteConstraintIdxs() {
		final ImmutableListMultimap.Builder<String, Integer> definiteConstraintIdxs = ImmutableListMultimap.builder();
		for (int i = 0; i < upsertComponents.size(); i++) {
			final UpsertComponent component = upsertComponents.get(i);
			if (component.constraintIdx != null && component.columnUpdateIdx == null) {
				definiteConstraintIdxs.put(component.column, i);
			}
		}
		return definiteConstraintIdxs.build();
	}

	public Constraint getConstraint(int idx) {
		return constraints.getConstraint(upsertComponents.get(idx).constraintIdx);
	}

	public String getLabel() {
		final StringBuilder sb = new StringBuilder();
		final Iterator<UpsertComponent> compIt = upsertComponents.iterator();
		sb.append(getLabel(compIt.next()));
		while (compIt.hasNext()) {
			sb.append("_AND_").append(getLabel(compIt.next()));
		}
		return sb.toString();
	}

	private String getLabel(final UpsertComponent component) {
		if (component.columnUpdateIdx == null) {
			return constraints.getConstraint(component.constraintIdx).getLabel();
		} else if (component.constraintIdx == null) {
			return columnUpdates.get(component.columnUpdateIdx).getLabel();
		} else {
			return component.column + "_constrain_or_update";
		}
	}

	public int getComponentSize() {
		return upsertComponents.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columnUpdates == null) ? 0 : columnUpdates.hashCode());
		result = prime * result + ((constraints == null) ? 0 : constraints.hashCode());
		result = prime * result + ((upsertComponents == null) ? 0 : upsertComponents.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpsertComponents other = (UpsertComponents) obj;
		if (columnUpdates == null) {
			if (other.columnUpdates != null)
				return false;
		} else if (!columnUpdates.equals(other.columnUpdates))
			return false;
		if (constraints == null) {
			if (other.constraints != null)
				return false;
		} else if (!constraints.equals(other.constraints))
			return false;
		if (upsertComponents == null) {
			if (other.upsertComponents != null)
				return false;
		} else if (!upsertComponents.equals(other.upsertComponents))
			return false;
		return true;
	}

	private static class UpsertComponent {
		public final String column;
		public final Integer columnUpdateIdx;
		public final Integer constraintIdx;

		public UpsertComponent(final String column, final Integer columnUpdateIdx, final Integer constraintIdx) {
			this.column = Objects.requireNonNull(column);
			this.columnUpdateIdx = columnUpdateIdx;
			this.constraintIdx = constraintIdx;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((column == null) ? 0 : column.hashCode());
			result = prime * result + ((columnUpdateIdx == null) ? 0 : columnUpdateIdx.hashCode());
			result = prime * result + ((constraintIdx == null) ? 0 : constraintIdx.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UpsertComponent other = (UpsertComponent) obj;
			if (column == null) {
				if (other.column != null)
					return false;
			} else if (!column.equals(other.column))
				return false;
			if (columnUpdateIdx == null) {
				if (other.columnUpdateIdx != null)
					return false;
			} else if (!columnUpdateIdx.equals(other.columnUpdateIdx))
				return false;
			if (constraintIdx == null) {
				if (other.constraintIdx != null)
					return false;
			} else if (!constraintIdx.equals(other.constraintIdx))
				return false;
			return true;
		}
	}

	public static class Builder {

		private final ImmutableList.Builder<UpsertComponent> upsertComponents = ImmutableList.builder();
		private final ImmutableList.Builder<ColumnUpdate> columnUpdates = ImmutableList.builder();
		private final Constraints.Builder constraints = Constraints.builder();
		private int nextUpsertComponentIdx = 0;
		private int nextColumnUpdateIdx = 0;

		private Builder() {
		}

		public int addColumnUpdate(final ColumnUpdate columnUpdate) {
			columnUpdates.add(columnUpdate);
			upsertComponents.add(new UpsertComponent(columnUpdate.column, nextColumnUpdateIdx++, null));
			return nextUpsertComponentIdx++;
		}

		public int addConstraint(final Constraint constraint) {
			int constraintIdx = constraints.add(constraint);
			upsertComponents.add(new UpsertComponent(constraint.column, null, constraintIdx));
			return nextUpsertComponentIdx++;
		}

		public int addColumnUpdateOrConstraint(final ColumnUpdate columnUpdate, final Constraint constraint) {
			columnUpdates.add(columnUpdate);
			int constraintIdx = constraints.add(constraint);
			upsertComponents.add(new UpsertComponent(columnUpdate.column, nextColumnUpdateIdx++, constraintIdx));
			return nextUpsertComponentIdx++;
		}

		public UpsertComponents build() {
			return new UpsertComponents(upsertComponents.build(), columnUpdates.build(), constraints.build());
		}
	}
}
