package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ResultSetValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;

public interface ConcreteValueVisitor<R> {

	R visit(final ConcreteValue value);

	R visitStringBuilderValue(final StringBuilderValue stringBuilderValue);

	R visitStatementValue(final StatementValue statementValue);

	R visitResultSetValue(final ResultSetValue resultSetValue);

	R visitUnknownMutableValue(final UnknownMutableValue unknownValue);

	R visitImmutableValue(final ImmutableValue immutableValue);
}
