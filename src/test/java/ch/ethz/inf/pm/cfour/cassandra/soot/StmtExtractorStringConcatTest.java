package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorStringConcatTest extends AbstractSootTest {

	public StmtExtractorStringConcatTest() throws IOException {
		super("StringConcat", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());
		final SessionExecuteInvoke nextCall = result.iterator().next();
		final Set<String> cqls = getCQLs(nextCall);
		assertEquals(1, cqls.size());
		assertEquals("SELECT * FROM test_table WHERE k = ? AND i = ? AND k = 3", cqls.iterator().next());
	}
}
