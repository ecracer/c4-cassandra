import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Feli
 */
public class Query {

    private Cluster cluster;
    private Session session;

    public Query(String IP, String Keyspace) {
        cluster = Cluster.builder().addContactPoint(IP).build();
        session = cluster.connect(Keyspace);
    }

    public Query() {
        cluster = Cluster.builder().addContactPoint("167.205.35.19").build();
        session = cluster.connect("cassatweetclp");
    }

    public Session getSession() {
        return session;
    }
}
