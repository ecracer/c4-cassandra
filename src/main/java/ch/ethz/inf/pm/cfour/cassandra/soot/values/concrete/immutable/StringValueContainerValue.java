package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ImmutableValueVisitor;

/**
 * Holder of a value. Captures .toString() method.
 */
public class StringValueContainerValue extends AbstractImmutableValue {

	public final ConcreteValue value;

	private StringValueContainerValue(final ConcreteValue value) {
		Objects.requireNonNull(value);
		this.value = value;
	}

	public static ImmutableValue create(final ConcreteValue value) {
		if (value instanceof StringValue) {
			return (StringValue) value;
		} else if (value instanceof StringBuilderValue) {
			final StringBuilderValue newStringBuilder = (StringBuilderValue) value;
			if (newStringBuilder.values.size() == 1 && newStringBuilder.values.get(0) instanceof StringValue) {
				return (StringValue) newStringBuilder.values.get(0);
			}
		}
		return new StringValueContainerValue(value);
	}

	@Override
	public <R> R apply(ImmutableValueVisitor<R> visitor) {
		return visitor.visitStringValueContainer(this);
	}

	@Override
	public StringValueContainerValue clone() {
		return new StringValueContainerValue(this.value);
	}

	@Override
	public ImmutableValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (newValue instanceof StringValueContainerValue) {
			return StringValueContainerValue.create(value.widenWith(((StringValueContainerValue) newValue).value, updatedFrom));
		} else {
			return UnknownImmutableValue.create(updatedFrom);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringValueContainerValue other = (StringValueContainerValue) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StringValueContainer [value=" + value + "]";
	}
}
