package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Collections;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnSelection;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.ColumnUpdate.ColumnSetUpdate.SetOp;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraint.EQConstraint;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.Constraints;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart.QueryPart;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart.SelectColsPart;
import ch.ethz.inf.pm.cfour.cassandra.beans.parsedstmts.StatementPart.UpsertPart;

public class CommutativityAbsorptionExprGeneratorTest {

	@Test
	public void testStrictAndDisplay() {
		final CommutativityAbsorptionExprGenerator generator = CommutativityAbsorptionExprGenerator
				.create(SchemaInformation.builder().addPrimaryKeyColumn("users", "login")
						.addNonPrimaryKeyColumn("users", "chat_rooms")
						.addNonPrimaryKeyColumn("users", "pass").build(), new Options());

		final UpsertPart upsert = UpsertPart
				.createUpdate("users", Collections.singletonList(ColumnSetUpdate.create("chat_rooms", SetOp.ADD)),
						Collections.singletonList(EQConstraint.create("login")), false, false)
				.setCanInsert(false);
		final QueryPart query = SelectColsPart
				.create("users", ImmutableList.of(ColumnSelection.create("login"), ColumnSelection.create("chat_rooms")),
						Constraints.create(EQConstraint.create("login")))
				.setDisplayColumns(Collections.singleton("chat_rooms"));

		assertEquals(generator.getCommutativityExpr(upsert, query), CFourFactory.TRUE);
		assertNotEquals(generator.getCommutativityExpr(upsert.setCanInsert(true), query), CFourFactory.TRUE);
		assertNotEquals(generator.getCommutativityExpr(upsert, query.setDisplayColumns(Collections.emptySet())), CFourFactory.TRUE);
	}
}
