package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;

public class StmtExtractorDoWhileLoopTest extends AbstractSootTest {

	public StmtExtractorDoWhileLoopTest() throws IOException {
		super("DoWhileLoop", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(3, result.size());

		int i0Count = 0;
		int widenedUnknownCount = 0;
		int choiceValueCount = 0;
		Set<ImmutableChoiceValue> distinctValues = new HashSet<>();

		for (final SessionExecuteInvoke invoke : result) {
			for (final StatementValue nextStatement : invoke.statementArg) {
				if (nextStatement.query.equals("SELECT * FROM test_table WHERE i = 0")) {
					i0Count++;
				} else {
					assertEquals("SELECT * FROM test_table WHERE i = ?", nextStatement.getQueryWithUnnamedBindMarkers());
					assertEquals(1, nextStatement.binds.size());
					final ConcreteValue nextBindVal = nextStatement.binds.iterator().next().value;
					if (nextBindVal instanceof ImmutableChoiceValue) {
						choiceValueCount++;
						distinctValues.add((ImmutableChoiceValue) nextBindVal);
					} else if (nextBindVal instanceof UnknownImmutableValue && ((UnknownImmutableValue) nextBindVal).isWidened) {
						widenedUnknownCount++;
					} else {
						assertEquals(1, 0);
					}
				}
			}
		}

		assertEquals(0, i0Count);
		assertEquals(3, widenedUnknownCount);
		assertEquals(0, choiceValueCount);
		assertEquals(0, distinctValues.size());
	}
}