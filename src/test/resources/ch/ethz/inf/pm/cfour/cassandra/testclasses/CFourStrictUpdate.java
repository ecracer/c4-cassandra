

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import com.datastax.driver.core.Session;


public class CFourStrictUpdate {

	private final Session session;
	
	public CFourStrictUpdate(final Session session){
		this.session = session;
	}
	
	@Transaction
	// from KillrChat (ChatRoomResource#addUserToRoom)
	public void addUserToRoom(String username, String roomname){
		ClientLocalValues.set("username", username);
        session.execute("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms + {?} WHERE login = ?", roomname, username);
	}
	
	@Transaction
	// from KillrChat (ChatRoomResource#removeUserFromRoom)
	public void removeUserFromRoom(String username, String roomname){
		ClientLocalValues.set("username", username);
        session.execute("UPDATE /*!STRICT!*/ users SET chat_rooms = chat_rooms - {?} WHERE login = ?", roomname, username);
	}
	
	@Transaction
	// from KillrChat (UserResource#findByLogin)
	public void getUser(String username){
		ClientLocalValues.set("username", username);
		session.execute("SELECT username FROM users WHERE login = ?", username);
	}
}
