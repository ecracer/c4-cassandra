package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.Value;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors.ConcreteValueVisitor;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;

public class ResultSetValue implements ConcreteValue {

	public final SessionExecuteInvoke resultFrom;
	public final boolean isOnFirstRow;

	private ResultSetValue(final SessionExecuteInvoke resultFrom, final boolean isBeforeFirstRow) {
		this.resultFrom = Objects.requireNonNull(resultFrom);
		this.isOnFirstRow = isBeforeFirstRow;
	}

	public static ConcreteValue create(final SessionExecuteInvoke resultFrom) {
		return new ResultSetValue(resultFrom, true);
	}

	public ResultSetValue iteratorNext() {
		return new ResultSetValue(resultFrom, false);
	}

	@Override
	public <R> R apply(ConcreteValueVisitor<R> visitor) {
		return visitor.visitResultSetValue(this);
	}

	@Override
	public ConcreteValue widenWith(Value newValue, ProgramPointId updatedFrom) {
		if (newValue instanceof ResultSetValue) {
			return (ResultSetValue) newValue;
		} else {
			return UnknownMutableValue.create();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isOnFirstRow ? 1231 : 1237);
		result = prime * result + ((resultFrom == null) ? 0 : resultFrom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultSetValue other = (ResultSetValue) obj;
		if (isOnFirstRow != other.isOnFirstRow)
			return false;
		if (resultFrom == null) {
			if (other.resultFrom != null)
				return false;
		} else if (!resultFrom.equals(other.resultFrom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultSetValue [resultFrom=" + resultFrom + ", isBeforeFirstRow=" + isOnFirstRow + "]";
	}
}
