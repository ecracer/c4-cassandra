package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsWithBottomTest extends AbstractSootTest {

	public StmtExtractorFieldsWithBottomTest() throws IOException {
		super("FieldsWithBottom", "test1", "test2");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());

		for (final SessionExecuteInvoke nextCall : result) {
			final Set<String> cqls = getCQLs(nextCall);
			switch (nextCall.executedFrom.callStack.method.methodName) {
				case "test1" :
					assertEquals(2, cqls.size());
					assertTrue(cqls.contains("SELECT val1 FROM table1"));
					assertTrue(cqls.contains("SELECT val2 FROM table1"));
					break;
				case "test2" :
					assertEquals(1, cqls.size());
					assertTrue(cqls.contains("SELECT val1 FROM table2"));
					break;
				default :
					assertTrue(false);
			}
		}
	}
}
