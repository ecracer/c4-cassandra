package com.yan2.currencyexchange.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.yan2.currencyexchange.dao.TradeDAO;
import com.yan2.currencyexchange.model.Trade;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;

/**
 * The implemetation of the TradeService class.
 */
@Service
public class TradeServiceImpl implements TradeService {

    @Inject
    TradeDAO tradeDAO;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transaction
    public boolean saveTrade(Trade trade0) {

        final Trade trade = new Trade();
        trade.setId(UUID.randomUUID());
        trade.setCurrencyFrom(trade0.getCurrencyFrom());
        trade.setCurrencyTo(trade0.getCurrencyTo());
        trade.setAmountSell(trade0.getAmountSell());
        trade.setAmountBuy(trade0.getAmountBuy());
        trade.setRate(trade0.getRate());
        trade.setTimePlaced(trade0.getTimePlaced());
        trade.setOriginatingCountry(trade0.getOriginatingCountry());

        tradeDAO.saveTrade(trade);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transaction(onlyForDisplaying = true)
    public List<Trade> getListTradesFromDate(Date fromTimePlaced) {
        return tradeDAO.getListTradesFromDate(new java.sql.Date(fromTimePlaced.getTime()));
    }

}
