package ch.ethz.inf.pm.cfour.cassandra.soot.reference;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.reference.visitor.ReferenceVisitor;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;

/**
 * A reference is an abstraction of one memory location, i.e. despite that multiple values can be assigned to a
 * reference, only one of these values can exist at runtime.
 */
public abstract class AbstractReference {

	public final ProgramPointId createdIn;

	protected AbstractReference(final ProgramPointId createdIn) {
		this.createdIn = Objects.requireNonNull(createdIn);
	}

	public abstract <R> R apply(final ReferenceVisitor<R> visitor);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdIn == null) ? 0 : createdIn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractReference other = (AbstractReference) obj;
		if (createdIn == null) {
			if (other.createdIn != null)
				return false;
		} else if (!createdIn.equals(other.createdIn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Ref " + createdIn.callStack + "@" + Integer.toHexString(createdIn.hashCode());
	}
}
