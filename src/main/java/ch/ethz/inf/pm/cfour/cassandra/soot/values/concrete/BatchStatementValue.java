package ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete;

import java.util.List;
import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;
import com.google.common.collect.ImmutableList;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.AbstractBind.UserBind;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.IntValue;

public class BatchStatementValue extends StatementValue {

	public ImmutableList<StatementValue> statements;

	private BatchStatementValue(final ImmutableList<StatementValue> statements, final ImmutableValue consistencyLevel,
			final ImmutableValue serialConsistencyLevel) {

		super(getQuery(statements), getBinds(statements), consistencyLevel, serialConsistencyLevel);
		this.statements = Objects.requireNonNull(statements);
	}

	public static BatchStatementValue create() {
		return new BatchStatementValue(ImmutableList.of(), NullValue.create(), NullValue.create());
	}

	private static String getQuery(final List<StatementValue> statements) {
		final StringBuilder sb = new StringBuilder();
		sb.append("BEGIN LOGGED BATCH");
		for (final StatementValue statement : statements) {
			sb.append(" ");
			sb.append(statement.query);
			if (!statement.query.endsWith(";")) {
				sb.append(";");
			}
		}
		sb.append(" APPLY BATCH;");
		return sb.toString();
	}

	private static ImmutableList<AbstractBind<ConcreteValue>> getBinds(final List<StatementValue> statements) {
		final ImmutableList.Builder<AbstractBind<ConcreteValue>> binds = ImmutableList.builder();
		int offset = 0;
		int maxIdx = 0;
		for (final StatementValue statement : statements) {
			for (final AbstractBind<ConcreteValue> newStmtBind : statement.binds) {
				if (newStmtBind instanceof UserBind && ((UserBind<ConcreteValue>) newStmtBind).idx instanceof IntValue) {
					int idx = offset + ((IntValue) ((UserBind<ConcreteValue>) newStmtBind).idx).number;
					maxIdx = Math.max(maxIdx, idx);
					binds.add(UserBind.create(IntValue.create(idx), newStmtBind.value));
				} else {
					return ImmutableList.of();
				}
			}
			offset = maxIdx;
		}
		return binds.build();
	}

	public BatchStatementValue addStatement(final StatementValue statement) {
		final ImmutableList.Builder<StatementValue> builder = ImmutableList.builder();
		builder.addAll(statements);
		builder.add(statement);
		return new BatchStatementValue(builder.build(), consistencyLevel, serialConsistencyLevel);
	}

	@Override
	protected BatchStatementValue createNewInternal(final ImmutableList<AbstractBind<ConcreteValue>> binds, ImmutableValue consistencyLevel,
			ImmutableValue serialConsistencyLevel) {
		return new BatchStatementValue(statements, consistencyLevel, serialConsistencyLevel);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((statements == null) ? 0 : statements.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BatchStatementValue other = (BatchStatementValue) obj;
		if (statements == null) {
			if (other.statements != null)
				return false;
		} else if (!statements.equals(other.statements))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BatchStatementValue [statements=" + statements + "]";
	}
}
