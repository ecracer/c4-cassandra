import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.utils.UUIDs;

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author feli
 */
public class UserLine {

    private String username;
    private Query query;
    private ArrayList<Tweet> Tweets;

    public UserLine(String username) {
        this.username = username;
        query = new Query();
        Tweets = new ArrayList<Tweet>();
    }

    public void loadUserline(int limit) {
    	final String username = this.username;
    	ClientLocalValues.set("username", username);
        ResultSet set = query.getSession().execute(QueryBuilder.select().all()
                .from("userline")
                .where(QueryBuilder.eq("username", username))
                .limit(limit)
        );

        List<Row> row = set.all();
        Tweets = new ArrayList<Tweet>();

        for (Row tuple : row) {
            Tweet twit = new Tweet();
            twit.setUsername(username);
            twit.setUuids(tuple.getUUID("tweet_id"));
            twit.Load();
            Tweets.add(twit);
        }
    }

    public String getUserline() {
        String retString = "";
        for (Tweet twit : Tweets) {
            retString += twit.getTweetString();
        }
        return retString;
    }

    public void putToUserline(UUID tweet_id) {
    	final String username = this.username;
    	ClientLocalValues.set("username", username);
    	
        // adds to own timeline
        query.getSession().execute(QueryBuilder.insertInto("userline")
                .value("tweet_id", tweet_id)
                .value("username", username)
                .value("time", UUIDs.timeBased())
        );
    }
}
