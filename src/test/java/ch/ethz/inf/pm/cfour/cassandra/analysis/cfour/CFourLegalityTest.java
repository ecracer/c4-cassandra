package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import ch.ethz.inf.pm.cfour.cassandra.analysis.SchemaInformation;
import com.google.common.collect.ImmutableSet;

public class CFourLegalityTest extends AbstractCFourTest {

	public CFourLegalityTest() throws IOException {
		super("CFourLegalitySpec");
	}

	@Override
	protected SchemaInformation buildSchemaInformation() {
		return SchemaInformation.builder().addPrimaryKeyColumn("users", "username").addNonPrimaryKeyColumn("users", "playlist_names")
				.addTableWithoutDelete("users").addColumnsWritten("users", ImmutableSet.of("username", "playlist_names")).build();
	}

	@Override
	protected void fillOptionsList(List<Options> optionsList) {
		Options options = getInitialOptions();
		options.setLegalitySpecEnabled(true);
		optionsList.add(options);
		options = getInitialOptions();
		options.setLegalitySpecEnabled(false);
		optionsList.add(options);
	}

	@Override
	protected void checkResult(Options options, CFourResult result) {
		if (options.isLegalitySpecEnabled()) {
			assertEquals(0, result.getTotalVerifiedViolationsSize4());
		} else {
			assertEquals(3, result.getTotalVerifiedViolationsSize4());
		}
	}
}
