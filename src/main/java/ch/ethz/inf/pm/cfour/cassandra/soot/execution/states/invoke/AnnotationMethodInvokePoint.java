package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.invoke;

import java.util.List;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootMethodUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions;
import ch.ethz.inf.pm.cfour.cassandra.soot.VarLocalOrImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.CallStack;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ExecutionState;
import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.reference.ImmutableReference;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableChoiceValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.UnknownImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ClientImmutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.StringValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;

public class AnnotationMethodInvokePoint extends AbstractInvokeProgramPoint {

	private final SootOptions options;

	private AnnotationMethodInvokePoint(final ProgramPointId id, final Local left, final Stmt invokeStmt, final int methodId,
                                        final SootOptions options) {

		super(id, left, invokeStmt, methodId);
		this.options = options;
	}

	public static AnnotationMethodInvokePoint create(final CallStack callStack, final Local left, final Stmt invokeStmt,
                                                     final int methodId, final SootOptions options) {
		return new AnnotationMethodInvokePoint(ProgramPointId.create(callStack, invokeStmt), left, invokeStmt, methodId, options);
	}

	@Override
	public boolean transformStatic(VarLocal leftLocal, List<VarLocalOrImmutableValue> args, StaticInvokeExpr invokeExpr, int methodId,
                                   ExecutionState state, ExecutionState oldState) {

		if (methodId == SootMethodUtils.AN_CLIENT_LOCAL_SET) {
			if (options.isClientLocalValuesEnabled()) {
				if (args.get(1) instanceof VarLocal) {
					setClientImmutableValue((VarLocal) args.get(1), args.get(0), state, oldState);
				}
			}
			return true;
		} else if (methodId == SootMethodUtils.AN_CLIENT_LOCAL_GET) {
			if (leftLocal != null) {
				if (options.isClientLocalValuesEnabled()) {
					setClientImmutableValue(leftLocal, args.get(0), state, oldState);
				} else {
					state.setUnknown(leftLocal, id);
				}
			}
			return true;
		}
		return false;
	}

	private void setClientImmutableValue(final VarLocal left, final VarLocalOrImmutableValue varId, ExecutionState state,
			ExecutionState oldState) {
		final Set<ImmutableValue> vals = resolveConcreteAndTransform(varId, oldState, localid -> {
			if (localid instanceof StringValue) {
				return s(ClientImmutableValue.create(((StringValue) localid).string));
			} else {
				return s(UnknownImmutableValue.create(id));
			}
		});
		state.setImmutableValue(left, ImmutableReference.create(id), ImmutableChoiceValue.create(id, vals), id,
				oldState);
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new AnnotationMethodInvokePoint(id, left, invokeStmt, methodId, options);
	}
}
