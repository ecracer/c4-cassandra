#!/usr/bin/bash

rm -rf */target
rm -rf */*/target

echo ====================================== cassandra-lock ==================================== && cd cassandra-lock
mvn -Dmaven.test.skip=true package
cp target/cassandra-lock-0.0.1-SNAPSHOT.jar ../../jars/cassandra-lock.jar
cd ..

echo ====================================== cassandra-twitter ==================================== && cd cassandra-twitter
mvn -Dmaven.test.skip=true package
cp target/cassandra-twitter-1.0.jar ../../jars/cassandra-twitter.jar
cd ..

echo ====================================== cassatwitter ==================================== && cd cassatwitter
mvn -Dmaven.test.skip=true package
cp target/cassatwitter-0.0.1-SNAPSHOT.jar ../../jars/cassatwitter.jar
cd ..

# CASSIEQ
echo ====================================== cassieq/core ==================================== && cd cassieq/core
mvn -Dmaven.test.skip=true -Ddocker.skipBuild=true package
cp target/cassieq-core-0.12-SNAPSHOT-r*_dev.jar ../../../jars/cassieq-core.jar
cd ../..

echo ====================================== currency-exchange ==================================== && cd currency-exchange
mvn -Dmaven.test.skip=true package
cp target/CurrencyExchange-0.0.1-SNAPSHOT.jar ../../jars/currency-exchange.jar
cd ..

echo ====================================== datastax-queueing ==================================== && cd datastax-queueing
mvn -Dmaven.test.skip=true package
cp target/datastax-queueing-demo-0.1-SNAPSHOT.jar ../../jars/datastax-queueing.jar
cd ..

echo ====================================== killrchat ==================================== && cd killrchat
mvn -Dmaven.test.skip=true package
cp target/killrchat-1.0.jar ../../jars/killrchat.jar
cd ..

echo ====================================== playlist ==================================== && cd playlist
mvn -Dmaven.test.skip=true package
cp target/playlist-1.0-SNAPSHOT.jar ../../jars/playlist.jar
cd ..

echo ====================================== roomstore ==================================== && cd roomstore
mvn -Dmaven.test.skip=true package
cp target/roomstore-0.0.1-SNAPSHOT.jar ../../jars/roomstore.jar
cd ..

echo ====================================== shopping-cart ==================================== && cd shopping-cart
mvn -Dmaven.test.skip=true package
cp target/shopping-cart-angular-cassandra-1.0.jar ../../jars/shopping-cart.jar
cd ..

echo ====================================== simple-twitter ==================================== && cd simple-twitter
mvn -Dmaven.test.skip=true package
cp target/simpletwitter-1.0.jar ../../jars/simple-twitter.jar
cd ..

echo ====================================== twissandra ==================================== && cd twissandra
mvn -Dmaven.test.skip=true package
cp target/twissandra.jar ../../jars/twissandra.jar
cd ..
