package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorBoundStatementSimpleTest extends AbstractSootTest {

	public StmtExtractorBoundStatementSimpleTest() throws IOException {
		super("BoundStatementSimple", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(1, cqls.size());
		assertEquals(4, result.iterator().next().statementArg.iterator().next().binds.size());
		assertEquals("INSERT INTO test_table (col1, col2, col3, col4) VALUES (?, ?, ?, :val)", cqls.iterator().next());
	}
}