package ch.ethz.inf.pm.cfour.cassandra.soot;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootOptions.TransactionMethod;
import soot.EntryPoints;
import soot.SootMethod;

public class SootSetup {

	public static ImmutableSet<SootMethod> setupSoot(final SootOptions userOptions) {
		final String[] additionalClasspath = getAdditionalClasspathEntries(userOptions);

		soot.G.reset();
		soot.G.v().out = new SootLogger();

		final soot.options.Options sootOptions = soot.options.Options.v();
		// ignore new classes from JDK-8
		sootOptions.set_exclude(Collections.singletonList("jdk.internal.*"));

		// shimple in whole program mode
		sootOptions.set_whole_program(true);
		sootOptions.set_via_shimple(true);
		sootOptions.set_whole_shimple(true);

		// output is not needed
		sootOptions.set_output_format(soot.options.Options.output_format_none);

		sootOptions.set_soot_classpath(userOptions.getClasspathAsString(additionalClasspath));
		sootOptions.set_no_bodies_for_excluded(true);
		sootOptions.set_allow_phantom_refs(true);

		sootOptions.set_app(true);
		sootOptions.set_process_dir(userOptions.getClasspath());

		sootOptions.setPhaseOption("cg", "verbose:true");
		sootOptions.setPhaseOption("cg", "jdkver:6");
		sootOptions.setPhaseOption("wsop", "enabled:true");
		sootOptions.setPhaseOption("sop", "enabled:true");

		final soot.Scene scene = soot.Scene.v();

		scene.loadNecessaryClasses();
		final ImmutableSet<SootMethod> transactionMethods = resolveTransactionMethods(userOptions, scene);
		scene.setEntryPoints(new ArrayList<>(transactionMethods));
		return transactionMethods;
	}

	private static String[] getAdditionalClasspathEntries(final SootOptions options) {
		final List<String> additionalEntries = new ArrayList<>();
		final String rt = resolveJarIfNecessary(options, "rt.jar");
		if (rt != null) {
			additionalEntries.add(rt);
		}
		final String jce = resolveJarIfNecessary(options, "jce.jar");
		if (rt != null) {
			additionalEntries.add(jce);
		}
		return additionalEntries.toArray(new String[additionalEntries.size()]);
	}

	private static String resolveJarIfNecessary(final SootOptions options, final String jarname) {
		for (final String cp : options.getClasspath()) {
			if (cp.endsWith(jarname)) {
				return null;
			}
		}
		final String fromSootJavaHome = resolveJarFromJavaDir(System.getenv("SOOT_JAVA_HOME"), jarname);
		if (fromSootJavaHome == null) {
			final String fromJavaHome = resolveJarFromJavaDir(System.getenv("JAVA_HOME"), jarname);
			if (fromJavaHome == null) {
				throw new RuntimeException(jarname + " not found. Is SOOT_JAVA_HOME or JAVA_HOME set correctly?");
			}
			return fromJavaHome;
		} else {
			return fromSootJavaHome;
		}
	}

	private static String resolveJarFromJavaDir(final String javadir, final String jarname) {
		if (javadir == null) {
			return null;
		}
		final File javaHome = new File(javadir);
		File jarFile;
		if (javaHome.exists()) {
			final File jre = new File(javaHome, "jre");
			if (jre.exists() && (jarFile = new File(jre, "lib" + File.separator + jarname)).exists()) {
				return jarFile.getAbsolutePath();
			}
			if ((jarFile = new File(javaHome, "lib" + File.separator + jarname)).exists()) {
				return jarFile.getAbsolutePath();
			}
		}
		return null;
	}

	private static ImmutableSet<SootMethod> resolveTransactionMethods(final SootOptions userOptions, final soot.Scene scene) {
		final Set<SootMethod> transactionMethods = new HashSet<>();
		for (final SootMethod method : EntryPoints.v().methodsOfApplicationClasses()) {
			if (SootAnnotationUtils.getTransactionAnnotation(method) != null) {
				transactionMethods.add(method);
			}
		}
		if (!userOptions.getTransactionMethods().isEmpty()) {
			userOptions.getTransactionMethods().forEach(entryMethod -> {
				final Set<SootMethod> methods = resolveSootMethod(scene, entryMethod);
				if (methods.isEmpty()) {
					throw new IllegalArgumentException("Method " + entryMethod + " could not be resolved.");
				} else {
					transactionMethods.addAll(methods);
				}
			});
		}
		if (!userOptions.getExcludedTransactionMethods().isEmpty()) {
			userOptions.getExcludedTransactionMethods().forEach(entryMethod -> {
				final Set<SootMethod> methods = resolveSootMethod(scene, entryMethod);
				if (methods.isEmpty()) {
					throw new IllegalArgumentException("Method " + entryMethod + " could not be resolved.");
				} else {
					transactionMethods.removeAll(methods);
				}
			});
		}
		return ImmutableSet.copyOf(transactionMethods);
	}

	private static Set<SootMethod> resolveSootMethod(final soot.Scene scene, TransactionMethod entryMethod) {
		final Set<SootMethod> result = new HashSet<>();
		scene.getSootClass(entryMethod.className).getMethods().forEach(method -> {
			if (entryMethod.methodName.equals(method.getName())) {
				if (entryMethod.parameterClassNames == null) {
					result.add(method);
				} else if (method.getParameterCount() == entryMethod.parameterClassNames.length) {
					boolean matches = true;
					for (int i = 0; i < entryMethod.parameterClassNames.length; i++) {
						if (!entryMethod.parameterClassNames[i].equals(method.getParameterType(i).toString())) {
							matches = false;
						}
					}
					if (matches) {
						result.add(method);
					}
				}
			}
		});
		return result;
	}
}
