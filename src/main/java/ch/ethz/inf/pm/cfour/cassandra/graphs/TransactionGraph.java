package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.util.Collection;
import java.util.function.Function;

import ch.ethz.inf.pm.cfour.cassandra.beans.ParsedSessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.SetMultimap;
import com.google.common.graph.ImmutableValueGraph;

public class TransactionGraph extends AbstractTransactionGraph<ParsedSessionExecuteInvoke, TransactionGraph, TransactionGraph.Builder> {

	protected TransactionGraph(
			ImmutableValueGraph<ParsedSessionExecuteInvoke, EdgeConstraint<ParsedSessionExecuteInvoke>> graph,
			ImmutableMap<ParsedSessionExecuteInvoke, EdgeConstraint<ParsedSessionExecuteInvoke>> sourceNodes,
			ImmutableMap<ParsedSessionExecuteInvoke, EdgeConstraint<ParsedSessionExecuteInvoke>> sinkNodes,
			boolean bypassPossible) {
		super(graph, sourceNodes, sinkNodes, bypassPossible);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static TransactionGraph createEmpty() {
		return builder().build();
	}

	public static TransactionGraph createFrom(final UnparsedTransactionGraph unparsedTransactionGraph,
			final SetMultimap<SessionExecuteInvoke, ParsedSessionExecuteInvoke> transformedNodes) {
		return new Builder(unparsedTransactionGraph, oldNode -> transformedNodes.get(oldNode)).build();
	}

	@Override
	public Builder toBuilder() {
		return new Builder(this);
	}

	public static class Builder extends AbstractTransactionGraph.AbstractBuilder<ParsedSessionExecuteInvoke, TransactionGraph> {

		public Builder() {
			super();
		}

		public Builder(final TransactionGraph from) {
			super(from);
		}

		public <oldT> Builder(final AbstractTransactionGraph<oldT, ?, ?> from,
				final Function<oldT, Collection<ParsedSessionExecuteInvoke>> transformer) {
			super(from, transformer);
		}

		@Override
		public TransactionGraph build() {
			return new TransactionGraph(ImmutableValueGraph.copyOf(graph), ImmutableMap.copyOf(sourceNodes),
					ImmutableMap.copyOf(sinkNodes), bypassPossible);
		}
	}
}
