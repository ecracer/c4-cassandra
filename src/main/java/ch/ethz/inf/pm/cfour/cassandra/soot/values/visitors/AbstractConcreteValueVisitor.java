package ch.ethz.inf.pm.cfour.cassandra.soot.values.visitors;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ConcreteValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.UnknownMutableValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.ResultSetValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StringBuilderValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ImmutableValue;

public abstract class AbstractConcreteValueVisitor<R> implements ConcreteValueVisitor<R> {

	@Override
	public R visit(final ConcreteValue value) {
		return value.apply(this);
	}

	public List<R> visit(final List<? extends ConcreteValue> values) {
		final List<R> result = new ArrayList<>();
		values.forEach(val -> result.add(visit(val)));
		return result;
	}

	public R visitDefault(final ConcreteValue value) {
		return null;
	}

	@Override
	public R visitStringBuilderValue(final StringBuilderValue stringBuilderValue) {
		return visitDefault(stringBuilderValue);
	}

	@Override
	public R visitResultSetValue(ResultSetValue resultSetValue) {
		return visitDefault(resultSetValue);
	}

	@Override
	public R visitUnknownMutableValue(final UnknownMutableValue unknownValue) {
		return visitDefault(unknownValue);
	}

	@Override
	public R visitStatementValue(StatementValue statementValue) {
		return visitDefault(statementValue);
	}

	@Override
	public R visitImmutableValue(ImmutableValue immutableValue) {
		return visitDefault(immutableValue);
	}
}
