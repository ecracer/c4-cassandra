package ch.ethz.inf.pm.cfour.cassandra.soot.execution.states;

import java.util.Objects;

import ch.ethz.inf.pm.cfour.cassandra.soot.SootValUtils;
import ch.ethz.inf.pm.cfour.cassandra.soot.vars.VarLocal;
import soot.Local;
import soot.jimple.Constant;
import soot.jimple.ReturnStmt;

/**
 * Transformer for a returnStmt
 */
public class ReturnStmtProgramPoint extends AbstractProgramPoint {

	private final ReturnStmt returnStmt;

	private ReturnStmtProgramPoint(final ProgramPointId id, final ReturnStmt returnStmt) {
		super(id);
		this.returnStmt = Objects.requireNonNull(returnStmt);
	}

	public static ReturnStmtProgramPoint create(final CallStack callStack, final ReturnStmt returnStmt) {
		return new ReturnStmtProgramPoint(ProgramPointId.create(callStack, returnStmt), returnStmt);
	}

	@Override
	public void transform(final ExecutionState state, final ExecutionState oldState) {
		if (returnStmt.getOp() instanceof Local) {
			final VarLocal opLocal = SootValUtils.transformLocal((Local) returnStmt.getOp(), id.callStack);
			state.setMethodCallReturnVal(id.callStack, opLocal);
		} else {
			state.setMethodCallReturnVal(id.callStack, SootValUtils.transformImmutableValue((Constant) returnStmt.getOp(), id));
		}
	}

	@Override
	protected AbstractProgramPoint createWithProgramPointId(ProgramPointId id) {
		return new ReturnStmtProgramPoint(id, returnStmt);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((returnStmt == null) ? 0 : returnStmt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReturnStmtProgramPoint other = (ReturnStmtProgramPoint) obj;
		if (returnStmt == null) {
			if (other.returnStmt != null)
				return false;
		} else if (!returnStmt.equals(other.returnStmt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReturnStmtProgramPoint [" + returnStmt + "]";
	}
}
