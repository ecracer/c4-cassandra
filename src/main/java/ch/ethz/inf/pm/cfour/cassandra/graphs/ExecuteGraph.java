package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.graph.EndpointPair;
import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.Graphs;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.AbstractProgramPoint;

/**
 * Representation of an abstract CFG
 */
public class ExecuteGraph {

	private final ImmutableGraph<AbstractProgramPoint> graph;
	private final ImmutableSet<AbstractProgramPoint> sourceNodes;
	private final ImmutableSet<AbstractProgramPoint> sinkNodes;
	private final boolean bypassPossible;

	private ExecuteGraph(final ImmutableGraph<AbstractProgramPoint> graph, final ImmutableSet<AbstractProgramPoint> sourceNodes,
			final ImmutableSet<AbstractProgramPoint> sinkNodes,
			final boolean bypassPossible) {
		this.graph = Objects.requireNonNull(graph);
		this.sourceNodes = Objects.requireNonNull(sourceNodes);
		this.sinkNodes = Objects.requireNonNull(sinkNodes);
		this.bypassPossible = bypassPossible;
	}

	public static ExecuteGraph create(final Graph<AbstractProgramPoint> graph, final Set<AbstractProgramPoint> sourceNodes,
			final Set<AbstractProgramPoint> sinkNodes, final boolean bypassPossible) {
		return new ExecuteGraph(ImmutableGraph.copyOf(graph), ImmutableSet.copyOf(sourceNodes), ImmutableSet.copyOf(sinkNodes),
				bypassPossible);
	}

	public static ExecuteGraph createEmpty() {
		return new ExecuteGraph(ImmutableGraph.copyOf(GraphBuilder.directed().allowsSelfLoops(true).build()), ImmutableSet.of(),
				ImmutableSet.of(), true);
	}

	public ImmutableSet<AbstractProgramPoint> sourceNodes() {
		return sourceNodes;
	}

	public ImmutableSet<AbstractProgramPoint> sinkNodes() {
		return sinkNodes;
	}

	public boolean isBypassPossible() {
		return bypassPossible;
	}

	public Set<AbstractProgramPoint> nodes() {
		return graph.nodes();
	}

	public Set<EndpointPair<AbstractProgramPoint>> edges() {
		return graph.edges();
	}

	public Set<AbstractProgramPoint> predecessors(final AbstractProgramPoint node) {
		return graph.predecessors(node);
	}

	public Set<AbstractProgramPoint> successors(final AbstractProgramPoint node) {
		return graph.successors(node);
	}

	public MutableGraph<AbstractProgramPoint> getMutableGraphCopy() {
		final MutableGraph<AbstractProgramPoint> resGraph = GraphBuilder.from(this.graph).build();
		for (final AbstractProgramPoint node : this.graph.nodes()) {
			resGraph.addNode(node);
		}
		for (final EndpointPair<AbstractProgramPoint> edge : this.graph.edges()) {
			resGraph.putEdge(edge.nodeU(), edge.nodeV());
		}
		return resGraph;
	}

	public ExecuteGraph mergeWith(final ExecuteGraph other) {
		if (this == other) {
			return this;
		}
		final MutableGraph<AbstractProgramPoint> resGraph = getMutableGraphCopy();
		for (final AbstractProgramPoint node : other.graph.nodes()) {
			resGraph.addNode(node);
		}
		for (final EndpointPair<AbstractProgramPoint> edge : other.graph.edges()) {
			resGraph.putEdge(edge.nodeU(), edge.nodeV());
		}
		final ImmutableSet.Builder<AbstractProgramPoint> sourceNodesBuilder = ImmutableSet.builder();
		sourceNodesBuilder.addAll(this.sourceNodes).addAll(other.sourceNodes);
		final ImmutableSet.Builder<AbstractProgramPoint> sinkNodesBuilder = ImmutableSet.builder();
		sinkNodesBuilder.addAll(this.sinkNodes).addAll(other.sinkNodes);
		return ExecuteGraph.create(resGraph, sourceNodesBuilder.build(), sinkNodesBuilder.build(),
				this.bypassPossible || other.bypassPossible);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bypassPossible ? 1231 : 1237);
		// do not include as it is only based on reference equality
		//result = prime * result + ((graph == null) ? 0 : graph.hashCode());
		result = prime * result + ((sinkNodes == null) ? 0 : sinkNodes.hashCode());
		result = prime * result + ((sourceNodes == null) ? 0 : sourceNodes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExecuteGraph other = (ExecuteGraph) obj;
		if (bypassPossible != other.bypassPossible)
			return false;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!Graphs.equivalent(graph, other.graph))
			// use graph equivalent
			return false;
		if (sinkNodes == null) {
			if (other.sinkNodes != null)
				return false;
		} else if (!sinkNodes.equals(other.sinkNodes))
			return false;
		if (sourceNodes == null) {
			if (other.sourceNodes != null)
				return false;
		} else if (!sourceNodes.equals(other.sourceNodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImmutableSourceSinkGraph [graph=" + graph + ", sourceNodes=" + sourceNodes + ", sinkNodes=" + sinkNodes
				+ ", bypassPossible=" + bypassPossible + "]";
	}
}
