package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;

public class StmtExtractorFieldsStringBuilderTest extends AbstractSootTest {

	public StmtExtractorFieldsStringBuilderTest() throws IOException {
		super("FieldsStringBuilder", "test1", "test2");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(2, result.size());
		final Set<String> cqls = new HashSet<>();
		result.forEach(call -> cqls.addAll(getCQLs(call)));

		assertEquals(2, cqls.size());
		assertTrue(cqls.contains("SELECT * FROM test1"));
		assertTrue(cqls.contains("SELECT * FROM test1 WHERE k=2"));
	}
}
