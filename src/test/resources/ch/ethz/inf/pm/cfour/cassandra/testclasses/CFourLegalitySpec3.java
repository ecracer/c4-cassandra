

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import ch.ethz.inf.pm.ecchecker.cassandra.annotations.ClientLocalValues;

import java.util.Collections;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;


public class CFourLegalitySpec3 {
	
	private final Session session;
	
	public CFourLegalitySpec3(final Session session){
		this.session = session;
	}
	
	@Transaction
	// KillrChat: ChatRoomResource#addUserToChatRoom
	public void addUserToChatRoom(final String username, final String roomName){
		ClientLocalValues.set("username", username);
		session.execute("UPDATE chat_rooms SET participants = participants + {?} WHERE room_name = ? IF EXISTS", username, roomName);
	}

	@Transaction
	// KillrChat: ChatRoomResource#findRoomByName
	public void findRoom(final String roomName){
		session.execute("SELECT room_name, participants FROM chat_rooms WHERE room_name = ?", roomName);
	}

	
	@Transaction
	// KillrChat: ChatRoomResource#createChatRoom
	public void createChatRoom(final String username, final String roomName){
		ClientLocalValues.set("username", username);
		session.execute("INSERT INTO chat_rooms (room_name, participants) VALUES (?, ?) IF NOT EXISTS", roomName, username, Collections.singleton(username));
	}
}
