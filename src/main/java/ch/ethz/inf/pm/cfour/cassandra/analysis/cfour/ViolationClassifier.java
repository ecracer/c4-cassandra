package ch.ethz.inf.pm.cfour.cassandra.analysis.cfour;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.ethz.inf.pm.cfour.cassandra.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableMap;

/**
 * Classifies the violations based on a file that can be set in the options
 */
public class ViolationClassifier {

	private final static Logger LOG = LogManager.getLogger(ViolationClassifier.class);

	private final ImmutableMap<String, Classification> classifications;

	public ViolationClassifier(final Options options) {
		final ImmutableMap.Builder<String, Classification> classificationsBuilder = ImmutableMap.builder();
		if (options.getViolationClassificationFile() != null) {
			BufferedReader reader = null;
			try {
				final Pattern classificationPattern = Pattern.compile("^(.+)=([uUnNhHwWeE])$");
				final File classificationFile = new File(options.getViolationClassificationFile());
				if (!classificationFile.exists()) {
					LOG.warn("Classification File is missing");
				}
				reader = new BufferedReader(new FileReader(classificationFile));
				String nextLine;
				while ((nextLine = reader.readLine()) != null) {
					if (nextLine.trim().isEmpty() || nextLine.startsWith("#")) {
						continue;
					}
					final Matcher matcher = classificationPattern.matcher(nextLine.trim());
					if (matcher.matches()) {
						classificationsBuilder.put(matcher.group(1), Classification.forShortName(matcher.group(2)));
					} else {
						LOG.warn("Skipping line " + nextLine);
					}
				}
			} catch (IOException e) {
				LOG.error("Error reading classification file", e);
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
			}
		}
		classifications = classificationsBuilder.build();
	}

	public Classification classificationOf(final String violation) {
		return classifications.getOrDefault(violation, Classification.UNCLASSIFIED);
	}

	public static enum Classification {
		UNCLASSIFIED("u"), FALSE_POSITIVE("n"), ERROR("e"), WARNING("w"), HARMLESS("h");

		private final String shortName;

		private Classification(final String shortName) {
			this.shortName = Objects.requireNonNull(shortName);
		}

		public String getShortName() {
			return shortName;
		}

		public static Classification forShortName(final String shortName) {
			for (final Classification c : values()) {
				if (shortName.equalsIgnoreCase(c.shortName)) {
					return c;
				}
			}
			throw new IllegalArgumentException();
		}
	}
}
