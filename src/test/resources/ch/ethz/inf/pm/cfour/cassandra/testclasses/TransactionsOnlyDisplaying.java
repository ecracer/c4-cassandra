

import ch.ethz.inf.pm.ecchecker.cassandra.annotations.Transaction;
import com.datastax.driver.core.Session;

public class TransactionsOnlyDisplaying {

	private final Session session;
	
	public TransactionsOnlyDisplaying(final Session session){
		this.session = session;
	}
	
	@Transaction(onlyForDisplaying = true)
	public void method1(int i){
		session.execute("SELECT * FROM method1");
	}
	
	@Transaction(onlyForDisplaying = false)
	public void method2(int a){
		session.execute("SELECT * FROM method2");
	}
	
	@Transaction
	public void method3(int a){
		session.execute("SELECT * FROM method3");
	}
}
