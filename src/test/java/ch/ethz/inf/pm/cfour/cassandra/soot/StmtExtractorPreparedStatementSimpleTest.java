package ch.ethz.inf.pm.cfour.cassandra.soot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.StatementValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.ConsistencyLevelValue;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.concrete.immutable.NullValue;

public class StmtExtractorPreparedStatementSimpleTest extends AbstractSootTest {

	public StmtExtractorPreparedStatementSimpleTest() throws IOException {
		super("PreparedStatementSimple", "test");
	}

	@Override
	protected void checkSessionExecuteCalls(Set<SessionExecuteInvoke> result) {
		assertEquals(1, result.size());

		final Set<String> cqls = new HashSet<>();
		result.forEach(res -> cqls.addAll(getCQLs(res)));

		assertEquals(1, cqls.size());
		assertTrue(cqls.contains("INSERT INTO test_table (col1, col2) VALUES (?,?)"));

		final Set<StatementValue> statementSet = result.iterator().next().statementArg;

		assertEquals(2, statementSet.size());

		boolean defaultConsistencyLevelSet = false;
		boolean quorumConsistencyLevelset = false;

		for (final StatementValue stmt : statementSet) {
			if (stmt.consistencyLevel instanceof NullValue) {
				defaultConsistencyLevelSet = true;
			} else if (stmt.consistencyLevel instanceof ConsistencyLevelValue
					&& ConsistencyLevelValue.Level.QUORUM.equals(((ConsistencyLevelValue) stmt.consistencyLevel).level)) {
				quorumConsistencyLevelset = true;
			}
		}

		assertTrue(defaultConsistencyLevelSet);
		assertTrue(quorumConsistencyLevelset);
	}
}