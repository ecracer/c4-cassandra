package ch.ethz.inf.pm.cfour.cassandra.graphs;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import ch.ethz.inf.pm.cfour.cassandra.soot.execution.states.ProgramPointId;
import ch.ethz.inf.pm.cfour.cassandra.soot.values.SessionExecuteInvoke;
import com.google.common.collect.ImmutableMap;
import com.google.common.graph.ImmutableValueGraph;

public class UnparsedTransactionGraph
		extends
			AbstractTransactionGraph<SessionExecuteInvoke, UnparsedTransactionGraph, UnparsedTransactionGraph.Builder> {

	protected UnparsedTransactionGraph(
			ImmutableValueGraph<SessionExecuteInvoke, EdgeConstraint<SessionExecuteInvoke>> graph,
			ImmutableMap<SessionExecuteInvoke, EdgeConstraint<SessionExecuteInvoke>> sourceNodes,
			ImmutableMap<SessionExecuteInvoke, EdgeConstraint<SessionExecuteInvoke>> sinkNodes,
			boolean bypassPossible) {
		super(graph, sourceNodes, sinkNodes, bypassPossible);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static UnparsedTransactionGraph createEmpty() {
		return builder().build();
	}

	public static UnparsedTransactionGraph createFrom(final IdTransactionGraph idTransactionGraph,
			final Map<ProgramPointId, SessionExecuteInvoke> idToSessionExecuteMap) {
		return new Builder(idTransactionGraph, ppid -> Collections.singleton(idToSessionExecuteMap.get(ppid))).build();
	}

	@Override
	public Builder toBuilder() {
		return new Builder(this);
	}

	public static class Builder extends AbstractTransactionGraph.AbstractBuilder<SessionExecuteInvoke, UnparsedTransactionGraph> {

		public Builder() {
			super();
		}

		public Builder(final UnparsedTransactionGraph from) {
			super(from);
		}

		public <oldT> Builder(final AbstractTransactionGraph<oldT, ?, ?> from,
				final Function<oldT, Collection<SessionExecuteInvoke>> transformer) {
			super(from, transformer);
		}

		@Override
		public UnparsedTransactionGraph build() {
			return new UnparsedTransactionGraph(ImmutableValueGraph.copyOf(graph), ImmutableMap.copyOf(sourceNodes),
					ImmutableMap.copyOf(sinkNodes), bypassPossible);
		}
	}
}
